using System.Collections;
using System.Collections.Generic;
using System.Text;
using Gestrument;
using Reactional;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SimonUI : MonoBehaviour
{
    [Header("Scale Stuff")]
    [SerializeField] Slider scaleKeySlider;
    [SerializeField] TMP_Text scaleKeyText;
    [SerializeField] TMP_Dropdown scaleModeDropdown;
    [SerializeField] Button scaleButton;
    
    [Header("Transpose Stuff")]
    [SerializeField] Slider transposeModalSlider;
    [SerializeField] TMP_Text transposeModalText;
    [SerializeField] Slider transposeChromaticSlider;
    [SerializeField] TMP_Text transposeChromaticText;
    
    [Header("Tempo Stuff")]
    [SerializeField] Slider tempoSlider;
    [SerializeField] TMP_Text tempoText;
    
    [Header("Stuff")]
    [SerializeField] Slider progressSlider;
    [SerializeField] Button playMotifButton;
    [SerializeField] Button playStingerButton;
    
    [Header("Section Stuff")]
    [SerializeField] Transform sectionButtonsPanel;
    [SerializeField] Button sectionButtonPrefab;    
    [Header("Motifs Stuff")]
    [SerializeField] Transform motifButtonsPanel;
    [SerializeField] Button motifButtonPrefab;    
    [Header("Stingers Stuff")]
    [SerializeField] Transform stingerButtonsPanel;
    [SerializeField] Button stingerButtonPrefab;
    
    ReactionalPlayer rplayer;
    GestrumentCore core;
    
    
    
    
    
    
    string[] _names = { "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" };
    string[] _functionNames = { "I", "II", "III", "IV", "V", "VI", "VII" };
    
    void Start()
    {
        // reactional stuff
        rplayer = FindObjectOfType<ReactionalPlayer>();
        core = rplayer.m_core;
        
        
        // event listeners
        scaleKeySlider.onValueChanged.AddListener(ScaleKeySlider_OnValueChanged);
        scaleModeDropdown.onValueChanged.AddListener(ScaleModeDropdown_OnValueChanged);
        scaleButton.onClick.AddListener(ScaleButton_OnClick);
        
        transposeModalSlider.onValueChanged.AddListener(TransposeModalSlider_OnValueChanged);
        transposeChromaticSlider.onValueChanged.AddListener(TransposeChromaticSlider_OnValueChanged);
        
        tempoSlider.onValueChanged.AddListener(TempoSlider_OnValueChanged);

        playMotifButton.onClick.AddListener(PlayMotifButton_OnClick);
        playStingerButton.onClick.AddListener(PlayStingerButton_OnClick);

        

        // update text with default
        SetScaleKeyText((int)scaleKeySlider.value);
        SetTransposeModalText((int)transposeModalSlider.value);
        SetTransposeChromaticText((int)transposeChromaticSlider.value);
        SetTempoText((int)tempoSlider.value);
        
        // build buttons
        
        BuildSectionButtons();
        BuildMotifButtons();
        BuildStingerButtons();
        
        StartCoroutine(UpdateProgress(progressSlider));
    }



    void BuildSectionButtons()
    {
        // clear anything there on load
        ClearChildren(sectionButtonsPanel);
        
        // loop through sections and make buttons
        for (int i = 0; i < rplayer.m_track.m_sections.Count; i++)
        {
            int index = i + 1;
            Debug.Log($"Section {rplayer.m_track.m_sections[i].name}");

            Button newSectionButton = Instantiate(sectionButtonPrefab, sectionButtonsPanel);
            newSectionButton.GetComponentInChildren<TMP_Text>().text = rplayer.m_track.m_sections[i].name;
            newSectionButton.onClick.AddListener(() => rplayer.SetPart(index, 1));
        }
    }
    
    void BuildMotifButtons()
    {
        // clear anything there on load
        ClearChildren(motifButtonsPanel);
        
        // loop through motifs and make buttons
        for (int i = 0; i < rplayer.m_track.m_motifs.Count; i++)
        {
            int index = i + 1;
            Debug.Log($"Motif {rplayer.m_track.m_motifs[i].name}");

            Button newMotifButton = Instantiate(motifButtonPrefab, motifButtonsPanel);
            newMotifButton.GetComponentInChildren<TMP_Text>().text = rplayer.m_track.m_motifs[i].name;
            newMotifButton.onClick.AddListener(() => rplayer.PlayMotif(index, 1));
        }
    }
    
    void BuildStingerButtons()
    {
        // clear anything there on load
        ClearChildren(stingerButtonsPanel);
        
        // loop through motifs and make buttons
        for (int i = 0; i < rplayer.m_track.m_stingers.Count; i++)
        {
            int index = i + 1;
            Debug.Log($"Stinger {rplayer.m_track.m_stingers[i].name}");

            Button newStingerButton = Instantiate(stingerButtonPrefab, stingerButtonsPanel);
            newStingerButton.GetComponentInChildren<TMP_Text>().text = rplayer.m_stingers[i].name;
            newStingerButton.onClick.AddListener(() => rplayer.PlayStinger(index, 1));
        }
    }
    

    // EVENT HANDLERS
    
   
    
    void ScaleKeySlider_OnValueChanged(float value)
    {
        SetScaleKeyText((int) value);
        ScaleButton_OnClick();
    }
    
    void ScaleModeDropdown_OnValueChanged(int value)
    {
        ScaleButton_OnClick();
    } 
    
    void ScaleButton_OnClick()
    {
        string mode = scaleModeDropdown.options[scaleModeDropdown.value].text;
        int key = (int) scaleKeySlider.value;

        SetScale(key, mode);
    }
    
    
    void TransposeModalSlider_OnValueChanged(float value)
    {
        int n = (int) value;
        SetTransposeModalText(n);
        SetTransposeSlot(n);
    }

    
    void TransposeChromaticSlider_OnValueChanged(float value)
    {
        int n = (int) value;
        SetTransposeChromaticText(n);
        SetChromaticTranspose(n);
    }

    void TempoSlider_OnValueChanged(float value)
    {
        int n = (int) value;

        SetTempoText(n);
        SetTempo(n);
    }

    void PlayStingerButton_OnClick()
    {
        PlayStinger();
    }

    void PlayMotifButton_OnClick()
    {
        PlayMotif();
    }

    
    
    
    
    // UI UPDATING STUFF

    void SetScaleKeyText(int value)
    {
        scaleKeyText.text = _names[value];
    }
    
    
    /*
        string[] functionNames = { "I", "II", "III", "IV", "V", "VI", "VII" };
        n = (n % 7);
        Debug.Log(n);
        if (n < 0) n = n + 7;
        return functionNames[n];

     */
    
    void SetTransposeModalText(int value)
    {
        transposeModalText.text = GetTransposeModalName(value);
    }
    
    void SetTransposeChromaticText(int value)
    {
        string s = (value > 0) ? "+" : ""; // if positive number, display a +

        transposeChromaticText.text = s + value;
    }

    void SetTempoText(int value)
    {
        tempoText.text = value.ToString();
    }
    
    
    
    
    
    // REACTIONAL SETTERS
    
    void SetScale(int key, string mode)
    {
        Debug.Log($"SetScale {_names[key]} {mode}");

        core.engine.SetScaleForSlot(0, key, mode);
    }
    
    void SetTransposeSlot(int transpose)
    {
        Debug.Log($"SetTransposeSlot {transpose} ({GetTransposeModalName(transpose)})");
        //Debug.Log($"SetTransposeSlot {transpose}");
        rplayer.SetTransposeSlot(transpose);
    }

    void SetChromaticTranspose(int value)
    {
        Debug.Log($"SetChromaticTranspose {value}");
        
        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            rplayer.m_core.engine.SetParamValue(Engine.EventObject.PitchGen, i,"ctranspose", value);
        }
    }

    void SetTempo(int value)
    {
        Debug.Log($"SetTempo {value}");
        
        core.engine.SetTempo(value);
    }
    
    void PlayMotif()
    {
        Reactional.Request.Motif(1);
    }

    void PlayStinger()
    {
        Reactional.Request.Stinger(1);
    }
    
    
    
    IEnumerator UpdateProgress(Slider slider)
    {
        Color color = new Color(1f, 0.24f, 0.8f, 1);
        
        while (true)
        {
            yield return new WaitForSeconds(0.05f);
            slider.value = (float)((rplayer.m_beat / (rplayer.m_currentSection.stopBeat - rplayer.m_currentSection.startBeat)) % (rplayer.m_currentSection.stopBeat - rplayer.m_currentSection.startBeat));

            /*
            for (int i = 0; i < sectionbuttons.Count; i++)
            {
                if (rplayer.m_currentSection == rplayer.m_sections[i / 3] || rplayer.m_currentSection == rplayer.m_sectionsHalf[i / 3] || rplayer.m_currentSection == rplayer.m_sectionsDouble[i / 3] )
                    sectionbuttons[i].GetComponent<Image>().color = colr;
                else
                    sectionbuttons[i].GetComponent<Image>().color = Color.white;
            }

            for (int i = 0; i < motifbuttons.Count; i++)
            {
                if (rplayer.m_currentmotif == rplayer.m_motifs[i / 3] || rplayer.m_currentmotif == rplayer.m_motifsHalf[i / 3] || rplayer.m_currentmotif == rplayer.m_motifsDouble[i / 3])
                    motifbuttons[i].GetComponent<Image>().color = colr;
                else
                    motifbuttons[i].GetComponent<Image>().color = Color.white;
            }*/
        }
    }
    
    
    
    // REACTIONAL HELPERS

    string GetTransposeModalName(int value)
    {
            value = (value % 7);
        
            if (value < 0) 
                value += 7;

            return _functionNames[value];
    }
    
    // UTILITIES
    
    /// <summary>
    /// Loop through a parent transform and destroy all children
    /// </summary>
    /// <param name="parent">Transform of parent object</param>
    public static void ClearChildren(Transform parent, bool immediately = false)
    {
        if (immediately)
        {
            // reverse iterate so that we can delete stuff
            for (int i = parent.childCount; i > 0; --i)
                GameObject.DestroyImmediate(parent.GetChild(0).gameObject);
        }
        else
        {
            foreach (Transform child in parent)
            {
                // set to destroy, it'll wait til end of frame tho
                GameObject.Destroy(child.gameObject);
            }

            // gameobjects don't disappear until end of frame so this will clear them out from the parent until deletion
            parent.DetachChildren();
        }
    }

    
}
