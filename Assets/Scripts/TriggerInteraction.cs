﻿using Macedon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class TriggerInteraction : BaseInteraction
{
    [SerializeField] string inkFilename;
    //[SerializeField] DialogueManager dialogueManager;

    // events
    public delegate void InteractionStarted();
    public event InteractionStarted OnInteractionStarted;

    public delegate void InteractionEnded(TriggerInteraction trigger);
    public event InteractionEnded OnInteractionEnded;


    private void Awake()
    {
    }


    public override void EndInteract()
    {
        throw new NotImplementedException();
    }

    public override void EndInteractEarly()
    {
        //dialogueManager.BreakStory();
    }

    public override void StartInteract()
    {
        Debug.Log($"TriggerInteraction Interact() Play Conversation inkFilename={inkFilename}");

        //dialogueManager.OnStoryFinished += DialogueManager_OnStoryFinished;

        //dialogueManager.StartDialogue(inkFilename);

    }
    private void DialogueManager_OnStoryFinished()
    {
        //dialogueManager.OnStoryFinished -= DialogueManager_OnStoryFinished;

        Debug.Log($"ACTOR: Raising OnInteractionEnded() event.");
        OnInteractionEnded?.Invoke(this);
    }
}

