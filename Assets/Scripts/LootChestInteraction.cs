﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

    public class LootChestInteraction : BaseInteraction
    {
    [SerializeField] Animator animator;
    bool isOpen = false;





    public override void Reset()
    {
        base.Reset();

        Debug.Log($"LootChestInteraction Reset");

        if (TryGetComponent(out Animator animator))
        {
            this.animator = animator;
        }

    }

    public override void StartInteract()
    {
        if (animator && !isOpen)
        {
            animator.SetTrigger("Open");
            isOpen = true;
            isInteractable = false; // set base isInteractable bool as we don't want to be interacted with again
        }
    }

    public override void EndInteract()
    {
        throw new NotImplementedException();
    }

    public override void EndInteractEarly()
    {
        throw new NotImplementedException();
    }
}

