using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildInfo
{
    public int buildNumber;
    public string buildDate;

    public override string ToString()
    {
        return $"BuildInfo - buildNumber={buildNumber}, buildDate={buildDate}";
    }
}
