// GENERATED AUTOMATICALLY FROM 'Assets/MacedonInputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MacedonInputs : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @MacedonInputs()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MacedonInputs"",
    ""maps"": [
        {
            ""name"": ""Game Controls"",
            ""id"": ""9c4fe1af-48fd-4ba1-92c1-aa5ee9ba5704"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""6dc38a10-c5ca-47fa-9b57-d4a51042f7f0"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Button"",
                    ""id"": ""5329af1c-0b33-492f-9630-9db5e0074a28"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Switch Cameras"",
                    ""type"": ""Button"",
                    ""id"": ""c9558d9f-d5e9-4f34-969e-b60bfeecd9cf"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""65e4d974-5e12-46bd-a93d-915c46f2d80b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block"",
                    ""type"": ""Button"",
                    ""id"": ""72be6a47-7861-4171-a47a-073bbe61dd0a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""a44c2473-b594-490c-94c3-b4dfc9f479fc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Crouch"",
                    ""type"": ""Button"",
                    ""id"": ""8e773b81-dd2e-4aae-8c7c-e1392b3760b7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""e7904275-5891-4fee-a46e-effbdd2d0237"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""9818b5ba-25cb-4a75-b3fb-a5c8391d029d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleMenu"",
                    ""type"": ""Button"",
                    ""id"": ""c369894a-4463-48c1-8947-dc8ae3b870d7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Console"",
                    ""type"": ""Button"",
                    ""id"": ""f92cba12-4c88-480f-b5bb-2758858e6fcd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dialog Response 1"",
                    ""type"": ""Button"",
                    ""id"": ""0e215a3e-d212-4d42-ab03-cc5bcba4bda7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dialog Response 2"",
                    ""type"": ""Button"",
                    ""id"": ""07e64597-c14d-4a4e-afc1-5d9e061d4a3c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dialog Response 3"",
                    ""type"": ""Button"",
                    ""id"": ""a751acf2-3a57-4177-8825-04616d7027f2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dialog Response 4"",
                    ""type"": ""Button"",
                    ""id"": ""8f09d345-ad29-493b-9fef-e36a823ae521"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Tactical Mode"",
                    ""type"": ""Button"",
                    ""id"": ""594fd7a3-b460-4924-8be9-b38e55de9f1c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DodgeLeft"",
                    ""type"": ""Button"",
                    ""id"": ""656da2ba-4768-45e7-bcad-a4bcd3eb4003"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap""
                },
                {
                    ""name"": ""Map"",
                    ""type"": ""Button"",
                    ""id"": ""f978a7ea-bbdd-44d4-af51-b23daa162e93"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Feedback"",
                    ""type"": ""Button"",
                    ""id"": ""6432343d-a89e-4c1c-acdd-1720626d6673"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Screenshot"",
                    ""type"": ""Button"",
                    ""id"": ""7e9e79a1-0d21-4357-8702-e8c5a5f2e435"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DebugScreen"",
                    ""type"": ""Button"",
                    ""id"": ""b75cc1ea-7ff8-45b5-8d5d-125669bc6eb9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""NoClip"",
                    ""type"": ""Button"",
                    ""id"": ""6a981048-6045-448a-8a36-679e9f79e227"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""648f91fa-b1c9-497a-81bf-d607e06bafe6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""9648eab6-1fde-452e-8255-2b0a3673589e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""284286c9-d156-45c2-8dce-7da1b2afcabd"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""70ffb7e8-0afb-4aa7-b16f-87e58a4cd840"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a505b6ba-c904-40dd-9bbd-7f2baa84df77"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""0aa25389-62dd-45ff-abdf-c404f86b4dbd"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ad64155c-acd8-4172-ace9-3ed54c983f0f"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Switch Cameras"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""30e38437-e61e-420a-95b1-058539448de1"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0b10c245-0c9d-4291-93e7-4614a21fb3e9"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Block"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0e923218-0646-48fe-b9d9-3b3f60070ce0"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef3cd25e-d5b8-4dda-9e87-01cd121b1aec"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4d45f290-991a-4a8e-966a-b45e8c9abbd8"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e615c7c7-b564-4b6c-99c0-0f44661e053f"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e6adb0e-586d-46a9-80bc-1f8c44708242"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ab0d06fc-efac-42b7-a279-e5a5729e3080"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""61a38059-abd1-489e-b69e-71726f2441d1"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""83d34a50-1918-4c96-b324-2966febe40a3"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a39a9f4-77c9-4e1b-b9c7-af680b697831"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e209ea3-222c-4966-8b8d-25a7b92bc31d"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4625abd8-2250-4ce5-abd0-6deb3969a6fd"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c2ce9b16-06b5-4cd9-9637-bac6fd62cea7"",
                    ""path"": ""<Keyboard>/backquote"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Console"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8bcd47ec-b4c6-4ea4-80a2-450b9461f4e8"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dialog Response 1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e12c8e2d-4180-4482-b091-828e7de76bc3"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dialog Response 2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""664fe7d6-d930-4056-b6b9-eed2cb9690a8"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dialog Response 3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""876d66d7-4179-48e0-b477-9562b2cc223c"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dialog Response 4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2a3c5549-3f62-47d0-92a1-72d7ab9a47b0"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Tactical Mode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fd98d612-b169-4c5c-915e-afed97dcf775"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DodgeLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8df726bb-8a85-4516-b721-247de9390a00"",
                    ""path"": ""<Keyboard>/m"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Map"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""233b9302-a5f5-4293-8d87-da79d0689d63"",
                    ""path"": ""<Keyboard>/f3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""DebugScreen"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""22d6a727-48d5-4031-ad66-a01002123558"",
                    ""path"": ""<Keyboard>/f1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Feedback"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df6c8486-7c62-483b-b36e-fdda5a7f2a4c"",
                    ""path"": ""<Keyboard>/f2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Screenshot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dec271a4-a207-434d-97c3-6137f68eab42"",
                    ""path"": ""<Keyboard>/f5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""NoClip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menu Controls"",
            ""id"": ""92e8d9e6-352f-4bbe-916a-d720c45f4b3a"",
            ""actions"": [
                {
                    ""name"": ""ToggleMenu"",
                    ""type"": ""Button"",
                    ""id"": ""53004170-bd6d-4969-a88f-f8c6cea8b6be"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Page Right"",
                    ""type"": ""Button"",
                    ""id"": ""5e3c14af-20b7-4295-964c-f0d7772072f7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Page Left"",
                    ""type"": ""Button"",
                    ""id"": ""33b03134-97b4-486f-9dcf-fc43383ac3b1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Console"",
                    ""type"": ""Button"",
                    ""id"": ""4dcb12a8-c94a-4fb0-8b48-2b21311aa786"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Tactical Mode"",
                    ""type"": ""Button"",
                    ""id"": ""abd404b0-9f51-412c-b58e-8018510f7319"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Feedback"",
                    ""type"": ""Button"",
                    ""id"": ""e77e1104-e799-41f7-a260-0c30fd0c879a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Screenshot"",
                    ""type"": ""Button"",
                    ""id"": ""7da5e6df-cf23-47ef-85ed-a498ebf44f0a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DebugScreen"",
                    ""type"": ""Button"",
                    ""id"": ""96e37aaa-46af-48bb-8aa6-fa43edd8efb7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5db795f1-586c-4fa7-834f-5574877d7d1d"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Page Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c072aee8-c123-4c0b-ba33-845c4595dd93"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Page Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b6212d20-5b99-4959-8315-ebc76579f95b"",
                    ""path"": ""<Keyboard>/backquote"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Console"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7462a765-8203-4ab7-91e1-c6cab1767992"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Tactical Mode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b1e015a9-7223-449b-a85f-98c7a11a9814"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d149cd41-e197-46f5-b3db-8d87e7b3dc26"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a49581b5-bd1b-497d-b1ec-6fe1b7802282"",
                    ""path"": ""<Keyboard>/f3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""DebugScreen"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9289d84c-1b07-447a-9640-e4d96678cde3"",
                    ""path"": ""<Keyboard>/f2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Screenshot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b48dd5b5-5e03-45e1-8a33-e5bf8b6a4fb3"",
                    ""path"": ""<Keyboard>/f1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Feedback"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyboardMouse"",
            ""bindingGroup"": ""KeyboardMouse"",
            ""devices"": []
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": []
        }
    ]
}");
        // Game Controls
        m_GameControls = asset.FindActionMap("Game Controls", throwIfNotFound: true);
        m_GameControls_Move = m_GameControls.FindAction("Move", throwIfNotFound: true);
        m_GameControls_Look = m_GameControls.FindAction("Look", throwIfNotFound: true);
        m_GameControls_SwitchCameras = m_GameControls.FindAction("Switch Cameras", throwIfNotFound: true);
        m_GameControls_Attack = m_GameControls.FindAction("Attack", throwIfNotFound: true);
        m_GameControls_Block = m_GameControls.FindAction("Block", throwIfNotFound: true);
        m_GameControls_Jump = m_GameControls.FindAction("Jump", throwIfNotFound: true);
        m_GameControls_Crouch = m_GameControls.FindAction("Crouch", throwIfNotFound: true);
        m_GameControls_Run = m_GameControls.FindAction("Run", throwIfNotFound: true);
        m_GameControls_Interact = m_GameControls.FindAction("Interact", throwIfNotFound: true);
        m_GameControls_ToggleMenu = m_GameControls.FindAction("ToggleMenu", throwIfNotFound: true);
        m_GameControls_Console = m_GameControls.FindAction("Console", throwIfNotFound: true);
        m_GameControls_DialogResponse1 = m_GameControls.FindAction("Dialog Response 1", throwIfNotFound: true);
        m_GameControls_DialogResponse2 = m_GameControls.FindAction("Dialog Response 2", throwIfNotFound: true);
        m_GameControls_DialogResponse3 = m_GameControls.FindAction("Dialog Response 3", throwIfNotFound: true);
        m_GameControls_DialogResponse4 = m_GameControls.FindAction("Dialog Response 4", throwIfNotFound: true);
        m_GameControls_TacticalMode = m_GameControls.FindAction("Tactical Mode", throwIfNotFound: true);
        m_GameControls_DodgeLeft = m_GameControls.FindAction("DodgeLeft", throwIfNotFound: true);
        m_GameControls_Map = m_GameControls.FindAction("Map", throwIfNotFound: true);
        m_GameControls_Feedback = m_GameControls.FindAction("Feedback", throwIfNotFound: true);
        m_GameControls_Screenshot = m_GameControls.FindAction("Screenshot", throwIfNotFound: true);
        m_GameControls_DebugScreen = m_GameControls.FindAction("DebugScreen", throwIfNotFound: true);
        m_GameControls_NoClip = m_GameControls.FindAction("NoClip", throwIfNotFound: true);
        // Menu Controls
        m_MenuControls = asset.FindActionMap("Menu Controls", throwIfNotFound: true);
        m_MenuControls_ToggleMenu = m_MenuControls.FindAction("ToggleMenu", throwIfNotFound: true);
        m_MenuControls_PageRight = m_MenuControls.FindAction("Page Right", throwIfNotFound: true);
        m_MenuControls_PageLeft = m_MenuControls.FindAction("Page Left", throwIfNotFound: true);
        m_MenuControls_Console = m_MenuControls.FindAction("Console", throwIfNotFound: true);
        m_MenuControls_TacticalMode = m_MenuControls.FindAction("Tactical Mode", throwIfNotFound: true);
        m_MenuControls_Feedback = m_MenuControls.FindAction("Feedback", throwIfNotFound: true);
        m_MenuControls_Screenshot = m_MenuControls.FindAction("Screenshot", throwIfNotFound: true);
        m_MenuControls_DebugScreen = m_MenuControls.FindAction("DebugScreen", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Game Controls
    private readonly InputActionMap m_GameControls;
    private IGameControlsActions m_GameControlsActionsCallbackInterface;
    private readonly InputAction m_GameControls_Move;
    private readonly InputAction m_GameControls_Look;
    private readonly InputAction m_GameControls_SwitchCameras;
    private readonly InputAction m_GameControls_Attack;
    private readonly InputAction m_GameControls_Block;
    private readonly InputAction m_GameControls_Jump;
    private readonly InputAction m_GameControls_Crouch;
    private readonly InputAction m_GameControls_Run;
    private readonly InputAction m_GameControls_Interact;
    private readonly InputAction m_GameControls_ToggleMenu;
    private readonly InputAction m_GameControls_Console;
    private readonly InputAction m_GameControls_DialogResponse1;
    private readonly InputAction m_GameControls_DialogResponse2;
    private readonly InputAction m_GameControls_DialogResponse3;
    private readonly InputAction m_GameControls_DialogResponse4;
    private readonly InputAction m_GameControls_TacticalMode;
    private readonly InputAction m_GameControls_DodgeLeft;
    private readonly InputAction m_GameControls_Map;
    private readonly InputAction m_GameControls_Feedback;
    private readonly InputAction m_GameControls_Screenshot;
    private readonly InputAction m_GameControls_DebugScreen;
    private readonly InputAction m_GameControls_NoClip;
    public struct GameControlsActions
    {
        private @MacedonInputs m_Wrapper;
        public GameControlsActions(@MacedonInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_GameControls_Move;
        public InputAction @Look => m_Wrapper.m_GameControls_Look;
        public InputAction @SwitchCameras => m_Wrapper.m_GameControls_SwitchCameras;
        public InputAction @Attack => m_Wrapper.m_GameControls_Attack;
        public InputAction @Block => m_Wrapper.m_GameControls_Block;
        public InputAction @Jump => m_Wrapper.m_GameControls_Jump;
        public InputAction @Crouch => m_Wrapper.m_GameControls_Crouch;
        public InputAction @Run => m_Wrapper.m_GameControls_Run;
        public InputAction @Interact => m_Wrapper.m_GameControls_Interact;
        public InputAction @ToggleMenu => m_Wrapper.m_GameControls_ToggleMenu;
        public InputAction @Console => m_Wrapper.m_GameControls_Console;
        public InputAction @DialogResponse1 => m_Wrapper.m_GameControls_DialogResponse1;
        public InputAction @DialogResponse2 => m_Wrapper.m_GameControls_DialogResponse2;
        public InputAction @DialogResponse3 => m_Wrapper.m_GameControls_DialogResponse3;
        public InputAction @DialogResponse4 => m_Wrapper.m_GameControls_DialogResponse4;
        public InputAction @TacticalMode => m_Wrapper.m_GameControls_TacticalMode;
        public InputAction @DodgeLeft => m_Wrapper.m_GameControls_DodgeLeft;
        public InputAction @Map => m_Wrapper.m_GameControls_Map;
        public InputAction @Feedback => m_Wrapper.m_GameControls_Feedback;
        public InputAction @Screenshot => m_Wrapper.m_GameControls_Screenshot;
        public InputAction @DebugScreen => m_Wrapper.m_GameControls_DebugScreen;
        public InputAction @NoClip => m_Wrapper.m_GameControls_NoClip;
        public InputActionMap Get() { return m_Wrapper.m_GameControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameControlsActions set) { return set.Get(); }
        public void SetCallbacks(IGameControlsActions instance)
        {
            if (m_Wrapper.m_GameControlsActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnMove;
                @Look.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnLook;
                @SwitchCameras.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnSwitchCameras;
                @SwitchCameras.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnSwitchCameras;
                @SwitchCameras.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnSwitchCameras;
                @Attack.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnAttack;
                @Block.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnBlock;
                @Block.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnBlock;
                @Block.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnBlock;
                @Jump.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnJump;
                @Crouch.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnCrouch;
                @Crouch.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnCrouch;
                @Crouch.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnCrouch;
                @Run.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnRun;
                @Run.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnRun;
                @Run.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnRun;
                @Interact.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnInteract;
                @ToggleMenu.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnToggleMenu;
                @Console.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnConsole;
                @Console.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnConsole;
                @Console.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnConsole;
                @DialogResponse1.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse1;
                @DialogResponse1.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse1;
                @DialogResponse1.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse1;
                @DialogResponse2.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse2;
                @DialogResponse2.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse2;
                @DialogResponse2.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse2;
                @DialogResponse3.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse3;
                @DialogResponse3.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse3;
                @DialogResponse3.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse3;
                @DialogResponse4.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse4;
                @DialogResponse4.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse4;
                @DialogResponse4.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDialogResponse4;
                @TacticalMode.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnTacticalMode;
                @TacticalMode.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnTacticalMode;
                @TacticalMode.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnTacticalMode;
                @DodgeLeft.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDodgeLeft;
                @DodgeLeft.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDodgeLeft;
                @DodgeLeft.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDodgeLeft;
                @Map.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnMap;
                @Map.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnMap;
                @Map.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnMap;
                @Feedback.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnFeedback;
                @Feedback.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnFeedback;
                @Feedback.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnFeedback;
                @Screenshot.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnScreenshot;
                @Screenshot.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnScreenshot;
                @Screenshot.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnScreenshot;
                @DebugScreen.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDebugScreen;
                @DebugScreen.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDebugScreen;
                @DebugScreen.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnDebugScreen;
                @NoClip.started -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnNoClip;
                @NoClip.performed -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnNoClip;
                @NoClip.canceled -= m_Wrapper.m_GameControlsActionsCallbackInterface.OnNoClip;
            }
            m_Wrapper.m_GameControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @SwitchCameras.started += instance.OnSwitchCameras;
                @SwitchCameras.performed += instance.OnSwitchCameras;
                @SwitchCameras.canceled += instance.OnSwitchCameras;
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
                @Block.started += instance.OnBlock;
                @Block.performed += instance.OnBlock;
                @Block.canceled += instance.OnBlock;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Crouch.started += instance.OnCrouch;
                @Crouch.performed += instance.OnCrouch;
                @Crouch.canceled += instance.OnCrouch;
                @Run.started += instance.OnRun;
                @Run.performed += instance.OnRun;
                @Run.canceled += instance.OnRun;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @ToggleMenu.started += instance.OnToggleMenu;
                @ToggleMenu.performed += instance.OnToggleMenu;
                @ToggleMenu.canceled += instance.OnToggleMenu;
                @Console.started += instance.OnConsole;
                @Console.performed += instance.OnConsole;
                @Console.canceled += instance.OnConsole;
                @DialogResponse1.started += instance.OnDialogResponse1;
                @DialogResponse1.performed += instance.OnDialogResponse1;
                @DialogResponse1.canceled += instance.OnDialogResponse1;
                @DialogResponse2.started += instance.OnDialogResponse2;
                @DialogResponse2.performed += instance.OnDialogResponse2;
                @DialogResponse2.canceled += instance.OnDialogResponse2;
                @DialogResponse3.started += instance.OnDialogResponse3;
                @DialogResponse3.performed += instance.OnDialogResponse3;
                @DialogResponse3.canceled += instance.OnDialogResponse3;
                @DialogResponse4.started += instance.OnDialogResponse4;
                @DialogResponse4.performed += instance.OnDialogResponse4;
                @DialogResponse4.canceled += instance.OnDialogResponse4;
                @TacticalMode.started += instance.OnTacticalMode;
                @TacticalMode.performed += instance.OnTacticalMode;
                @TacticalMode.canceled += instance.OnTacticalMode;
                @DodgeLeft.started += instance.OnDodgeLeft;
                @DodgeLeft.performed += instance.OnDodgeLeft;
                @DodgeLeft.canceled += instance.OnDodgeLeft;
                @Map.started += instance.OnMap;
                @Map.performed += instance.OnMap;
                @Map.canceled += instance.OnMap;
                @Feedback.started += instance.OnFeedback;
                @Feedback.performed += instance.OnFeedback;
                @Feedback.canceled += instance.OnFeedback;
                @Screenshot.started += instance.OnScreenshot;
                @Screenshot.performed += instance.OnScreenshot;
                @Screenshot.canceled += instance.OnScreenshot;
                @DebugScreen.started += instance.OnDebugScreen;
                @DebugScreen.performed += instance.OnDebugScreen;
                @DebugScreen.canceled += instance.OnDebugScreen;
                @NoClip.started += instance.OnNoClip;
                @NoClip.performed += instance.OnNoClip;
                @NoClip.canceled += instance.OnNoClip;
            }
        }
    }
    public GameControlsActions @GameControls => new GameControlsActions(this);

    // Menu Controls
    private readonly InputActionMap m_MenuControls;
    private IMenuControlsActions m_MenuControlsActionsCallbackInterface;
    private readonly InputAction m_MenuControls_ToggleMenu;
    private readonly InputAction m_MenuControls_PageRight;
    private readonly InputAction m_MenuControls_PageLeft;
    private readonly InputAction m_MenuControls_Console;
    private readonly InputAction m_MenuControls_TacticalMode;
    private readonly InputAction m_MenuControls_Feedback;
    private readonly InputAction m_MenuControls_Screenshot;
    private readonly InputAction m_MenuControls_DebugScreen;
    public struct MenuControlsActions
    {
        private @MacedonInputs m_Wrapper;
        public MenuControlsActions(@MacedonInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @ToggleMenu => m_Wrapper.m_MenuControls_ToggleMenu;
        public InputAction @PageRight => m_Wrapper.m_MenuControls_PageRight;
        public InputAction @PageLeft => m_Wrapper.m_MenuControls_PageLeft;
        public InputAction @Console => m_Wrapper.m_MenuControls_Console;
        public InputAction @TacticalMode => m_Wrapper.m_MenuControls_TacticalMode;
        public InputAction @Feedback => m_Wrapper.m_MenuControls_Feedback;
        public InputAction @Screenshot => m_Wrapper.m_MenuControls_Screenshot;
        public InputAction @DebugScreen => m_Wrapper.m_MenuControls_DebugScreen;
        public InputActionMap Get() { return m_Wrapper.m_MenuControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuControlsActions set) { return set.Get(); }
        public void SetCallbacks(IMenuControlsActions instance)
        {
            if (m_Wrapper.m_MenuControlsActionsCallbackInterface != null)
            {
                @ToggleMenu.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnToggleMenu;
                @PageRight.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnPageRight;
                @PageRight.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnPageRight;
                @PageRight.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnPageRight;
                @PageLeft.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnPageLeft;
                @PageLeft.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnPageLeft;
                @PageLeft.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnPageLeft;
                @Console.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnConsole;
                @Console.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnConsole;
                @Console.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnConsole;
                @TacticalMode.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnTacticalMode;
                @TacticalMode.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnTacticalMode;
                @TacticalMode.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnTacticalMode;
                @Feedback.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnFeedback;
                @Feedback.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnFeedback;
                @Feedback.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnFeedback;
                @Screenshot.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnScreenshot;
                @Screenshot.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnScreenshot;
                @Screenshot.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnScreenshot;
                @DebugScreen.started -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnDebugScreen;
                @DebugScreen.performed -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnDebugScreen;
                @DebugScreen.canceled -= m_Wrapper.m_MenuControlsActionsCallbackInterface.OnDebugScreen;
            }
            m_Wrapper.m_MenuControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ToggleMenu.started += instance.OnToggleMenu;
                @ToggleMenu.performed += instance.OnToggleMenu;
                @ToggleMenu.canceled += instance.OnToggleMenu;
                @PageRight.started += instance.OnPageRight;
                @PageRight.performed += instance.OnPageRight;
                @PageRight.canceled += instance.OnPageRight;
                @PageLeft.started += instance.OnPageLeft;
                @PageLeft.performed += instance.OnPageLeft;
                @PageLeft.canceled += instance.OnPageLeft;
                @Console.started += instance.OnConsole;
                @Console.performed += instance.OnConsole;
                @Console.canceled += instance.OnConsole;
                @TacticalMode.started += instance.OnTacticalMode;
                @TacticalMode.performed += instance.OnTacticalMode;
                @TacticalMode.canceled += instance.OnTacticalMode;
                @Feedback.started += instance.OnFeedback;
                @Feedback.performed += instance.OnFeedback;
                @Feedback.canceled += instance.OnFeedback;
                @Screenshot.started += instance.OnScreenshot;
                @Screenshot.performed += instance.OnScreenshot;
                @Screenshot.canceled += instance.OnScreenshot;
                @DebugScreen.started += instance.OnDebugScreen;
                @DebugScreen.performed += instance.OnDebugScreen;
                @DebugScreen.canceled += instance.OnDebugScreen;
            }
        }
    }
    public MenuControlsActions @MenuControls => new MenuControlsActions(this);
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("KeyboardMouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IGameControlsActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnSwitchCameras(InputAction.CallbackContext context);
        void OnAttack(InputAction.CallbackContext context);
        void OnBlock(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnCrouch(InputAction.CallbackContext context);
        void OnRun(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnToggleMenu(InputAction.CallbackContext context);
        void OnConsole(InputAction.CallbackContext context);
        void OnDialogResponse1(InputAction.CallbackContext context);
        void OnDialogResponse2(InputAction.CallbackContext context);
        void OnDialogResponse3(InputAction.CallbackContext context);
        void OnDialogResponse4(InputAction.CallbackContext context);
        void OnTacticalMode(InputAction.CallbackContext context);
        void OnDodgeLeft(InputAction.CallbackContext context);
        void OnMap(InputAction.CallbackContext context);
        void OnFeedback(InputAction.CallbackContext context);
        void OnScreenshot(InputAction.CallbackContext context);
        void OnDebugScreen(InputAction.CallbackContext context);
        void OnNoClip(InputAction.CallbackContext context);
    }
    public interface IMenuControlsActions
    {
        void OnToggleMenu(InputAction.CallbackContext context);
        void OnPageRight(InputAction.CallbackContext context);
        void OnPageLeft(InputAction.CallbackContext context);
        void OnConsole(InputAction.CallbackContext context);
        void OnTacticalMode(InputAction.CallbackContext context);
        void OnFeedback(InputAction.CallbackContext context);
        void OnScreenshot(InputAction.CallbackContext context);
        void OnDebugScreen(InputAction.CallbackContext context);
    }
}
