using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{    // Adjust the speed for the application.
    [SerializeField] [Range(1, 20)] float moveSpeed = 1f;
    [SerializeField] [Range(1, 20)] float rotationSpeed = 1f;


    // The target (cylinder) position.
    [SerializeField] Transform target;
    [SerializeField] [ReadOnly] Vector3 velocity;
    Vector3 lastPosition;
    [SerializeField] bool movePlatform;

    public Vector3 Velocity { get { return velocity; } }

    private void Awake()
    {
        lastPosition = transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        if (movePlatform)
        {
            // calculate velocity by taking last frames and current position
            velocity = (transform.position - lastPosition) / Time.deltaTime;
            lastPosition = transform.position;


            // directional vector to target
            Vector3 direction = (target.position - transform.position);
            Debug.DrawLine(transform.position, transform.position + direction);

            // rotate object to point to it
            Quaternion toRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);

            // move object a step closer to the target.
            float step = moveSpeed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);



            // Check if the position of the cube and target are approximately equal.
            if (Vector3.Distance(transform.position, target.position) < 0.005f)
            {
                // move the target
                //target.position += new Vector3(Random.Range(25, 50), 0, Random.Range(25, 50));

                // stop the moving
                movePlatform = false;
                
                velocity = Vector3.zero;
            }
        }

    }

    private void FixedUpdate()
    {
    }
}
