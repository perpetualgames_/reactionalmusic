﻿
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using Ionic.Zip;
    using Newtonsoft.Json;
    using UnityEngine.PlayerLoop;
    using Debug = UnityEngine.Debug;

    public class ModPackageZip
    {
        public enum ModPackageType { Folder, Zip }
        
        string _modInfoFileName = "mod.info";
        ModPackageType _packageType;
        string _filePath;
        ZipFile _zip;
        
        public ModInfo ModInfo { get; private set; }
        
        public bool IsValid { get; private set; }
        
        public ModPackageZip(string pathToZipFile)
        {
            _packageType = ModPackageType.Zip;
            _filePath = pathToZipFile;
        }

        public IEnumerator Init()
        {        
            Stopwatch sw = new Stopwatch();
            sw.Start();
            
            _zip = ZipFile.Read(_filePath);
            
            if (TryGetModInfoFileFromZip(_zip, out ModInfo modInfo))
            {
                LoopThroughZip();
                ModInfo = modInfo;
                IsValid = true;
            }
            else
            {
                IsValid = false;
            }
            
            sw.Stop();
            Debug.Log($"{this} Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
            
            yield return null;
        }

        string ReadZippedTextFile(ZipEntry zipEntry)
        {
            using Stream s = zipEntry.OpenReader();
            using StreamReader sr = new StreamReader(s);
            string content = sr.ReadToEnd();
            //Debug.Log(content);

            return content;
        }



        void LoopThroughZip()
        {
            foreach (ZipEntry entry in _zip.Entries)
            {
                Debug.Log(entry.FileName + " isDirectory=" + entry.IsDirectory);

                // if it's not a directory, do something with the file maybe
                if (!entry.IsDirectory)
                {
                    if (Path.GetExtension(entry.FileName).EndsWith("material"))
                    {
                        //ProcessZippedMaterial(entry);
                    }
                    else if (Path.GetExtension(entry.FileName).EndsWith("mesh"))
                    {
                        //ProcessZippedMesh(entry);
                    }
                    else if (Path.GetExtension(entry.FileName).EndsWith("gameobject"))
                    {
                        //ProcessZippedGameObject(entry);
                    }
                }
            }
        }
        
        public bool TryGetModInfoFileFromZip(ZipFile zipFile, out ModInfo modInfo)
        {
            ZipEntry modInfoZipEntry = zipFile[_modInfoFileName];

            if (modInfoZipEntry == null)
            {
                Debug.LogWarning($"Failed to find {_modInfoFileName} in {zipFile}");
                modInfo = null;
                return false;
            }

            string json;
            
            try
            {
                json = ReadZippedTextFile(modInfoZipEntry);
            }
            catch (System.Exception)
            {
                Debug.LogWarning($"Failed to read json from zip at {modInfoZipEntry}");
                modInfo = null;
                return false;
            }

            try
            {
                modInfo = JsonConvert.DeserializeObject<ModInfo>(json);
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Failed to deserialize json to config. {e.Message}");
                modInfo = null;
                return false;
            }
            
            // if deserialized correctly, update some dynamic fields
            modInfo.ZipPath = _filePath;
            modInfo.Path = modInfoZipEntry.FileName;
            modInfo.LoadedFolder = Path.GetDirectoryName(modInfoZipEntry.FileName);

            Debug.Log(modInfo);
            return true;
        }
        
        
    }
