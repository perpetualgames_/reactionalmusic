using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class GameScene : MonoBehaviour
{
    [SerializeField] bool checkForCoreScene = true;
    [SerializeField] Player player;
    [SerializeField] Enemy enemy;
    
    void Awake()
    {
        Debug.Log($"GameScene Awake()");

        // wait for services before doing ANYTHING else
        CoreManager.OnLoaded += ServiceLocator_OnInitFinished;

        // might need to load the core scene in order to get the other services started
        if(checkForCoreScene)
            LoadCoreSceneIfNeeded();
    }

    void OnValidate()
    {
        Assert.IsNotNull(player, "player not assigned in inspector?");
        //Assert.IsNotNull(enemy, "enemy not assigned in inspector?");
    }

    void ServiceLocator_OnInitFinished()
    {
        Debug.Log($"GameScene ServiceLoader_OnInitFinished()");
        
        player.Initialize();
        //enemy.OnCreated();
    }


    void LoadCoreSceneIfNeeded()
    {
        Scene coreScene = SceneManager.GetSceneByName("Core");

        // check to see if the core scene is in the hierarchy and loaded
        if (coreScene.IsValid() == true)
        {
            Debug.Log($"MenuScene: Core scene is valid. ");

            if (coreScene.isLoaded == true)
            {
                Debug.Log($"MenuScene: Core scene is loaded. ");
            }

            return;
        }

        Debug.LogWarning($"MenuScene: Core scene not found. Needs to be loaded");

        SceneManager.LoadScene("Core", LoadSceneMode.Additive);
    }
}
