﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Ink.Runtime;
using Ink;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class ElegosInkStory
{
    public enum ElegosInkStoryState { Running, Stopped }
    
    public struct InkLine
    {
        public ParsedInkLine parsedInkLine;
        public string id;
        public string line;
        public string character;
        public float duration;
        public AudioClip audioClip;
        public string audioFilePath;
        //public Actor actor;
        public string inkPathString;

        public override string ToString()
        {
            return $"InkLine inkPathString={inkPathString} id={id}, line={line}, line={character}, duration={duration}, audioFilePath={audioFilePath}";
        }
    }

    public struct ParsedInkLine
    {
        public string character;
        public int characterNumber;
        public int sceneNumber;
        public int decisionNumber;
        public int lineNumber;
        public string id;
        public string line;
        public string unparsedLine;

        public override string ToString()
        {
            return $"ParseInkLine id={id}, character={character}, sceneNumber={sceneNumber}, decisionNumber={decisionNumber}, lineNumber={lineNumber}, line={line}";
        }
    }


    Story _inkStory;
    public Dictionary<string, dynamic> Variables { get; private set; }
    public string[] Characters { get; private set; }
    public string[] Knots { get; private set; }
    public string Name { get; private set; }
    public bool IsPaused { get; private set; }
    public bool IsWaitingForChoice { get; private set; }
    
    
    string[] _customInkKeywords = new[] {"CHARLIST"};
    bool _storyInterruptFlag = false;
    float _timerDuration = 0f;
    float _choiceTimerDuration = 15f;

    public string[] BoundFunctions { get; private set; }
    
    public event Action<string> InkTrigger;
    public event Action<string> InkDebug;

    public event Action Started;
    public event Action Paused;
    public event Action Unpaused;
    public event Action Interrupted;
    public event Action<List<Choice>> WaitingForChoice;
    public event Action Finished;

    /// <summary>
    /// New InkStory from jsonString, normally used for pre-compiled ink files
    /// </summary>
    /// <param name="name"></param>
    /// <param name="jsonString"></param>
    public ElegosInkStory(string name, string jsonString)
    {
        Name = name;
        _inkStory = new Story(jsonString);
    }
    
    /// <summary>
    /// New InkStory from an uncompiled file, normally used at runtime when loading from disk
    /// </summary>
    /// <param name="name"></param>
    /// <param name="compiler"></param>
    public ElegosInkStory(string name, Compiler compiler)
    {
        Name = name;
        _inkStory = compiler.Compile();
    } 
    
    public IEnumerator Init()
    {        
        Stopwatch sw = new Stopwatch();
        sw.Start();

        //Debug.Log($"MacedonInkStory name={Name}");

        // get variables
        Variables = InkUtils.GetVariablesDictionary(_inkStory);
        Debug.Log($"MacedonInkStory name={Name} Variables={string.Join(",", Variables.Keys.ToArray())}");
        
        // bind external function(s)
        BoundFunctions = BindExternalFunctions();
        Debug.Log($"MacedonInkStory name={Name} BoundFunctions={string.Join(",", BoundFunctions)}");
        
        // get knots
        Knots = InkUtils.GetAllKnots(_inkStory);
        Debug.Log($"MacedonInkStory name={Name} Knots={string.Join(",", Knots)}");
        
        // get character list
        Characters = LookForCharacterList(_inkStory);
        Debug.Log($"MacedonInkStory name={Name} Characters={string.Join(",", Characters)}");


        Debug.Log($"MacedonInkStory name={Name} complete in {sw.Elapsed.TotalSeconds:0.00} seconds");

        yield return null;
    }
    
    void Pause()
    {
        Paused?.Invoke();
        Debug.LogWarning($"Story is Paused.");
        IsPaused = true;
    }

    public void Unpause()
    {
        if (!IsPaused)
        {
            Debug.LogWarning($"Can't Continue as the Story isn't Paused");
            return;
        }
        
        Unpaused?.Invoke();
        IsPaused = false;
    }

    public void MakeChoice(int choiceIndex)
    {
        Debug.Log($"choiceIndex={choiceIndex}, _inkStory.currentChoices.Count={_inkStory.currentChoices.Count}");
        
        if (!IsWaitingForChoice)
        {
            Debug.LogWarning($"Can't MakeChoice as the Story isn't Waiting For Choice");
            return;
        }

        if (choiceIndex > _inkStory.currentChoices.Count)
        {
            Debug.LogWarning($"Can't MakeChoice as the Choice made is out of bounds for the choices available");
            return;    
        } 
        
        // this function is zero-indexed
        _inkStory.ChooseChoiceIndex(choiceIndex-1);
        
        IsWaitingForChoice = false;
    }

    public IEnumerator Play()
    {
        Started?.Invoke();
        
        _timerDuration = 0;
        float timer = 0;

        string inkLineString;                
        string returnPathString = "";
        
        // go back to beginning of file?
        _inkStory.ChoosePathString("0");
        
        // load variables from blackboard
        
        // always loop through
        while (true)
        {         
            // label for the interrupt to goto
            BeginningInterrupt:
            
            // break out of this while loop if an interrupt is felt
            if (_storyInterruptFlag)
            {
                Interrupted?.Invoke();
                
                Debug.LogWarning($"Story is Interrupted.");
                _storyInterruptFlag = false;
                
                // cancel timers
                _timerDuration = 0;
                timer = 0;
                
                // jump to specific knot (in this case only used to interrupt flow and jump to walked_away
                _inkStory.ChoosePathString("walked_away");
            }

            // timer duration stuff
            // if we have a duration, then we prob just needs to count the timer up until we move on
            if (_timerDuration > 0)
            {
                // if timer is over the duration, then we can reset and move onto the inkstory stuff
                if (timer > _timerDuration)
                {
                    _timerDuration = 0;
                    timer = 0;
                }
                // if it's not reached the duration yet, move the loop on without doing everything else
                else
                {
                    //Debug.Log($"{timer}/{duration}");
                    timer += Time.deltaTime;

                    // waits a frame and then continues the loop
                    yield return null;
                    continue;
                }
            }
            
            // ink story flow checking
            if (_inkStory.canContinue)
            {
                inkLineString = _inkStory.Continue();

                // NOTE:
                // ExternalFunctions in Ink are called during the Continue() function and so if anything needs to happen
                // before the story continues, like a cutscene trigger or something, then Pause() needs to be called in
                // the external event Handler so that the below Pause loop waits until Continue() is called, probably
                // from the calling DialogueManager
                

                // if Paused is set in the external function handler, wait until it's told to continue
                while (IsPaused)
                {
                    // break out of this while loop if an interrupt is felt. It will deal with the redirect in a second
                    if (_storyInterruptFlag) goto BeginningInterrupt;

                    yield return null;
                }

                // process the inkline into all it's parts
                if (!TryProcessInkLine(inkLineString, out InkLine inkLine))
                {
                    Debug.LogWarning($"inkLine isn't valid.");
                    continue;
                }
                
                Debug.Log($"SUCCESSFUL INK LINE {inkLine}");
                
                // set timerduration so we wait until it's done before we read the next line in ink
                _timerDuration = 0.5f;
            }
            // run out of ink lines but we do have a choice to make
            else if (_inkStory.currentChoices.Count > 0)
            {
                foreach (Choice choice in _inkStory.currentChoices)
                {
                    Debug.Log($"CHOICE: {choice.text}");
                }

                Debug.LogWarning($"Story is Waiting For Choice.");
                IsWaitingForChoice = true;
                
                WaitingForChoice?.Invoke(_inkStory.currentChoices);

                float choiceTimer = 0f;
                
                // loop around here until a choice is made and IsWaitingForChoice is set to false via MakeChoice()
                while (IsWaitingForChoice)
                {                    
                    // break out of this while loop if an interrupt is felt. The next interation of the main loop will deal
                    // with the redirect
                    if (_storyInterruptFlag) break;
                    
                    choiceTimer += Time.deltaTime;
     
                    if (choiceTimer > _choiceTimerDuration)
                    {
                        // reset timer
                        choiceTimer = 0f;
         
                        // Do Stuff
                        Debug.LogWarning($"Waited too long for a choice");
                        
                        // jump to specific knot (in this case only used to interrupt flow and jump to walked_away
                        _inkStory.ChoosePathString("no_choice_made");
                        
                        // jump to beginning of story loop to hopefully redirect to the above knot
                        goto BeginningInterrupt;
                    }

                    yield return null;
                }
                
            }
            // no ink lines and no choices so we've finished the story
            else
            {
                // do we have somewhere else we need to return to before we finish?
                
                // save variables to blackboard
                
                Finished?.Invoke();
                break;
            }
            
            // keep looping until we break the loop manually
            yield return null;
        }
        

        // end of the play coroutine, only reached when 'break' is call during the while loop
    }

    public void Interrupt()
    {
        Interrupted?.Invoke();
        _storyInterruptFlag = true;
    }

    public bool TryProcessInkLine(string inkLineString, out InkLine inkLine) // out parameter for result
    
    {
        //Debug.Log($"Processing inkLine={inkLine}");
        
        inkLine = new InkLine();

        if (string.IsNullOrEmpty(inkLineString))
        {
            Debug.LogWarning($"inkLine is empty.");
            return false;
        }
        
        // if the line contains any of our custom keywords (like CHARLIST) then just skip it
        if (_customInkKeywords.Any(inkLineString.Contains))
        {
            Debug.LogWarning($"inkLine contains a custom keyword.");
            return false;
        }

        // if the inkLine doesn't parse correctly (checking for linecode) then just skip it
        if (!TryParseInkLine(inkLineString, out ParsedInkLine parsedInkLine))
        {
            Debug.LogWarning($"inkLine didn't parse correctly.");
            return false;
        }

        inkLine.inkPathString = _inkStory.state.currentPathString;
        inkLine.parsedInkLine = parsedInkLine;
        inkLine.line = parsedInkLine.line; // this is maybe what we use if nothing overrides it in a sec
        
        // this is the bare minimum that we need, text to show

        // do we have an ID from a tag?
        inkLine.id = parsedInkLine.id;

        return true;
    }


    public bool TryParseInkLine(string inkLine, out ParsedInkLine parsedInkLine) // out parameter for result
    {
        // sailor3_S1_D1_L1 Blah Blah Blah
        // ACTORNAME_SCENENUMBER_DECISION_LINE CONTENT

        //Debug.Log($"ParseInkLine inkLine={inkLine}");

        parsedInkLine = new ParsedInkLine();

        // record the original line
        parsedInkLine.unparsedLine = inkLine;

        // the regular expression we use to match.
        Regex regex =
            new Regex(
                @"(?<id>(?<actor>[^_]+)_S(?<scene>[\d]+)_D(?<decision>[\d]+)_L(?<line>[\d]+))\s+(?<content>[^_]+)");

        // ... See if we matched.
        Match match = regex.Match(inkLine);

        if (!match.Success)
        {
            // shouldn't get here, but if it does, then just send back false so it skips it
            Debug.LogWarning($"Failed to parse a valid linecode from the inkLine");
            return false;
        }


        // ... get values from group names.
        parsedInkLine.id = match.Groups["id"].Value;
        parsedInkLine.character = match.Groups["actor"].Value;
        parsedInkLine.sceneNumber = int.Parse(match.Groups["scene"].Value);
        parsedInkLine.decisionNumber = int.Parse(match.Groups["decision"].Value);
        parsedInkLine.lineNumber = int.Parse(match.Groups["line"].Value);
        parsedInkLine.line = match.Groups["content"].Value;

        // do we have a number at the end of the actor name?
        // ... See if we matched.
        Match match2 = Regex.Match(parsedInkLine.character, @"\d+$");

        if (match2.Success)
        {
            parsedInkLine.characterNumber = int.Parse(match2.Value);
        }

//        Debug.Log($"ParseInkLine id={parsedInkLine.id}, character={parsedInkLine.character}, characterNumber={parsedInkLine.characterNumber}, sceneNumber={parsedInkLine.sceneNumber}, decisionNumber={parsedInkLine.decisionNumber}, lineNumber={parsedInkLine.lineNumber}, line={parsedInkLine.line}");

        return true;
    }


    string[] BindExternalFunctions()
    {
        _inkStory.BindExternalFunction("Trigger", new Action<string>(InkFunction_Trigger));
        _inkStory.BindExternalFunction("Debug", new Action<string>(InkFunction_Debug));
        
        return new string[2] {"Trigger", "Debug"};
    }

    void InkFunction_Debug(string obj)
    {
        Debug.Log($"InkFunction_Debug={obj}");
        InkDebug?.Invoke(obj);
    }

    void InkFunction_Trigger(string obj)
    {
        Debug.Log($"InkFunction_Trigger={obj}");

        Pause();
        InkTrigger?.Invoke(obj);
    }


    string[] LookForCharacterList(Story inkStory)
    {
        string inkLine, characterLine;
        
        // go back to beginning of file?
        inkStory.ChoosePathString("0");

        // loop through first lines, looking for a 'CHARLIST'
        while (inkStory.canContinue)
        {
            inkLine = inkStory.Continue();
            
            //Debug.Log($"MacedonInkStory inkLine={inkLine}");

            // if this isn't a CHARLIST tag, then skip to next line
            if (!inkLine.Contains("CHARLIST")) continue;
            
            // get a comma delimited string after the first space i.e. guard1,guard2
            characterLine = inkLine.Substring(inkLine.IndexOf(" ") + 1);
            
            // go back to beginning of file just in case?
            inkStory.ChoosePathString("0");
            
            // split into an array and trim the whitespace from each item
            return Array.ConvertAll(characterLine.Split(','), s => s.Trim());
        }
        
        // go back to beginning of file?
        inkStory.ChoosePathString("0");
        
        // if we got this far, then we haven't found a CHARLIST and so return empty array
        return new string[0];
    }


}