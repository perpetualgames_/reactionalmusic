using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseInteraction : MonoBehaviour
{

    [SerializeField] protected bool isInteractable = true;

    public bool IsInteractable => isInteractable;

    public abstract void StartInteract();
    public abstract void EndInteract();
    public abstract void EndInteractEarly();

    // called when component is added to an object, editor only
    public virtual void Reset()
    {
        Debug.Log($"BaseInteraction Reset");
    }
}
