using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Enemy : MonoBehaviour, IUpdate
{
    public void OnCreated()
    {
        CoreManager.Instance.RegisterForUpdate(this);
    }
    
    public void OnUpdate()
    {
        //Debug.Log($"{name} OnUpdate() frameCount={Time.frameCount}");
        //SlowWork();
    }

    void SlowWork()
    {
        int time = 100;
        Thread.Sleep(time);    // A lot of calculations, trust me!
    }

    void OnDestroy()
    {
        CoreManager.Instance.UnregisterUpdate(this);
    }
}