using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    [SerializeField] bool checkForCoreScene = true;
    [SerializeField] string[] scenes;
    [SerializeField] GameObject menuCanvas;
    [SerializeField] GameObject loadingCanvas;
    [SerializeField] Camera cam;

    private ConfigService configService;
    private InputService inputService;

    void OnValidate()
    {
        Assert.IsNotNull(menuCanvas, "menuCanvas not assigned in inspector?");
        Assert.IsNotNull(loadingCanvas, "loadingCanvas not assigned in inspector?");
        Assert.IsNotNull(cam, "camera not assigned in inspector?");
    }

    private void Awake()
    {
        Debug.Log($"MenuScene Awake()");
        
        // turn on menu canvas
        menuCanvas.SetActive(true);
        
        // turn off loading canvas
        loadingCanvas.SetActive(false);
        
        // wait for gamemanager before doing ANYTHING else
        CoreManager.OnLoaded += ServiceLoader_OnInitFinished;

        // might need to load the core scene in order to get the gamemanager and other services started
        if(checkForCoreScene)
            LoadCoreSceneIfNeeded();
    }



    private void ServiceLoader_OnInitFinished()
    {
        Debug.Log($"MenuScene ServiceLoader_OnInitFinished()");

        configService = CoreManager.Instance.Get<ConfigService>();
        inputService = CoreManager.Instance.Get<InputService>();

        configService.OnConfigChanged += OnConfigChanged;
        configService.OnConfigLoaded += OnConfigLoaded;
    }

    private void OnConfigLoaded()
    {
        Debug.Log($"MenuScene OnConfigLoaded()");
    }

    private void OnConfigChanged(string arg1, dynamic arg2)
    {
        Debug.Log($"MenuScene OnConfigChanged() arg1={arg1}, arg2={arg2}");
    }

    private void OnDestroy()
    {
        CoreManager.OnLoaded -= ServiceLoader_OnInitFinished;

        if (configService != null)
        {
            configService.OnConfigChanged -= OnConfigChanged;
            configService.OnConfigLoaded -= OnConfigLoaded;
        }
    }

    void LoadCoreSceneIfNeeded()
    {
        Scene coreScene = SceneManager.GetSceneByName("Core");

        // check to see if the core scene is in the hierarchy and loaded
        if (coreScene.IsValid() == true)
        {
            Debug.Log($"MenuScene: Core scene is valid. ");

            if (coreScene.isLoaded == true)
            {
                Debug.Log($"MenuScene: Core scene is loaded. ");
            }

            return;
        }

        Debug.LogWarning($"MenuScene: Core scene not found. Needs to be loaded");

        SceneManager.LoadScene("Core", LoadSceneMode.Additive);
    }

    public void LoadGame()
    {
        StartCoroutine(LoadAllScenes());
    }


    IEnumerator LoadAllScenes()
        {
            // turn on loading canvas
            loadingCanvas.SetActive(true);
            
            yield return null;

            // loop through each scene and load them in order
            for (int i = 0; i < scenes.Length; i++)
            {
                Debug.Log(scenes[i]);
                yield return StartCoroutine(LoadScene(scenes[i]));
            }

            // wait for 2 seconds before we turn off the splash screen UI
            Debug.Log("Waiting 2 seconds for final clean up...");
            yield return new WaitForSeconds(2);
            
            // turn off this camera
            cam.enabled = false;

            // set last scene as active
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(scenes[scenes.Length - 1]));
            
            // turn off loading canvas
            loadingCanvas.SetActive(false);
            
            // turn off menu canvas
            menuCanvas.SetActive(false);

            //GameSceneManager.Instance.MainSceneLoaded();
        }


        IEnumerator LoadScene(string sceneName)
        {
            Debug.Log("Loading scene: " + sceneName);

            yield return null;

            AsyncOperation asyncScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            // asyncScene.isDone includes activation time at the end, the asyncScene.progress stops at 0.9f when initial loading
            // is complete, and activation to go.

            // this value stops the scene from displaying when it's finished loading
            asyncScene.allowSceneActivation = false;

            while (!asyncScene.isDone)
            {
                // loading bar progress
                //LoadingProgress = Mathf.Clamp01(asyncScene.progress / 0.9f) * 100;

                Debug.Log(asyncScene.progress);

                // scene has loaded as much as possible, the last 10% can't be multi-threaded
                if (asyncScene.progress >= 0.9f)
                {
                    // we finally show the scene
                    asyncScene.allowSceneActivation = true;
                }

                yield return null;
            }

        }
}
