using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceChecker : MonoBehaviour
{
    AudioService audioService;
    ConfigService configService;
    InputService inputService;

    private void Awake()
    {
        CoreManager.OnLoaded += GameManager_OnLoaded;
    }


    // Start is called before the first frame update
    void Initialize()
    {
        audioService = CoreManager.Instance.Get<AudioService>();
        configService = CoreManager.Instance.Get<ConfigService>();
        inputService = CoreManager.Instance.Get<InputService>();

        configService.OnConfigChanged += OnConfigChanged;
        configService.OnConfigLoaded += OnConfigLoaded;

        Debug.Log($"ServiceChecker Init() audioService={audioService}, configService={configService}, inputService={inputService}");

        inputService.Inputs.GameControls.Run.performed += Run_performed;

    }

    private void Run_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        Debug.Log($"ServiceChecker Run_performed({obj})");
    }

    private void GameManager_OnLoaded()
    {
        Initialize();
    }

    private void OnConfigLoaded()
    {
        Debug.Log($"ServiceChecker OnConfigLoaded()");
    }

    private void OnConfigChanged(string arg1, dynamic arg2)
    {
        Debug.Log($"ServiceChecker OnConfigChanged() arg1={arg1}, arg2={arg2}");
    }

    private void OnDestroy()
    {
        if (configService != null)
        {
            configService.OnConfigChanged -= OnConfigChanged;
            configService.OnConfigLoaded -= OnConfigLoaded;
        }
    }
}
