using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Macedon
{ 
    [ExecuteAlways]
    public class Ladder : MonoBehaviour
    {
        [SerializeField] bool showDebug;

        [SerializeField] Transform topPoint;
        //[SerializeField] Transform bottomPoint;
        //[SerializeField] Transform topDismount;
        //[SerializeField] Transform bottomDismount;

        [SerializeField] Transform debugTop;
        [SerializeField] Transform debugBottom;
        [SerializeField] Transform debugTrigger;

        //Vector3 pointSize = new Vector3(1, 2, 1);
        //[SerializeField] Vector3 dismountSize = new Vector3(1, 0.1f, 1);

        [SerializeField] Transform trigger;
        private float triggerHeight;

        [SerializeField] [Range(1, 5)] float width;
        [SerializeField] Vector3 dirFromBottomToTop;
        ConfigService configService;

        //[SerializeField] float height;

        private void Start()
        {
            configService = CoreManager.Instance.Get<ConfigService>();

            //Debug.Log(name + " is awake");

            if (Application.IsPlaying(gameObject))
            {
                // Play logic

                //Debug.Log(ConfigLoader.Instance.Config["ShowLadderDebug"]);

                showDebug = configService.GetValue("debug-showladders");
            }
            else
            {
                // Editor logic
            }            
        }

        private void Update()
        {
            debugTop.gameObject.SetActive(showDebug);
            debugBottom.gameObject.SetActive(showDebug);
            debugTrigger.gameObject.SetActive(showDebug);

            if (Application.IsPlaying(gameObject))
            {
                // Play logic


            }
            else
            {
                // Editor logic


                // restrict movements when moving in editor

                // stop top point being moved 'sideways', z-axis/forward only i.e. away from the wall and clamping to relevant values
                topPoint.localPosition = new Vector3(0, Mathf.Clamp(topPoint.localPosition.y, 3, 30), Mathf.Clamp(topPoint.localPosition.z, -2, 0));
                
                transform.localScale = Vector3.one;
                topPoint.localScale = Vector3.one;

                debugTop.localScale = new Vector3(width, 2, 1);
                debugTop.localPosition = new Vector3(0, 1, 0);

                debugBottom.localScale = new Vector3(width, 2, 1);
                debugBottom.localPosition = new Vector3(0, 1, 0);

                // genereally drawing debug stuff

                if (topPoint != null && trigger != null)
                {
                    //Vector3 dirFromBottomToTop = (bottomPoint.position - topPoint.position).normalized;
                    dirFromBottomToTop = (topPoint.position - transform.position).normalized;
                    //Debug.DrawLine(transform.position, transform.position + dirFromBottomToTop);
                    //Debug.Log(dirFromBottomToTop);

                    // set trigger size and rotate towards the top
                    triggerHeight = Vector3.Distance(topPoint.position, transform.position);
                    //Debug.Log(topPoint.position.y - bottomPoint.position.y);


                    debugTrigger.localScale = new Vector3(width, triggerHeight, 0.5f);

                    trigger.GetComponent<BoxCollider>().size = new Vector3(width, triggerHeight, 0.5f);
                    trigger.localPosition = Vector3.zero;

                    if (dirFromBottomToTop != Vector3.up) // if trigger is straight up, then we need to do some manual adjustments for some odd Quarternion action
                    {
                        trigger.GetComponent<BoxCollider>().center = new Vector3(0, triggerHeight / 2, 0.25f);
                        trigger.rotation = Quaternion.LookRotation(dirFromBottomToTop) * Quaternion.FromToRotation(Vector3.up, Vector3.forward);

                        debugTrigger.localPosition = new Vector3(0, triggerHeight / 2, 0.25f);
                    }
                    else
                    {
                        trigger.GetComponent<BoxCollider>().center = new Vector3(0, triggerHeight / 2, -0.25f);
                        trigger.rotation = new Quaternion(0, 0, 0, 0);

                        debugTrigger.localPosition = new Vector3(0, triggerHeight / 2, -0.25f);
                    }


                    // stop top point being moved 'sideways', z-axis/forward only i.e. away from the wall and clamping to relevant values
                    topPoint.localPosition = new Vector3(0, Mathf.Clamp(topPoint.localPosition.y, 3, 30), Mathf.Clamp(topPoint.localPosition.z, -2, 0));

                    /*
                    trigger.center = new Vector3(0, (topPoint.position.y + pointSize.y) / 2, 0);
                    trigger.size = new Vector3(pointSize.x, topPoint.position.y - bottomPoint.position.y, pointSize.z);
                    */
                }
            }


        }


        public Vector3 GetTopPosition()
        {
            return topPoint.position;
        }

        public Vector3 GetBottomPosition()
        {
            return transform.position;

        }

        private void OnDrawGizmos()
        {
            if (topPoint)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, topPoint.position);
            }
        }
    }
}