﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Ink.Runtime;

public static class InkUtils
{
    public static Dictionary<string, dynamic> GetVariablesDictionary(Story inkStory)
    {
        VariablesState variablesState = inkStory.variablesState;

        if (variablesState.Count() == 0)
        {
            //Debug.Log($"No variables found in this inkStory.");

            return new Dictionary<string, dynamic>();
        }
        else
        {
            //Debug.Log($"{variablesState.Count()} variables found.");

            return variablesState.ToDictionary(x => x, x => (dynamic)variablesState[x]);
        }
    }

    public static string[] GetAllKnots(Story inkStory)
    {
        Dictionary<string, Ink.Runtime.Object> namedContent = inkStory.mainContentContainer.namedOnlyContent;

        // remove the global one which we don't need to know about
        if(namedContent.ContainsKey("global decl"))
            namedContent.Remove("global decl");

            /*
            foreach (KeyValuePair<string, Ink.Runtime.Object> kv in namedContent)
            {
                Debug.Log($"GetAllKnots key:{kv.Key} Value:{kv.Value}");
            }*/

        return namedContent.Keys.ToArray();
    }




    public static List<string> GetAllVariableKeys(Story inkStory)
    {
        Dictionary<string, dynamic> dict = GetVariablesDictionary(inkStory);

        foreach (KeyValuePair<string, dynamic> kvp in dict)
        {
            Debug.Log($"key={kvp.Key}, value={kvp.Value}");
        }

        return dict.Keys.ToList();
    }


    public static bool DoesKnotExist(string[] allKnots, string knot)
    {
        for (int i = 0; i < allKnots.Length; i++)
        {
            if (allKnots[i] == knot)
            {
                Debug.Log($"DoesKnotExist {knot} found");
                return true;
            }
        }

        Debug.Log($"DoesKnotExist {knot} not found");
        return false;
    }





}
