using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;

public class LocalizationService: BaseService
{

    
    
    
    public enum LanguageCodes { EN, ES, DE, FR }

    [SerializeField] LanguageCodes preferedLanguage = LanguageCodes.EN;

    // fields
    Dictionary<string, Dictionary<string, string>> allLanguages = new Dictionary<string, Dictionary<string, string>>();
    private string parentLanguageFolder;
    ConfigService configService;

    // parameters
    public string[] AvailableLanguages
    {
        get { return allLanguages.Keys.ToArray(); }
    }

    public string CurrentLanguage { get; private set; }

    // events
    public event Action OnLanguagesLoaded;


    public override IEnumerator Initialize()
    {
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();
        
        // get references to services that we need
        configService = CoreManager.Instance.Get<ConfigService>();

        parentLanguageFolder = Path.Combine(configService.RootFolderPath, "data", "languages");
        DebugLog($"parentLanguageFolder={parentLanguageFolder}");

        yield return LoadLanguages();

        foreach (string key in allLanguages.Keys)
        {
            DebugLog($"key={key}");
        }

        DebugLog($"Raising OnLanguagesLoaded() event. AvailableLanguages={string.Join(",", AvailableLanguages)}");
        OnLanguagesLoaded?.Invoke();

        if (allLanguages.Count > 0)
        {
            // check for prefered language, if not, just use the first one
            if (AvailableLanguages.Contains(preferedLanguage.ToString()))
            {
                CurrentLanguage = preferedLanguage.ToString();
            }
            else
            {
                CurrentLanguage = AvailableLanguages[0];
                DebugLogWarning($"Preferred language {preferedLanguage} not found. Using the first in the list instead which is {CurrentLanguage}.");
            }
        }





        yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1f));
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }

    IEnumerator LoadLanguages()
    {
        if (!Directory.Exists(parentLanguageFolder))
        {
            DebugLogWarning($"Language folder not found. {parentLanguageFolder}");
            yield break;
        }

        string[] filePaths = Directory.GetFiles(parentLanguageFolder, "*.json", SearchOption.AllDirectories);

        DebugLog($"{filePaths.Length} JSON files found in the Languages folder.");

        string json;
        Dictionary<string, string> languageDict;

        for (int i = 0; i < filePaths.Length; i++)
        {
            DebugLog($"Processing {filePaths[i]}...");

            try
            {
                json = File.ReadAllText(filePaths[i]);
            }
            catch (Exception e)
            {
                DebugLogWarning($"Failed to read json from disk at {filePaths[i]}. {e.Message}");
                continue;
            }

            try
            {
                languageDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            }
            catch (Exception e)
            {
                DebugLogWarning($"Failed to deserialize to json. {e.Message}");
                continue;
            }

            DebugLog($"json={json}");

            foreach (KeyValuePair<string, string> item in languageDict)
            {
                DebugLog($"item={item}");
            }

            // add to big dictionary
            allLanguages.Add(Path.GetFileNameWithoutExtension(filePaths[i]).ToUpper(), languageDict);

            yield return null;
        }
    }
}
