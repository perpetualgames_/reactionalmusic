using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class ConfigService : BaseService
{
    // unity references

    [SerializeField] string configFileName = "config.json";

    // fields

    string _filePath;
    Dictionary<string, dynamic> config = new Dictionary<string, dynamic>();
    
    public string RootFolderPath { get; private set; }

    // actions

    public event Action OnConfigLoaded;
    public event Action<string, dynamic> OnConfigChanged;
    public event Action OnConfigSaved;


    public override IEnumerator Initialize()
    {
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();

        SetupFolderAndFilePaths();
        


        LoadConfigFromDisk();

        yield return null;
        

        yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1f));
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }

    public void SetupFolderAndFilePaths()
    {
        // set config folder and path from the filename
        RootFolderPath = Directory.GetParent(Application.dataPath).ToString();
        _filePath = Path.Combine(RootFolderPath, configFileName);

        DebugLog($"RootFolderPath={RootFolderPath}, _filePath={_filePath}");
    }


    [ContextMenu("TestActionFire")]
    void TestActionFire()
    {
        DebugLog($"OnConfigLoaded Fired");
        OnConfigLoaded?.Invoke();

        DebugLog($"OnConfigChanged(something, whatever) Fired");
        OnConfigChanged?.Invoke("something", "whatever");
    }

    /// <summary>
    /// Loads the json on disk into the config dictionary
    /// </summary>
    public void LoadConfigFromDisk()
    {
        DebugLog($"Loading config from disk - {_filePath}");

        string json = "";

        if (!File.Exists(_filePath))
        {
            DebugLogWarning($"Config file doesn't exist {_filePath}");
            return;
        }

        try
        {
            json = File.ReadAllText(_filePath);
        }
        catch (System.Exception)
        {
            DebugLogWarning($"Failed to read json from disk at {_filePath}");
            return;
        }


        try
        {
            config = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);
        }
        catch (Exception e)
        {
            DebugLogWarning($"Failed to deserialize json to config. {e.Message}");
            return;
        }

        DebugLog($"OnConfigLoaded Fired");
        OnConfigLoaded?.Invoke();

        DebugLog($"json={json}");

        /* no need for debug spam right now
        foreach (KeyValuePair<string, dynamic> item in config)
        {
            Debug.Log($"{item.Key} : {item.Value}");
        }
        */
    }

    /// <summary>
    /// Gets a value from the config dictionary via it's key
    /// </summary>
    /// <param name="key">Config key to return the value of</param>
    /// <returns>Value of the key in the config dictionary</returns>
    public dynamic GetValue(string key)
    {
        if (string.IsNullOrEmpty(key))
        {
            DebugLogWarning($"GetValue(): Key is blank.");
            return null;
        }

        if (!config.ContainsKey(key))
        {
            DebugLogWarning($"GetValue(): Key '{key}' not found.");
            return null;
        }

        return config[key];
    }

    
    /// <summary>
    /// Sets a value in the config dictionary via it's key
    /// </summary>
    public void SetValue(string key, dynamic value)
    {
        if (string.IsNullOrEmpty(key))
        {
            DebugLogWarning($"SetValue(): Key is empty.");
            return;
        }

        if (!config.ContainsKey(key))
        {
            DebugLogWarning($"SetValue(): Key '{key}' not found. Will add a new entry.");
            config.Add(key, value);
        }
        else
        {
            config[key] = value;
        }

        DebugLog($" Raising OnConfigChanged({key},{value}) event.");
        OnConfigChanged?.Invoke(key, value);
    }
}
