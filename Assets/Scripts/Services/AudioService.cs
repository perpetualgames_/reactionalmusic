using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Audio;
using System.Diagnostics;

public class AudioService : BaseService
{
    [SerializeField] AudioMixer mixer;
    [SerializeField] AudioClip[] voiceovers;
    [SerializeField] AudioClip[] music;
    
    ConfigService configService;
    
    string volMasterKey = "vol-master";
    string volMusicKey = "vol-music";
    string volSfxKey = "vol-sfx";

    Dictionary<string, AudioClip> Voiceovers;
    Dictionary<string, AudioClip> Music;

    public override IEnumerator Initialize()
    {   
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();
        
        // get references to services that we need
        configService = CoreManager.Instance.Get<ConfigService>();
        
        // build dictionaries for unity editor arrays
        Voiceovers = voiceovers.ToDictionary(x => x.name);
        Music = music.ToDictionary(x => x.name);
        
        // listen for events
        configService.OnConfigChanged += ConfigService_OnConfigChanged;
        
        // set initial mixer levels from config values
        ApplyAudioInitial();

        // test some dictionary stuff
        DebugLog($"Voiceovers={string.Join(",", Voiceovers.Keys)}");
        DebugLog($"Music={string.Join(",", Music.Keys)}");
        
        
        yield return null;
        //float rand = Random.Range(0.3f, 1f);
        //yield return new WaitForSeconds(rand);
        
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }

    void ConfigService_OnConfigChanged(string key, dynamic newValue)
    {
        // only pay attention if our configKey we are listening out for is heard
        if (key == volMasterKey)
        {
            DebugLog($"ConfigService_OnConfigChanged({key}, {newValue}) event. Handling it...");
            SetAudioLevel(volMasterKey, (float)newValue);
        }
        else if (key == volMusicKey)
        {
            DebugLog($"ConfigService_OnConfigChanged({key}, {newValue}) event. Handling it...");
            SetAudioLevel(volMusicKey, (float)newValue);
        }
        else if (key == volSfxKey)
        {
            DebugLog($"ConfigService_OnConfigChanged({key}, {newValue}) event. Handling it...");
            SetAudioLevel(volSfxKey, (float)newValue);
        }
        else
        {
            DebugLog($"ConfigService_OnConfigChanged({key}, {newValue}) event. Ignoring as it's nothing to do with us.");
        }
    }
    
    void ApplyAudioInitial()
    {
        DebugLog($"ApplyAudioInitial()");

        // values in config between 0-100
        dynamic volMasterValueFromSettingKey = configService.GetValue(volMasterKey);
        dynamic volMusicValueFromSettingKey = configService.GetValue(volMusicKey);
        dynamic volSfxValueFromSettingKey = configService.GetValue(volSfxKey);

        SetAudioLevel(volMasterKey, (float)volMasterValueFromSettingKey);
        SetAudioLevel(volMusicKey, (float)volMusicValueFromSettingKey);
        SetAudioLevel(volSfxKey, (float)volSfxValueFromSettingKey);
    }
    
    void SetAudioLevel(string configKey, float value)
    {
        DebugLog($"SetAudioLevel({configKey}, {value}).");
            
        // value conversion from 0-100 (config/slider value) to Audio Mixer readable (0.0001 - 1) --> (-80db - 20db)
        float decibelValue = Mathf.Log10(Mathf.Clamp(value / 100, 0.0001f, 1)) * 20;
        DebugLog($"decibelValue {decibelValue}.");

        mixer.SetFloat(configKey, decibelValue);
    }
    
    void OnDestroy()
    {
        configService.OnConfigChanged -= ConfigService_OnConfigChanged;
    }
}
