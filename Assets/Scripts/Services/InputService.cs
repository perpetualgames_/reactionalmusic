using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class InputService : BaseService
{
    MacedonInputs macedonInputs;

    public MacedonInputs Inputs
    {
        get { return macedonInputs; }
    }

    // event actions

    public event Action OnGameControlsActive;
    public event Action OnMenuControlsActive;


    public override IEnumerator Initialize()
    {
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();

        macedonInputs = new MacedonInputs();

        EnableGameplayControls();

        yield return null;
        

        yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1f));
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }

    void EnableGameplayControls()
    {
        DebugLog("Swap to Gameplay Controls");

        DebugLog($"OnGameControlsActive Fired");
        OnGameControlsActive?.Invoke();

        macedonInputs.MenuControls.Disable();
        macedonInputs.GameControls.Enable();
    }

    void EnableMenuControls()
    {
        DebugLog("Swap to Menu Controls");

        DebugLog($"OnMenuControlsActive Fired");
        OnMenuControlsActive?.Invoke();

        macedonInputs.MenuControls.Enable();
        macedonInputs.GameControls.Disable();
    }
}
