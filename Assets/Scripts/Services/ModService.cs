using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System.IO;

public class ModService : BaseService
{
    public enum ModSearchMode { None, Zips, Folders, Both }

    [SerializeField] ModSearchMode modSearchMode;
    [SerializeField] string modsFolder = "mods";
    
    //private string currentZipFilePath;
    string pathToZipFile;
    string _modsFolderPath;
    ConfigService configService;

    Dictionary<string, ModPackageZip> _modPackages = new Dictionary<string, ModPackageZip>();

    public override IEnumerator Initialize()
    {
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();
        
        // get references to services that we need
        configService = CoreManager.Instance.Get<ConfigService>();
        
        // set folder and path from the filename
        _modsFolderPath = System.IO.Path.Combine(configService.RootFolderPath, modsFolder);
        
        DebugLog($"modsFolderPath={_modsFolderPath}");

        yield return ProcessModFilesOnDisk();


        yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1f));
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }
    
    IEnumerator ProcessModFilesOnDisk()
    {

        if (!Directory.Exists(_modsFolderPath))
        {
            DebugLogWarning("Mods folder not found.");
            yield break;
        }

        switch (modSearchMode)
        {
            case ModSearchMode.None:
                break;
            case ModSearchMode.Zips:
                yield return LookForModZips();
                break;
            case ModSearchMode.Folders:
                yield return LookForModFolders();
                break;
            case ModSearchMode.Both:
                yield return LookForModZips();
                yield return LookForModFolders();
                break;
        }
        
        
    }

    IEnumerator LookForModZips()
    {
        string[] zipPaths = Directory.GetFiles(_modsFolderPath, "*.zip", SearchOption.TopDirectoryOnly);

        DebugLog($"{zipPaths.Length} zip files found in {_modsFolderPath}.");

        foreach (string path in zipPaths)
        {
            ModPackageZip mp = new ModPackageZip(path);
            yield return StartCoroutine(mp.Init());
            
            if(mp.IsValid)
                _modPackages.Add(mp.ModInfo.Name, mp);

            yield return null;
        }
        
        DebugLog($"_modPackages={string.Join(",", _modPackages.Keys)}");
    }

    IEnumerator LookForModFolders()
    {
        string[] folderPaths = Directory.GetDirectories(_modsFolderPath, "*", SearchOption.TopDirectoryOnly);
        
        DebugLog($"{folderPaths.Length} folders found in {_modsFolderPath}.");
        
        foreach (string path in folderPaths)
        {
            yield return null;
        }
    }
}
