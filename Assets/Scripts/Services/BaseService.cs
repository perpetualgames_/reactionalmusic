using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using NaughtyAttributes;
using UnityEngine;

public abstract class BaseService : MonoBehaviour
{
    // unity references
    
    //[Header("Debug")]
    [Foldout("Debug")] [SerializeField] bool debugLogging = true;
    [Foldout("Debug")] [SerializeField] string debugPrefix;

    public string Status { get; protected set; }
    
    public abstract IEnumerator Initialize();

    protected void DebugLog(object message)
    {
        if(debugLogging)
            Debug.Log($"{debugPrefix}: {message}");
    }
    protected void DebugLogWarning(object message)
    {
        if (debugLogging)
            Debug.LogWarning($"{debugPrefix}: {message}");
    }

    void Reset()
    {
        // set debugPrefix when the component is added in editor
        if (string.IsNullOrEmpty(debugPrefix))
            debugPrefix = GetType().Name.ToUpper();
    }

}
