﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;

/// <summary>
/// Simple service locator for <see cref="BaseService"/> instances.
/// </summary>
public class CoreManager : MonoBehaviour
{
    [SerializeField] GameObject canvas;
    [SerializeField] TMPro.TextMeshProUGUI serviceText;
    [SerializeField] TMPro.TextMeshProUGUI versionText;
    [SerializeField] TMPro.TextMeshProUGUI statusText;
    [SerializeField] TextAsset buildInfoTextAsset;

    [SerializeField] List<BaseService> allServices;
    
    readonly HashSet<IUpdate> _updateBucket = new HashSet<IUpdate>();
    readonly HashSet<ILateUpdate> _lateUpdateBucket = new HashSet<ILateUpdate>();
    
    bool isInitializing = false;

    public static event Action OnLoading;
    public static event Action OnLoaded;

    /// <summary>
    /// currently registered services.
    /// </summary>
    readonly Dictionary<string, BaseService> _services = new Dictionary<string, BaseService>();

    BaseService currentService;

    /// <summary>
    /// Gets the currently active service locator instance.
    /// </summary>
    public static CoreManager Instance { get; private set; }

    
    void OnValidate()
    {
        Assert.IsNotNull(canvas, "canvas not assigned in inspector?");
        Assert.IsNotNull(serviceText, "serviceText not assigned in inspector?");
        Assert.IsNotNull(versionText, "versionText not assigned in inspector?");
        Assert.IsNotNull(buildInfoTextAsset, "buildInfoTextAsset not assigned in inspector?");
        Assert.IsNotNull(statusText, "statusText not assigned in inspector?");
    }
    
    void Awake()
    {
        Instance = this;
        //DontDestroyOnLoad(gameObject);
    }
    
    void Start()
    {
        Debug.Log($"ServiceLoader: Start()");

        // turn on UI
        canvas.SetActive(true);
        
        // display buildinfo
        DisplayBuildInfo();

        // start the coroutine to go through each service at a time
        StartCoroutine(InitializeAllServices());
    }


    public void RegisterForUpdate(IUpdate updateBehaviour)
    {
        if (_updateBucket.Add(updateBehaviour))
        {
            Debug.Log($"{updateBehaviour} has registered for Update");
        }
    }
    
    public void RegisterForLateUpdate(ILateUpdate lateUpdateBehaviour)
    {
        if (_lateUpdateBucket.Add(lateUpdateBehaviour))
        {
            Debug.Log($"{lateUpdateBehaviour} has registered for LateUpdate");
        }
        
    }
    
    public void UnregisterUpdate(IUpdate updateBehaviour)
    {
        _updateBucket.Remove(updateBehaviour);
    }
    
    public void UnregisterLateUpdate(ILateUpdate lateUpdateBehaviour)
    {
        _lateUpdateBucket.Remove(lateUpdateBehaviour);
    }
    
    void Update()
    {
        if (isInitializing)
        {
            if(currentService != null)
                statusText.text = currentService.Status;
            else
                statusText.text = "";
        }
        
        foreach (IUpdate updateBehaviour in _updateBucket)
        {
            updateBehaviour.OnUpdate();
        }
    }

    void LateUpdate()
    {
        foreach (ILateUpdate updateBehaviour in _lateUpdateBucket)
        {
            updateBehaviour.OnLateUpdate();
        }
    }

    IEnumerator InitializeAllServices()
    {
        Debug.Log($"CORE: OnLoading Fired");
        OnLoading?.Invoke();

        Stopwatch sw = new Stopwatch();
        sw.Start();
        
        isInitializing = true;
        
        // initialize the monobehaviour services and wait for each to finish before registering it and moving on
        foreach (BaseService item in allServices)
        {
            currentService = item;
            serviceText.text = $"Initializing {item.GetType().Name}...";
            yield return StartCoroutine(item.Initialize());
            RegisterService(item);
        }

        currentService = null;
        isInitializing = false;
        statusText.text = "";
        yield return null;
        
        sw.Stop();

        Debug.Log($"CORE: InitializeAllServices completed in {sw.ElapsedMilliseconds:N}ms");
        
        serviceText.text = $"Initialization Complete";
        yield return new WaitForSeconds(2f);

        Debug.Log($"CORE: OnLoaded Fired");
        OnLoaded?.Invoke();

        // turn off UI
        canvas.SetActive(false);
    }

    void DisplayBuildInfo()
    {
        if (buildInfoTextAsset == null)
        {
            versionText.text = "Can't read from BuildInfo";
            return;
        }

        BuildInfo buildInfo = new BuildInfo();

        try
        {
            buildInfo = JsonConvert.DeserializeObject<BuildInfo>(buildInfoTextAsset.text);
        }
        catch (Exception e)
        {
            Debug.LogWarning($"Failed to deserialize to json. {e.Message}");
        }

        DateTime buildDate = DateTime.ParseExact(buildInfo.buildDate, "yyyyMMdd_HHmm", CultureInfo.InvariantCulture);
        Debug.Log($"buildInfo={buildInfo})");

        StringBuilder sb = new StringBuilder();

        sb.AppendLine($"Elegos {Application.version} (build {buildInfo.buildNumber})");
        sb.AppendLine($"Unity Editor: {Application.unityVersion}");

        versionText.text = sb.ToString();
    }


    /// <summary>
    /// Gets the service instance of the given type.
    /// </summary>
    /// <typeparam name="T">The type of the service to lookup.</typeparam>
    /// <returns>The service instance.</returns>
    public T Get<T>() where T : BaseService
    {
        string key = typeof(T).Name;
        if (!_services.ContainsKey(key))
        {
            Debug.LogError($"{key} not registered with {GetType().Name}");
            throw new InvalidOperationException();
        }

        return (T)_services[key];
    }

    /// <summary>
    /// Registers the service with the current service locator.
    /// </summary>
    /// <typeparam name="T">Service type.</typeparam>
    /// <param name="service">Service instance.</param>
    public void RegisterService<T>(T service) where T : BaseService
    {
        //string key = typeof(T).Name;
        string key = service.GetType().Name;
        if (_services.ContainsKey(key))
        {
            Debug.LogError($"Attempted to register service of type {key} which is already registered with the {GetType().Name}.");
            return;
        }

        _services.Add(key, service);
    }

    /// <summary>
    /// Unregisters the service from the current service locator.
    /// </summary>
    /// <typeparam name="T">Service type.</typeparam>
    public void UnregisterService<T>() where T : BaseService
    {
        string key = typeof(T).Name;
        //string key = service.GetType().Name;
        if (!_services.ContainsKey(key))
        {
            Debug.LogError($"Attempted to unregister service of type {key} which is not registered with the {GetType().Name}.");
            return;
        }

        _services.Remove(key);
    }
}
