using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class BlackboardService : BaseService
{
    // unity references

    [SerializeField] string filename = "blackboard.json";

    // services
    
    ConfigService configService;
    
    
    string _filePath;
    Dictionary<string, dynamic> _blackboard = new Dictionary<string, dynamic>();
 


    public static event Action OnBlackboardLoaded;
    public static event Action<string, dynamic> OnBlackboardChanged;
    public static event Action OnBlackboardSaved;
    
    
    public override IEnumerator Initialize()
    {
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();

        // get references to services that we need
        configService = CoreManager.Instance.Get<ConfigService>();
        
        // set file path from the filename
        _filePath = Path.Combine(configService.RootFolderPath, filename);
        
        LoadBlackboardFromDisk();
        
        yield return null;




        yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1f));
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }
    
    
    
    
    /// <summary>
    /// Loads the json on disk into the config dictionary
    /// </summary>
    public void LoadBlackboardFromDisk()
    {
        DebugLog($"Loading blackboard from disk - {_filePath}");

        string json = "";

        if (!File.Exists(_filePath))
        {
            DebugLogWarning($"Blackboard file doesn't exist {_filePath}");
            return;
        }

        try
        {
            json = File.ReadAllText(_filePath);
        }
        catch (System.Exception)
        {
            DebugLogWarning($"Failed to read json from disk at {_filePath}");
            return;
        }


        try
        {
            // have to go via a temp dictionary so we can add the string comparer in a constructor
            Dictionary<string, dynamic> tempDict = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);
            _blackboard = new Dictionary<string, dynamic>(tempDict, StringComparer.InvariantCultureIgnoreCase);
        }
        catch (Exception e)
        {
            DebugLogWarning($"Failed to deserialize json to config. {e.Message}");
            return;
        }
        
        DebugLog($"OnBlackboardLoaded Fired");
        OnBlackboardLoaded?.Invoke();

        DebugLog($"json={json}");

        /* no need for debug spam right now
        foreach (KeyValuePair<string, dynamic> item in config)
        {
            Debug.Log($"{item.Key} : {item.Value}");
        }
        */
    }
    
    
    /// <summary>
    /// Gets a value from the dictionary via it's key
    /// </summary>
    /// <param name="key">Key to return the value of</param>
    /// <returns>Value of the key in the dictionary</returns>
    public dynamic GetValue(string key)
    {
        if (string.IsNullOrEmpty(key) )
        {
            DebugLogWarning($"GetValue(): Key is blank.");
            return null;
        }

        if (!_blackboard.ContainsKey(key))
        {
            DebugLogWarning($"GetValue(): Key '{key}' not found.");
            return null;
        }

        return _blackboard[key];
    }

    /// <summary>
    /// Sets a value in the dictionary via it's key (case insensitive)
    /// </summary>
    public void SetValue(string key, dynamic value)
    {
        if (string.IsNullOrEmpty(key))
        {
            DebugLogWarning($"SetValue(): Key is empty.");
            return;
        }

        if (!_blackboard.ContainsKey(key))
        {
            DebugLogWarning($"SetValue(): Key '{key}' not found. Will add a new entry.");
            _blackboard.Add(key, value);
        } 
        else
        {
            _blackboard[key] = value;
        }

        DebugLog($"Raising OnBlackboardChanged({key},{value}) event.");
        OnBlackboardChanged?.Invoke(key, value);
    }

}
