using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Ink;
using Ink.Runtime;
using UnityEngine;
using UnityEngine.InputSystem;

//using Path = Ink.Runtime.Path;

public class DialogueService : BaseService
{
    [SerializeField] TextAsset[] compiledInkFiles;
    [SerializeField] string externalInkFolder = "data";
    
    ConfigService configService;
    InputService inputService;

    
    Dictionary<string, ElegosInkStory> inkFilesDict = new Dictionary<string, ElegosInkStory>();
    ElegosInkStory _currentElegosInkStory;

    public override IEnumerator Initialize()
    {
        DebugLog($"Initialize()");
        Stopwatch sw = new Stopwatch();
        sw.Start();
        
        // get references to services that we need
        configService = CoreManager.Instance.Get<ConfigService>();
        inputService = CoreManager.Instance.Get<InputService>();

        // input stuff
        inputService.Inputs.GameControls.DialogResponse1.performed += DialogResponse1_OnPerformed;
        inputService.Inputs.GameControls.DialogResponse2.performed += DialogResponse2_OnPerformed;
        inputService.Inputs.GameControls.DialogResponse3.performed += DialogResponse3_OnPerformed;
        inputService.Inputs.GameControls.DialogResponse4.performed += DialogResponse4_OnPerformed;
        
        yield return ProcessCompiledInkFiles();
        yield return ProcessRawInkFilesOnDisk();

        DebugLog($"inkFilesDict={string.Join(",", inkFilesDict)}");

        yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1f));
        sw.Stop();
        DebugLog($"Initialization complete in {sw.Elapsed.TotalSeconds:0.00} seconds");
    }


    void DialogResponse1_OnPerformed(InputAction.CallbackContext obj)
    {
        if(_currentElegosInkStory.IsWaitingForChoice)
            _currentElegosInkStory.MakeChoice(1);
    }
    void DialogResponse2_OnPerformed(InputAction.CallbackContext obj)
    {        
        if(_currentElegosInkStory.IsWaitingForChoice)
            _currentElegosInkStory.MakeChoice(2);
    }
    void DialogResponse3_OnPerformed(InputAction.CallbackContext obj)
    {        
        if(_currentElegosInkStory.IsWaitingForChoice)
            _currentElegosInkStory.MakeChoice(3);
    }
    void DialogResponse4_OnPerformed(InputAction.CallbackContext obj)
    {        
        if(_currentElegosInkStory.IsWaitingForChoice)
            _currentElegosInkStory.MakeChoice(4);
    }

    IEnumerator ProcessRawInkFilesOnDisk()
    {
        // set folder and path from the filename
        string dialogueFolder = System.IO.Path.Combine(configService.RootFolderPath, externalInkFolder);
        //DebugLog($"parentLanguageFolder={parentLanguageFolder}");

        if (!Directory.Exists(dialogueFolder))
        {
            DebugLogWarning($"Data folder doesn't exist at {dialogueFolder}");
            yield break;
        }
            
        
        string[] filePaths = Directory.GetFiles(dialogueFolder, "*.ink", SearchOption.AllDirectories);

        DebugLog($"{filePaths.Length} Ink files found in the {dialogueFolder}.");

        string rawInkFile;
        
        foreach (string filepath in filePaths)
        {
            try
            {
                rawInkFile = File.ReadAllText(filepath);
            }
            catch (Exception e)
            {
                DebugLogWarning($"Failed to read ink file from disk at {filepath}. {e.Message}");
                continue;
            }

            Status = filepath;

            // inkFileContents: linked TextAsset, or Resources.Load, or even StreamingAssets
            Compiler compiler = new Ink.Compiler(rawInkFile);
            
            ElegosInkStory elegosInkStory = new ElegosInkStory(System.IO.Path.GetFileNameWithoutExtension(filepath), compiler);  
            yield return StartCoroutine(elegosInkStory.Init());
            inkFilesDict.Add(elegosInkStory.Name, elegosInkStory);

            yield return null;
        }

    }


    /// <summary>
    /// Loop through each compiled ink file that has been set in the inspector
    /// </summary>
    /// <returns></returns>
    IEnumerator ProcessCompiledInkFiles()
    {
        DebugLog($"ProcessCompiledInkFiles()");
        
        foreach (TextAsset inkFile in compiledInkFiles)
        {
            DebugLog($"Processing inkFile={inkFile.name}");

            Status = inkFile.name;

            ElegosInkStory elegosInkStory = new ElegosInkStory(inkFile.name,inkFile.text);  
            yield return StartCoroutine(elegosInkStory.Init());
            inkFilesDict.Add(inkFile.name, elegosInkStory);
        }
        
        DebugLog($"End of ProcessCompiledInkFiles()");
    }

    [ContextMenu("TestInterrupt")]
    void TestInterrupt()
    {
        _currentElegosInkStory.Interrupt();
    }

    [ContextMenu("TestDialogue")]
    void TestDialogue()
    {
        string storyName = "Scene4_YoungGuard";
        
        // is there already a story playing?
        if (_currentElegosInkStory != null)
        {
            DebugLogWarning($"Elegos Ink Story is already running. Stop all coroutines before starting new one and unsubscribe from any events.");
            StopAllCoroutines();    
            InkStory_OnFinished();
        }
        
        // check to see if macedonInkStory exists in the dict and overwrite the currentMacedonInkStory object
        if(inkFilesDict.TryGetValue(storyName, out _currentElegosInkStory))
        {
            _currentElegosInkStory.Finished += InkStory_OnFinished;
            _currentElegosInkStory.InkDebug += InkStory_OnInkDebug;
            _currentElegosInkStory.InkTrigger += InkStory_OnInkTrigger;
            _currentElegosInkStory.WaitingForChoice += InkStory_OnWaitingForChoice;
            
            StartCoroutine(_currentElegosInkStory.Play());
        }
        else
        {
            DebugLogWarning($"Elegos Ink Story {storyName} not found.");
        }
    }

    void InkStory_OnWaitingForChoice(List<Choice> currentChoices)
    {
        DebugLog($"InkStory_OnWaiting()");
        
        //StartCoroutine(ChoiceCoroutine());
    }

    void InkStory_OnFinished()
    {
        _currentElegosInkStory.Finished -= InkStory_OnFinished;
        _currentElegosInkStory.InkDebug -= InkStory_OnInkDebug;
        _currentElegosInkStory.InkTrigger -= InkStory_OnInkTrigger;
        _currentElegosInkStory.WaitingForChoice -= InkStory_OnWaitingForChoice;

        _currentElegosInkStory = null;
        
        DebugLog($"currentMacedonInkStory.Finished");
    }

    void InkStory_OnInkTrigger(string obj)
    {
        DebugLog($"InkStory_OnInkTrigger({obj})");
        
        StartCoroutine(AutoUnpauseCoroutine());
    }

    void InkStory_OnInkDebug(string obj)
    {
        DebugLog($"InkStory_OnInkDebug({obj})");
    }

    IEnumerator AutoUnpauseCoroutine()
    {
        yield return new WaitForSeconds(3f);
        _currentElegosInkStory.Unpause();
    }
    
    IEnumerator AutoMakeChoiceCoroutine()
    {
        yield return new WaitForSeconds(3f);
        _currentElegosInkStory.MakeChoice(1);
    }
}
