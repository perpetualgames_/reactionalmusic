﻿using UnityEngine;

public class Spinner : MonoBehaviour
{
    RectTransform rect;
    public float rotateSpeed = 200f;

    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    private void Update()
    {
        rect.Rotate(0f, 0f, -rotateSpeed * Time.deltaTime);
    }
}