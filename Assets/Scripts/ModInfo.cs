﻿using System;
using System.Collections.Generic;
using Ink.Runtime;
using Newtonsoft.Json;

public class ModInfo
    {
        public string Name { get; set; }
        public string FriendlyName { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string Version { get; set; }
        public string Link { get; set; }
        public string SteamContentId { get; set; }        
        public string Image { get; set; }
        public string Tags { get; set; }
        public string[] Includes { get; set; }
        public string[] Requires { get; set; }
        public int Priority { get; set; }
        
        [JsonIgnore] public string Path { get; set; }

        [JsonIgnore] public string ZipPath { get; set; }

        [JsonIgnore] public string LoadedFolder { get; set; }
        
        public override string ToString()
        {
            return $"{GetType().FullName} Name={Name}, Author={Author}, Path={Path}, ZipPath={ZipPath}, LoadedFolder={LoadedFolder}";
        } 
        
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
