﻿using Macedon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLadderClimb : ElegosBehaviour, IUpdate
{
    private PlayerCharacterController m_PlayerCharacterController;
    CharacterController m_Controller;
    //PlayerInputHandler m_InputHandler;
    PlayerInputHandlerNew m_InputHandlerNew;

    
    [SerializeField] bool isTriggeringLadder = false;
    [SerializeField] private bool leftTheGround;
    //[SerializeField] private bool isClimbingLadder;

    //Transform ladderTransform;
    private Vector3 playerVelocity;
    [SerializeField] private float scaledVerticalCameraAngle;
    [SerializeField] private float verticalVelocity;
    private float horizontalVelocity;
    [SerializeField] private float horizontalSpeed = 10f;
    [SerializeField] private float verticalSpeed = 10f;
    [SerializeField] private float distanceToTop;
    [SerializeField] float distanceToTopVerticalOnly;
    [SerializeField] private float distanceToBottom;
    [SerializeField] float distanceToBottomVerticalOnly;
    private Vector3 dirFromPlayerToTop;
    private Vector3 dirFromPlayerToBottom;
    [SerializeField] bool isClimbingLadder;
    [SerializeField] Ladder currentLadder;

    Vector3 attachmentPosition;
    [SerializeField] private float verticalCameraAngle;
    [SerializeField] float horizonDegreesOffCenter = -10f;


    public override void Initialize()
    {
        m_PlayerCharacterController = GetComponent<PlayerCharacterController>();
        //m_InputHandler = GetComponent<PlayerInputHandler>();
        m_Controller = GetComponent<CharacterController>();
        m_InputHandlerNew = GetComponent<PlayerInputHandlerNew>();
            
        CoreManager.Instance.RegisterForUpdate(this);
    }


    float Scale(float value, float min, float max, float minScale, float maxScale)
    {
        float scaled = minScale + (value - min) / (max - min) * (maxScale - minScale);
        return scaled;
    }

    // Update is called once per frame
    public void OnUpdate()
    {        
        if (!isTriggeringLadder) // don't run the update if a ladder isn't being triggered
            return;


        if (leftTheGround && m_InputHandlerNew.GetJumpInputDown()) // if we jumped, then just end the climb and push away from ladder
        {
            Debug.Log("LADDER: Jumped while on ladder");

            // put a bit of force backwards?
            m_PlayerCharacterController.characterVelocity = currentLadder.transform.forward * 10f;

            EndLadderClimbing();
            return;
        }


        // player is definately triggering the ladder at this point so run the calculations

        distanceToTop = Vector3.Distance(transform.position, currentLadder.GetTopPosition());
        distanceToTopVerticalOnly = currentLadder.GetTopPosition().y - transform.position.y;

        distanceToBottom = Vector3.Distance(transform.position, currentLadder.GetBottomPosition());
        distanceToBottomVerticalOnly = transform.position.y - currentLadder.GetBottomPosition().y;

        dirFromPlayerToTop = (currentLadder.GetTopPosition() - transform.position).normalized;
        dirFromPlayerToBottom = (transform.position - currentLadder.GetBottomPosition()).normalized;

        verticalCameraAngle = m_PlayerCharacterController.GetCameraVerticalAngle() * -1; // is going to be between -89 and 89 

        // +89 is straight up / move up
        // -89 is straight down / move down

        if (verticalCameraAngle > horizonDegreesOffCenter) // if greater than x degrees from horizon level, then we go up.
        {
            verticalVelocity = m_InputHandlerNew.GetMoveInput().z * verticalSpeed;
        }
        else if (verticalCameraAngle < horizonDegreesOffCenter) // if less than x degrees from horizon, then we go down.
        {
            verticalVelocity = m_InputHandlerNew.GetMoveInput().z * verticalSpeed * -1; // * -1 to reverse the input so that movement does the opposite of where you are lookingup
        }
        else
        {
            verticalVelocity = 0;
        }



        if (!leftTheGround && distanceToBottomVerticalOnly > 0.5f) // are we off the ground yet?
        {
            // we've definately left the ground at least once
            leftTheGround = true;
            isClimbingLadder = true;
            Debug.Log("LADDER: Left the ground");

            StartLaddingClimbing();
        }


        if (leftTheGround) // all movement is as normal
        {            
            if (verticalVelocity > 0) // need to move vertically up the ladder
            {
                m_PlayerCharacterController.characterVelocity = dirFromPlayerToTop * verticalVelocity; // move towards top point of ladder
            }
            else if (verticalVelocity < 0) // need to move vertically down the ladder
            {                
                m_PlayerCharacterController.characterVelocity = dirFromPlayerToBottom * verticalVelocity; // move towards bottom point of ladder
            }
            else
            {
                m_PlayerCharacterController.characterVelocity = Vector3.zero;
            }
        }
        else // not off the ground yet, so probably just on the ground but triggering. at this point, we just want to be able to move up
        {
            //Debug.Log("LADDER: Not off the ground yet but we are triggering");

            // need to move vertically up the ladder, and only if pushing forward
            if (!isClimbingLadder && verticalCameraAngle > horizonDegreesOffCenter && verticalVelocity > 0) 
            {
                StartLaddingClimbing();
                isClimbingLadder = true;
                //leftTheGround = true;

                Debug.Log($"LADDER: isClimbing={isClimbingLadder}. verticalCameraAngle={verticalCameraAngle} verticalVelocity={verticalVelocity} m_InputHandlerNew.GetMoveInput()={m_InputHandlerNew.GetMoveInput()}");
                m_PlayerCharacterController.characterVelocity = dirFromPlayerToTop * verticalVelocity * 3.5f;
            } 
        }







        if (leftTheGround && distanceToBottomVerticalOnly < 0.35f)
        {
            Debug.Log("LADDER: Below 0.35f after we've left the ground. End ladder climbing.");
            EndLadderClimbing();
        }

        //Debug.Log(m_PlayerCharacterController.characterVelocity);




    }


    void EndLadderClimbing()
    {
        Debug.Log("Ending Ladder Climbing");

        leftTheGround = false;
        m_PlayerCharacterController.SetIsLadderClimbing(false);
        isClimbingLadder = false;

        currentLadder = null;
        isTriggeringLadder = false;
    }

    void StartLaddingClimbing()
    {
        Debug.Log("Starting Ladder Climbing");

        m_PlayerCharacterController.SetIsLadderClimbing(true);
        //isClimbingLadder = true;
    }


    private void OnTriggerEnter(Collider other)
    {        
        Debug.Log("triggered (" + other.tag + ") " + other.name);

        if (other.tag == "Ladder")
        {
            currentLadder = other.gameObject.GetComponentInParent<Ladder>();

            if (currentLadder == null)
                Debug.LogWarning("Can't find a Ladder component even though we've trigger the Ladder tag");

            attachmentPosition = transform.position;
            isClimbingLadder = false;
            isTriggeringLadder = true;

            //StartLaddingClimbing();
        }

    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("end trigger (" + other.tag + ") " + other.name);

        if (other.tag == "Ladder")
        {
            isTriggeringLadder = false;
            EndLadderClimbing();
        }
    }


}
