﻿
using Macedon;
using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering.HighDefinition;

public class PlayerCombat : ElegosBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] PlayerStats playerStats;
    
    bool isblocking;

    [SerializeField] Camera playerCamera;
    private Ray ray;
    [SerializeField] WeaponData currentWeapon;

    [SerializeField] CameraShake cameraShake;
    
    public WeaponData CurrentWeapon => currentWeapon;

    [SerializeField] CustomPassVolume customPassVolume;
    SlightBlur slightBlurCustomPass;

    InputService inputService;


    //public BehaviorTree behaviorTree;

    // stats

    [SerializeField] [Range(0, 1)] [Tooltip("Critical Hit chance of the player. 0%-100%")]
    float critChance = 0;

    [SerializeField] [Range(0, 1)] [Tooltip("Additonal damage that a critical hit adds to the damage. 0%-100%")]
    float critDamageBoost = 0;


    public float CritChance => critChance; 
    public float CritDamageBoost => critDamageBoost; 
    //

    [SerializeField] [ReadOnly] float finalCritChance;
    [SerializeField] [ReadOnly] float finalDamage;
    [SerializeField] [ReadOnly] float finalCritDamage;


    // Attack * (100 / (100 + Defense))  

    
    public override void Initialize()
    {
        Assert.IsNotNull(animator, $"Animator has not been set in the inspector.");
        Assert.IsNotNull(playerStats, $"PlayerStats has not been set in the inspector.");
        
        // what services do we need?
        inputService = CoreManager.Instance.Get<InputService>();

        inputService.Inputs.GameControls.Attack.performed += Attack_performed;
        inputService.Inputs.GameControls.Block.performed += Block_performed;
        inputService.Inputs.GameControls.Block.canceled += Block_canceled;
        
        /*foreach (var pass in customPassVolume.customPasses)
        {
            Debug.Log(pass);
            
            if (pass is SlightBlur sb)
                slightBlurCustomPass = sb;
        }*/
    }
    
    void OnDestroy()
    {
        inputService.Inputs.GameControls.Attack.performed -= Attack_performed;
        inputService.Inputs.GameControls.Block.performed -= Block_performed;
        inputService.Inputs.GameControls.Block.canceled -= Block_canceled;
    }


    void Block_canceled(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        isblocking = false;
        Debug.Log("End block");
        animator.SetBool("Block", isblocking);
    }

    void Block_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        isblocking = true;
        Debug.Log("Start block");
        animator.SetBool("Block", isblocking);
    }

    void Attack_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        // cancel animation for now as we don't have them
        //animator.SetTrigger("Attack");

        // skip straight to what the animation would be calling
        AnimationEvent_HitCheck();

        // Let's say here that a random boolean says if it hit left or right
        // 0 left, 1 right
        //int attackDirection = Mathf.RoundToInt(UnityEngine.Random.value);
        //GlobalVariables.Instance.SetVariable("attackDirection", (SharedInt)attackDirection);     
        
        
        
        int stingerRandom = UnityEngine.Random.Range(1, 4);
        Reactional.Request.Stinger(stingerRandom, 8);
    }



    public void AnimationEvent_HitCheck()
    {
        Debug.Log("AnimationEvent_HitCheck");

        ray = playerCamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        
        Debug.DrawRay(ray.origin, ray.direction * Mathf.Infinity, Color.red, 1f);

        if (Physics.SphereCast(ray, 0.25f, out RaycastHit hit))
        {
            Debug.Log(hit.distance + " to " + hit.collider.name);

            if (hit.distance > currentWeapon.Range)
            {
                Debug.LogWarning($"PLAYER COMBAT: Distance is too far away. hit.distance={hit.distance}, currentWeapon.Range={currentWeapon.Range}");
                return;
            }

            switch (hit.collider.gameObject.tag)
            {
                case "Actor":
                    HitActor(hit);
                    break;
                
                case "PhysicsObject":
                    HitPhysicsObject(hit);
                    break;
                
                default:
                    DrawDebugSphere(hit.point, 1f);
                    break;
            }
            
        }
        
    }

    void HitActor(RaycastHit hit)
    {
        if (!hit.collider.TryGetComponent(out ActorCombat actorCombat))
        {
            Debug.LogWarning($"ActorCombat component not found on {hit.collider.name}");
            return;
        }

        DrawDebugSphere(hit.point, 1f);

        float damage = CalculateAttackScore();
        Debug.Log($"ACTOR HIT damage={damage}");
        actorCombat.TakeDamage(damage);
    }

    void HitPhysicsObject(RaycastHit hit)
    {
        if (!hit.collider.TryGetComponent(out Rigidbody rb))
        {
            Debug.LogWarning($"Rigidbody component not found on {hit.collider.name}");
            return;
        }

        DrawDebugSphere(hit.point, 1f);

        rb.AddForce(transform.forward * 20f, ForceMode.Impulse);
    }


    void DrawDebugSphere(Vector3 position, float duration)
    {
        // draw a debug sphere in the game view and then remove a second later
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(go.GetComponent<Collider>());
        go.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
        go.transform.position = position;
        go.GetComponent<MeshRenderer>().material.color = Color.red;
        Destroy(go, duration);
    }

    private void PerformAttack(Enemy enemy)
    {
        CalculateAttackScore();

        float randValue = UnityEngine.Random.value;

        Debug.Log("Do Attack with " + finalCritChance.ToString("P1") + " chance of crit hit");

        if (randValue < finalCritChance)
        {
            // Do crit attack

            Debug.Log("CRIT ATTACK " + randValue);
            //enemy.TakeDamage(finalCritDamage);
        }
        else
        {
            // Do normal attack

            Debug.Log("ATTACK " + randValue);
            //enemy.TakeDamage(finalDamage);
        }

    }
/*
    void Update()
    {
        if (slightBlurCustomPass.enabled)
        {
            if (slightBlurCustomPass.radius < 0.3f)
            {
                slightBlurCustomPass.enabled = false;
            }
            else
            {
                slightBlurCustomPass.radius -= 0.3f;
            }
            
        }
    }*/

    public void TakeDamage(float damage)
    {
        float finalDamageReceived = CalculateDefenceScore(damage);

        float newHealth = playerStats.Health - finalDamageReceived;

        Debug.Log($"Taking {finalDamageReceived} damage after original damage of {damage} due to armor value of {playerStats.Armor}. Health is going to be {newHealth:N}/{playerStats.MaxHealth:N}");

        if (newHealth < 0)
        {
            Debug.Log("Player would be dead now.");
            //animator.SetTrigger("Die");
            //Destroy(gameObject);
        }

        playerStats.SetHealth(newHealth);

        //slightBlurCustomPass.radius = 16f;
        //slightBlurCustomPass.enabled = true;
        
        cameraShake.Shake(0.15f, 0.15f);
        
        //Debug.Log($"customPassVolume fadeRadius={customPassVolume.fadeRadius}");
    }    
    
    float CalculateDefenceScore(float damage)
    {
        float derivedStatusEffects = 0f; // not calculated yet

        float comparisonOfDamageAndDefence = 1 / (damage / (playerStats.Armor + 0.00001f)); // add a really small number in case of zero armor

        float damageReductionPercentage = (comparisonOfDamageAndDefence * playerStats.Armor) / (10 + comparisonOfDamageAndDefence * playerStats.Armor);

        return damage * (1 - damageReductionPercentage) * (1 + derivedStatusEffects);
    }

    float CalculateAttackScore()
    {        
        float derivedWeaponDamage = CurrentWeapon.Damage * (1 + (((int)CurrentWeapon.Rarity + 1) * 0.3f)); // extra +1 to rarity to account for zero indexed

        float derivedCritChance = CritChance + CurrentWeapon.CritChance; // adding player and weapon crit chance
        float derivedCritDamageBoost = CritDamageBoost + CurrentWeapon.CritDamageBoost; // adding player and weapon crit damage boost
        float derivedStatusEffects = 0.1f; // not calculated yet

        finalCritChance = derivedCritChance; // no further calculations needed yet
        finalDamage = derivedWeaponDamage * (1 + derivedStatusEffects); // weapon damage increased/decreased by derived status effects
        finalCritDamage = finalDamage * (1 + derivedCritDamageBoost); // final damage increased by derived crit damage
        return finalCritDamage;
        // Attack * (100 / (100 + Defense))  
    }

}
