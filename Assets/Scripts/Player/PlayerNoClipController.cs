using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class PlayerNoClipController : ElegosBehaviour, IUpdate
    {
        [Tooltip("Reference to the main camera used for the player")]
        public Camera playerCamera;

        [Header("Rotation")]
        [Tooltip("Rotation speed for moving the camera")]
        public float rotationSpeed = 1f;
        [Range(0.1f, 1f)]
        [Tooltip("Rotation speed multiplier when aiming")]
        public float aimingRotationMultiplier = 0.4f;

        [SerializeField] [ReadOnly] float m_CameraVerticalAngle = 0f;



        PlayerInputHandlerNew m_InputHandlerNew;

        
        public override void Initialize()
        {
            // fetch components on the same gameObject
            m_InputHandlerNew = GetComponent<PlayerInputHandlerNew>();
            
            CoreManager.Instance.RegisterForUpdate(this);
        }


        // Update is called once per frame
        public void OnUpdate()
        {
            HandleCharacterMovement();
        }

        void HandleCharacterMovement()
        {
            // horizontal character rotation

            // rotate the transform with the input speed around its local Y axis
            //transform.Rotate(new Vector3(0f, (m_InputHandlerNew.GetLookInputsHorizontal() * rotationSpeed * RotationMultiplier), 0f), Space.Self);
            transform.Rotate(new Vector3(0f, (m_InputHandlerNew.GetLookInputsHorizontal() * rotationSpeed), 0f), Space.Self);

            // vertical camera rotation

            // add vertical inputs to the camera's vertical angle
            //m_CameraVerticalAngle += m_InputHandlerNew.GetLookInputsVertical() * rotationSpeed * RotationMultiplier;
            m_CameraVerticalAngle += m_InputHandlerNew.GetLookInputsVertical() * rotationSpeed * -1f;

            //Debug.Log($"m_CameraVerticalAngle]{m_CameraVerticalAngle}");

            // limit the camera's vertical angle to min/max
            m_CameraVerticalAngle = Mathf.Clamp(m_CameraVerticalAngle, -89f, 89f);

            // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
            playerCamera.transform.localEulerAngles = new Vector3(m_CameraVerticalAngle, 0, 0);
            //playerCamera.transform.rotation.eulerAngles = new Vector3(m_CameraVerticalAngle, 0, 0);
            //playerCamera.transform.rotation = Quaternion.Euler(m_CameraVerticalAngle, 0, 0);
        }


    }
