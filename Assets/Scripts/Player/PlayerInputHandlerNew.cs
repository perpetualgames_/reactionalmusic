﻿using System;
using UnityEngine;

    public class PlayerInputHandlerNew : ElegosBehaviour
    {
        [Tooltip("Sensitivity multiplier for moving the camera around")]
        public float lookSensitivity = 1f;
        [Tooltip("Additional sensitivity multiplier for WebGL")]
        public float webglLookSensitivityMultiplier = 0.25f;
        [Tooltip("Limit to consider an input when using a trigger on a controller")]
        public float triggerAxisThreshold = 0.4f;
        [Tooltip("Used to flip the vertical input axis")]
        public bool invertYAxis = false;
        [Tooltip("Used to flip the horizontal input axis")]
        public bool invertXAxis = false;

        bool isMouseInput = false;

        //GameFlowManager m_GameFlowManager;
        PlayerCharacterController characterController;
        //PlayerNoClipController noClipController;

        bool m_FireInputWasHeld;

        InputService inputService;
        ConfigService configService;
        
        
        private Vector2 moveInput;
        private Vector2 lookInput;

        bool jumpPressed;
        private bool crouchPressed;
        private bool runPressed;

        bool crouchToggle;
        private bool isNoClip;

        public override void Initialize()
        {
            // what services do we need?
            inputService = CoreManager.Instance.Get<InputService>();
            configService = CoreManager.Instance.Get<ConfigService>();
            
            //Debug.Log($"PlayerInputHandlerNew inputManager={inputService}");
            
            characterController = GetComponent<PlayerCharacterController>();
            //noClipController = GetComponent<PlayerNoClipController>();

            inputService.Inputs.GameControls.Move.performed += Move_performed;

            inputService.Inputs.GameControls.Look.performed += Look_performed;
            inputService.Inputs.GameControls.Look.canceled += Look_canceled;

            inputService.Inputs.GameControls.Jump.performed += Jump_performed;
            inputService.Inputs.GameControls.Jump.canceled += Jump_canceled;

            inputService.Inputs.GameControls.Run.performed += Run_performed;
            inputService.Inputs.GameControls.Run.canceled += Run_canceled;

            inputService.Inputs.GameControls.Crouch.started += Crouch_started;
            inputService.Inputs.GameControls.Crouch.performed += Crouch_performed;
            inputService.Inputs.GameControls.Crouch.canceled += Crouch_canceled;

            configService.OnConfigChanged += ConfigManager_OnConfigChanged;

            inputService.Inputs.GameControls.NoClip.performed += NoClip_performed;

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        

        void NoClip_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            if (isNoClip)
            {
                isNoClip = false;
                Debug.Log($"PLAYERINPUT: NoClip is off");
            }
            else
            {
                isNoClip = true;
                Debug.Log($"PLAYERINPUT: NoClip is on");
            }

            characterController.SetNoClip(isNoClip);
        }

        void ConfigManager_OnConfigChanged(string key, dynamic newValue)
        {
            if (key == "invert-y")
            {
                SetInvertYFromConfig();
            }
        }

        void SetInvertYFromConfig()
        {
            if (Convert.ToBoolean(configService.GetValue("invert-y")))
            {
                invertYAxis = true;
            }
            else
            {
                invertYAxis = false;
            }
        }

        /*
        private void OnDestroy()
        {
            inputService.Inputs.GameControls.Move.performed -= Move_performed;

            inputService.Inputs.GameControls.Look.performed -= Look_performed;
            inputService.Inputs.GameControls.Look.canceled -= Look_canceled;

            inputService.Inputs.GameControls.Jump.performed -= Jump_performed;
            inputService.Inputs.GameControls.Jump.canceled -= Jump_canceled;

            inputService.Inputs.GameControls.Run.performed -= Run_performed;
            inputService.Inputs.GameControls.Run.canceled -= Run_canceled;

            inputService.Inputs.GameControls.Crouch.started -= Crouch_started;
            inputService.Inputs.GameControls.Crouch.performed -= Crouch_performed;
            inputService.Inputs.GameControls.Crouch.canceled -= Crouch_canceled;
        }*/




        void Crouch_started(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            //Debug.Log("Crouch_started");
            crouchPressed = true;
        }
        void Crouch_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            //Debug.Log("Crouch_performed");
        }

        void Crouch_canceled(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            //Debug.Log("Crouch_canceled");
            crouchPressed = false;
        }




        void Run_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            runPressed = true;
        }
        void Run_canceled(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            runPressed = false;
        }




        void Jump_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            jumpPressed = true;
        }
        void Jump_canceled(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            jumpPressed = false;
        }


        void Look_canceled(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            //Debug.Log("PlayerInputHandler Look_canceled");
            lookInput = Vector2.zero;
        }

        void Look_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            lookInput = inputService.Inputs.GameControls.Look.ReadValue<Vector2>();

            //Debug.Log("PlayerInputHandler " + obj.control.path + " lookInput=" + lookInput);

            if (obj.control.path.ToLower().Contains("mouse"))
            {
                lookInput.x *= 0.5f; // Account for scaling applied directly in Windows code by old input system.
                lookInput.x *= 0.1f; // Account for sensitivity setting on old Mouse X and Y axes.

                lookInput.y *= 0.5f; // Account for scaling applied directly in Windows code by old input system.
                lookInput.y *= 0.1f; // Account for sensitivity setting on old Mouse X and Y axes.
            }

        }



        void Move_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            moveInput = inputService.Inputs.GameControls.Move.ReadValue<Vector2>();

            //Debug.Log("PlayerInputHandler moveInput=" + moveInput);
        }




        public bool CanProcessInput()
        {
            //return Cursor.lockState == CursorLockMode.Locked && !m_GameFlowManager.gameIsEnding;
            return Cursor.lockState == CursorLockMode.Locked;
        }

        public Vector3 GetMoveInput()
        {
            return new Vector3(moveInput.x, 0, moveInput.y);
        }

        public float GetLookInputsHorizontal()
        {
            return lookInput.x;
        }

        public float GetLookInputsVertical()
        {
            if (invertYAxis)
                return lookInput.y *= -1f;
            else
                return lookInput.y;
        }

        public bool GetJumpInputDown()
        {
            return jumpPressed;
        }




        public bool GetSprintInputHeld()
        {
            return runPressed;
        }

        public bool GetCrouchInput()
        {
            return crouchPressed;
        }
    }
