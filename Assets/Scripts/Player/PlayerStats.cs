﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Macedon
{

    public class PlayerStats : ElegosBehaviour
    {
        [SerializeField] [Range(0f, 1f)] float visibility = 1f;
        [SerializeField] [Range(0f, 500f)] float health = 100f;
        [SerializeField] [Range(0f, 500f)] float maxHealth = 200f;
        [SerializeField] [Range(0f, 100f)] float power = 10f;
        [SerializeField] [Range(0f, 1f)] float threat = 0f;
        [SerializeField] [Range(0f, 50f)] float armor = 20f;

        public float Visibility => visibility;
        public float Health => health; 
        public float MaxHealth => maxHealth; 
        public float Power => power; 
        public float Threat => threat; 
        public float Armor => armor; 

        public void SetVisibility(float value)
        {
            visibility = Mathf.Clamp(value, 0f, 1f);
        }

        public void SetHealth(float value)
        {
            // make sure health is between 0 and max
            health = Mathf.Clamp(value, 0, maxHealth);
        }

        public override void Initialize()
        {
            //throw new System.NotImplementedException();
        }
    }
}