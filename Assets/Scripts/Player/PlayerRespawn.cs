using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    private PlayerCharacterController playerCharacterController;
    Transform playerSpawn;

    private void Awake()
    {
        playerCharacterController = GetComponent<PlayerCharacterController>();

        LookForPlayerSpawn();
    }

    IEnumerator RespawnCoroutine()
    {
        // disable character controller while we teleport so that it isn't keeping tracking of position until re-enabled

        playerCharacterController.enabled = false;

        // wait for next frame before re-enabling
        yield return null;

        playerCharacterController.characterVelocity = Vector3.zero;
        transform.position = playerSpawn.position;
        transform.rotation = playerSpawn.rotation;

        // wait for next frame before re-enabling
        yield return null;

        playerCharacterController.enabled = true;
    }

    

    void LookForPlayerSpawn()
    {
        GameObject playerSpawnGo = GameObject.FindGameObjectWithTag("PlayerSpawn");

        if (playerSpawnGo == null)
        {
            Debug.LogWarning($"PlayerRespawn: PlayerSpawn not found.");
            return;
        }

        playerSpawn = playerSpawnGo.transform;
        Debug.Log($"PlayerRespawn: PlayerSpawn found.");
    }


    public void Respawn()
    {
        if (playerSpawn != null)
        {
            Debug.Log($"PlayerRespawn: playerSpawn.position={playerSpawn.position}");

            // start courotine as we need to seperate somethings by a frame
            StartCoroutine(RespawnCoroutine());
        }
        else
        {
            Debug.LogError($"PlayerRespawn: No PlayerSpawn has been found.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "KillPlane")
        {
            Debug.Log($"PlayerRespawn: Need to respawn the player as we've hit the kill plane.");

            Respawn();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "KillPlane")
        {
            //playerCharacterController.enabled = true;
        }
    }
}
