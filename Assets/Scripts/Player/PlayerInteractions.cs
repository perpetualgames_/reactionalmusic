using Macedon;
using NaughtyAttributes;
using System;
using TMPro;
using UnityEngine;

public class PlayerInteractions : ElegosBehaviour, IUpdate, ILateUpdate
{
    // unity stuff

    [Header("Unity References")]
    [SerializeField] Camera playerCamera;

    [Header("UI Stuff")]
    [SerializeField] GameObject uiPressToInteract;
    [SerializeField] GameObject uiIndicator;
    [SerializeField] GameObject uiInteractingWith;
    [SerializeField] RectTransform canvasRect;
    [SerializeField] Vector3 offset;

    [Header("Misc")]
    [SerializeField] [ReadOnly] bool potentiallyInteracting = false;
    [SerializeField] [ReadOnly] bool currentlyInteracting = false;
    [SerializeField] [Range(0.1f, 1f)] float rayDetectionWidth = 1f;
    [SerializeField] [Range(1f, 5f)] float rayDetectionDistance = 1f;

    // fields

    float distanceToInteraction;

    BaseInteraction currentInteraction;
    BaseInteraction potentialInteraction;

    Vector3 rayOrigin;
    Vector3 rayDestination;
    
    RaycastHit rayHit;
    float interactionDistanceLimit = 10f;
    float outOfSightOffset = 20f;


    // events

    public delegate void BeginInteraction();
    public static event BeginInteraction OnBeginInteraction;

    public delegate void EndInteraction();
    public static event EndInteraction OnEndInteraction;

    public delegate void EndInteractionEarly();
    public static event EndInteractionEarly OnEndInteractionEarly;

    
    
    public override void Initialize()
    {
        // register for update and late update
        CoreManager.Instance.RegisterForUpdate(this);
        CoreManager.Instance.RegisterForLateUpdate(this);
        
        
        // default UI states
        uiPressToInteract.SetActive(false);
        uiInteractingWith.SetActive(false);
        uiIndicator.SetActive(false);
        
        
    }
    


    bool CheckForInteractionInFront()
    {
        // origin is detectionWidth behind the head so we can make sure the the 1u sphere is defo not starting inside of the interaction we are trying to find
        rayOrigin = playerCamera.transform.position;

        Debug.DrawLine(rayOrigin, playerCamera.transform.position + playerCamera.transform.forward * rayDetectionDistance);

        if (Physics.SphereCast(rayOrigin, rayDetectionWidth, playerCamera.transform.forward, out rayHit, rayDetectionDistance))
        {
            //Debug.Log($"{hit.collider.name} is in front of us.");

            //Debug.Log($"{rayHit.collider.name} is in front of us.");

            if (rayHit.collider.TryGetComponent(out BaseInteraction component))
            {
                potentialInteraction = component;

                // only need to check if we aren't already interacting with something
                if (!currentlyInteracting && potentialInteraction.IsInteractable)
                {
                    potentiallyInteracting = true;
                    return true;
                }
            }

        }

        potentialInteraction = null;
        potentiallyInteracting = false;

        return false;
    }

    public void OnUpdate()
    {
        CheckForInteractionInFront();
    }

    public void OnLateUpdate()
    {
        // using late update so movement and everything else is completed before we update the UI. Reduces jumpiness
        UpdateUIForInteractions();
    }



    void UpdateUIForInteractions()
    {
        // do we have an interaction in range and in view?  
        if (potentiallyInteracting && potentialInteraction != null)
        {
                // move potential target panel to follow character
                uiPressToInteract.SetActive(true);
                uiPressToInteract.transform.position = playerCamera.WorldToScreenPoint(potentialInteraction.transform.position + offset);
        } 
        else
        {
            uiPressToInteract.SetActive(false);
        }

        // are we currently interacting with something?
        if (currentlyInteracting)
        {
            distanceToInteraction = Vector3.Distance(transform.position, currentInteraction.transform.position);
            uiInteractingWith.GetComponent<TextMeshProUGUI>().text = $"<b>{currentInteraction.name}</b>\r\n{distanceToInteraction:0.00}";

            SetIndicatorPosition();

            if (distanceToInteraction > interactionDistanceLimit)
            {
                Debug.Log($"We've moved too far away from target");

                // run method on the interaction object
                currentInteraction.EndInteractEarly();

                FinishInteraction();
            }
        }            
    }

    private Vector3 OutOfRangeindicatorPositionB(Vector3 indicatorPosition)
    {
        //Set indicatorPosition.z to 0f; We don't need that and it'll actually cause issues if it's outside the camera range (which easily happens in my case)
        indicatorPosition.z = 0f;

        //Calculate Center of Canvas and subtract from the indicator position to have indicatorCoordinates from the Canvas Center instead the bottom left!
        Vector3 canvasCenter = new Vector3(canvasRect.rect.width / 2f, canvasRect.rect.height / 2f, 0f) * canvasRect.localScale.x;
        indicatorPosition -= canvasCenter;

        //Calculate if Vector to target intersects (first) with y border of canvas rect or if Vector intersects (first) with x border:
        //This is required to see which border needs to be set to the max value and at which border the indicator needs to be moved (up & down or left & right)
        float divX = (canvasRect.rect.width / 2f - outOfSightOffset) / Mathf.Abs(indicatorPosition.x);
        float divY = (canvasRect.rect.height / 2f - outOfSightOffset) / Mathf.Abs(indicatorPosition.y);

        //In case it intersects with x border first, put the x-one to the border and adjust the y-one accordingly (Trigonometry)
        if (divX < divY)
        {
            float angle = Vector3.SignedAngle(Vector3.right, indicatorPosition, Vector3.forward);
            indicatorPosition.x = Mathf.Sign(indicatorPosition.x) * (canvasRect.rect.width * 0.5f - outOfSightOffset) * canvasRect.localScale.x;
            indicatorPosition.y = Mathf.Tan(Mathf.Deg2Rad * angle) * indicatorPosition.x;
        }

        //In case it intersects with y border first, put the y-one to the border and adjust the x-one accordingly (Trigonometry)
        else
        {
            float angle = Vector3.SignedAngle(Vector3.up, indicatorPosition, Vector3.forward);

            indicatorPosition.y = Mathf.Sign(indicatorPosition.y) * (canvasRect.rect.height / 2f - outOfSightOffset) * canvasRect.localScale.y;
            indicatorPosition.x = -Mathf.Tan(Mathf.Deg2Rad * angle) * indicatorPosition.y;
        }

        //Change the indicator Position back to the actual rectTransform coordinate system and return indicatorPosition
        indicatorPosition += canvasCenter;
        return indicatorPosition;
    }

    private void Interact_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        PerformInteraction();
    }

    void PerformInteraction()
    {
        // if there is an interaction availble and we aren't currently interacting 
        if (potentiallyInteracting && !currentlyInteracting)
        {
            Type t = potentialInteraction.GetType();

            if (t == typeof(ActorInteraction))
            {
                ActorInteraction actorInteraction = (ActorInteraction)potentialInteraction;

                StartActorInteraction();

                // listen for this actors itneraction ended event
                actorInteraction.OnInteractionEnded += ActorInteraction_OnInteractionEnded;
            }
            else if (t == typeof(TriggerInteraction))
            {
                TriggerInteraction triggerInteraction = (TriggerInteraction)potentialInteraction;

                StartTriggerInteraction();

                // listen for this actors itneraction ended event
                triggerInteraction.OnInteractionEnded += TriggerInteraction_OnInteractionEnded;
            }
            else if (t == typeof(LootChestInteraction))
            {
                StartLootChestInteraction();
            }
            else
            {
                // Default action
                Debug.Log($"if/else on interaction type has failed badly. We shouldn't get here.");
            }
        }
    }

    private void TriggerInteraction_OnInteractionEnded(TriggerInteraction trigger)
    {
        Debug.Log($"PLAYER: TriggerInteraction_OnInteractionEnded({trigger.name})");

        // unsubscribe from this event just to be sure?
        trigger.OnInteractionEnded -= TriggerInteraction_OnInteractionEnded;

        // reset our interactions ui/variables etc.
        FinishInteraction();
    }

    private void ActorInteraction_OnInteractionEnded(ActorInteraction actor)
    {
        Debug.Log($"PLAYER: ActorInteraction_OnInteractionEnded({actor.name})");

        // unsubscribe from this event just to be sure?
        actor.OnInteractionEnded -= ActorInteraction_OnInteractionEnded;

        // reset our interactions ui/variables etc.
        FinishInteraction();
    }

    bool IsTargetOnScreen(Vector2 input)
    {
        return !(input.x > Screen.width || input.x < 0 || input.y > Screen.height || input.y < 0);
    }

    bool CheckInFrontOfPlayer()
    {
        // origin is detectionWidth behind the head so we can make sure the the 1u sphere is defo not starting inside of the interaction we are trying to find
        rayOrigin = playerCamera.transform.position - playerCamera.transform.forward * rayDetectionWidth;

        //Gizmos.DrawWireSphere(origin, 0.1f);
        Debug.DrawLine(rayOrigin, playerCamera.transform.position + playerCamera.transform.forward);

        if (Physics.SphereCast(rayOrigin, rayDetectionWidth, playerCamera.transform.forward, out rayHit, 10f))
        {
            //Debug.Log($"{hit.collider.name} is in front of us.");

            // have we collided with the gameobject we should be interacting with?
            if (rayHit.collider.gameObject == potentialInteraction.gameObject)
            {
                //Debug.Log($"{hit.collider.name} is in front of us.");
                return true;
            }
        }

        return false;
    }

    void StartLootChestInteraction()
    {
        Debug.Log($"START LOOT CHEST INTERACTION");

        // don't need to do anything other than run the Interact on the object

        // run method on the interaction object
        potentialInteraction.StartInteract();

        currentlyInteracting = false;
        currentInteraction = null;

        potentiallyInteracting = false;
        potentialInteraction = null;

        // ui
        uiInteractingWith.SetActive(false);
        uiIndicator.SetActive(false);
    }

    void StartTriggerInteraction()
    {
        Debug.Log($"START TRIGGER INTERACTION");

        // run method on the interaction object
        potentialInteraction.StartInteract();

        currentlyInteracting = true;
        currentInteraction = potentialInteraction;

        potentiallyInteracting = false;
        potentialInteraction = null;



        // ui
        uiInteractingWith.SetActive(true);
        uiIndicator.SetActive(true);
    }


    void StartActorInteraction()
    {
        Debug.Log($"START ACTOR INTERACTION");

        // run method on the interaction object
        potentialInteraction.StartInteract();

        currentlyInteracting = true;
        currentInteraction = potentialInteraction;

        potentiallyInteracting = false;
        potentialInteraction = null;



        // ui
        uiInteractingWith.SetActive(true);
        uiIndicator.SetActive(true);
    }

    void FinishInteraction()
    {
        Debug.Log($"FINISH INTERACTION");


        potentiallyInteracting = false;
        potentialInteraction = null;

        currentlyInteracting = false;
        currentInteraction = null;


        // ui
        uiInteractingWith.SetActive(false);
        uiIndicator.SetActive(false);
    }

    
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log($"PlayerInteractions {other.name}");

        if(other.tag == "DialogueTrigger" && other.TryGetComponent(out BaseInteraction component))
        {
            potentialInteraction = component;
            Debug.Log($"PlayerInteractions OnTriggerEnter {potentialInteraction.name}, IsInteractable={potentialInteraction.IsInteractable}, GetType()={potentialInteraction.GetType()}");

            potentiallyInteracting = true;

            PerformInteraction();



        }

    }


    private void OnDrawGizmosSelected()
    {        
        // draw chunky ray in scene view
        
        // origin is detectionWidth behind the head so we can make sure the the 1u sphere is defo not starting inside of the interaction we are trying to find
        rayOrigin = playerCamera.transform.position;
        rayDestination = playerCamera.transform.position + playerCamera.transform.forward * rayDetectionDistance;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(rayOrigin, rayDetectionWidth);
        Gizmos.DrawWireSphere(playerCamera.transform.position + playerCamera.transform.forward * rayDetectionDistance, rayDetectionWidth);
        Gizmos.DrawLine(rayOrigin + playerCamera.transform.right * rayDetectionWidth, rayDestination + playerCamera.transform.right * rayDetectionWidth);
        Gizmos.DrawLine(rayOrigin + -playerCamera.transform.right * rayDetectionWidth, rayDestination + -playerCamera.transform.right * rayDetectionWidth);
        Gizmos.DrawLine(rayOrigin + playerCamera.transform.up * rayDetectionWidth, rayDestination + playerCamera.transform.up * rayDetectionWidth);
        Gizmos.DrawLine(rayOrigin + -playerCamera.transform.up * rayDetectionWidth, rayDestination + -playerCamera.transform.up * rayDetectionWidth);


    }

    protected void SetIndicatorPosition()
    {
        // based on: https://github.com/Omti90/Off-Screen-Target-Indicator-Tutorial/blob/main/Scripts/TargetIndicator.cs

        //Get the position of the target in relation to the screenSpace 
        Vector3 indicatorPosition = playerCamera.WorldToScreenPoint(currentInteraction.transform.position + offset);
        //Debug.Log("GO: "+ gameObject.name + "; slPos: " + indicatorPosition + "; cvWidt: " + canvasRect.rect.width + "; cvHeight: " + canvasRect.rect.height);
        //Debug.Log($"indicatorPosition={indicatorPosition}");

        //In case the target is both in front of the camera and within the bounds of its frustrum
        if (indicatorPosition.z >= 0f & indicatorPosition.x <= canvasRect.rect.width * canvasRect.localScale.x
         & indicatorPosition.y <= canvasRect.rect.height * canvasRect.localScale.x & indicatorPosition.x >= 0f & indicatorPosition.y >= 0f)
        {

            //Set z to zero since it's not needed and only causes issues (too far away from Camera to be shown!)
            indicatorPosition.z = 0f;

            //Target is in sight, change indicator parts around accordingly
            //targetOutOfSight(false, indicatorPosition);
        }

        //In case the target is in front of the ship, but out of sight
        else if (indicatorPosition.z >= 0f)
        {
            //Set indicatorposition and set targetIndicator to outOfSight form.
            indicatorPosition = OutOfRangeindicatorPositionB(indicatorPosition);
            //targetOutOfSight(true, indicatorPosition);
        }
        else
        {
            //Invert indicatorPosition! Otherwise the indicator's positioning will invert if the target is on the backside of the camera!
            indicatorPosition *= -1f;

            //Set indicatorposition and set targetIndicator to outOfSight form.
            indicatorPosition = OutOfRangeindicatorPositionB(indicatorPosition);
            //targetOutOfSight(true, indicatorPosition);

        }

        //Set the position of the indicator
        uiIndicator.transform.position = indicatorPosition;

    }

}
