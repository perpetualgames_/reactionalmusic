using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Player : ElegosBehaviour
{
    [SerializeField] ElegosBehaviour[] elegosBehaviours;

    
    public override void Initialize()
    {
        //Debug.Log($"Player Initializing... elegosBehaviours.Length={elegosBehaviours.Length}");
        
        foreach (ElegosBehaviour elegosBehaviour in elegosBehaviours)
        {
            
            if (elegosBehaviour == null)
            {
                Debug.LogWarning($"{elegosBehaviour} is null");
                continue;
            }
            
            Debug.Log($"Initializing {elegosBehaviour}...");
            
            // initialize any scripts that this controls
            elegosBehaviour.Initialize();
        }
    }

}
