﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLookAt : ElegosBehaviour, IUpdate
{
    [SerializeField] Camera playerCamera;
    private Ray ray;

    [SerializeField] GameObject currentTarget;

    public GameObject CurrentTarget => currentTarget;

    public override void Initialize()
    {
        CoreManager.Instance.RegisterForUpdate(this);
    }

    // Update is called once per frame
    public void OnUpdate()
    {
        ray = playerCamera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

        //Debug.DrawRay(ray.origin, ray.direction * Mathf.Infinity, Color.yellow);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            currentTarget = hit.transform.gameObject;
        }
        else
        {
            currentTarget = null;
        }
    }


}
