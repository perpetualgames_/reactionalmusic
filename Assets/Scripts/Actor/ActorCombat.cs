using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Random = System.Random;

public class ActorCombat : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] [Expandable] WeaponData currentWeapon;
    [SerializeField] [Expandable] ActorStats stats;
    float _rayDistance = 10f;
    [SerializeField] Actor _actor;
    [SerializeField] CapsuleCollider _capsuleCollider;
    [SerializeField] NavMeshAgent _navMeshAgent;
    public WeaponData CurrentWeapon => currentWeapon;    
    
    // stats

    [SerializeField] [Range(0, 1)] [Tooltip("Critical Hit chance of the player. 0%-100%")]
    float critChance = 0;

    [SerializeField] [Range(0, 1)] [Tooltip("Additonal damage that a critical hit adds to the damage. 0%-100%")]
    float critDamageBoost = 0;

    [SerializeField] [ReadOnly] float finalCritChance;
    [SerializeField] [ReadOnly] float finalDamage;
    [SerializeField] [ReadOnly] float finalCritDamage;

    public float CritChance => critChance; 
    public float CritDamageBoost => critDamageBoost; 
    void OnValidate()
    {
        Assert.IsNotNull(animator, "Animator reference has probably not been set in the inspector");
        Assert.IsNotNull(currentWeapon, "Weapon reference has probably not been set in the inspector");
        //Assert.IsNotNull(stats, "ActorStats reference has probably not been set in the inspector");
    }

    void Start()
    {
        //StartCoroutine(AttackCoroutine());
    }

    IEnumerator AttackCoroutine()
    {
        Debug.Log($"AttackCoroutine: {this} is going to attack every few seconds");

        while (true)
        {
            animator.SetTrigger("attacktrigger");

            float randomWait = UnityEngine.Random.Range(2f, 6f);
            Debug.Log($"{this} is pausing for {randomWait:N} seconds");
            yield return new WaitForSeconds(randomWait);
        }
    }

    // Animation Event called anonymously via the Animation Controller
    public void Hit()
    {
        // new ray, approx starting half way up the body and then firing forward 10 units
        Ray ray = new Ray(transform.position + new Vector3(0, 1, 0), Vector3.forward * _rayDistance);
        
        Debug.Log($"AttackCoroutine: {this} animation event Hit() has been called");

        Debug.DrawRay(ray.origin, ray.direction * _rayDistance, Color.red, 1f);

        if (Physics.SphereCast(ray, 0.25f, out RaycastHit hit))
        {
            Debug.Log(hit.distance + " to " + hit.collider.name);
            
            if (hit.distance < currentWeapon.Range)
            {
                Debug.Log("HIT SOMETHING! " + hit.collider.gameObject.name);

                // draw a debug sphere in the game view and then remove a second later
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                Destroy(go.GetComponent<Collider>());
                go.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                go.transform.position = hit.point;
                go.GetComponent<MeshRenderer>().material.color = Color.red;
                Destroy(go, 1f);

                // is this an enemy hit?
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    if(hit.collider.TryGetComponent(out PlayerCombat playerCombat))
                    {
                        float damage = CalculateAttackScore();
                        Debug.Log($"PLAYER HIT damage={damage}");
                        playerCombat.TakeDamage(damage);
                    }
                    else
                    {
                        Debug.LogWarning($"PlayerCombat component not found on {hit.collider.name}");
                    }
                }
            }

        }
    }
    
    public void TakeDamage(float damage)
    {
        // calculate defence score
        float finalDamageReceived = CalculateDefenceScore(damage);

        float newHealth = stats.Health - finalDamageReceived;

        Debug.Log($"Taking {finalDamageReceived} damage after original damage of {damage} due to armor value of {stats.Armor}. Health is going to be {newHealth:N}/{stats.MaxHealth:N}");

        if (newHealth < 0)
        {
            Debug.Log($"{name} would be dead now.");
            animator.SetTrigger("dietrigger");
            
            // disable any colliders and objects so it ends up jsut being dead?
            _capsuleCollider.enabled = false;
            _navMeshAgent.enabled = false;
            _actor.enabled = false;
            this.enabled = false;
            StopAllCoroutines();
            //Destroy(gameObject, 2f);
        }

        stats.SetHealth(newHealth);
    }
    

    float CalculateDefenceScore(float damage)
    {
        float derivedStatusEffects = 0f; // not calculated yet

        float comparisonOfDamageAndDefence = 1 / (damage / (stats.Armor + 0.00001f)); // add a really small number in case of zero armor

        float damageReductionPercentage = (comparisonOfDamageAndDefence * stats.Armor) / (10 + comparisonOfDamageAndDefence * stats.Armor);

        return damage * (1 - damageReductionPercentage) * (1 + derivedStatusEffects);
    }
    
    
    float CalculateAttackScore()
    {        
        float derivedWeaponDamage = CurrentWeapon.Damage * (1 + (((int)CurrentWeapon.Rarity + 1) * 0.3f)); // extra +1 to rarity to account for zero indexed

        float derivedCritChance = CritChance + CurrentWeapon.CritChance; // adding player and weapon crit chance
        float derivedCritDamageBoost = CritDamageBoost + CurrentWeapon.CritDamageBoost; // adding player and weapon crit damage boost
        float derivedStatusEffects = 0.1f; // not calculated yet

        finalCritChance = derivedCritChance; // no further calculations needed yet
        finalDamage = derivedWeaponDamage * (1 + derivedStatusEffects); // weapon damage increased/decreased by derived status effects
        finalCritDamage = finalDamage * (1 + derivedCritDamageBoost); // final damage increased by derived crit damage
        return finalCritDamage;
        // Attack * (100 / (100 + Defense))  
    }
}
