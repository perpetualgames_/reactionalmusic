using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.AI;
//using BehaviorDesigner.Runtime;

[RequireComponent(typeof(AudioSource))]
public class Actor : ElegosBehaviour, IUpdate
{
    [SerializeField] [Expandable] ActorStats actorStats;
    [SerializeField] [Expandable] ActorData actorData;
    [SerializeField] AudioSource audioSource;
    [SerializeField] SkinnedMeshRenderer smr;
    [SerializeField] NavMeshAgent navMeshAgent;
    //[SerializeField] BehaviorTree behaviorTree; 

    public ActorData ActorData => actorData;
    public AudioClip CurrentAudioClip => audioSource.clip;
    
    
    
    
    public override void Initialize()
    {
        CoreManager.Instance.RegisterForUpdate(this);
        
        smr.material.color = Color.white;
        
        if (actorStats)
            actorStats.ResetHealthToMax();
    }
    

    public void OnUpdate()
    {
        CorrectBaseHeight();
    }

    public void SetData(ActorData actorData)
    {
        this.actorData = actorData;
        name = this.actorData.id;
    }
    
    public void StartConversation()
    {
        Debug.Log($"{name} has FinishConversation()");
        
        //behaviorTree.SetVariable("shouldInterrupt", (SharedBool)true);
    }
    
    public void StartTalking()
    {
        Debug.Log($"{name} StartTalking()");

        smr.material.color = Color.blue;

        audioSource.Stop();
        audioSource.Play();
    }

    public void StopTalking()
    {
        smr.material.color = Color.white;
        audioSource.Stop();
    }

    public void FinishConversation()
    {
        Debug.Log($"{name} has FinishConversation()");
        
        //behaviorTree.SetVariable("shouldInterrupt", (SharedBool)false);
    }


    public void SetAudioClip(AudioClip clip)
    {
        audioSource.clip = clip;
    }



    
    void CorrectBaseHeight()
    {
        NavMeshHit navhit;
        if (NavMesh.SamplePosition(transform.position, out navhit, 10f, NavMesh.AllAreas))
        {
            Ray ray = new Ray(navhit.position, Vector3.down);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 10f))
            {
                navMeshAgent.baseOffset = -hit.distance;
            }
        }
    }

}
