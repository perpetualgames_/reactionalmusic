using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class SimonStateSaveLoad : MonoBehaviour
{
    [SerializeField] string statesFolder = "states";
    string _statesFolderPath;
    Dictionary<string, GestrumentState> allStates = new Dictionary<string, GestrumentState>();


    // Start is called before the first frame update
    void Start()
    {
        string rootFolderPath = Directory.GetParent(Application.dataPath).ToString();

        _statesFolderPath = Path.Combine(rootFolderPath, statesFolder);

        LookForStatesInFolder();
    }

    void LookForStatesInFolder()
    {
        if (!Directory.Exists(_statesFolderPath))
        {
            Debug.LogWarning("States folder doesn't exist.");
            return;
        }
        
        string[] jsonFilePaths = Directory.GetFiles(_statesFolderPath, "*.json", SearchOption.TopDirectoryOnly);

        Debug.Log($"{jsonFilePaths.Length} json files found in {_statesFolderPath}.");

        foreach (string path in jsonFilePaths)
        {
            string json;
            GestrumentState state;
            
            // read json text into a variables

            try
            {
                json = File.ReadAllText(path);
            }
            catch (System.Exception)
            {
                Debug.LogWarning($"Failed to read json file at {path}");
                continue;
            }
            
            // deserialize json to a GestrumentState object

            try
            {
                state = JsonConvert.DeserializeObject<GestrumentState>(json);
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Failed to deserialize json to GestrumentState. {e.Message}");
                continue;
            }
            
            // add deserialized GestrumentState to a dictionary
            
            allStates.Add(state.stateName, state);
        }

        Debug.Log($"allStates={string.Join(",", allStates.Keys)}");
    }
    
}
