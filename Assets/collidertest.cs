using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collidertest : MonoBehaviour
{

    public int motif;
    public int stinger;
    public int section;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        Reactional.Request.Motif(motif);
        Reactional.Request.Stinger(stinger);
        Reactional.Request.Section(section);
    }


}
