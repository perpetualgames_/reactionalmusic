using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ActorData", menuName = "Macedon/ActorData")]
public class ActorData : ScriptableObject
{
    public enum Archetype { None, Sailor, Villager, Guard, Cultist, Hero, Custom }
    public enum Gender {None, Male, Female, Custom }
    
    public string id;
    public string realName;
    public Archetype archetype;
    public string customArchetype;
    public Gender gender;
    public string customGender;
}
