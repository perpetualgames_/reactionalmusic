using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseStats : ScriptableObject
{
    [SerializeField] [Range(0f, 500f)] float health = 100f;
    [SerializeField] [Range(0f, 500f)] float maxHealth = 200f;
    [SerializeField] [Range(0f, 50f)] float armor = 20f;
    
    public float Health => health;
    public float Armor => armor;
    public float MaxHealth => maxHealth;


    public void ResetHealthToMax()
    {
        health = maxHealth;
    }
    
    public void SetHealth(float value)
    {
        // make sure health is between 0 and max
        health = Mathf.Clamp(value, 0, maxHealth);
    }
}
