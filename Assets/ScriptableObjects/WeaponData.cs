using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New WeaponData", menuName = "Macedon/WeaponData")]
public class WeaponData : ScriptableObject
{
    public enum WeaponRarity { Normal, Rare, Legendary }
    
    [SerializeField] 
    [Tooltip("Distance from Player camera that the weapon can hit e.g. length of weapon")]
    float range = 1f;

    public float Range  =>  range; 

    [SerializeField]
    [Range(0,100)]
    [Tooltip("Base damage that this weapon can deal.")]
    float damage = 10f;
    public float Damage  =>  damage; 

    // could this be an enum?
    [SerializeField]    
    [Tooltip("Rarity of the weapon.")]
    WeaponRarity rarity = 0;
    public WeaponRarity Rarity => rarity; 

    [SerializeField]
    [Range(0, 1)]
    [Tooltip("Critical Hit chance of the weapon. 0%-100%")]
    float critChance = 0;
    public float CritChance => critChance; 

    [SerializeField]
    [Range(0, 1)]
    [Tooltip("Additonal damage that a critical hit adds to the weapons damage. 0%-100%")]
    float critDamageBoost = 0;
    public float CritDamageBoost => critDamageBoost; 


    // value?
    // weight?
}
