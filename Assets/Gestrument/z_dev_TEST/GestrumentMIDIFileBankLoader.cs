﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;
// using System.IO;
// using GE_Core;

// public class GestrumentMIDIFileBankLoader : MonoBehaviour
// {

//     public Object midiFolder;

//     public List<string> midifileNames;

//     public GestrumentOrchestrator ge;

//     bool allowSwitch = true;


//     // Start is called before the first frame update
//     void Start()
//     {

//     }

//     // Update is called once per frame
//     void Update()
//     {
//         if (ge)
//         {
           
//             var beat = ge.core.engine.GetCurrentBeat();
//             var subdiv = 0.25;
//             var mod = (4 * (1 / subdiv));
            
//             if (beat % mod > (mod * 0.75f))
//             {
//                  //Debug.Log(ge.core.engine.GetCurrentBeat());
//                 if (allowSwitch)
//                 {
//                 int rand = Random.Range(0,midifileNames.Count-1);
//                 InjectMidi(0, "pulse", midifileNames[rand]);
//                 InjectMidi(0, "pitch", midifileNames[rand]);
//                 Debug.Log("switch!" + "     " + midifileNames[rand]);
//                 }
//                 allowSwitch = false;
//             }
//             else
//             {
//                 allowSwitch = true;
//             }
//         }
//     }

//     private void OnValidate()
//     {
//         if (midiFolder is DefaultAsset && midifileNames.Count < 1)
//         {
//             var path = AssetDatabase.GetAssetPath(midiFolder);
//             var filePathsInDirs = Directory.GetFiles(path);
//             foreach (var fPath in filePathsInDirs)
//             {
//                 var assetPath = fPath.Replace('\\', '/');
//                 assetPath = assetPath.Remove(0, 23);
//                 var obj = AssetDatabase.LoadMainAssetAtPath(fPath);
//                 if (obj is DefaultAsset)
//                     midifileNames.Add(assetPath);
//             }
//         }
//     }


//     public void InjectMidi(int instrument, string pitch_or_pulse, string midiPath)
//     {
//         if (pitch_or_pulse == "pitch")
//         {
//             ge.core.engine.LoadPitchMIDI(instrument, Application.streamingAssetsPath+"/"+midiPath);
//         }
//         else if (pitch_or_pulse == "pulse")
//         {
//             ge.core.engine.LoadPulseMIDI(instrument, Application.streamingAssetsPath+"/"+midiPath);
//         }
//     }
// }
