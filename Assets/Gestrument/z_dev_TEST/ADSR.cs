﻿using System;
using UnityEngine;

public class ADSR : MonoBehaviour
{
    public bool Enabled;

    [Range(0.001f, 5)]
    public float AttackTime = 0f;
    [Range(0, 5)]
    public float DecayTime = 0f;
    [Range(0, 5)]
    public float SustainTime = 0f;
    [Range(0, 5)]
    public float ReleaseTime = 0f;
    [Range(0, 1)]
    public float SustainLevel = 1f;

    private bool _triggered;
    private double _startTime;
    private double _sampleDuration;
    private double _attackFinishTime;
    private double _decayFinishTime;
    private double _sustainFinishTime;
    private double _releaseFinishTime;

    private float startVol;

    AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void NoteOff()
    {       
        //SustainTime = 0;
        _sustainFinishTime = 0;
        _releaseFinishTime = AudioSettings.dspTime + ReleaseTime;
    }

    public void Trigger(double triggerTime)
    {
        if (SustainTime <= 0f) SustainTime = 999;
        triggerTime = triggerTime + AudioSettings.dspTime;
        _triggered = true;
        _startTime = triggerTime;
        _sampleDuration = 1.0 / AudioSettings.outputSampleRate;
        _attackFinishTime = triggerTime + AttackTime;
        _decayFinishTime = _attackFinishTime + DecayTime;
        _sustainFinishTime = _decayFinishTime + SustainTime;
        _releaseFinishTime = _sustainFinishTime + ReleaseTime;
        
    }

    private void OnAudioFilterRead(float[] buffer, int channels)
    {
        // if not enabled, don't do any attenuation
        if (!Enabled)
        {
            return;
        }

        // if enabled, but outside the range of the envelope, attenuate fully
        if (!_triggered || AudioSettings.dspTime < _startTime)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0f;
            }

            return;
        }

        float volume;
        double sampleTime = AudioSettings.dspTime;

        for (int i = 0; i < buffer.Length; i += channels)
        {
            sampleTime += _sampleDuration;

            if (AttackTime > 0 && sampleTime < _attackFinishTime)
            {
                volume = startVol + (1 - startVol) * (float)Math.Pow((sampleTime - _startTime) / AttackTime, 4);
            }
            else if (DecayTime > 0 && sampleTime < _decayFinishTime)
            {
                volume = SustainLevel + ((1 - SustainLevel) * (float)Math.Pow((_decayFinishTime - sampleTime) / DecayTime, 4));
                startVol = volume;
            }
            else if (SustainTime > 0 && sampleTime < _sustainFinishTime)
            {
                volume = SustainLevel;
                startVol = volume;
            }
            else if (ReleaseTime > 0 && sampleTime < _releaseFinishTime)
            {
                volume = SustainLevel * (float)Math.Pow((_releaseFinishTime - sampleTime) / ReleaseTime, 4);
                startVol = volume;
            }
            else
            {
                volume = 0f;
                _triggered = false;
            }



            for (int j = 0; j < channels; j++)
            {
                buffer[i + j] *= volume;
            }
        }
    }
}
