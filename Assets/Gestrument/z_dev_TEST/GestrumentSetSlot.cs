﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;

public enum mFunction : int
{
    I,
    II,
    III,
    IV,
    V,
    VI,
    VII
};

public class GestrumentSetSlot : MonoBehaviour
{
    public GestrumentGlobalSync global;
    public int instrument;
    public int scaleslot = 0;
    public float transposeslot = 0;
    public int inversion = 0;

    public GestrumentSequence seq;

    public List<int> midiChordDegrees;
    public List<int> desiredChordDegrees;

    public bool keepMelody = true;

    public int chordDur;

    public int chordloop;
    int currentChordBeat = 0;
    int currentChord = 0;

    public float[] slotplot;
    // Start is called before the first frame update

    void Start()
    {
        //GestrumentSequence seq = new GestrumentSequence();
        //seq.midiChordDegrees = midiChordDegrees;
      //  seq.desiredChordDegrees = desiredChordDegrees;
       // seq.oscmessages = new List<string>();
        //seq.setcur = new List<float[]>();
        global = FindObjectOfType<GestrumentGlobalSync>();
        global.currentCore.engine.SetScaleForSlot(0, 9, "dorian");
      
       // global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.PitchGen, 3, "absoluteindexing", true);
      // global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.PitchGen, 3, "index", 0);
       //global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.PitchGen, 3, "pitchindexing", 1);
        //global.currentCore.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, 0, 3, "degrees", "[ [0,2,4], [1, 3, 5], [2, 4, 6], [3, 5, 7], [4, 6, 1], [5, 7, 2], [6, 1, 3] ]");
        
        // seq.oscmessages.Add(System.Convert.ToBase64String(msg));


        string path = Application.streamingAssetsPath + "/test.json";
        string contents = System.IO.File.ReadAllText(path);
        seq = JsonUtility.FromJson<GestrumentSequence>(contents);
        
 
        //global.currentCore.engine.SetTempo(128);
        

        global.currentCore.engine.SetCursorGate(0, true);
        global.currentCore.engine.SetCursorGate(1, true);
        // global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.Engine,0, )

        for (int i = 0; i < slotplot.Length; i++)
        {
            slotplot[i] = i - 7;
            global.currentCore.engine.SetValueForTransposeSlot(i, slotplot[i]);

        }


        var adjustedDegree = desiredChordDegrees[0] - midiChordDegrees[0];
        global.currentCore.engine.ScheduleTransposeSlot(0, (int)adjustedDegree + 7);

        //for (int i = 0; i < desiredChordDegrees.Count; i++)
        //{
        //    var adjustedDegree = desiredChordDegrees[i] - midiChordDegrees[i];
        //    var beat = (i * chordDur) - 0.125f;
        //    global.currentCore.engine.ScheduleTransposeSlot(beat, adjustedDegree+7);
        //    global.currentCore.engine.ScheduleRelPitchIndex(beat, 2, (int)midiChordDegrees[i]);
        //    global.currentCore.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, beat, 0, "mtranspose", -adjustedDegree);
        //}

       // string json = JsonUtility.ToJson(seq);
       // System.IO.File.WriteAllText(Application.streamingAssetsPath + "/test.json", json);


    }

    // Update is called once per frame
    void FixedUpdate()
    {

         //global.currentCore.engine.ScheduleTransposeSlot(0, (int)transposeslot);


        double beat = global.currentCore.engine.GetCurrentBeat();
        if (beat > currentChordBeat + chordDur/2)
        {
            currentChord = (int)(beat % (chordloop*chordDur)) / (chordDur);//(currentChord + 1) % chordloop;
            currentChord = (currentChord + 1) % chordloop;

            beat = beat + (chordDur/2); //- (beat % chordDur));
            beat = Mathf.Round((float)beat);
            currentChordBeat = (int)beat;
           

            int adjustedDegree = (int)desiredChordDegrees[currentChord] - (int)midiChordDegrees[currentChord];
            Debug.Log(currentChord);
            global.currentCore.engine.ScheduleTransposeSlot(beat-0.15f, adjustedDegree + 7);
            //global.currentCore.engine.ScheduleRelPitchIndex(beat, 3, (int)midiChordDegrees[currentChord]);
            global.currentCore.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, beat, 4, "mtranspose", (int)desiredChordDegrees[currentChord]);

            if(keepMelody)
                global.currentCore.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, beat, 0, "mtranspose", -adjustedDegree);
            else
                global.currentCore.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, beat, 0, "mtranspose", 0);


        }

        //int slot;
        //global.currentCore.engine.GetParamValue(Gestrument.Engine.EventObject.Engine, 0, "transpose", out slot);
        //Debug.Log(slot);
    }

    void SetChord()
    {

    }

    public void Test()
    {
        Debug.Log("TADAA");
    }


    //private void OnValidate()
    //{
    //    global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.Engine, 0, "transposeslotindex", transposeslot);
    //    global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.Engine, 0, "scaleslotindex", scaleslot);
    //    global.currentCore.engine.SetParamValue(Gestrument.Engine.EventObject.PitchGen, 1, "inversion", inversion);
    //}



}
