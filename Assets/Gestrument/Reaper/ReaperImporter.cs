﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.AssetImporters;
using System.IO;
using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Audio;
using UnityEngine.Audio;
using UnityOSC;

[ScriptedImporter(1, "rpp")]
public class ReaperImporter : ScriptedImporter
{
    public float m_Scale = 1;
    CursorSequenceAsset cac;
    public string path;
    public string filename;

    public override void OnImportAsset(AssetImportContext rpp)
    {


        // rpp.AddObjectToAsset("main obj", sb);
        path = rpp.assetPath;
        path = Path.GetDirectoryName(path);
        filename = Path.GetFileName(rpp.assetPath);
        filename = filename.Remove(filename.Length - 4, 4);

        TextAsset subAsset = new TextAsset(File.ReadAllText(rpp.assetPath));
        ReadRpp(subAsset);

        rpp.AddObjectToAsset("text", subAsset);
        rpp.SetMainObject(subAsset);


        AssetDatabase.SaveAssets();
    }

    void ReadRpp(TextAsset t)
    {
        //GestrumentSettings gS = new GestrumentSettings();
        TimelineTrack trackAsset = ScriptableObject.CreateInstance<TimelineTrack>();
        TimelineSectionAsset sectionAsset = ScriptableObject.CreateInstance<TimelineSectionAsset>();
        SetupMessages keyScaleMessage = new SetupMessages();
        List<TimelineSectionAsset> sectionAssetList = new List<TimelineSectionAsset>();
        List<TimelineSectionAsset> motifAssetList = new List<TimelineSectionAsset>();
        List<TimelineSectionAsset> stingerAssetList = new List<TimelineSectionAsset>();

        List<TimelineTrack> trackAssetList = new List<TimelineTrack>();
        ReactionalTrack reactionalTrack = ScriptableObject.CreateInstance<ReactionalTrack>();

        if (!Directory.Exists(Application.dataPath + path.Remove(0, 6) + "/" + filename))
            Directory.CreateDirectory(Application.dataPath + path.Remove(0, 6) + "/" + filename);


        reactionalTrack = AssetDatabase.LoadMainAssetAtPath(path + "/" + filename + ".asset") as ReactionalTrack;
        if (reactionalTrack != null)
        {
            reactionalTrack.Reset();
        }
        else
        {
            reactionalTrack = ScriptableObject.CreateInstance<ReactionalTrack>();
            AssetDatabase.CreateAsset(reactionalTrack, path + "/" + filename + ".asset");
        }

        reactionalTrack.m_trackName = filename;



        string p = t.ToString();
        var lines = p.Split("\n"[0]);

        bool track = false; bool midiitem = false; string name = "test"; int count = -1; string trackName = ""; double beat = 0; int quant = 960; double position = 0; double soffset = 0; double tempo = 120;
        string sectionName; float rootOffset = 0; int trackCount = 0;
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].StartsWith("  TEMPO "))
            {
                string tmpo = lines[i].Remove(0, 8);
                tmpo = tmpo.Remove(tmpo.Length - 4);
                tempo = Convert.ToDouble(tmpo);
                reactionalTrack.m_tempo = tempo;
                Debug.Log("Tempo: " + tempo);
            }

            if (lines[i].StartsWith("  MARKER "))
            {

                var l = lines[i].Remove(0, 9).Split();

                if (l[2].StartsWith("\"Key:"))
                {
                    l[3] = l[3].Replace("\"", "");
                    var key = l[3];
                    string[] k = Regex.Split(key, "[\'_'\r\n]+");

                    rootOffset = NoteNameToOffset(k[0]); // not used for now
                    rootOffset = remap(rootOffset, 0, 127, 0, 1);

                    OSCMessage msg = new OSCMessage("/slt/eng");
                    msg.Append<string>("scale");
                    msg.Append<int>(0);
                    msg.Append<float>(NoteNameToOffset(k[0]));
                    msg.Append<string>(k[1]);

                    keyScaleMessage.data = msg.BinaryData;

                    if (sectionAsset != null)
                    {
                        sectionAsset.setupMessages.Add(keyScaleMessage);
                    }

                    reactionalTrack.m_setupMessages.Add(keyScaleMessage);
                    reactionalTrack.m_scale = k[0] + " " + k[1];
                }


                if (l[2].StartsWith("\"Part:") || l[2].StartsWith("\"Stinger:") || l[2].StartsWith("\"Motif:"))
                {
                    l[3] = l[3].Replace("\"", "");
                    sectionAsset = null;
                    sectionAsset = ScriptableObject.CreateInstance<TimelineSectionAsset>();

                    var sal = sectionAssetList;

                    string s_type = "";

                    if (l[2].StartsWith("\"Stinger:"))
                    {
                        sal = stingerAssetList;
                        s_type = "Stingers/S_";
                        if (!Directory.Exists(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/" + "Stingers"))
                            Directory.CreateDirectory(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/" + "Stingers");
                    }

                    if (l[2].StartsWith("\"Motif:"))
                    {
                        sal = motifAssetList;
                        s_type = "Motifs/M_";
                        if (!Directory.Exists(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/" + "Motifs"))
                            Directory.CreateDirectory(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/" + "Motifs");
                    }



                    sectionAsset = AssetDatabase.LoadMainAssetAtPath(path + "/" + filename + "/" + s_type + filename + "_" + l[3] + ".asset") as TimelineSectionAsset;
                    if (sectionAsset != null)
                    {
                        sectionAsset.tracks.Clear();
                        sectionAsset.setupMessages.Clear();
                        sectionAsset.instruments.Clear();
                    }
                    else
                    {
                        sectionAsset = ScriptableObject.CreateInstance<TimelineSectionAsset>();
                        AssetDatabase.CreateAsset(sectionAsset, path + "/" + filename + "/" + s_type + filename + "_" + l[3] + ".asset");
                    }
                    sectionName = l[3];
                    sectionAsset.startBeat = double.Parse(l[1], System.Globalization.CultureInfo.InvariantCulture) / (60f / tempo);
                    sectionAsset.id = Convert.ToInt32(l[0]);

                    if (keyScaleMessage != null)
                    {
                        sectionAsset.setupMessages.Add(keyScaleMessage);
                    }

                    sal.Add(sectionAsset);

                    if (sectionAsset.name.StartsWith("M_"))
                        reactionalTrack.m_motifs.Add(sectionAsset);
                    else if (sectionAsset.name.StartsWith("S_"))
                        reactionalTrack.m_stingers.Add(sectionAsset);
                    else
                        reactionalTrack.m_sections.Add(sectionAsset);

                    EditorUtility.SetDirty(sectionAsset);

                    var lplus = lines[i + 1].Remove(0, 9).Split();

                    sectionAsset.stopBeat = Convert.ToDouble(lplus[1], System.Globalization.CultureInfo.InvariantCulture) / (60 / tempo);



                }


            }

            if (lines[i].StartsWith("  <TRACK"))
            {
                track = true;
                int toffset = 0;

                if (!lines[i + 1].Contains("\""))
                    toffset = 1;

                trackName = lines[i + 1].Remove(0, 10 - toffset);
                trackName = trackName.Remove(trackName.Length - (2 - toffset));
                if (trackName == "") continue;

                trackAsset = ScriptableObject.CreateInstance<TimelineTrack>();
                if (!Directory.Exists(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/Tracks"))
                    Directory.CreateDirectory(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/Tracks");
                trackAsset = AssetDatabase.LoadMainAssetAtPath(path + "/" + filename + "/Tracks/" + trackName + ".asset") as TimelineTrack;

                if (trackAsset != null)
                {
                    //EditorUtility.CopySerialized (csa, cac);
                    // AssetDatabase.SaveAssets ();
                }
                else
                {
                    trackAsset = ScriptableObject.CreateInstance<TimelineTrack>();
                    AssetDatabase.CreateAsset(trackAsset, path + "/" + filename + "/Tracks/" + trackName + ".asset");
                    // AssetDatabase.SaveAssets(); // maybe necessary?
                }

                trackAsset.cursorSequences.Clear();
                trackAsset.instrumentName = Regex.Replace(trackName, "[0-9]", "");
                trackAsset.trackNumber = trackCount;

                EditorUtility.SetDirty(trackAsset); //

                trackCount++;

                if (trackName.StartsWith("Perc")) trackAsset.isPercussion = true;

                trackAssetList.Add(trackAsset);

                reactionalTrack.m_timelineTracks.Add(trackAsset);
                reactionalTrack.m_trackCount++;

               // var pn = lines[i + 5].Split();
              //  Single.TryParse(pn[2], out trackAsset.pan);

            }
            else if (lines[i].StartsWith("  >")) track = false;

            if (lines[i].StartsWith("      <SOURCE MIDI")) midiitem = true;
            else if (lines[i].StartsWith("      >")) midiitem = false;


            if (lines[i].StartsWith("      POSITION "))
            {
                string pos = lines[i].Remove(0, 15);
                position = double.Parse(pos, System.Globalization.CultureInfo.InvariantCulture) / (60 / tempo);

            }





            if (lines[i].StartsWith("      NAME "))
            {
                var index = lines[i].IndexOf("      NAME ");
                var l = lines[i];
                name = l.Remove(0, index + 11);
                name = name.Replace("\"", "");
                name = name.Remove(name.Length - 1, 1);
            }


            if (lines[i].StartsWith("      SOFFS "))
            {
                string[] soff = Regex.Split(lines[i].Remove(0, 12), "[\' '\r\n]+");
                Double.TryParse(soff[1], out soffset);
            }


            double item_length = 0;
            if (lines[i].StartsWith("      LENGTH "))
            {
                string[] len = Regex.Split(lines[i].Remove(0, 13), "[\' '\r\n]+");
                Double.TryParse(len[0], out item_length);
            }
                


            if (track && midiitem)
            {
                count++;
                //cac = ScriptableObject.CreateInstance<CursorSequenceAsset>();

                if (!Directory.Exists(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/Items"))
                    Directory.CreateDirectory(Application.dataPath + path.Remove(0, 6) + "/" + filename + "/Items");

                cac = AssetDatabase.LoadMainAssetAtPath(path + "/" + filename + "/Items/" + trackName + count + name + ".asset") as CursorSequenceAsset;

                if (cac != null)
                {
                    //EditorUtility.CopySerialized (csa, cac);
                    // AssetDatabase.SaveAssets ();
                    cac.cursorArrayContainer.Clear();
                    
                }
                else
                {
                    cac = ScriptableObject.CreateInstance<CursorSequenceAsset>();
                    AssetDatabase.CreateAsset(cac, path + "/" + filename + "/Items/" + trackName + count + name + ".asset");
                }
                

                var seq = new GestrumentCursorArrayContainer();
                cac.cursorArrayContainer = new List<GestrumentCursorArrayContainer>();
                cac.cursorArrayContainer.Add(new GestrumentCursorArrayContainer());

                cac.beatPosition = position;
                cac.trackNumber = trackAsset.trackNumber;

                if (trackAsset != null)
                {
                    trackAsset.cursorSequences.Add(cac);
                }


                cac.beatLength = item_length / (60f / tempo);

                reactionalTrack.m_items.Add(cac);

                

                beat = 0;
                //gS.instruments.Add(AssetDatabase.FindAssets())

                // gS.instrumentSettings.Add(new InstrumentSettings());
                //gS.instrumentSettings[count].cursor = count;
                // gS.instrumentSettings[count].output = count;
                midiitem = false;

                EditorUtility.SetDirty(cac);
            }





            if (lines[i].StartsWith("        HASDATA 1 "))
            {
                var l = lines[i].Remove(0, 18);
                l = l.Remove(3, l.Length - 3);
                int.TryParse(l, out quant);
                //Debug.Log(quant);
            }

            if (lines[i].StartsWith("        E ") || lines[i].StartsWith("        e "))
            {
                var l = lines[i].Remove(0, 10);
                //var values = l.Split(;
                string[] values = Regex.Split(l, "[\' '\r\n]+");
                double[] vals = new double[5];

                double.TryParse(values[0], out vals[0]);

                if (values[1] == "b0") continue; // End of item ? skip

                vals[0] = vals[0] / quant;

                beat = beat + vals[0] - soffset; // Adjust SOFFSET (Start Offset)
                soffset = 0; // Reset SOFFSET
                if (beat < 0) continue;

                if (values[1] == "90" && values[3] != "00")
                    vals[3] = 1;
                else if (values[1] == "80" || values[3] == "00")
                    vals[3] = 0;

                // if(values[1] != "90" || values[1] != "80") continue;

                vals[2] = Convert.ToInt32(values[2], 16); // PITCH
                vals[2] = remap((float)vals[2], 0, 127, 0, 1);
                vals[4] = Convert.ToInt32(values[3], 16); // VELOCITY

                if (vals[2] < cac.minpitch) cac.minpitch = vals[2];
                if (vals[2] > cac.maxpitch) cac.maxpitch = vals[2];

                var stp = new CursorArray();
                stp.pos = new double[6];
                stp.pos[0] = beat;
                stp.pos[1] = 1; // x
                stp.pos[2] = vals[2]; // y
                stp.pos[3] = vals[3]; // gate
                stp.pos[4] = vals[4];  // vel
                stp.pos[5] = 1; // Flag for playback

                if (cac.cursorArrayContainer.Count > 0)
                {
                    if (cac.cursorArrayContainer[0].steps == null) cac.cursorArrayContainer[0].steps = new List<CursorArray>();
                    cac.cursorArrayContainer[0].steps.Add(stp);
                }



            }



        }

        for (int i = 0; i < 3; i++)
        {
            var sal = sectionAssetList;
            if (i == 1) sal = reactionalTrack.m_motifs;
            if (i == 2) sal = reactionalTrack.m_stingers;

            if (sal.Count > 0)
            {
                foreach (TimelineSectionAsset sa in sal)
                {
                    foreach (TimelineTrack ta in trackAssetList)
                    {
                        foreach (CursorSequenceAsset csa in ta.cursorSequences)
                        {
                            if (csa.beatPosition >= sa.startBeat && (csa.beatPosition ) <= sa.stopBeat)
                            {
                                bool isMotif = false;
                                if (i == 1) isMotif = true;

                                if ( (isMotif && !csa.name.Contains("motif")) || (!isMotif && csa.name.Contains("motif")) )
                                        continue;

                                if (!sa.tracks.Contains(ta))
                                    sa.tracks.Add(ta);
                                if (!sa.instruments.Contains(ta.instrumentName))
                                    sa.instruments.Add(ta.instrumentName);
                            }
                        }
                    }
                }
            }
        }


        foreach (TimelineTrack ta in reactionalTrack.m_timelineTracks)
        {
            if (!reactionalTrack.m_instrumentList.Contains(ta.instrumentName))
                reactionalTrack.m_instrumentList.Add(ta.instrumentName);
        }

        while (reactionalTrack.m_samplers.Count < reactionalTrack.m_instrumentList.Count)
        {
            reactionalTrack.m_samplers.Add(null);
        }


        for (int i = 0; i < reactionalTrack.m_timelineTracks.Count; i++)
        {
            reactionalTrack.m_instrumentation.Add(0);
            var pick = reactionalTrack.m_timelineTracks[i].instrumentName;
            int pos = reactionalTrack.m_instrumentList.IndexOf(pick);
            reactionalTrack.m_instrumentation[i] = pos;//reactionalTrack.m_samplers[pos];
        }

        if (reactionalTrack.m_altInstrumentations.Count == 0)
        {
            ReactionalTrack.InstrumentationAlternatives instrumentationAlternatives = new ReactionalTrack.InstrumentationAlternatives();
            instrumentationAlternatives.instrumentation = reactionalTrack.m_instrumentation;
            reactionalTrack.m_altInstrumentations.Add(instrumentationAlternatives);
        }

    }
    float remap(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    int NoteNameToOffset(string n)
    {
        n = n.ToLower();
        int pos;

        string[] names = { "c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b" };
        string[] namesAlt = { "c", "db", "d", "eb", "e", "f", "gb", "g", "ab", "a", "bb", "b" };
        if (System.Array.Exists(names, element => element == n))
            pos = System.Array.IndexOf(names, n);
        else
            pos = System.Array.IndexOf(namesAlt, n);

        return pos;
    }

}
#endif