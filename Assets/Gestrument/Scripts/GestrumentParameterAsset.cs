﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Advanced Parameter", menuName = "Gestrument/Advanced Parameter", order = 0)]
public class GestrumentParameterAsset : ScriptableObject
{
    public string parameterName;
    public List<GestrumentSubParameter> gestrumentSubParameters;
}