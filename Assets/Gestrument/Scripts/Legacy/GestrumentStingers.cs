﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestrumentStingers : MonoBehaviour
{
    // Start is called before the first frame update
    public ReactionalUIHelper timeline;
    public List<CursorSequenceAsset> cursorSequenceAssets;
    public int CursorNum = 0;
    public CursorSequenceAsset activeSequenceAssets;

    double beatOffset = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ScheduleRandomStinger()
    {
        CursorSequenceAsset temp;
        temp = Instantiate(cursorSequenceAssets[Random.Range(0, cursorSequenceAssets.Count - 1)]);
        activeSequenceAssets = null;
        
        activeSequenceAssets = temp;
        beatOffset = Mathf.Ceil((float)timeline.beat / 1) * 1;
    }


    //private void OnAudioFilterRead(float[] data, int channels)
    //{

    //    if (activeSequenceAssets == null) return;
    //    if (activeSequenceAssets.cursorArrayContainer[0].steps.Count == 0)
    //    {
    //        activeSequenceAssets = null;
    //        return;
    //    }

    //    var st = activeSequenceAssets.cursorArrayContainer[0].steps;
    //    for (int i = 0; i < st.Count; i++)
    //    {
    //        var push = 0f;
    //        var b = st[i].pos[0] + beatOffset; //+ cs[sq].beatPosition - sections[s].startBeat;

    //        int g = (int)st[i].pos[3];
    //        var x = st[i].pos[1];
    //        var y = st[i].pos[2];

    //            if (st[i].pos[5] == 1)
    //            {
    //            st[i].pos[5] = 0;
    //            Debug.Log(b);
    //            if (i > 1 && g == 0 && i < st.Count-1)
    //                    if (st[i].pos[2] != st[i - 1].pos[2] && st[i + 1].pos[3] != 1) // Legato overlap hack
    //                        continue;

    //                double offset = 0;
    //                if (timeline.core.engine.GetCurrentBeat() > timeline.beat)
    //                    offset = timeline.core.engine.GetCurrentBeat() - (timeline.beat);

                

    //                timeline.ScheduleCursor(CursorNum, b + offset - push, g, x, y);
                    
    //            }
    //       // activeSequenceAssets.cursorArrayContainer[0].steps.RemoveAt(i);

    //    }

    //}
}
