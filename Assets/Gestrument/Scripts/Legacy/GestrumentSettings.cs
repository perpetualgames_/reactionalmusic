﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Settings", menuName = "Gestrument/Gestrument Settings", order = 1)]



public class GestrumentSettings : ScriptableObject
{
    public string presetFileName;
    public Object presetFile;
    //TODO - add all relevant settings for a preset here.

    public int numCursors = 1;
    public int numInstruments = 1;

    public int totalPolyphony = 16;
    public List<GameObject> instruments;
    public bool playMidiOnStart;
    public bool autoPlayPreset;
    public Object[] midiFile;
    public string[] midiFileName;
    public List<InstrumentSettings> instrumentSettings;



    private void OnValidate()
    {
        if (presetFile && presetFileName == null || presetFile && presetFileName == "")
            presetFileName = presetFile.name;

        if (midiFile.Length > 0 && midiFileName.Length <= 0)
        {
            midiFileName = new string[midiFile.Length];

            while (instrumentSettings.Count < midiFile.Length)
            {
                instrumentSettings.Add(new InstrumentSettings());
            }
            for (int i = 0; i < midiFile.Length; i++)
            {
                midiFileName[i] = midiFile[i].name;
                instrumentSettings[i].midiFileName = midiFileName[i];
            }
        }

        if (instrumentSettings.Count != numInstruments)
        {
            var currentCount = instrumentSettings.Count;
            if (instrumentSettings.Count > numInstruments)
                instrumentSettings.RemoveAt(instrumentSettings.Count - (currentCount - numInstruments));
            else
                for (int i = 0; i < numInstruments - currentCount; i++)
                {
                    instrumentSettings.Add(new InstrumentSettings());

                }
        }
    }
}

