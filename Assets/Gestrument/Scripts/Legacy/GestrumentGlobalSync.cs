﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using GE_Core;
using Gestrument;
using UnityOSC;
    
public class GestrumentGlobalSync : MonoBehaviour
{
    static GestrumentGlobalSync instance; 
    static bool hasLookedForInstance = false;

    public GestrumentCore[] Cores;

    public GestrumentCore currentCore;
    public Engine engine;

    public Music currentMusic;

    public bool testTimingStuff = true;

    public GestrumentChords chords;

    OSCReciever oscReciever;

    public enum Music
    {
        _1,
        _2
    };

    // SET UP 
    void Start()
    {
        for (int i = 0; i < Cores.Length; i++)
        {
            if (i == 0)
            {
                Cores[0].GetComponent<GestrumentOrchestrator>().enabled = true;
                currentCore = Cores[0];
                engine = currentCore.GetComponent<GestrumentOrchestrator>().core.engine;
            }
            else
                Cores[i].GetComponent<GestrumentOrchestrator>().enabled = false;
        }

        // oscReciever = new OSCReciever();
        // oscReciever.Open(9000);
    }

    static GestrumentGlobalSync TryFindInstance()
    {
        if (hasLookedForInstance)
            return instance;
        hasLookedForInstance = true;
        instance = FindObjectOfType<GestrumentGlobalSync>();
        return instance;
    }

    

    private void Update()
    {

        // OSCPacket packet;
        // int count = 0;
        // while (oscReciever.hasWaitingMessages())
        // {

        //     packet = oscReciever.getNextMessage();
        //     if (packet != null && count < 1024)
        //     {

        //         //for (int i = 0; i < packet.Data.Count; i++)
        //         //{
        //         //    Debug.Log(packet.Data[i]);
        //         //}

        //         if (packet.Address == "/arr")
        //             SwtichEngine(false);

        //         if (packet.Address == "/ravel")
        //             SwtichEngine(true);

        //         if (packet.Address == "/mute")
        //             currentCore.GetComponentInChildren<GestrumentAudioSync>().GetComponent<AudioSource>().Stop();

        //         engine.Enqueue(packet.BinaryData);
        //         count++;
        //     }
        // }
    }

    private void OnApplicationQuit()
    {
        //oscReciever.Close();
    }


    //  ------------------------------------------------------------------------------------------ CHANGE MUSIC ENGINE PLAYING
    public static void SetMusic(Music music, float stopDelay, float startDelay)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;

        ResetAllSliders();
        inst.engine.SetPlaying(false);

        switch (music)
        {
            case Music._1:
                inst.StartCoroutine(inst.ChangeMusicWithDelay(0, stopDelay, startDelay));
                break;
            case Music._2:
                inst.StartCoroutine(inst.ChangeMusicWithDelay(1, stopDelay, startDelay));
                break;
        }
    }

    IEnumerator ChangeMusicWithDelay(int coreInstance, float stopDelay, float startDelay)
    {
        yield return new WaitForSeconds(stopDelay);
        var beat = engine.GetCurrentBeat();
        currentCore.GetComponent<GestrumentOrchestrator>().enabled = false;
  //      currentCore.GetComponentInChildren<GestrumentCursor>().GetComponent<AudioSource>().Stop();
        currentCore = Cores[coreInstance];

        yield return new WaitForSeconds(startDelay);
        currentCore.GetComponent<GestrumentOrchestrator>().enabled = true;
        engine = currentCore.GetComponent<GestrumentOrchestrator>().core.engine;
        engine.SetPlaying(true);
       // chords.gO = currentCore.GetComponent<GestrumentOrchestrator>();
        //GestrumentGlobalSync.NextChord();
        engine.SetCurrentBeat(beat);
 //       currentCore.GetComponentInChildren<GestrumentCursor>().GetComponent<AudioSource>().Play();

    }

    public void SwtichEngine(bool toggle)
    {

        if (toggle)
        {
            SetMusic(Music._1, 0, 0);

        } else
        {
            SetMusic(Music._2, 0, 0);
        }
    }


    // ------------------------------------------------------------------------------------------ CONTROL CURSOR

    public static void MoveCursor(int cursorNum, Vector3 from, Vector3 to, float time)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;

        inst.StartCoroutine(inst.LerpCursor(cursorNum, from, to, time));
    }

    public IEnumerator LerpCursor(int cursorNum, Vector3 from, Vector3 to, float time)
    {
        var t = 0f;
        UpdateCursorPlayer(cursorNum, 0, 0, 0, false);
        while (t < 1)
        {
            t += Time.deltaTime / time;
            var lerp = Vector3.Lerp(from, to, t);
            UpdateCursorPlayer(cursorNum, lerp.x, lerp.y, lerp.z, true);
            yield return null;
        }
        UpdateCursorPlayer(cursorNum, 0, 0, 0, false);
    }

    void UpdateCursorPlayer(int curs, float x, float y, float z, bool gate)
    {
        engine.SetCursorX(curs, x);
        engine.SetCursorY(curs, y);
        engine.SetCursorZ(curs, z);
        engine.SetCursorGate(curs, gate);
    }



    // ------------------------------------------------------------------------------------------ CONTROL SLIDERS 

    public static void SliderControl(int sliderNumber, float value)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        inst.engine.SetSliderValue(sliderNumber, value);
    }

    public static void SliderControlSmooth(int sliderNumber, float value, float smoothness)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        value = (float)(inst.engine.GetSliderValue(sliderNumber) * (1 - smoothness)) + (value * smoothness);
        inst.engine.SetSliderValue(sliderNumber, value);
    }

    public static void SliderControlNextDivision(int sliderNumber, float value, float subdiv)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;

        inst.StartCoroutine(inst.SliderWaitForBeat(sliderNumber, value, subdiv));
    }

    public IEnumerator SliderWaitForBeat(int sliderNumber, float value, float subdiv)
    {
        var beat = engine.GetCurrentBeat();
        var mod = (4 * (1 / subdiv));
        while (beat % mod < (mod * 0.9f))
        {
            beat = engine.GetCurrentBeat();
            yield return new WaitForEndOfFrame();
        }
        engine.SetSliderValue(sliderNumber, value);
    }

    public static void SliderControlOverTime(int sliderNumber, float from, float value, float time)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        inst.StartCoroutine(inst.SliderOverTime(sliderNumber, from, value, time));
    }

    IEnumerator SliderOverTime(int sliderNumber, float from, float value, float time)
    {
        var t = 0f;

        while (t < 1)
        {
            t += Time.deltaTime / time;
            var lerp = Mathf.Lerp((float)from, value, t);
            engine.SetSliderValue(sliderNumber, lerp);

            yield return null;
        }
    }

    public static void ResetAllSliders()
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        for (int i = 0; i < 7; i++)
        {
            inst.engine.SetSliderValue(i, 0);
        }
    }

    // ------------------------------------------------------------------------------------------ CONTROL TEMPO

    public static void TempoControl(float from, float to, float time)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        //value = AudioManager.Remap(value,0f, 1f, tempoMin, tempoMax);
        inst.StartCoroutine(inst.SetTempoOverTime(from, to, time));

    }

    IEnumerator SetTempoOverTime(float from, float to, float time)
    {
        var t = 0f;

        while (t < 1)
        {
            t += Time.deltaTime / time;
            var lerp = Mathf.Lerp(from, to, t);
            engine.SetTempo((double)lerp);
            //Debug.Log(engine.GetTempo());
            yield return null;
        }
    }

    public static double GetTempo()
    {
        var inst = TryFindInstance();
        if (inst == null)
            return -1;
        //value = AudioManager.Remap(value,0f, 1f, tempoMin, tempoMax);
        var tempo = inst.engine.GetTempo();
        return tempo;
    }

    public static double GetCurrentBeat()
    {
        var inst = TryFindInstance();
        if (inst == null)
            return 0;
        var beat = inst.engine.GetCurrentBeat();
        return beat;
    }


    // ------------------------------------------------------------------------------------------ CHORD CONTROL

    public static void NextChord()
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        //value = AudioManager.Remap(value,0f, 1f, tempoMin, tempoMax);
        inst.currentCore.GetComponent<GestrumentOrchestrator>().chords.NextChord();
    }

    public void NewChord()
    {
        currentCore.GetComponent<GestrumentOrchestrator>().chords.NextChord();
    }

    public static void NextChordAtNextDiv(float subdiv)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;
        //value = AudioManager.Remap(value,0f, 1f, tempoMin, tempoMax);
        inst.StartCoroutine(inst.NextChordAtDiv(subdiv));
    }

    IEnumerator NextChordAtDiv(float subdiv)
    {
        var beat = engine.GetCurrentBeat();
        var mod = (4 * (1 / subdiv));
        while (beat % mod < (mod * 0.9f))
        {
            beat = engine.GetCurrentBeat();
            yield return new WaitForEndOfFrame();
        }
        GestrumentGlobalSync.NextChord();
    }


    // ------------------------------------------------------------------------------------------ INSTRUMENTATION AND ORHCESTRATION CONTROL

    public static void ChangeInstrumentRouting(int instrument, int destination)
    {
        var inst = TryFindInstance();
        if (inst == null)
            return;

        var gO = inst.currentCore.GetComponent<GestrumentOrchestrator>();
        gO.instrumentSettings[instrument].output = destination;

    }




    // ------------------------------------------------------------------------------------------ SHOULD GO ELSEWHERE 
    public void SetSlider2(int val)
    {
        GestrumentGlobalSync.SliderControl(2, val);

    }

    public void SpecialEvent()
    {
        GestrumentGlobalSync.SliderControlOverTime(1, 0, 0.1f, 4);
        GestrumentGlobalSync.MoveCursor(6, new Vector3(0.9f, 0, 1), new Vector3(1, 1, 1), 0.5f);
        //GestrumentGlobalSync.SliderControlOverTime(0,1f,0,1f);
        GestrumentGlobalSync.MoveCursor(5, new Vector3(0.9f, 0, 1), new Vector3(1, 0.2f, 1), 0.5f);
    }

    public void Part2()
    {
        GestrumentGlobalSync.SliderControl(2, 1);
        GestrumentGlobalSync.MoveCursor(5, new Vector3(1f, 0.185f, 1), new Vector3(1, 0.21f, 1), 0.4f);
        //GestrumentGlobalSync.SliderControlOverTime(0, 1f, 0, 1f);
    }

}
