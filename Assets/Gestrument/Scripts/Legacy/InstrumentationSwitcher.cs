﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentationSwitcher : MonoBehaviour
{

    public GestrumentOrchestrator gestrumentOrchestrator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeInstrumentation()
    {
        gestrumentOrchestrator.manualInstrumentRouting[13] = 16;
    }


    public void Sneak(bool value)
    {
        if (value == true)
        {
            StartCoroutine(QuantizedInstrumentation(value));
        }
        else
        {
            StartCoroutine(QuantizedInstrumentation(value));
        }
    }


    IEnumerator QuantizedInstrumentation(bool value)
    {
        yield return new WaitForDivision(gestrumentOrchestrator.core,4);
        if (value == true)
        {
            gestrumentOrchestrator.instrumentSettings[11].output = 7;
            gestrumentOrchestrator.instrumentSettings[12].output = 7;
            gestrumentOrchestrator.instrumentSettings[13].output = 7;
            gestrumentOrchestrator.instrumentSettings[14].output = 7;

            gestrumentOrchestrator.core.engine.SetParamValue(Gestrument.Engine.EventObject.PulseGen, 0, "velocity", 0.3f);
            gestrumentOrchestrator.core.engine.SetParamValue(Gestrument.Engine.EventObject.PulseGen, 1, "velocity", 0.3f);
            gestrumentOrchestrator.core.engine.SetParamValue(Gestrument.Engine.EventObject.PulseGen, 2, "velocity", 0.3f);
        }
        else
        {
            gestrumentOrchestrator.instrumentSettings[11].output = 11;
            gestrumentOrchestrator.instrumentSettings[12].output = 12;
            gestrumentOrchestrator.instrumentSettings[13].output = 13;
            gestrumentOrchestrator.instrumentSettings[14].output = 14;

            gestrumentOrchestrator.core.engine.SetParamValue(Gestrument.Engine.EventObject.PulseGen, 0, "velocity", 1f);
            gestrumentOrchestrator.core.engine.SetParamValue(Gestrument.Engine.EventObject.PulseGen, 1, "velocity", 1f);
            gestrumentOrchestrator.core.engine.SetParamValue(Gestrument.Engine.EventObject.PulseGen, 2, "velocity", 1f);
        }

    }

}
