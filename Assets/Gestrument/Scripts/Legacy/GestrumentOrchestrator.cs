﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gestrument;


public enum PulsePlayMode { None, Always, GateOn, GateStep };
public enum PulseGenType { DefaultPulseGen, PatternPulseGen, SeqPulseGen };
public enum PitchGenType { DefaultPitchGen, RelPitchGen, SeqPitchGen };


[System.Serializable]
public class Cursors
{
    public bool gateOn;
    [Range(0, 1)]
    public double Xpos;
    [Range(0, 1)]
    public double Ypos;
    [Range(0, 1)]
    public double Zpos;
}

[System.Serializable]
public class InstrumentSettings
{
    public int cursor;
    public int output = 0;
    public bool legato = false;
    public PulsePlayMode pulsePlayMode = PulsePlayMode.GateOn;
    public PulseGenType PulseGen = PulseGenType.DefaultPulseGen;
    public PitchGenType PitchGen = PitchGenType.DefaultPitchGen;
    public string midiFileName;
    public bool[] noteValues = new bool[8];
}

public class GestrumentOrchestrator : MonoBehaviour
{
    [HideInInspector]
    [SerializeField]
    public GestrumentCore core;

    //[Header("Presets")]
    public GestrumentSettings settings;                 // SETTINGS ARE RELOADED ON OnValidate(), Sampler instrument assignment only realoded if set to 0
    
    public float tempo = 120;

    //public bool reloadSettings = true;
    public int cursorCount = 8;
    public int instrumentCount = 8;

    public int totalPolyphony = 16;

    public List<GameObject> instruments;
    List<GestrumentSampler> samplers = new List<GestrumentSampler>();
    public int[] manualInstrumentRouting;

   // [HideInInspector]
    public List<InstrumentSettings> instrumentSettings;

    //[Header("Presets")]
   [HideInInspector]
    public bool autoPlayPreset;
    [HideInInspector]
    public Object preset;
    [HideInInspector]
    public string presetName;
    [HideInInspector]
    public bool AutoLoadInstrumentsBasedOnName = false;

    //[Header("MIDI Loops")]
    [HideInInspector]
    public bool autoPlayMidiFiles;
    [HideInInspector]
    public Object[] midiFile;
    [HideInInspector]
    public string[] midiFileName;
    [HideInInspector]
    public int[] manualMidiRouting;

    [Header("Performance")]

    [Range(0, 1)]
    public List<double> sliders = new List<double>(10);
    public List<Cursors> cursors = new List<Cursors>();

    string pathToPreset;

    bool bypassValuesUpdate = false;

    double oldDspTime;

    [HideInInspector]
    public bool waitforstart = false;

    [HideInInspector]
    public GestrumentChords chords;

    void Start()
    {
        for (int i = 0; i < instruments.Count; i++)
        {
            samplers.Add(null);
            if (instruments[i] == null) continue;
            if (instruments[i].gameObject.scene.name == null)
            { }
            instruments[i] = Instantiate(instruments[i]);
            instruments[i].transform.parent = gameObject.transform;
            samplers[i] = instruments[i].GetComponent<GestrumentSampler>();

            if (totalPolyphony != -1) instruments[i].GetComponent<GestrumentSampler>().polyphony = totalPolyphony / instruments.Count;
        }

        //core.NewNoteOn += ProcessAudioEvent; 

        StartCoroutine(WaitForStart());
    }

    IEnumerator WaitForStart()
    {
        yield return new WaitForSeconds(3f);
        waitforstart = false;
    }

    public void ProcessConfigEvent(GestrumentCore core)
    {
        this.core = core;
        Debug.Log("Configuring engine");
        Engine engine = core.engine;

        engine.NumCursors = cursorCount;
        engine.NumInstruments = instrumentCount;
        engine.SetParamValue(Engine.EventObject.Engine, 0, "numcursors", cursorCount);
        engine.SetParamValue(Engine.EventObject.Engine, 0, "numinstruments", instrumentCount);

        int numcursors;
        engine.GetParamValue(Engine.EventObject.Engine, 0, "numcursors", out numcursors);

        if (settings)
        {
            LoadSettingsFileSetup();
        }
        else
        {
            for (int i = 0; i < numcursors; i++)
            {
                cursors.Add(new Cursors());
                engine.SetInstrumentCursor(i, i);
            }
        }

        if (autoPlayPreset)
        {
            LoadDefaultPreset();
            //core.engine.SetPlaying(true);
        }

        for (int i = 0; i < 10; i++)
            sliders.Add(engine.GetSliderValue(i));

        //engine.SetPlaying(true);
        engine.SetTempo(tempo);
    }

    public void LoadSettingsFileSetup()
    {
        if (settings)
        {
            for (int i = 0; i < cursorCount; i++)
            {
                cursors.Add(new Cursors());
            }
            for (int i = 0; i < settings.instrumentSettings.Count; i++)
            {
                var getSet = settings.instrumentSettings[i];

                core.engine.SetParamValue(Engine.EventObject.Instrument, i, "cursor", getSet.cursor);
                core.engine.SetInstrumentCursor(i, getSet.cursor);

                core.engine.SetPulseGen(i, getSet.PulseGen.ToString());
                core.engine.SetPitchGen(i, getSet.PitchGen.ToString());

                core.engine.SetPulsePlayMode(i, getSet.pulsePlayMode.ToString());
                //core.engine.SetInputValue(i, GE_Control.GenType.Pulse, 1, getSet.legato ? 1 : 0);
                if (getSet.midiFileName != "")
                {
                    var midiPath = Application.streamingAssetsPath + "/Gestrument/MIDI/" + getSet.midiFileName + ".mid";
                    core.engine.LoadPulseMIDI(i, midiPath);
                    core.engine.LoadPitchMIDI(i, midiPath);
                    //core.engine.SetInputSource(i, GE_Control.GenType.Pitch, "ctranspose", "Slider 8");
                }
            }
        }
    }


    public void LoadDefaultPreset()
    {
        if (settings)
            presetName = settings.presetFileName;

        if (presetName == "") return;

        pathToPreset = Application.streamingAssetsPath + "/Gestrument/Presets/" + presetName + ".g2preset";
        core.engine.LoadG2Preset(pathToPreset);

        if (core.engine.NumCursors > cursors.Count)
        {
            var addCount = core.engine.NumCursors - cursors.Count;
            for (int i = 0; i < addCount; i++)
                cursors.Add(new Cursors());
        }
        else
        {
            core.engine.NumCursors = cursors.Count;
        }

        if (settings.instrumentSettings.Count > core.engine.NumInstruments)
        {
            var offset = core.engine.NumInstruments;
            core.engine.NumInstruments = settings.instrumentSettings.Count;
            LoadExtraIfPreset(offset);
        }

        cursorCount = cursors.Count;
        instrumentCount = core.engine.NumInstruments;

    }

    public void LoadExtraIfPreset(int offset)  // --------------------------------------------------------------------- Load extra additional settings for cursors etc, even if a preset is used.
    {
        if (settings)
        {
            for (int i = offset; i < settings.instrumentSettings.Count; i++)
            {
                var getSet = settings.instrumentSettings[i];
                core.engine.SetInstrumentCursor(i, getSet.cursor);
                core.engine.SetPulseGen(i, getSet.PulseGen.ToString());
                core.engine.SetPitchGen(i, getSet.PitchGen.ToString());
                core.engine.SetPulsePlayMode(i, getSet.pulsePlayMode.ToString());
                //core.engine.SetInputValue(i, GE_Control.GenType.Pulse, 1, getSet.legato ? 1 : 0);
                if (getSet.midiFileName != "")
                {
                    var midiPath = Application.streamingAssetsPath + "/Gestrument/MIDI/" + getSet.midiFileName + ".mid";
                    core.engine.LoadPulseMIDI(i, midiPath);
                    core.engine.LoadPitchMIDI(i, midiPath);
                  //  core.engine.SetInputSource(i, GE_Control.GenType.Pitch, "ctranspose", "Slider 8");
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------------- Hacky way to display data and reload settings

    void OnValidate()
    {
        if (Application.isPlaying && waitforstart == false)
        {
            bypassValuesUpdate = true;
            for (int i = 0; i < core.engine.NumCursors; i++)
            {
                core.engine.SetCursorGate(i, cursors[i].gateOn);
                core.engine.SetCursorX(i, cursors[i].Xpos);
                core.engine.SetCursorY(i, cursors[i].Ypos);
                core.engine.SetCursorZ(i, cursors[i].Zpos);
            }

            for (int i = 0; i < 10; i++)
            {
                core.engine.SetSliderValue(i, sliders[i]);
            }

            bypassValuesUpdate = false;

        }

        if (settings && Application.isPlaying == false)// && reloadSettings == true)
        {
            UpdateSettingsFileVars();
            //reloadSettings = false;
        }
    }

    public void UpdateSettingsFileVars()
    {
        if (settings)
        {
            if (instruments.Count == 0) instruments = settings.instruments;
            autoPlayMidiFiles = settings.playMidiOnStart;
            midiFileName = settings.midiFileName;
            totalPolyphony = settings.totalPolyphony;
            autoPlayPreset = settings.autoPlayPreset;
            cursorCount = settings.numCursors;
            instrumentCount = settings.numInstruments;
            preset = settings.presetFile;
            manualInstrumentRouting = new int[settings.instrumentSettings.Count];
            for (int i = 0; i < settings.instrumentSettings.Count; i++)
            {
                manualInstrumentRouting[i] = settings.instrumentSettings[i].output;
            }
            instrumentSettings = settings.instrumentSettings;
        }
    }


    // ---------------------------------------------------------------------------------------------------- SAMPLER PLAYBACK

    public bool StartVoice(int voice, float pitch, float velocity, int instrument, double beat)
    {
        int channelAssign;
        if (instrumentSettings.Count > 0 && (instrumentSettings.Count) > instrument)    // Route instrument to ouput if exists
            channelAssign = instrumentSettings[instrument].output;
        else
            channelAssign = instrument;


        if ((channelAssign) > instruments.Count - 1) return false;
        if (instruments[channelAssign] == null) return false;

        samplers[channelAssign].StartVoice(voice, pitch, velocity, instrument, beat);

        return true;
    }

    public bool StopVoice(int voice, double beat, int instrument)
    {
        int channelAssign;
        if (instrumentSettings.Count > 0 && (instrumentSettings.Count) > instrument)
            channelAssign = instrumentSettings[instrument].output;
        else
            channelAssign = instrument;

        if (channelAssign > instruments.Count - 1) return false;
        if (instruments[channelAssign] == null) return false;

        samplers[channelAssign].StopVoice(voice, instrument, beat);

        return true;
    }

    public void ProcessAudioEvent(GestrumentCore.Event engineEvent)
    {
        if (engineEvent is GestrumentCore.NoteOnEvent){
            GestrumentCore.NoteOnEvent noteEvent = (GestrumentCore.NoteOnEvent)engineEvent;
             StartVoice(noteEvent.Voice, noteEvent.Pitch, noteEvent.Velocity, noteEvent.Instrument, noteEvent.Beat);
        }
        else if (engineEvent is GestrumentCore.NoteOffEvent){
            GestrumentCore.NoteOffEvent noteEvent = (GestrumentCore.NoteOffEvent)engineEvent;
            StopVoice(noteEvent.Voice, noteEvent.Beat, noteEvent.Instrument);
        }
            
    }

    //     void OnAudioFilterRead(float[] data, int channels)
    // {
    //     if (core.engine != null && waitforstart == false) {
    //         int size = data.Length / channels;
    //         core.engine.AudioRender(core.sampleRate, size);
    //     }
    // }


    void FixedUpdate()
    {
        if (waitforstart == false)
        {
            for (int i = 0; i < 10; i++)
            {
                sliders[i] = core.engine.GetSliderValue(i);
            }

            for (int i = 0; i < core.engine.NumCursors; i++)
            {
                if (bypassValuesUpdate) break;
                cursors[i].gateOn = core.engine.GetCursorGate(i);
                cursors[i].Xpos = core.engine.GetCursorValue(i,"x");
                cursors[i].Ypos = core.engine.GetCursorValue(i,"y");
                cursors[i].Zpos = core.engine.GetCursorValue(i,"z");
            }

        }
    }

}