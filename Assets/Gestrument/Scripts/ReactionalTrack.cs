﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Reactional Track", menuName = "Gestrument/Reactional Track", order = 0)]
public class ReactionalTrack : ScriptableObject
{
    public string m_trackName;
    public double m_tempo = 120;
    public string m_scale;

    public int m_trackCount;

    public List<TimelineTrack> m_timelineTracks = new List<TimelineTrack>();
    public List<TimelineSectionAsset> m_sections = new List<TimelineSectionAsset>();
    public List<TimelineSectionAsset> m_motifs = new List<TimelineSectionAsset>();
    public List<TimelineSectionAsset> m_stingers = new List<TimelineSectionAsset>();
    public List<CursorSequenceAsset> m_items = new List<CursorSequenceAsset>();
    public List<GestrumentParameterAsset> m_parameters = new List<GestrumentParameterAsset>();


    public List<string> m_instrumentList = new List<string>();
    public List<GameObject> m_samplers = new List<GameObject>();


    public List<int> m_instrumentation = new List<int>();

    public List<InstrumentationAlternatives> m_altInstrumentations = new List<InstrumentationAlternatives>();
    public List<SamplerAlternatives> m_altSamplers = new List<SamplerAlternatives>();

    public List<SetupMessages> m_setupMessages = new List<SetupMessages>();

    public void Reset()
    {
        m_timelineTracks.Clear();
        m_sections.Clear();
        m_motifs.Clear();
        m_stingers.Clear();
        m_items.Clear();
        m_instrumentList.Clear();
        m_setupMessages.Clear();
        m_instrumentation.Clear();
        m_trackCount = 0;
    }

    //void OnValidate()
    //{
    //    if(m_altSamplers.Count == 0 && m_samplers.Count != 0)
    //    {
    //        m_altSamplers.Add(new SamplerAlternatives());
    //        m_altSamplers[0].samplers = m_samplers;
    //    } else
    //    {
    //        m_altSamplers[0].samplers = m_samplers;
    //    }
    //}

    [System.Serializable]
    public class InstrumentationAlternatives {
        public string name;
        public List<int> instrumentation = new List<int>();
    }

    [System.Serializable]
    public class SamplerAlternatives
    {

        public List<GameObject> samplers = new List<GameObject>();
    }


}
