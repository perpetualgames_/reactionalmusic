﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Gestrument;
using System.IO;
using UnityOSC;
using Reactional;

public class GestrumentStateSaveLoad : MonoBehaviour
{

    private string jsonPath;
    private GestrumentCore core;
    private ReactionalPlayer reactionalPlayer;
    private GestrumentStates states = new GestrumentStates();


    void Start()
    {
        jsonPath = Application.persistentDataPath + "/saveload.json";
        if (File.Exists(jsonPath) == false) File.Create(jsonPath);
        string file = File.ReadAllText(jsonPath);
        states = JsonConvert.DeserializeObject<GestrumentStates>(file);
        core = GetComponent<GestrumentCore>();
        reactionalPlayer = GetComponent<ReactionalPlayer>();
    }


    public void SaveState(GestrumentState state)
    {

        if (states == null) states = new GestrumentStates();

        bool exists = false;

        for (int i = 0; i < states.states.Count; i++)
        {
            if (states.states[i].stateName == state.stateName)
            {
                states.states[i] = state;
                exists = true;
            }
        }

        if (!exists)
            states.states.Add(state);

        string jsn = JsonConvert.SerializeObject(states);
        File.WriteAllText(jsonPath, jsn);
    }

    public GestrumentState LoadState(string name)
    {
        jsonPath = Application.persistentDataPath + "/saveload.json";
        string file = File.ReadAllText(jsonPath);
        states = JsonConvert.DeserializeObject<GestrumentStates>(file);

        if (states == null) return null;

        GestrumentState state = new GestrumentState();

        for (int i = 0; i < states.states.Count; i++)
        {
            if (states.states[i].stateName == name)
            {
                state = states.states[i];
            }
        }

        for (int i = 0; i < state.oscMessages.Count; i++)
        {

            // UNSUCCESSFUL 
            //OSCMessage msg = new OSCMessage(state.oscMessages[i].Adress);
            //msg.Address = "/slt/eng";
            //for (int j = 0; j < state.oscMessages[i].Data.Count; j++)
            //{
            //    object obj = state.oscMessages[i].Data[j];
            //    msg.Append(obj);
            //    Debug.Log(state.oscMessages[i].Data[j]);
            //}
            //core.engine.Enqueue(msg.BinaryData);



            //if (message.Address == "/slt/eng")
            //{
            //    message.Append<string>((string)state.oscMessages[i].Data[0]);
            //    message.Append<int>(System.Convert.ToInt32(state.oscMessages[i].Data[1]));
            //    message.Append<float>(System.Convert.ToSingle(state.oscMessages[i].Data[2]));
            //    message.Append<string>((string)state.oscMessages[i].Data[3]);
            //} else
            //{
            //    for (int j = 0; j < state.oscMessages[i].Data.Count; j++)
            //    {
            //        if(j > 0 && state.oscMessages[i].Data[j].GetType().Name == "Double")
            //        {
            //            message.Append<float>(System.Convert.ToSingle(state.oscMessages[i].Data[j]));
            //        } else
            //        {
            //            message.Append(state.oscMessages[i].Data[j]);
            //        }
            //        Debug.Log(state.oscMessages[i].Data[j] + "   " + state.oscMessages[i].Data[j].GetType().ToString());
            //    }
            //}

            core.engine.Enqueue(state.oscMessages[i].bytes);
        }

        return state;
    }

    public void LoadSettings(string name, int division = 4)
    {
        StartCoroutine(LoadCoroutine(name, division));
    }

    IEnumerator LoadCoroutine(string name, int division = 4)
    {
        yield return new WaitForDivision(core, division);
        GestrumentState state = LoadState(name);

        if (state.selectedBools["transp"])
        {
            for (int i = 0; i < reactionalPlayer.m_track.m_trackCount; i++)
            {
                core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "ctranspose", state.ctranspose[i]);
                core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "mtranspose", state.mtranspose[i]);
            }
            core.engine.SetParamValue(Engine.EventObject.Engine, 0, "transposeslotindex", state.transposeslot);
        }

        if (state.selectedBools["instrumentation"])
        {
            reactionalPlayer.instrumentationId = state.instrumentation;
            reactionalPlayer.m_track.m_instrumentation = reactionalPlayer.m_track.m_altInstrumentations[state.instrumentation].instrumentation;
        }

        if (state.selectedBools["section"])
        {
            for (int i = 0; i < reactionalPlayer.m_sections.Count; i++)
            {
                if (reactionalPlayer.m_sections[i].id == state.activeSectionId) reactionalPlayer.m_currentSection = reactionalPlayer.m_sections[i];
                else if (reactionalPlayer.m_sectionsHalf[i].id == state.activeSectionId) reactionalPlayer.m_currentSection = reactionalPlayer.m_sectionsHalf[i];
                else if (reactionalPlayer.m_sectionsDouble[i].id == state.activeSectionId) reactionalPlayer.m_currentSection = reactionalPlayer.m_sectionsDouble[i];
            }
        }

        for (int i = 0; i < state.mutetoggles.Count; i++)
        {
            reactionalPlayer.m_mutetoggles = state.mutetoggles;
            reactionalPlayer.m_core.engine.ScheduleParam(Engine.OSCParam.ins, 0, i, "mute", state.mutetoggles[i]);
        }

        //StartCoroutine(reactionalPlayer.KillCursors(reactionalPlayer.m_currentmotif));
        if (state.selectedBools["motif"])
        {
            if (state.activeMotifId != -1)
            {
                TimelineSectionAsset mot = null;
                for (int i = 0; i < reactionalPlayer.m_motifs.Count; i++)
                {

                    if (reactionalPlayer.m_motifs[i].id == state.activeMotifId) mot = reactionalPlayer.m_motifs[i];
                    else if (reactionalPlayer.m_motifsHalf[i].id == state.activeMotifId) mot = reactionalPlayer.m_motifsHalf[i];
                    else if (reactionalPlayer.m_motifsDouble[i].id == state.activeMotifId) mot = reactionalPlayer.m_motifsDouble[i];
                }

                StartCoroutine(reactionalPlayer.RecallMotif(mot));
            }
        }

    }


    public void SaveSetting(string name, bool _saveAll = false, bool _scale = false, bool _transp = false, bool _tempo = false, bool _section = false, bool _motif = false, bool _instrumentation = false, bool _sliders = false)
    {
        GestrumentState state = new GestrumentState();
        state.stateName = name;

        if (_saveAll)
        {
            state.selectedBools.Add("scale", true);
            state.selectedBools.Add("transp", true);
            state.selectedBools.Add("tempo", true);
            state.selectedBools.Add("section", true);
            state.selectedBools.Add("motif", true);
            state.selectedBools.Add("instrumentation", true);
            state.selectedBools.Add("sliders", true);
        } else
        {
            state.selectedBools.Add("scale", _scale);
            state.selectedBools.Add("transp", _transp);
            state.selectedBools.Add("tempo", _tempo);
            state.selectedBools.Add("section", _section);
            state.selectedBools.Add("motif", _motif);
            state.selectedBools.Add("instrumentation", _instrumentation);
            state.selectedBools.Add("sliders", _sliders);
        }


        if (state.selectedBools["scale"])
        {
            state.AddOSC(core.engine.SetScaleForSlot(0, (float)reactionalPlayer.currentRoot, (string)reactionalPlayer.currentScale));
        }

        //// Save transpose slot
        ///

        if (state.selectedBools["transp"])
        {
            Debug.Log(reactionalPlayer.currentTranspose);
            state.AddOSC(core.engine.ScheduleTransposeSlot(0, reactionalPlayer.currentTranspose + 7));
        }

        core.engine.GetParamValue(Engine.EventObject.Engine, 0, "transposeslotindex", out int ind);
        state.transposeslot = ind;

        for (int i = 0; i < reactionalPlayer.m_track.m_trackCount; i++)
        {
            core.engine.GetParamValue(Engine.EventObject.PitchGen, i, "ctranspose", out int c);
            state.ctranspose.Add(c);
            core.engine.GetParamValue(Engine.EventObject.PitchGen, i, "mtranspose", out int m);
            state.mtranspose.Add(m);
        }


        state.mutetoggles = reactionalPlayer.m_mutetoggles;

        //state.editorVals.Add("transpose", transpose);
        //state.editorVals.Add("velocity", velocity);
        //state.editorVals.Add("tempo", (float)tempo);
        //state.editorVals.Add("root", root);
        // state.editorVals.Add("ctranspose", )


        if (state.selectedBools["sliders"])
        {
            for (int i = 1; i < 8; i++)
                state.AddOSC(core.engine.ScheduleParam(Engine.OSCParam.eng, 0, 0, "slider" + i, core.engine.GetSliderValue(i)));
        }


            //Tempo
            core.engine.GetParamValue(Engine.EventObject.Engine, 0, "tempo", out double tem);
            state.tempo = tem;
        
        
            state.activeSectionId = reactionalPlayer.m_currentSection.id;

        
            if (reactionalPlayer.m_currentmotif) state.activeMotifId = reactionalPlayer.m_currentmotif.id;

        
            state.instrumentation = reactionalPlayer.instrumentationId;

        SaveState(state);

    }
}


[System.Serializable]
public class GestrumentStates
{
    public List<GestrumentState> states = new List<GestrumentState>();
}


[System.Serializable]
public class GestrumentState{

    public string stateName;
    public List<OSCSimplified> oscMessages = new List<OSCSimplified>();
    public Dictionary<string, float> editorVals = new Dictionary<string, float>();
    public Dictionary<string, bool> editorBools = new Dictionary<string, bool>();
    public Dictionary<string, bool> selectedBools = new Dictionary<string, bool>();

    public int activeSectionId;
    public int activeMotifId = -1;
    public List<int> ctranspose = new List<int>();
    public List<int> mtranspose = new List<int>();
    public List<int> mutetoggles = new List<int>();
    public int transposeslot;
    public int instrumentation;
    public double tempo;

    public void AddOSC(byte[] bytes)
    {
        OSCPacket packet;
        OSCSimplified simplified = new OSCSimplified();
        packet = OSCPacket.Unpack(bytes);
        simplified.Data = packet.Data;
        simplified.Adress = packet.Address;
        simplified.bytes = bytes;
        oscMessages.Add(simplified);
    }
}


[System.Serializable]
public class OSCSimplified
{
    public string Adress;
    public List<object> Data;
    public byte[] bytes;

}

