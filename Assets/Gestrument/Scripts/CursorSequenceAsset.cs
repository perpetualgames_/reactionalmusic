﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



[CreateAssetMenu(fileName = "CursorSequence", menuName = "Gestrument/CursorSequence", order = 10)]

public class CursorSequenceAsset : ScriptableObject
{
    public double beatPosition = 0;
    public double beatLength = 0;
    public double minpitch = 1;
    public double maxpitch = 0;
    public int trackNumber = 0;
    public List<GestrumentCursorArrayContainer> cursorArrayContainer = new List<GestrumentCursorArrayContainer>();

}

 