﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Gestrument;

namespace Reactional
{
    [RequireComponent(typeof(GestrumentCore))]
    public class ReactionalPlayer : MonoBehaviour
    {
        public GestrumentCore m_core;

        [SerializeField]
        public ReactionalTrack m_track;

        private List<GameObject> m_instrumentObjects = new List<GameObject>();
        public List<int> m_mutetoggles = new List<int>();

        [SerializeField]
        public List<GestrumentSampler> m_samplers = new List<GestrumentSampler>();

        [SerializeField]
        public List<TimelineSectionAsset> m_sections = new List<TimelineSectionAsset>();
        public List<TimelineSectionAsset> m_sectionsHalf = new List<TimelineSectionAsset>();
        public List<TimelineSectionAsset> m_sectionsDouble = new List<TimelineSectionAsset>();

        [SerializeField]
        public List<TimelineSectionAsset> m_motifs = new List<TimelineSectionAsset>();
        public List<TimelineSectionAsset> m_motifsHalf = new List<TimelineSectionAsset>();
        public List<TimelineSectionAsset> m_motifsDouble = new List<TimelineSectionAsset>();

        [SerializeField]
        public List<TimelineSectionAsset> m_stingers = new List<TimelineSectionAsset>();
        public List<TimelineSectionAsset> m_stingersHalf = new List<TimelineSectionAsset>();
        public List<TimelineSectionAsset> m_stingersDouble = new List<TimelineSectionAsset>();

        [SerializeField]
        public TimelineSectionAsset m_currentmotif;

        [SerializeField]
        public TimelineSectionAsset m_currentStinger;

        [SerializeField]
        public TimelineSectionAsset m_currentSection;
        private TimelineSectionAsset m_prevSection;

        public List<CursorSequenceAsset> m_availablePatterns = new List<CursorSequenceAsset>();
        public List<CursorSequenceAsset> m_activePatterns = new List<CursorSequenceAsset>();

        private bool m_play = false;
        public double m_beat;

        [SerializeField]
        public bool autoPlay = false;
        public double m_adjustedBeat = 0;
        private double m_motifAdjustedBeat = 0;

        public bool m_quant = false;
        public float m_quantVal = 4;
        public float m_speedscale = 1;

        public int m_activeId;
        public int instrumentationId = 0;
        public double m_processRange = 1;


        public double x = 1;
        public AudioMixer mixer;

        [HideInInspector]
        public string currentScale;
        [HideInInspector]
        public int currentRoot;
        [HideInInspector]
        public int currentTranspose;
        private bool tripletfix = true;

        public bool m_flipRange;

        // ---------------------------------------------------------------------------------------------------- SETUP

        private void Awake()
        {
            m_core = GetComponent<GestrumentCore>();

            m_core.OnConfig.AddListener(Setup);
            m_core.OnNoteOn.AddListener(ProcessAudioEvent);
            m_core.OnNoteOff.AddListener(ProcessAudioEvent);

            m_track = Instantiate(m_track);
            m_sections = m_track.m_sections;
            m_motifs = m_track.m_motifs;
            m_stingers = m_track.m_stingers;

            for (int i = 0; i < m_track.m_sections.Count; i++)
            {
                CopySection(m_sections[i]);
                m_sectionsHalf.Add(ScaleSpeed(m_sections[i], 0.5f));
                m_sectionsHalf[i].id = m_sections[i].id + 100;
                m_sectionsDouble.Add(ScaleSpeed(m_sections[i], 2f));
                m_sectionsDouble[i].id = m_sections[i].id + 200;
            }
            m_currentSection = m_sections[0];


            for (int i = 0; i < m_track.m_motifs.Count; i++)
            {
                CopySection(m_motifs[i]);
                m_motifsHalf.Add(ScaleSpeed(m_motifs[i], 0.5f));
                m_motifsHalf[i].id = m_motifs[i].id + 100;
                m_motifsDouble.Add(ScaleSpeed(m_motifs[i], 2));
                m_motifsDouble[i].id = m_motifs[i].id + 200;
            }

            for (int i = 0; i < m_track.m_stingers.Count; i++)
            {
                CopySection(m_stingers[i]);
                m_stingersHalf.Add(ScaleSpeed(m_stingers[i], 0.5f));
                m_stingersDouble.Add(ScaleSpeed(m_stingers[i], 2));
            }

            for (int i = 0; i < m_track.m_samplers.Count; i++)
            {
                if (m_track.m_samplers[i] != null)
                {
                    GameObject obj = Instantiate(m_track.m_samplers[i], gameObject.transform);
                    m_instrumentObjects.Add(obj);
                    m_samplers.Add(obj.GetComponent<GestrumentSampler>());

                    if (mixer)
                    {
                        var mixergroups = mixer.FindMatchingGroups(string.Empty);
                        obj.GetComponent<AudioSource>().outputAudioMixerGroup = mixergroups[i + 2]; // +2 offset (Master and Reverb)
                        if (m_track.m_instrumentList.Count > i)
                            mixergroups[i + 2].name = m_track.m_instrumentList[i];
                    }

                }
                else
                {
                    m_instrumentObjects.Add(null);
                    m_samplers.Add(null);
                }

            }

            for (int i = 0; i < m_track.m_trackCount; i++)
            {
                m_mutetoggles.Add(0);
            }
        }

        public void Setup(GestrumentCore.ConfigEvent configEvent)
        {
            m_core.engine.NumCursors = m_track.m_trackCount;
            m_core.engine.NumInstruments = m_track.m_trackCount;

            m_core.engine.SetTempo(m_track.m_tempo);


            for (int i = 0; i < 16; i++)
                m_core.engine.SetValueForTransposeSlot(i, i - 7);

            m_core.engine.ScheduleTransposeSlot(-1, 7);


            for (int i = 0; i < m_track.m_trackCount; i++)
            {
                if (tripletfix)
                    m_core.engine.ScheduleParam(Engine.OSCParam.pul, 0, i, "durationstrings", "1/1 1/2 1/4 1/8 1/16 1/32 1/1. 1/2. 1/4. 1/8. 1/16. 1/32. 1/1T 1/2T 1/4T 1/8T 1/16T 1/32T");
                else
                    m_core.engine.ScheduleParam(Engine.OSCParam.pul, 0, i, "durationstrings", "1/1 1/2 1/4 1/8 1/16 1/32 1/1. 1/2. 1/4. 1/8. 1/16. 1/32. 1/1T 1/2T 1/4T 1/8T 1/16T 1/32");
                m_core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "minpitch", 0);
                m_core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "maxpitch", 127);
                m_core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "forcetranstrig", 1);
                m_core.engine.SetInstrumentCursor(i, i);
                // m_core.engine.Map("/map/eng", 0, "Slider 1", "pul", i, "velocity");

                m_core.engine.Map("/map/cur", i, "Cursor Z", "pul", i, "velocity");
                // m_core.engine.Map("/mapv/eng", 0, "Slider 1", "pul", i, "velocity");
                //m_core.engine.Map("/map/eng", 0, "Slider 1", "pul", i, "velocity");
                //m_core.engine.Map("/mapv/cur", i, "Cursor Z", "eng", 0, "Slider 1");
                m_core.engine.SetParamValue(Engine.EventObject.PulseGen, i, "selecteddurations", "ttttttffffffffffft");

                if (m_track.m_timelineTracks[i].isPercussion)
                {
                    m_core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "uselocalscale", true);
                    m_core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "localscale", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]");
                    //m_core.engine.SetParamValue(Engine.EventObject.PulseGen, i, "selecteddurations", "tttttffffffffffftf");
                }
            }

            m_core.engine.SetPlaying(false);
            StartCoroutine(DelayedStart());
        }


        IEnumerator DelayedStart()
        {
            for (int i = 0; i < m_track.m_setupMessages.Count; i++)
            {
                m_core.engine.Enqueue(m_track.m_setupMessages[i].data);
            }

            m_core.engine.SetCurrentBeat(-2);
            yield return new WaitForSeconds(3);

            m_core.engine.SetPlaying(true);
            m_play = true;
        }

        // ---------------------------------------------------------------------------------------------------- DATA PLAYBACK

        public void OnAudioFilterRead(float[] data, int channels)
        {

            if (m_play)
            {
                if (m_prevSection != m_currentSection)
                {
                    for (int i = 0; i < m_track.m_trackCount; i++)
                    {
                        m_core.engine.SetCursorGate(i, false);
                    }
                    m_prevSection = m_currentSection;
                }

                double coreCurrentBeat = m_core.engine.GetCurrentBeat();

                for (int s = 0; s < m_sections.Count; s++)
                {
                    if (!autoPlay)
                    {
                        m_beat = ((coreCurrentBeat) - m_adjustedBeat) % (m_currentSection.stopBeat - m_currentSection.startBeat);
                        m_activeId = m_currentSection.id;
                    }
                    else
                    {
                        m_beat = ((coreCurrentBeat) - m_adjustedBeat) % (m_sections[m_sections.Count - 1].stopBeat);
                        if (m_sections[s].startBeat <= m_beat && m_sections[s].stopBeat >= m_beat)
                        {
                            m_activeId = m_sections[s].id;
                            m_currentSection = m_sections[s];
                        }
                    }
                }


                for (int j = 0; j < m_currentSection.tracks.Count; j++)
                {
                    ProcessCursors(m_currentSection, j, m_beat, coreCurrentBeat, m_processRange, quant: m_quant, quantVal: m_quantVal, speed: m_speedscale);
                }


                if (m_currentmotif != null)
                {
                    double motifbeat = (coreCurrentBeat - m_motifAdjustedBeat); // % (m_currentmotif.stopBeat - m_currentmotif.startBeat);
                    for (int i = 0; i < m_currentmotif.tracks.Count; i++)
                    {
                        ProcessCursors(m_currentmotif, i, motifbeat, coreCurrentBeat, m_processRange, 0, false);
                    }
                    if (motifbeat + 1 > m_currentmotif.stopBeat - m_currentmotif.startBeat)
                    {
                        KillCursorsInstant(m_currentmotif);
                        m_currentmotif = null;

                    }
                }


                if (m_activePatterns != null)
                {
                    for (int i = 0; i < m_activePatterns.Count; i++)
                    {
                        var beat = m_beat % Mathf.Ceil((float)m_activePatterns[i].cursorArrayContainer[0].steps[m_activePatterns[i].cursorArrayContainer[0].steps.Count - 1].pos[0]);
                        ProcessItem(m_activePatterns[i], beat, coreCurrentBeat, 1);
                    }
                }
            }
        }



        public void ProcessCursors(TimelineSectionAsset timelineSection, int index, double processBeat, double absoluteBeat, double processRange, double schedOffset = 0f, bool loop = true, bool quant = false, float quantVal = 2f, float speed = 1)
        {
            var cs = timelineSection.tracks[index].cursorSequences;
            for (int sq = 0; sq < cs.Count; sq++)
            {
                var bp = cs[sq].beatPosition;

                if (bp > timelineSection.stopBeat || bp < timelineSection.startBeat)
                    continue;

                if ((bp) < (processBeat + timelineSection.startBeat + 0.5f) % timelineSection.stopBeat)
                {
                    var st = cs[sq].cursorArrayContainer[0].steps;
                    for (int i = 0; i < st.Count; i++)
                    {
                        double b;

                        b = (st[i].pos[0]) + bp;

                        if (!autoPlay) b -= timelineSection.startBeat;

                        if (b >= processBeat - 0.1f && b <= processBeat - 0.1 + processRange)
                        {
                            if (!autoPlay && b >= timelineSection.stopBeat - timelineSection.startBeat) continue;
                            else if (b >= timelineSection.stopBeat) continue;

                            if (st[i].pos[5] == 1)
                            {
                                var g = st[i].pos[3];
                                var x = st[i].pos[1];
                                var y = st[i].pos[2];
                                var z = st[i].pos[4] / 127f;

                                //// Conditionals
                                //if (ignore_gateoff && g == 0) continue;
                                if (m_flipRange) y = Flip(y, cs[sq].minpitch, cs[sq].maxpitch);


                                // Legato overlap hack
                                if (i > 1 && g == 0 && i < st.Count - 1)
                                    if (y != st[i - 1].pos[2] && st[i - 1].pos[3] == 1)
                                        continue;

                                if (quant)
                                {
                                    b = (double)(Mathf.Round((float)b * quantVal) / quantVal);
                                }

                                double offset = 0;
                                if (absoluteBeat > processBeat)
                                    offset = absoluteBeat - (processBeat);


                                //int bb = (int)((b - System.Math.Truncate(b)) * 1000); // TRIPLET CHECK move to import ideally
                                //if (bb == 333 || bb == 666) { x = 1; } else { x = 6f / (7f + 0.1f); } // TRIPLET CHECK
                                //if (g == 1)
                                //{
                                //    var kukers = schedOffset + offset + b;
                                //    Debug.Log("REAL " + kukers + "  " + (b));
                                //}

                                //Debug.Log(x + "   " + bb);

                                m_core.engine.ScheduleCursor(timelineSection.tracks[index].trackNumber, schedOffset + offset + b, (int)g, x, y, z);// Remap(y, 0, 1, 0 - ranges, 1 + ranges));

                                // Set as inactive
                                st[i].pos[5] = 0;
                            }
                        }
                        else
                        {
                            st[i].pos[5] = 1;
                        }
                    }
                }
            }
        }


        void ProcessItem(CursorSequenceAsset csa, double processBeat, double absoluteBeat, double processRange)
        {
            var st = csa.cursorArrayContainer[0].steps;
            for (int i = 0; i < st.Count; i++)
            {
                double b;

                b = st[i].pos[0];

                if (b >= processBeat - 0.1f && b <= processBeat - 0.1f + processRange)
                {
                    if (st[i].pos[5] == 1)
                    {
                        var g = st[i].pos[3];
                        var x = st[i].pos[1];
                        var y = st[i].pos[2];
                        var z = st[i].pos[4] / 127f;

                        // Legato overlap hack
                        if (i > 1 && g == 0 && i < st.Count - 1)
                            if (y != st[i - 1].pos[2] && st[i - 1].pos[3] == 1)
                                continue;

                        double offset = 0;
                        if (absoluteBeat > processBeat)
                            offset = absoluteBeat - (processBeat);

                        //var push = 0f;
                        //if (g == 0) push = 0.0015f;
                        // Schedule movement
                        m_core.engine.ScheduleCursor(csa.trackNumber, offset + b, (int)g, x, y, z);// Remap(y, 0, 1, 0 - ranges, 1 + ranges));

                        // Set as inactive
                        st[i].pos[5] = 0;
                    }
                }
                else
                {
                    st[i].pos[5] = 1;
                }
            }
        }



        // ---------------------------------------------------------------------------------------------------- SAMPLER PLAYBACK

        public bool StartVoice(int voice, float pitch, float velocity, int instrument, double beat)
        {

            if (m_samplers.Count < m_track.m_instrumentation[instrument]) return false;
            if (m_samplers[m_track.m_instrumentation[instrument]] == null) return false;

            m_samplers[m_track.m_instrumentation[instrument]].StartVoice(voice, pitch, velocity, instrument, beat, m_core.engine.GetTempo(), m_core.engine.GetCurrentBeat());



            return true;
        }

        public bool StopVoice(int voice, double beat, int instrument)
        {

            if (m_samplers.Count < m_track.m_instrumentation[instrument]) return false;
            if (m_samplers[m_track.m_instrumentation[instrument]] == null) return false;

            m_samplers[m_track.m_instrumentation[instrument]].StopVoice(voice, instrument, beat, m_core.engine.GetTempo(), m_core.engine.GetCurrentBeat());

            return true;
        }

        public void ProcessAudioEvent(GestrumentCore.Event engineEvent)
        {

            if (engineEvent is GestrumentCore.NoteOnEvent)
            {
                GestrumentCore.NoteOnEvent noteEvent = (GestrumentCore.NoteOnEvent)engineEvent;
                StartVoice(noteEvent.Voice, noteEvent.Pitch, noteEvent.Velocity, noteEvent.Instrument, noteEvent.Beat);
            }
            else if (engineEvent is GestrumentCore.NoteOffEvent)
            {
                GestrumentCore.NoteOffEvent noteEvent = (GestrumentCore.NoteOffEvent)engineEvent;
                StopVoice(noteEvent.Voice, noteEvent.Beat, noteEvent.Instrument);
            }

        }


        // ------------------- ------------------- ------------------- ------------------- SECTION CHANGE LOGIC

        public void SetPart(int part, float speed)
        {
            StartCoroutine(SetPartRoutine(part, speed));
        }


        IEnumerator SetPartRoutine(int part, float speed)
        {
            if (part == 666) // FULL TRACK NORMAL PLAYBACK - "Disconnected"
            {
                yield return new WaitForDivision(m_core, 4);
                m_adjustedBeat = Mathf.Round((float)m_core.engine.GetCurrentBeat());
                m_activeId = 1;
                m_currentSection = m_sections[0];
                autoPlay = true;
                //if (partText) partText.text = "Full track";
                yield break;
            }

            KillCursors(m_sections[part - 1]);
            yield return new WaitForDivision(m_core, 4);
            m_adjustedBeat = Mathf.Round((float)m_core.engine.GetCurrentBeat());
            m_activeId = part;

            if (speed == 1)
                m_currentSection = m_sections[part - 1];
            else if (speed == 2)
                m_currentSection = m_sectionsDouble[part - 1];
            else if (speed == 0.5f)
                m_currentSection = m_sectionsHalf[part - 1];

            autoPlay = false;
        }


        // ------------------- ------------------- ------------------- ------------------- STINGER LOGIC
        public void PlayStinger(int stinger, float speed)
        {
            if (speed == 1)
                m_currentStinger = m_stingers[stinger - 1];
            else if (speed == 0.5f)
                m_currentStinger = m_stingersHalf[stinger - 1];
            else if (speed == 2f)
                m_currentStinger = m_stingersDouble[stinger - 1];


            var adjustedBeat = (Mathf.Ceil((float)(m_beat) / 2) * 2) - m_beat;// % (m_currentStinger.stopBeat - m_currentStinger.startBeat);

            for (int i = 0; i < m_currentStinger.tracks.Count; i++)
                for (int j = 0; j < m_currentStinger.tracks[i].cursorSequences.Count; j++)
                    for (int k = 0; k < m_currentStinger.tracks[i].cursorSequences[j].cursorArrayContainer[0].steps.Count; k++)
                        m_currentStinger.tracks[i].cursorSequences[j].cursorArrayContainer[0].steps[k].pos[5] = 1; // Make sure they are set to be allowed to play

            for (int i = 0; i < m_currentStinger.tracks.Count; i++)
                ProcessCursors(m_currentStinger, i, 0, m_core.engine.GetCurrentBeat(), (m_currentStinger.stopBeat - m_currentStinger.startBeat), adjustedBeat, false);
            
        }


        // ------------------- ------------------- ------------------- ------------------- MOTIF LOGIC

        public void PlayMotif(int motif, float speed)
        {
            StartCoroutine(ScheduledMotif(motif, speed));
        }

        IEnumerator ScheduledMotif(int motif, float speed)
        {
            TimelineSectionAsset mot = null;

            if (speed == 1)
                mot = m_motifs[motif - 1];
            else if (speed == 0.5f)
                mot = m_motifsHalf[motif - 1];
            else if (speed == 2)
                mot = m_motifsDouble[motif - 1];

            if (mot == m_currentmotif) yield break;

            StartCoroutine(KillCursors(m_currentmotif));
            yield return new WaitForDivision(m_core, 4);
            m_currentmotif = null;

            m_currentmotif = mot;


            m_motifAdjustedBeat = (Mathf.Ceil((float)m_core.engine.GetCurrentBeat() / 4) * 4);// % (m_currentStinger.stopBeat - m_currentStinger.startBeat);
        }

        public IEnumerator RecallMotif(TimelineSectionAsset motif)
        {
            if (motif == m_currentmotif) yield break;

            StartCoroutine(KillCursors(m_currentmotif));
            yield return new WaitForDivision(m_core, 4);
            m_currentmotif = motif;
            m_motifAdjustedBeat = (Mathf.Ceil((float)m_core.engine.GetCurrentBeat() / 4) * 4);// % (m_currentStinger.stopBeat - m_currentStinger.startBeat);
        }



        public void PlayPattern(int pattern)
        {
            var pat = Instantiate(m_availablePatterns[pattern - 1]);
            CursorSequenceAsset cas = (m_activePatterns.Find(x => x.trackNumber == pat.trackNumber));

            if (cas == null)
            {
                m_activePatterns.Add(pat);
            }
            else
            {
                m_core.engine.SetCursorGate(cas.trackNumber, false);
                m_activePatterns.Remove(cas);
            }
        }


        // ------------------- ------------------- ------------------- ------------------- ASSORTED FUNCTIONALITIES

        public IEnumerator KillCursors(TimelineSectionAsset tsa)
        {
            yield return new WaitForDivision(m_core, 4);
            for (int i = 0; i < tsa.tracks.Count; i++)
            {
                var index = tsa.tracks[i].trackNumber;
                m_core.engine.SetCursorGate(index, false);
            }
        }
        public void KillCursorsInstant(TimelineSectionAsset tsa)
        {
            for (int i = 0; i < tsa.tracks.Count; i++)
            {
                var index = tsa.tracks[i].trackNumber;
                m_core.engine.SetCursorGate(index, false);
            }
        }

        public void SwitchInstrumentation(int index)
        {
            for (int i = 0; i < m_samplers.Count; i++)
            {
                m_samplers[i].StopAllVoices();
            }
            m_track.m_instrumentation = m_track.m_altInstrumentations[index - 1].instrumentation;
            instrumentationId = index - 1;
        }

        public void ToggleMute(bool val, int index)
        {
            int onoff = 0;
            if (val) onoff = 1;
            m_mutetoggles[index] = onoff;
            m_core.engine.ScheduleParam(Engine.OSCParam.ins, 0, index, "mute", onoff);
        }


        public void SetTransposeSlot(System.Single s)
        {
            currentTranspose = (int)s;
            StartCoroutine(SetTransposeSlotRoutine(s));
        }


        IEnumerator SetTransposeSlotRoutine(float transpose)
        {
            yield return new WaitForDivision(m_core, 2);
            m_core.engine.ScheduleTransposeSlot(Mathf.Round((float)m_beat), (int)transpose + 7);
            for (int i = 0; i < m_track.m_trackCount; i++)
            {
                if (m_track.m_timelineTracks[i].isPercussion)
                {
                    m_core.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, 0, i, "mtranspose", -transpose);
                }
            }
        }

        double Flip(double num, double min, double max)
        {
            return (max + min) - num;
        }


        public void SetMixerMasterVolume(System.Single val)
        {
            mixer.SetFloat("master", Mathf.Log10(val) * 20);
        }

        public TimelineSectionAsset ScaleSpeed(TimelineSectionAsset sec, float speed)
        {

            TimelineSectionAsset tsa = Instantiate(sec);
            for (int i = 0; i < tsa.tracks.Count; i++)
            {
                tsa.tracks[i] = Instantiate(tsa.tracks[i]);
                for (int j = 0; j < tsa.tracks[i].cursorSequences.Count; j++)
                {
                    tsa.tracks[i].cursorSequences[j] = Instantiate(tsa.tracks[i].cursorSequences[j]);
                    tsa.tracks[i].cursorSequences[j].beatPosition /= speed;

                    var st = tsa.tracks[i].cursorSequences[j].cursorArrayContainer[0].steps;
                    for (int k = 0; k < st.Count; k++)
                    {
                        st[k].pos[0] = st[k].pos[0] / speed;
                    }
                }
            }
            tsa.startBeat = tsa.startBeat / speed;
            tsa.stopBeat = tsa.stopBeat / speed;

            return tsa;
        }

        public void CopySection(TimelineSectionAsset tsa)
        {
            tsa = Instantiate(tsa);
            for (int j = 0; j < tsa.tracks.Count; j++)
            {
                tsa.tracks[j] = Instantiate(tsa.tracks[j]);
                for (int k = 0; k < tsa.tracks[j].cursorSequences.Count; k++)
                {
                    tsa.tracks[j].cursorSequences[k] = Instantiate(tsa.tracks[j].cursorSequences[k]);
                }
            }
        }




        // ------------------- ------------------- ------------------- ------------------- PARAMETER LOGIC

        public void UpdateParameter(GestrumentParameterAsset parameterAsset, float parameterValue)
        {
            if (!parameterAsset) return;

            for (int i = 0; i < parameterAsset.gestrumentSubParameters.Count; i++)
            {
                var gsp = parameterAsset.gestrumentSubParameters[i];

                if (parameterValue >= gsp.inVal && parameterValue <= gsp.outVal)
                {
                    double beat = Mathf.Ceil((float)m_core.engine.GetCurrentBeat() * gsp.quant) / gsp.quant;

                    if (!gsp.isActive)
                    {
                        for (int j = 0; j < gsp.cursorSequenceAssets.Count; j++)
                        {
                            //  SequenceScheduler(gsp.cursorSequenceAssets[j], beat);
                        }

                        for (int k = 0; k < gsp.states.Count; k++)
                        {
                            GetComponent<GestrumentStateSaveLoad>().LoadSettings(gsp.states[k], 4);
                        }
                    }


                    for (int j = 0; j < gsp.setParameterList.Count; j++)
                    {
                        if (!gsp.isActive || gsp.setParameterList[j].isContinous)
                            ParseSetParameter(gsp.setParameterList[j], parameterValue, beat, gsp.inVal, gsp.outVal);
                    }

                    gsp.isActive = true;
                }
                else
                {
                    gsp.isActive = false;
                }
            }

        }

        void ParseSetParameter(SPParameter param, float value, double beat, float inVal, float outVal)
        {
            if (param.remap) value = (float)m_core.engine.Remap(value, inVal, outVal, param.minVal, param.maxVal);
            Debug.Log(value);
            m_core.engine.ScheduleParam(param.paramType, beat, param.index, param.parameterName, value);
        }


    }
}

