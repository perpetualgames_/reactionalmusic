﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestrumentChords : MonoBehaviour
{

    public List<GestrumentChordSequence> ChordSequences = new List<GestrumentChordSequence>();
    GestrumentChordSequence gestrumentChordSequence;
    public List<ChordSettings> chordList;
    public int ChordListModulo = 16;

    public GestrumentOrchestrator gO;

    public timeSignature timesig = timeSignature.FourFour;
    public rootNote root = rootNote.C;
    public chord currentChord;
    int chordID;
    public int currentBeat;
    public int currentBar;
    int prevBeat=-1;
    public Text text;


    void Awake(){
        gO.chords = this;
    }

    void FixedUpdate()
    {
        if (gO)
        {
            currentBeat = (int)gO.core.engine.GetCurrentBeat();
        }

        currentBar = (int)(currentBeat / 4f);



        if (ChordSequences.Count > 0)
        {
            for (int i = 0; i < ChordSequences.Count; i++)
            {
                if ((currentBeat) % ChordListModulo == ChordSequences[i].startBeat)
                {
                    gestrumentChordSequence = ChordSequences[i];
                    chordList = gestrumentChordSequence.ChordList;
                }
            }

            if (gestrumentChordSequence != null)
            {
                for (int j = 0; j < gestrumentChordSequence.changeAtBeat.Length; j++)
                {
                    var cS = gestrumentChordSequence;
                    if (currentBeat != prevBeat && (currentBeat) % ChordListModulo == cS.startBeat + cS.changeAtBeat[j])
                    {
                        NextChord();
                        prevBeat = currentBeat;
                    }
                }
            }
        }
    }

    public void NextChord()
    {
        var cS = gestrumentChordSequence;
        gO.core.engine.SetScaleForSlot(0, (int)cS.ChordList[chordID].root, chords(cS.ChordList[chordID].chord.ToString()).ToString());

        root = cS.ChordList[chordID].root;
        currentChord = cS.ChordList[chordID].chord;

        chordID++;
        chordID = chordID % chordList.Count;
        if(text) text.text = root.ToString() + " " + currentChord.ToString();
    }

    public float[] chords(string pos)
    {
        float[] chordNotes;
        switch (pos)
        {
            case "major":
                chordNotes = new float[] { 4, 7, 12 };
                break;
            case "minor":
                chordNotes = new float[] { 3, 7, 12 };
                break;
            case "_7":
                chordNotes = new float[] { 4, 7, 10, 12 };
                break;
            default:
                chordNotes = null;
                break;
        }

        return chordNotes;
    }
}
