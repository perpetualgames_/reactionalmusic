﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reactional
{
    public class ReactionalDispatcher : MonoBehaviour
    {
        static ReactionalDispatcher instance;
        static bool hasLookedForInstance = false;

        public ReactionalPlayer m_reactionalPlayer;

        public static ReactionalDispatcher TryFindInstance()
        {
            if (hasLookedForInstance)
                return instance;
            hasLookedForInstance = true;
            instance = FindObjectOfType<ReactionalDispatcher>();
            return instance;
        }

        public ReactionalPlayer Player
        {
            get {
                var inst = TryFindInstance();

                return inst.m_reactionalPlayer;
            }

            set
            {
                var inst = TryFindInstance();

                inst.m_reactionalPlayer = value;
            }
        }

        void Start()
        {
            m_reactionalPlayer = FindObjectOfType<ReactionalPlayer>();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

    public static class Request
    {
        public static void State(string stateName, int division = 4)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            rd.GetComponent<GestrumentStateSaveLoad>().LoadSettings(stateName, division);
        }

        public static void Motif(int motifId, int division = 4)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            rd.Player.PlayMotif(motifId, 1);
        }

        public static void Section(int sectionId, int division = 4)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            rd.Player.SetPart(sectionId, 1);
        }

        public static void Stinger(int stingerId, int division = 4)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            rd.Player.PlayStinger(stingerId, 1f);
        }

        public static void SetParameter(string parameterName, float value)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            GestrumentParameterAsset gpa = rd.Player.m_track.m_parameters.Find(x => x.parameterName == parameterName);
            if(gpa != null)
                rd.Player.UpdateParameter(gpa, value);
        }

        public static void SaveState(string name)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            rd.GetComponent<GestrumentStateSaveLoad>().SaveSetting(name, _saveAll: true);
        }

        public static void LoadState(string name)
        {
            ReactionalDispatcher rd = ReactionalDispatcher.TryFindInstance();
            rd.GetComponent<GestrumentStateSaveLoad>().LoadSettings(name, 4);
        }
    }
}