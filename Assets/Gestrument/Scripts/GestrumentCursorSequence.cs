﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;
using Gestrument;

[System.Serializable]
public class GestrumentSequence
{
    public List<mFunction> midiChordDegrees;
    public List<mFunction> desiredChordDegrees;
    public List<string> oscmessages;
    public List<GestrumentCursorArrayContainer> melodies;
    public List<GestrumentCursorArrayContainer> stingers;
    public List<GestrumentCursorArrayContainer> loops;
};

[System.Serializable]
public class GestrumentCursorArrayContainer
{
    public List<CursorArray> steps;
}

[System.Serializable]
public class CursorArray
{
    public double[] pos;
}

public class GestrumentCursorSequence : MonoBehaviour
{
    /*
    [SerializeField]
    private GestrumentGlobalSync m_global;

    [SerializeField]
    private TextAsset m_jsonData;

    [SerializeField]
    private TextAsset m_jsonStinger;

    public GestrumentSequence m_seq;

    public GestrumentSequence m_stingers;

    public bool m_global_reverse;
    public bool m_global_yinverse;

    IEnumerator[] m_loop = new IEnumerator[8];
    IEnumerator[] m_cursorSeq = new IEnumerator[8];

    double[] m_beatsleft = new double[] { 0, 0, 0, 0, 0, 0, 0 };

    public int transposeIndex = 0;

    public int cursorOffset = 0;
    // Start is called before the first frame update
    void Start()
    {
        m_global = FindObjectOfType<GestrumentGlobalSync>();
      //  m_seq = JsonUtility.FromJson<GestrumentSequence>(m_jsonData.text);
        m_stingers = JsonUtility.FromJson<GestrumentSequence>(m_jsonStinger.text);
        m_global.currentCore.engine.SetParamValue(Engine.EventObject.PitchGen, 3, "minpitch", 0);
        m_global.currentCore.engine.SetParamValue(Engine.EventObject.PitchGen, 3, "maxpitch", 127);
        m_global.currentCore.engine.SetParamValue(Engine.EventObject.PitchGen, 3, "uselocalscale", true);
        m_global.currentCore.engine.SetParamValue(Engine.EventObject.PitchGen, 3, "localscale", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]");
        m_global.currentCore.engine.SetParamValue(Engine.EventObject.PulseGen, 3, "durations", "[1/1, 1/2, 1/4, 1/8, 1/16, 1/32, 1/64]");
    }

    // Update is called once per frame
    void Update()
    {
        m_global.currentCore.engine.ScheduleTransposeSlot(0, transposeIndex);
    }

    public void PlayMelody(int i)
    {
        if (m_loop[0] != null)
            StopCoroutine(m_loop[0]);
        m_loop[0] = ExecuteSequence(cursorOffset, i, true);
        StartCoroutine(m_loop[0]);
    }

    private IEnumerator ExecuteSequence(int curs, int seq, bool loop)
    {
        yield return new WaitForDivision(m_global.currentCore, 2);

        if (m_cursorSeq[curs] != null)
            StopCoroutine(m_cursorSeq[curs]);
        
        m_cursorSeq[curs] = PlaySequence(curs, m_seq.melodies[seq], loop, 8, 1, 4);
        StartCoroutine(m_cursorSeq[curs]);
        yield return null;
    }

    public void PlayMelody2(int i)
    {
        PlaySequence(1, m_seq.melodies[i], true, 4, 1);
    }

    public void PlayMelodyHalfSpeed2(int i)
    {
        PlaySequence(1, m_seq.melodies[i], true, 4, 4);
    }

    public void PlayMelody3(int i)
    {
        PlaySequence(2, m_seq.melodies[i], true, 4, 1);
    }

    public void PlayMelodyHalfSpeed3(int i)
    {
        PlaySequence(2, m_seq.melodies[i], true, 4, 4);
    }

    public void PlayMelodyHalfSpeed(int i)
    {
        PlaySequence(0, m_seq.melodies[i], true, 4, 4);
    }

    public void PlayRandomStinger(float length)
    {
        StartCoroutine(PlaySequence(4, m_stingers.stingers[Random.Range(0, m_stingers.stingers.Count - 1)], false, 4, length, 1));
    }

    public IEnumerator PlaySequence(int cursorIndex, GestrumentCursorArrayContainer sq, bool loop, double limit = 4, float length = 1, float division = 4)
    {
        bool iterate = true;

        while (iterate)
        {

            // if (m_loop != null)
            //     if (m_loop[cursorIndex] != null) StopCoroutine(m_loop[cursorIndex]);

            var beatOffset = Mathf.Round((float)(m_global.currentCore.engine.GetCurrentBeat()));// + m_beatsleft[cursorIndex])); // Round to closest beat
            beatOffset = Mathf.Ceil(beatOffset/division) * division; // Round to closest division

            double loopOffset = 0;
            List<CursorArray> steps = sq.steps;



            ScheduleCursor(cursorIndex, beatOffset - 0.25f, 0, 0, 0);

            for (int i = 0; i < steps.Count - 1; i++)
            {
                var b = steps[i].pos[0] * length;
                int g = (int)steps[i].pos[3];
                var x = steps[i].pos[1];
                var y = steps[i].pos[2];

                if (m_global_reverse)
                {
                    b = Mathf.InverseLerp(0, (float)limit, (float)b);
                    b = Mathf.Lerp((float)limit, 0, (float)b);
                    g = Mathf.Abs(g - 1);
                }

                if (m_global_yinverse)
                {
                    y = Mathf.InverseLerp(1, 0, (float)y);
                    y = Mathf.Lerp(0, 1, (float)y);
                }

                if (limit != 0 && b < limit)
                {
                    yield return new WaitForBeat(m_global.currentCore, beatOffset + b - 0.5f);
                    double push = 0.15f;
                    if (g == 0) { push = 0.25f; y = 0f; x = 0f; }

                    ScheduleCursor(cursorIndex, b + beatOffset - push, g, x, y);
                    loopOffset = (b + beatOffset - push);
                    // Debug.Log(steps[i].pos[0]);
                }
            }



            iterate = loop;

            //            m_loop[cursorIndex] = LoopSequence(cursorIndex, sq, loop, loopOffset, limit, length, division);
            //           StartCoroutine(m_loop[cursorIndex]);

            yield return new WaitForBeat(m_global.currentCore, beatOffset + limit - 1f);
            //yield return new WaitForDivision(m_global.currentCore, division*4);
        }

        //yield return new WaitForBeat(m_global.currentCore,beatOffset+limit - 1);
        yield break;

    }

    void ScheduleCursor(int index, double beat, int gate, double x, double y)
    {
        var msg = new OSCMessage("/crs");
        msg.Append<double>(beat);
        msg.Append<int>(index);
        msg.Append<int>(gate);
        msg.Append<double>(x);
        msg.Append<double>(y);
        msg.Append<double>(0);
        m_global.currentCore.engine.Enqueue(msg.BinaryData);
    }

    // IEnumerator LoopSequence(int cursorIndex, GestrumentCursorArrayContainer sq, bool loop, double offset, double limit, float length, float division)
    // {
    //     // var waitbeat = offset + sq.steps[sq.steps.Count - 1].pos[0];
    //     while (m_global.currentCore.engine.GetCurrentBeat() < (offset))
    //     {
    //         m_beatsleft[cursorIndex] = offset - m_global.currentCore.engine.GetCurrentBeat(); //Update to keep time in case loop is aborted
    //         yield return new WaitForSeconds(0.05f);
    //     }

    //     m_beatsleft[cursorIndex] = 0;
    //     m_cursorSeq[cursorIndex] = PlaySequence(cursorIndex, sq, loop, limit, length, division);
    //     StartCoroutine(m_cursorSeq[cursorIndex]);

    // }

    private void OnValidate()
    {
//        m_seq = JsonUtility.FromJson<GestrumentSequence>(m_jsonData.text);
 //       m_stingers = JsonUtility.FromJson<GestrumentSequence>(m_jsonStinger.text);
//        Debug.Log(m_seq.melodies[0].steps.Count);
    }
    */
}
