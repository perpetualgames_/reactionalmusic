﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



[System.Serializable]
public class Group
{
    public List<Region> region = new List<Region>();
    public int seq_length = -1;
    public int key = -1;
    public int lokey = -1;
    public int hikey = -1;
    public int pitch_keycenter = -1;
    public int vel_min = -1;
    public int vel_max = -1;
}

[System.Serializable]
public class Region
{
    public int lokey = -1;
    public int hikey = -1;
    public int pitch_keycenter = -1;
    public int key = -1;
    public int vel_min = -1;
    public int vel_max = -1;
    public int tune = 0;
    public int transpose = 0;
    public AudioClip sample;
}

[System.Serializable]
public class Note
{

}

[CreateAssetMenu(fileName = "Sample Bank", menuName = "Gestrument/Sample Bank", order = 10)]

public class GestrumentSampleBank : ScriptableObject
{


    public List<Group> group = new List<Group>();
    int rr = 0; // Round robin counter


    private void OnValidate()
    {

    }

    public bool GetSample(float pitch, float velocity, AudioSource aS, int offset, GestrumentSampler gS)
    {
        velocity = velocity * 127;
        float p = pitch;
        pitch = (int)pitch;

        for (int i = 0; i < group.Count; i++)
        {
            bool groupNote = (group[i].key == -1 && group[i].pitch_keycenter == -1) ? false : true; // Check if pitch is defined in region or group
            bool groupVel = (group[i].vel_max == -1) ? false : true;    // Check if velocity is defined in region or group
            var grp = group[i];

            if (grp.seq_length > 0)
            {
                rr = (rr + 1) % grp.seq_length; // advance round robin counter if several groups exists matching
                i = rr;
            }

            for (int j = 0; j < grp.region.Count; j++)
            {
                var reg = group[i].region[j];

                if (reg.vel_max != -1 && grp.vel_max != -1)   // Check if velocity exists at all
                {               
                    if ((reg.vel_max < velocity || reg.vel_min > velocity) && !groupVel) // Check if region is in velocity range, else skip to next
                        continue;

                    if ((grp.vel_max < velocity || grp.vel_min > velocity) && groupVel) // Check if group is in velocity range
                        continue;
                }
                
                if (reg.key == pitch + offset || (reg.lokey <= pitch + offset && reg.hikey >= pitch + offset) && !groupNote) // Match pitch by region
                {
                    aS.clip = reg.sample;
                    int center = 0;
                    if(reg.pitch_keycenter != -1) center = reg.pitch_keycenter - reg.transpose;
                    if(reg.key != -1) center = reg.key - reg.transpose;
                    float fac = Mathf.Pow(2, (p - (center + (reg.tune / 100))) / 12f);   // SET PITCH
                    aS.pitch = fac;
                    return true;
                }
                else if (group[i].key == pitch + offset || group[i].lokey <= pitch + offset && group[i].hikey >= pitch + offset) // Else match pitch by group
                {
                    aS.clip = reg.sample;
                    int center = 0;
                    if(grp.pitch_keycenter != -1) center = grp.pitch_keycenter;
                    if(grp.key != -1) center = grp.key;
                    float fac = Mathf.Pow(2, (p - (center + (reg.tune / 100))) / 12f);   // SET PITCH
                    aS.pitch = fac; // + Random.Range(-0.05f,0.05f);
                    return true;
                }

                // If no match, check next group

            }

        }

        return false;

    }
}

