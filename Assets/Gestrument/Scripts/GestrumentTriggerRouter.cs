﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class GestrumentNoteEventPusher
{

    public int[] instrument;
    public int[] voice;
    public int[] pitches;
    public int division = 8;
    public UnityEvent NoteTriggerEvent;
}

[System.Serializable]
public class GestrumentBeatEventPusher
{
    public float division = 8;
    public float offset = 0;
    public UnityEvent BeatTriggerEvent;
}

public class GestrumentTriggerRouter : MonoBehaviour
{
    public List<GestrumentNoteEventPusher> NoteTrigger;
    public List<GestrumentBeatEventPusher> BeatTrigger;
    public GestrumentCore core;

    public double beatCounter;
    int currentBeat;
    int prevBeat;
    // Start is called before the first frame update

    //int colorint = 0;
    
//    public static List<GestrumentTestSizer> sizeGang = new List<GestrumentTestSizer>();

    void Start()
    {
        core.OnNoteOn.AddListener(ProcessAudioEvent);
    }

    void NoteOnChecker(int voice, float pitch, float velocity, int instrument, double beat)
    {

        for (int i = 0; i < NoteTrigger.Count; i++)
        {
            var nt = NoteTrigger[i];
            bool _inst = false, _voice = false, _pitch = false, _beat = false;

            var divbeat = (beat / 4) % 1;

            double div = (1f / nt.division);
            if (divbeat % div == 0) _beat = true;


            if (nt.instrument.Length == 0)
                _inst = true;
            else
                for (int j = 0; j < nt.instrument.Length; j++)
                    if (nt.instrument[j] == instrument) _inst = true;

            if (nt.voice.Length == 0)
                _voice = true;
            else
                for (int j = 0; j < nt.voice.Length; j++)
                    if (nt.voice[i] == voice) _voice = true;

            if (nt.pitches.Length == 0)
                _pitch = true;
            else
                for (int j = 0; j < nt.pitches.Length; j++)
                    if (nt.pitches[i] == pitch) _pitch = true;

            if (_inst && _voice && _pitch && _beat)
                nt.NoteTriggerEvent.Invoke();
        }
    }

    void NoteOffChecker(int voice, double beat, int instrument)
    {

    }

    void BeatChecker(double beat){
        for (int i = 0; i < BeatTrigger.Count; i++)
        {
           
            bool _beat = false;
            var bt = BeatTrigger[i];
            var btbeat = (beat)/8;
            var divbeat = (btbeat / 4) % 1;
            double div = (1f / bt.division);
            if ((divbeat+bt.offset) % div == 0) _beat = true;

            if(_beat)
                bt.BeatTriggerEvent.Invoke();
        }
    }

    private void FixedUpdate()
    {
        beatCounter = core.engine.GetCurrentBeat();
        currentBeat = (int)(beatCounter*8);
        if(currentBeat != prevBeat){
            BeatChecker(currentBeat);
        }
        prevBeat = currentBeat;
    }

    public void ProcessAudioEvent(GestrumentCore.Event engineEvent)
    {
        if (engineEvent is GestrumentCore.NoteOnEvent){
            GestrumentCore.NoteOnEvent noteEvent = (GestrumentCore.NoteOnEvent)engineEvent;
             NoteOnChecker(noteEvent.Voice, noteEvent.Pitch, noteEvent.Velocity, noteEvent.Instrument, noteEvent.Beat);
        }
        else if (engineEvent is GestrumentCore.NoteOffEvent){
            GestrumentCore.NoteOffEvent noteEvent = (GestrumentCore.NoteOffEvent)engineEvent;
            NoteOffChecker(noteEvent.Voice, noteEvent.Beat, noteEvent.Instrument);
        }
            
    }

    //public void SizeGang(float id){
    //    for (int i = 0; i < sizeGang.Count; i++)
    //    {
    //        sizeGang[i].SizeMeUp(id);
    //    }
    //}

    //public void spinGang(float id){
    //  for (int i = 0; i < sizeGang.Count; i++)
    //    {
    //        sizeGang[i].SpinMe(id);
    //    }
    //}

    public void SetColor(Image r){
 
        StartCoroutine(Flash(r));
    }

    public IEnumerator Flash(Image r){
        r.material.color = new Color(0.7f, 0.7f,1,1);
        
        yield return new WaitForSeconds(0.1f);
        r.material.color = new Color(1,1,1,1);
    }

}
