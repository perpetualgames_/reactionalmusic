﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class TimelineTrack : ScriptableObject
{

    public List<CursorSequenceAsset> cursorSequences = new List<CursorSequenceAsset>();
    public string instrumentName;
    public bool isPercussion = false;
    public int trackNumber;
    public float pan;
}


