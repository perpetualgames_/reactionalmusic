﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gestrument;

public class WaitForBeat : CustomYieldInstruction
{
    GestrumentCore core;
    double beat;
    public override bool keepWaiting
    {
        get
        {
            return (beat > (core.engine.GetCurrentBeat()-0.15f));
        }
    }

    public WaitForBeat(GestrumentCore c, double b)
    {
        core = c;
        beat = b;
        //Debug.Log("Waiting for beat");
    }
}

public class WaitForDivision : CustomYieldInstruction
{
    GestrumentCore core;
    double div;
    public override bool keepWaiting
    {
        get
        {
//            Debug.Log("test");
            return ((div - 0.1f) > ((core.engine.GetCurrentBeat())%div));
        }
    }

    public WaitForDivision(GestrumentCore c, double d)
    {
        core = c;
        div = d;
//        Debug.Log("Waiting for division");
    }
}