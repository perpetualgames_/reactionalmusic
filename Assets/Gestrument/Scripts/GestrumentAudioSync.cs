﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gestrument;
using UnityEngine.UI;
using Reactional;

public enum timeSignature { FourFour, ThreeFour }

public class GestrumentAudioSync : MonoBehaviour
{
    public List<GestrumentChordSequence> ChordSequences = new List<GestrumentChordSequence>();
    GestrumentChordSequence gestrumentChordSequence;
    public List<ChordSettings> chordList;
    public int ChordListModulo = 16;
    public GestrumentAudioSyncSettings syncAsset;
    public ReactionalPlayer rplayer;
    public GestrumentCore core;
    public AudioSource musicPlayer;
    public AudioClip musicClip;
    public float tempo = 128;
    public int currentBeat = 1;
    public int currentBar = -1;
    public timeSignature timesig = timeSignature.FourFour;
    public rootNote root = rootNote.C;
    public chord currentChord;
    public double length;
    public int startBeat = 0;
    public int cycleLength = 0;
    public double samplesPerBeat;
    public Text text;
    int prevBeat = -1;
    int chordID;
    int beatsTotal;

    public float offsetfix = 0.5f;

    public int transposeBeat;
    public int transposeSlot;
    public string scale;
    public int rootPitch;

    // Start is called before the first frame update
    void OnEnable()
    {   
        chordID = 0;
        currentBar = 0;
        currentBeat = 0;

        rplayer = FindObjectOfType<ReactionalPlayer>();
        core = FindObjectOfType<GestrumentCore>();

        musicPlayer.clip = musicClip;
        length = musicPlayer.clip.length;
        musicPlayer.time = startBeat * (60 / tempo);
        currentBeat = startBeat;

        beatsTotal = (int)Mathf.Round((float)(length / (60 / tempo)));

        samplesPerBeat = musicPlayer.clip.samples;
        samplesPerBeat = samplesPerBeat / beatsTotal;

        if (core)
        {
           core.engine.SetTempo(tempo);
         //  gO.core.engine.SetCurrentBeat(musicPlayer.timeSamples / samplesPerBeat);

        }
        
       // musicPlayer.PlayScheduled(AudioSettings.dspTime + 0.2f);

       //if(scale != null)
       //     core.engine.SetScaleForSlot(0,rootPitch,scale);

    }


    void FixedUpdate()
    {
        if (rplayer)
        {
            musicPlayer.timeSamples = (int)((rplayer.m_beat * samplesPerBeat - (AudioSettings.outputSampleRate * Gestrument.Engine.globalOffset)) % musicClip.samples);

            currentBeat = (int)rplayer.m_beat;
        }
        else
        {



            if (core)
            {
                // musicPlayer.timeSamples = ((int)((gO.core.engine.GetCurrentBeat()+1) * samplesPerBeat))%musicClip.samples;
                // musicPlayer.time = musicPlayer.time + 0.2f;
                //gO.core.engine.SetCurrentBeat(musicPlayer.timeSamples / samplesPerBeat + (0.2f/(60/GestrumentGlobalSync.GetTempo())));

                musicPlayer.timeSamples = (int)((core.engine.GetCurrentBeat() * samplesPerBeat - (AudioSettings.outputSampleRate * 0.2f)) % musicClip.samples);

                currentBeat = (int)core.engine.GetCurrentBeat();
            }
        }
        currentBar = (int)(currentBeat / 4f);





        if (ChordSequences.Count > 0)
        {
            for (int i = 0; i < ChordSequences.Count; i++)
            {
                if ((currentBeat) % ChordListModulo == ChordSequences[i].startBeat)
                {
                    gestrumentChordSequence = ChordSequences[i];
                    chordList = gestrumentChordSequence.ChordList;
                }
            }

            if (gestrumentChordSequence != null)
            {
                for (int j = 0; j < gestrumentChordSequence.changeAtBeat.Length; j++)
                {
                    var cS = gestrumentChordSequence;
                    if (currentBeat != prevBeat && (currentBeat) % ChordListModulo == cS.startBeat + cS.changeAtBeat[j])
                    {
                        NextChord();
                        prevBeat = currentBeat;
                    }
                }
            }
        } else {


        }


        if (cycleLength > 0 && currentBeat == startBeat + cycleLength + 1)
        {
            currentBeat = startBeat;
            musicPlayer.time = startBeat * (60 / tempo);
        }
    }


    public void NextChord()
    {
        var cS = gestrumentChordSequence;

        core.engine.SetScaleForSlot(0, 0, chords(cS.ChordList[chordID].chord.ToString()));

        for (int i = 0; i < core.engine.NumInstruments; i++)
        {
            core.engine.ScheduleParam(Engine.OSCParam.pit,0,i,"ctranspose", (float)cS.ChordList[chordID].root);
        }

        root = cS.ChordList[chordID].root;
        currentChord = cS.ChordList[chordID].chord;

        chordID++;
        chordID = chordID % chordList.Count;
        if (text) text.text = root.ToString() + " " + currentChord.ToString();
    }



    public string chords(string pos)
    {
        string chordNotes;
        switch (pos)
        {
            case "major":
                chordNotes = "[4, 7, 12]";
                break;
            case "minor":
                chordNotes = "[3, 7, 12]";
                break;
            case "_7":
                chordNotes = "[4, 7, 10, 12]";
                break;
            default:
                chordNotes = null;
                break;
        }

        return chordNotes;
    }

    private void OnValidate()
    {
        if (syncAsset != null && musicClip == null)
        {
            musicClip = syncAsset.musicClip;
            tempo = syncAsset.tempo;
            timesig = syncAsset.timesig;
            length = syncAsset.musicClip.length;
            startBeat = syncAsset.startBeat;
            cycleLength = syncAsset.cycleLength;

        }

    }
}
