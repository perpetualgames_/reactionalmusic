﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gestrument;
using UnityOSC;

[System.Serializable]
public class TimelineSectionAsset : ScriptableObject {

    public List<TimelineTrack> tracks = new List<TimelineTrack>();
    public GestrumentChordSequence chords;
    public double startBeat = 0;
    public double stopBeat = 0;
    public int id = -1;
    public List<SetupMessages> setupMessages = new List<SetupMessages>();
    public List<string> instruments = new List<string>();

}

[System.Serializable]
public class SetupMessages
{
    public byte[] data;
}