﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gestrument;
using UnityEngine.UI;
using Reactional;


public class ReactionalAudioSync : MonoBehaviour
{
    public List<GestrumentChordSequence> ChordSequences = new List<GestrumentChordSequence>();
    GestrumentChordSequence gestrumentChordSequence;
    public List<ChordSettings> chordList;
    public int ChordListModulo = 16;
    public GestrumentAudioSyncSettings syncAsset;
    public ReactionalPlayer rplayer;
    public GestrumentCore core;
    public AudioSource musicPlayer;
    public AudioClip musicClip;
    public float tempo = 128;
    public int currentBeat = 1;
    public int currentBar = -1;
    public timeSignature timesig = timeSignature.FourFour;
    public rootNote root = rootNote.C;
    public chord currentChord;
    public double length;
    public int startBeat = 0;
    public int cycleLength = 0;
    public double samplesPerBeat;
    public Text text;
    //int prevBeat = -1;
   // int chordID;
    int beatsTotal;
    int scalePos = 0;
    public float bpmscale = 1;

    public float offsetfix = 0;
    int samplerate;

    public int transposeBeat;
    public int transposeSlot;
    public string scale;
    public int rootPitch;

    bool m_allowPlay = false;
    public double m_beatOffset = 0;
    float m_startVolume;

    // Start is called before the first frame update
    void OnEnable()
    {
        m_startVolume = musicPlayer.volume;

    }

    public void TogglePlay(bool toggle)
    {
        if (toggle)
            StartTrack();
        else
            StopTrack();
    }

    public void StartTrack()
    {
       // chordID = 0;
        currentBar = 0;
        currentBeat = 0;

        rplayer = FindObjectOfType<ReactionalPlayer>();
        core = FindObjectOfType<GestrumentCore>();

        musicPlayer.clip = musicClip;
        length = musicPlayer.clip.length;
        musicPlayer.time = startBeat * (60 / tempo);
        currentBeat = startBeat;

        beatsTotal = (int)Mathf.Round((float)(length / (60 / tempo)));

     //   samplesPerBeat = musicPlayer.clip.samples;
      //  samplesPerBeat = samplesPerBeat / beatsTotal;
        samplesPerBeat = AudioSettings.outputSampleRate * (60 / tempo);
        

        samplerate = AudioSettings.outputSampleRate;

        StartCoroutine(DelayStart());
    }

    IEnumerator DelayStart()
    {
        var prevTempo = (float)core.engine.GetTempo();

        //var steps = 100;
        var b = (float)core.engine.GetCurrentBeat();
        //for (float i = steps; i > 0; i--)
        //{
        //    var t = Mathf.Lerp(prevTempo, tempo,  1f - (i/steps));
        //    float beet = Mathf.Lerp(b, (b + 16f), 1f - (i / steps));
        //    Debug.Log(beet + "  " + (i / steps));
        //    core.engine.ScheduleParam(Engine.OSCParam.eng, beet, 0, "tempo", t );
        //}
        if (tempo * bpmscale < core.engine.GetTempo())
        {
            while (tempo *bpmscale < core.engine.GetTempo())
            {
                prevTempo -= 0.5f;
                core.engine.SetTempo(prevTempo);
                yield return new WaitForSeconds(0.0125f);
            }
        } else
        {
            while (tempo * bpmscale > core.engine.GetTempo())
            {
                prevTempo += 0.5f;
                core.engine.SetTempo(prevTempo);
                yield return new WaitForSeconds(0.0125f);
            }
        }


        //var iter = 100;
        //var b = core.engine.GetCurrentBeat();
        //while (Mathf.Abs(prevTempo - tempo) != tempo)
        //{
        //    if(iter > 0)
        //        iter--;
        //    core.engine.ScheduleParam(Engine.OSCParam.eng, b + ((16 / (iter +1))), 0, "tempo", prevTempo - ((prevTempo - tempo) / (1+iter) ));
        //}


        yield return new WaitForDivision(rplayer.m_core, 4);
        

        chordList = ChordSequences[0].ChordList;
        m_beatOffset = Mathf.Ceil((float)core.engine.GetCurrentBeat());

        musicPlayer.volume = m_startVolume;
        musicPlayer.PlayScheduled(AudioSettings.dspTime + Gestrument.Engine.globalOffset + offsetfix + (m_beatOffset - core.engine.GetCurrentBeat()) * ((60 / core.engine.GetTempo())));
        for (int i = 0; i < chordList.Count; i++)
        {
            core.engine.SetScaleForSlot(i + 1, 0, chords(chordList[i].chord.ToString()));
        }
        m_allowPlay = true;

        core.engine.SetTempo(tempo);
    }

    void StopTrack()
    {
        StartCoroutine(DelayStop());
    }

    IEnumerator DelayStop()
    {
        while(musicPlayer.volume > 0)
        {
            musicPlayer.volume -= 0.05f;
            yield return new WaitForSeconds(0.1f);
        }
        musicPlayer.Stop();

        var prevTempo = (float)core.engine.GetTempo();
        while (core.engine.GetTempo() < rplayer.m_track.m_tempo)
        {
            prevTempo += 0.25f;
            core.engine.SetTempo(prevTempo);
            //core.engine.ScheduleParam(Engine.OSCParam.eng,)
            yield return new WaitForSeconds(0.0125f);
            //yield return new WaitForDivision(core, 2);
        }

        yield return new WaitForDivision(core, 4);
        m_allowPlay = false;
        core.engine.SetTempo(rplayer.m_track.m_tempo);
        core.engine.SetScaleForSlot(0, 0, rplayer.m_track.m_scale.Split()[1]);
        core.engine.SetParamValue(Engine.EventObject.Engine, 0, "scaleslotindex", 0);

        for (int i = 0; i < core.engine.NumInstruments; i++)
        {
            core.engine.ScheduleParam(Engine.OSCParam.pit, Mathf.Ceil((float)core.engine.GetCurrentBeat()), i, "ctranspose", 0);
        }

        
    }

    public void SetMixerMasterVolume(System.Single val)
    {
        musicPlayer.outputAudioMixerGroup.audioMixer.SetFloat("master", Mathf.Log10(val) * 20);
    }

    private void OnAudioFilterRead(float[] data, int channels)
    {
        if (rplayer && m_allowPlay)
        {
            currentBeat = (int)(rplayer.m_core.engine.GetCurrentBeat() - m_beatOffset + 1f); // chord lookahead//(int)(rplayer.m_beat+0.1f);

            if (ChordSequences.Count > 0)
            {
                for (int i = 0; i < ChordSequences.Count; i++)
                {
                    gestrumentChordSequence = ChordSequences[i];



                    if (gestrumentChordSequence != null)
                    {
                        for (int j = 0; j < gestrumentChordSequence.changeAtBeat.Length; j++)
                        {
                            if ((currentBeat % ChordListModulo) == gestrumentChordSequence.startBeat + gestrumentChordSequence.changeAtBeat[j])
                            {
                                NextChord(j);
                            }
                        }
                    }
                }
            }
        }
    }



    void FixedUpdate()
    {
        if (rplayer && m_allowPlay)     //      25                              24= 1  * 
        {
            var properTime = (int)(((rplayer.m_core.engine.GetCurrentBeat() - m_beatOffset) * samplesPerBeat - (samplerate * (Gestrument.Engine.globalOffset + offsetfix))) % (ChordListModulo * samplesPerBeat));//musicClip.samples);

            if (Mathf.Abs(musicPlayer.timeSamples - properTime) < 2000)
            {

                if (musicPlayer.timeSamples < properTime - 500f)
                    musicPlayer.pitch = 1.005f;
                else if (musicPlayer.timeSamples > properTime + 500f)
                    musicPlayer.pitch = 0.995f;
                else
                    musicPlayer.pitch = 1;
            }
            else
            {
                musicPlayer.timeSamples = properTime;
            }

            if (cycleLength > 0 && currentBeat == startBeat + cycleLength + 1)
            {
                currentBeat = startBeat;
                //    musicPlayer.time = startBeat * (60 / tempo);
            }
        }
    }


    public void NextChord(int index)
    {
        
        var cS = gestrumentChordSequence;
        if (currentChord == cS.ChordList[index].chord && root == cS.ChordList[index].root) return;

        scalePos = (scalePos + 1) % 2;

        core.engine.SetScaleForSlot(scalePos, 0, chords(cS.ChordList[index].chord.ToString()));

        //core.engine.ScheduleParam(Engine.OSCParam.eng, 0, 0, "scaleslotindex", index+1);

        var atBeat = (cS.startBeat + cS.changeAtBeat[index]) + (m_beatOffset - core.engine.GetCurrentBeat()) % ChordListModulo;

        for (int i = 0; i < core.engine.NumInstruments; i++)
        {
            core.engine.ScheduleParam(Engine.OSCParam.pit, core.engine.GetCurrentBeat()+atBeat,i,"ctranspose", (float)cS.ChordList[index].root);
        }

        core.engine.ScheduleParam(Engine.OSCParam.eng, core.engine.GetCurrentBeat()+atBeat, 0, "scaleslotindex", scalePos);

        root = cS.ChordList[index].root;
        currentChord = cS.ChordList[index].chord;

        
        //chordID = index % chordList.Count;
        if (text) text.text = root.ToString() + " " + currentChord.ToString();
    }



    public string chords(string pos)
    {
        string chordNotes;
        switch (pos)
        {
            case "major":
                chordNotes = "[4, 7, 12]";
                break;
            case "minor":
                chordNotes = "[3, 7, 12]";
                break;
            case "_7":
                chordNotes = "[4, 7, 10, 12]";
                break;
            default:
                chordNotes = null;
                break;
        }

        return chordNotes;
    }

    private void OnValidate()
    {
        if (syncAsset != null && musicClip == null)
        {
            musicClip = syncAsset.musicClip;
            tempo = syncAsset.tempo;
            timesig = syncAsset.timesig;
            length = syncAsset.musicClip.length;
            startBeat = syncAsset.startBeat;
            cycleLength = syncAsset.cycleLength;

        }

    }
}
