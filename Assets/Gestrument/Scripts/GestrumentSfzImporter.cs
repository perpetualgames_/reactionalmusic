﻿#if UNITY_EDITOR



using UnityEngine;
using UnityEditor.AssetImporters;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;

[ScriptedImporter(1, "sfz")]
public class GestrumentSfzImporter : ScriptedImporter
{
    public float m_Scale = 1;
    GestrumentSampleBank sb;

    public override void OnImportAsset(AssetImportContext sfz)
    {

        ReadSfz(sfz.assetPath);

        sfz.AddObjectToAsset("main obj", sb);

        var samplebank = sfz.assetPath.Remove(sfz.assetPath.Length - 4, 4);

        

        GameObject obj = new GameObject("test.prefab");
        obj.AddComponent<GestrumentSampler>();
        obj.AddComponent<AudioSource>();
        obj.GetComponent<GestrumentSampler>().sampleBank = (GestrumentSampleBank)AssetDatabase.LoadAssetAtPath(sfz.assetPath, typeof(GestrumentSampleBank));
        var path = Path.Combine(Path.GetDirectoryName(sfz.assetPath), obj.name);
        var pf = PrefabUtility.SaveAsPrefabAsset(obj, path);

        DestroyImmediate(obj);
    }

    void ReadSfz(string p)
    {
        sb = ScriptableObject.CreateInstance<GestrumentSampleBank>();

        var source = new StreamReader(p);
        var fileContents = source.ReadToEnd();
        source.Close();
        var lines = fileContents.Split("\n"[0]);

        int lo = 0, hi = 0, center = 0, seq_length = 0, hivel = 0, lovel = 0;// seq_position = 0;// loopStart = 0, loopEnd = 0, 
        int transpose = 0; 
        int group = -1;
        int region = -1;
        bool groupSetup = false;
        //foreach (string line in lines)
        for (int i = 0; i < lines.Length; i++)
        {
            var line = lines[i];

            if (line.Contains("<group>"))
            {
                sb.group.Add(new Group());
                group++;
                groupSetup = true;
                region = -1;
                transpose = 0;
            }
            //  Debug.Log(i);

            if (line.Contains("<region>"))
            {
                if (sb.group.Count == 0)
                {
                    sb.group.Add(new Group());
                    group++;
                }

                sb.group[group].region.Add(new Region());
                region++;
                groupSetup = false;
                transpose = 0;
            }
            if (line.Contains("seq_length="))                                   // If sfz contains seq_lenght (round robin), check and create extra NoteData instances
            {
                var index = line.IndexOf("seq_length=");
                var l = line;
                l = l.Remove(0, index + 11);

                int.TryParse(l, out seq_length);
                sb.group[group].seq_length = seq_length;
            }

            if (line.Contains("tune="))                                   // If sfz contains seq_lenght (round robin), check and create extra NoteData instances
            {
                var index = line.IndexOf("tune=");
                var l = line;
                l = l.Remove(0, index + 5);
                int tune = 0;
                int.TryParse(l, out tune);
                if (groupSetup == false) sb.group[group].region[region].tune = tune;
            }

            // if (line.Contains("seq_position="))
            // {
            //     var index = line.IndexOf("seq_position=");
            //     var l = line;
            //     l = l.Remove(0, index + 13);
            //     int.TryParse(l, out seq_position);
            //     seq_position = seq_position - 1;
            //     //sb.group[group].region.seq_position = seq_position;
            // }


            // if (line.Contains("loop_start="))
            // {
            //     var index = line.IndexOf("loop_start=");
            //     var l = line;
            //     l = l.Remove(0, index + 11);
            //     string[] numbers = Regex.Split(l, @"\D+");
            //     int.TryParse(numbers[0], out loopStart);
            // }

            // if (line.Contains("loop_end="))
            // {
            //     var index = line.IndexOf("loop_end=");
            //     var l = line;
            //     l = l.Remove(0, index + 9);
            //     string[] numbers = Regex.Split(l, @"\D+");
            //     int.TryParse(numbers[0], out loopEnd);
            // }
            if (line.Contains("hivel="))
            {
                var index = line.IndexOf("hivel=");
                var l = line;
                l = l.Remove(0, index + 6);
                string[] numbers = Regex.Split(l, @"\D+");
                int.TryParse(numbers[0], out hivel);
                if (groupSetup)
                    sb.group[group].vel_max = hivel;
                else
                    sb.group[group].region[region].vel_max = hivel;
            }

            if (line.Contains("lovel="))
            {
                var index = line.IndexOf("lovel=");
                var l = line;
                l = l.Remove(0, index + 6);
                string[] numbers = Regex.Split(l, @"\D+");
                int.TryParse(numbers[0], out lovel);
                if (groupSetup)
                    sb.group[group].vel_min = lovel;
                else
                    sb.group[group].region[region].vel_min = lovel;
            }

            if (line.Contains("lokey="))
            {
                var index = line.IndexOf("lokey=");
                var l = line;
                l = l.Remove(0, index + 6);
                //l = l.Remove(3, l.Length - 3);    
                string[] numbers = Regex.Split(l, @"\D+");
                if (!int.TryParse(numbers[0], out lo))
                    lo = NoteNameToMidi(l);

                if (groupSetup)
                    sb.group[group].lokey = lo;
                else
                    sb.group[group].region[region].lokey = lo;
            }

            if (line.Contains("hikey="))
            {
                var index = line.IndexOf("hikey=");
                var l = line;
                l = l.Remove(0, index + 6);
                //l = l.Remove(3, l.Length - 3);
                string[] numbers = Regex.Split(l, @"\D+");
                if (!int.TryParse(numbers[0], out hi))
                    hi = NoteNameToMidi(l);

                if (groupSetup)
                    sb.group[group].hikey = hi;
                else
                    sb.group[group].region[region].hikey = hi;

            }

            if (line.Contains("pitch_keycenter="))
            {
                var index = line.IndexOf("pitch_keycenter=");
                var l = line;
                l = l.Remove(0, index + 16);
                //l = l.Remove(3, l.Length - 3);

                string[] numbers = Regex.Split(l, @"\D+");
                if (!int.TryParse(numbers[0], out center))
                    center = NoteNameToMidi(l);

                if (groupSetup)
                    sb.group[group].pitch_keycenter = center;
                else
                    sb.group[group].region[region].pitch_keycenter = center;

            }

            if (line.Contains(" key="))
            {
                var index = line.IndexOf(" key=");
                var l = line;
                l = l.Remove(0, index + 5);

                string[] numbers = Regex.Split(l, @"\D+");
                if(!int.TryParse(numbers[0], out center))
                    center = NoteNameToMidi(l);

                if (groupSetup)
                    sb.group[group].key = center;
                else
                    sb.group[group].region[region].key = center;
            }



            if (line.Contains("transpose="))
            {
                var index = line.IndexOf("transpose=");
                var l = line;
                l = l.Remove(0, index + 10);
                //string numbers;
                //Regex.Match(l,@"^-?\d*\.{0,1}\d+$").Result(numbers);
                Regex rgxNumber = new Regex("([-+]{0,1}[0-9]+)");
                Match mNumber = rgxNumber.Match(l);
                string result = mNumber.Groups[1].Value;
                int.TryParse(result, out transpose);
                sb.group[group].region[region].transpose = transpose;
                 Debug.Log(transpose);
            }


            if (line.Contains("sample="))
            {
                int index = line.IndexOf("sample=");
                string str = line;
                str = line.TrimEnd('\r', '\n');
                str = str.Remove(0, index + 7);
                string[] file = Regex.Split(str, ".wav");
                var clipPath = (Path.GetDirectoryName(p) + "/" + file[0] + ".wav");
                if (file[0].StartsWith(".."))
                {
                    file[0] = file[0].Remove(0, 3);

                    clipPath = Path.GetDirectoryName(Path.GetDirectoryName(p)) + "/" + file[0] + ".wav";
                }

                Debug.Log(clipPath);
                sb.group[group].region[region].sample = AssetDatabase.LoadAssetAtPath<AudioClip>(clipPath) as AudioClip;

                if (sb.group[group].region[region].sample == null)
                {
                    var namesplit = Regex.Split(file[0], "\\\\");
                    var clipNames = AssetDatabase.FindAssets(namesplit[namesplit.Length - 1]);
                    //Debug.Log(file[0]);
                    var clipName = AssetDatabase.GUIDToAssetPath(clipNames[0]);
                    sb.group[group].region[region].sample = AssetDatabase.LoadAssetAtPath<AudioClip>(clipName) as AudioClip;
                }
            }
        }
    }
    int NoteNameToMidi(string n)
    {
        n = n.ToLower();
        string[] note = Regex.Split(n, @"\d");
        string oct = Regex.Match(n, @"\d").Value;
        string[] names = { "c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b" };
        var pos = System.Array.IndexOf(names, note[0]);

        int octave;
        int.TryParse(oct, out octave);
        int midinote = pos + ((octave + 1) * 12);

        return midinote;
    }



}

#endif