﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityOSC;
using Gestrument;
using Reactional;

public class ReactionalUIHelper : MonoBehaviour
{
    public List<TimelineSectionAsset> sections = new List<TimelineSectionAsset>();
    public TimelineSectionAsset currentSection;
    public GestrumentCore core;
    public double beat = 0;
    //public bool testplay = false;

    [Range(40, 350)]
    public double tempo = 120;

    [Range(-7, 7)]
    public int transpose = 0;

    [Range(0, 1)]
    public float velocity = 0.75f;

   // private float processRange = 1;
    public int processRangeCount = 0;

    public ReactionalPlayer rplayer;

    [Range(-1, 1)]
    public float ranges = 0;

    [Range(0, 1)]
    public float Slider2 = 1;

    [Range(0, 1)]
    public float Slider3 = 1;

    [Range(0, 1)]
    public float Slider4 = 0.5f;

    [Range(0, 1)]
    public float Slider5 = 0.5f;

    public float bend = 0;

    public UnityEngine.UI.Text scaleText;

    public UnityEngine.Canvas canvas;
    public GameObject buttonprefab;
    public GameObject progressPrefab;

    //IEnumerator[] m_loop = new IEnumerator[8];
    IEnumerator[] m_cursorSeq = new IEnumerator[8];
    bool[] trackIsPlaying = new bool[8];
   // bool gate = true;

//    bool play = false;

  //  bool audioGate = true;

    public bool ignore_gateoff = false;
    public bool autoPlay = true;

    double adjustedBeat;

    public int activeId = 0;
    public bool flipRange = false;

    public int root = 8;
    public UnityEngine.UI.Text rootText;
    public UnityEngine.UI.Slider progress;
    public UnityEngine.UI.Text partText;
    public UnityEngine.UI.Text functionText;

    public bool quant = false;
    public float quantVal = 4;
    public GameObject toggleprefab;
    public Text tempotext;
    public Text activePart;

    public List<Button> sectionbuttons = new List<Button>();
    public List<Button> motifbuttons = new List<Button>();
    public List<Button> stingerbuttons = new List<Button>();

    // Start is called before the first frame update
    void Awake()
    {
        rplayer = FindObjectOfType<ReactionalPlayer>();
        core = rplayer.m_core;
        sections = rplayer.m_track.m_sections;
        currentSection = sections[0];
        if (partText) partText.text = currentSection.name;
        Debug.Log(currentSection.name);
    }

    private void Start()
    {
        //canvas = new UnityEngine.Canvas();
        for (int n= 0; n < rplayer.m_track.m_sections.Count; n++)
        {
            if (buttonprefab)
            {
                int offset = 0;
                int index = n + 1;
                GameObject btn;
                btn = Instantiate(buttonprefab, canvas.transform);
                btn.name = "button" + n;
                //btn.transform.position = new Vector2(0, (30 * index));
                Button but = btn.GetComponent<Button>();
                //btn.GetComponent<RectTransform>(). //anchoredPosition.Set(100, 100);
                btn.GetComponent<RectTransform>().position = new Vector2(100 + offset, 200-(30 * index));
                btn.GetComponentInChildren<Text>().text = rplayer.m_track.m_sections[n].name;
                but.onClick.RemoveAllListeners();
                
                but.onClick.AddListener( () => rplayer.SetPart(index,1) );
                sectionbuttons.Add(but);

                GameObject btn2;
                btn2 = Instantiate(buttonprefab, canvas.transform);
                Button but2 = btn2.GetComponent<Button>();
                btn2.GetComponent<RectTransform>().position = new Vector2(30 + offset, 200 - (30 * index));
                btn2.GetComponent<RectTransform>().sizeDelta = new Vector2(35, 20);
                btn2.GetComponentInChildren<Text>().text = "/ 2";
                but2.onClick.AddListener(() => rplayer.SetPart(index,0.5f));
                sectionbuttons.Add(but2);

                btn2 = Instantiate(buttonprefab, canvas.transform);
                but2 = btn2.GetComponent<Button>();
                btn2.GetComponent<RectTransform>().position = new Vector2(170 + offset, 200 - (30 * index));
                btn2.GetComponent<RectTransform>().sizeDelta = new Vector2(35, 20);
                btn2.GetComponentInChildren<Text>().text = "* 2";
                but2.onClick.AddListener(() => rplayer.SetPart(index, 2));
                sectionbuttons.Add(but2);

            }
        }
        if (progressPrefab)
        {
            GameObject prog;
            prog = Instantiate(progressPrefab, canvas.transform);
            prog.GetComponent<RectTransform>().position = new Vector2(100, 200);
            StartCoroutine(UpdateProgress(prog.GetComponent<Slider>(), prog.GetComponentInChildren<Text>()));
        }




        for (int n = 0; n < rplayer.m_track.m_motifs.Count; n++)
        {
            if (buttonprefab)
            {
                int offset = 200;
                int index = n + 1;
                GameObject btn;
                btn = Instantiate(buttonprefab, canvas.transform);
                btn.name = "button" + n;
                //btn.transform.position = new Vector2(0, (30 * index));
                Debug.Log(n);
                Button but = btn.GetComponent<Button>();
                //btn.GetComponent<RectTransform>(). //anchoredPosition.Set(100, 100);
                btn.GetComponent<RectTransform>().position = new Vector2(100 + offset, 200 - (30 * index));
                btn.GetComponentInChildren<Text>().text = rplayer.m_track.m_motifs[n].name;
                but.onClick.RemoveAllListeners();

                but.onClick.AddListener(() => rplayer.PlayMotif(index, 1));
                motifbuttons.Add(but);

                GameObject btn2;
                btn2 = Instantiate(buttonprefab, canvas.transform);
                Button but2 = btn2.GetComponent<Button>();
                btn2.GetComponent<RectTransform>().position = new Vector2(30 + offset, 200 - (30 * index));
                btn2.GetComponent<RectTransform>().sizeDelta = new Vector2(35, 20);
                btn2.GetComponentInChildren<Text>().text = "/ 2";
                but2.onClick.AddListener(() => rplayer.PlayMotif(index, 0.5f));
                motifbuttons.Add(but2);

                btn2 = Instantiate(buttonprefab, canvas.transform);
                but2 = btn2.GetComponent<Button>();
                btn2.GetComponent<RectTransform>().position = new Vector2(170 + offset, 200 - (30 * index));
                btn2.GetComponent<RectTransform>().sizeDelta = new Vector2(35, 20);
                btn2.GetComponentInChildren<Text>().text = "* 2";
                but2.onClick.AddListener(() => rplayer.PlayMotif(index, 2));
                motifbuttons.Add(but2);
            }
        }

        for (int n = 0; n < rplayer.m_track.m_stingers.Count; n++)
        {
            if (buttonprefab)
            {
                var offset = 400;
                int index = n + 1;
                GameObject btn;
                btn = Instantiate(buttonprefab, canvas.transform);
                btn.name = "button" + n;
                //btn.transform.position = new Vector2(0, (30 * index));
                Debug.Log(n);
                Button but = btn.GetComponent<Button>();
                //btn.GetComponent<RectTransform>(). //anchoredPosition.Set(100, 100);
                btn.GetComponent<RectTransform>().position = new Vector2(100 + offset, 200 - (30 * index));
                btn.GetComponentInChildren<Text>().text = rplayer.m_track.m_stingers[n].name;
                but.onClick.RemoveAllListeners();

                but.onClick.AddListener(() => rplayer.PlayStinger(index, 1));
                stingerbuttons.Add(but);

                GameObject btn2;
                btn2 = Instantiate(buttonprefab, canvas.transform);
                Button but2 = btn2.GetComponent<Button>();
                btn2.GetComponent<RectTransform>().position = new Vector2(30 + offset, 200 - (30 * index));
                btn2.GetComponent<RectTransform>().sizeDelta = new Vector2(35, 20);
                btn2.GetComponentInChildren<Text>().text = "/ 2";
                but2.onClick.AddListener(() => rplayer.PlayStinger(index, 0.5f));
                stingerbuttons.Add(but2);

                btn2 = Instantiate(buttonprefab, canvas.transform);
                but2 = btn2.GetComponent<Button>();
                btn2.GetComponent<RectTransform>().position = new Vector2(170 + offset, 200 - (30 * index));
                btn2.GetComponent<RectTransform>().sizeDelta = new Vector2(35, 20);
                btn2.GetComponentInChildren<Text>().text = "* 2";
                but2.onClick.AddListener(() => rplayer.PlayStinger(index, 2));
                stingerbuttons.Add(but2);


            }
        }


        for (int n = 0; n < rplayer.m_availablePatterns.Count; n++)
        {
            if (buttonprefab)
            {
                int index = n + 1;
                GameObject btn;
                btn = Instantiate(buttonprefab, canvas.transform);
                btn.name = "button" + n;
                //btn.transform.position = new Vector2(0, (30 * index));
                Debug.Log(n);
                Button but = btn.GetComponent<Button>();
                //btn.GetComponent<RectTransform>(). //anchoredPosition.Set(100, 100);
                btn.GetComponent<RectTransform>().position = new Vector2(700, 200 - (30 * index));
                btn.GetComponentInChildren<Text>().text = rplayer.m_availablePatterns[n].name;
                but.onClick.RemoveAllListeners();

                but.onClick.AddListener(() => rplayer.PlayPattern(index));

            }
        }

        for (int n = 0; n < rplayer.m_track.m_altInstrumentations.Count; n++)
        {
            if (buttonprefab)
            {
                int index = n + 1;
                GameObject btn;
                btn = Instantiate(buttonprefab, canvas.transform);
                btn.name = "button" + n;
                //btn.transform.position = new Vector2(0, (30 * index));
                
                Button but = btn.GetComponent<Button>();
                //btn.GetComponent<RectTransform>(). //anchoredPosition.Set(100, 100);
                btn.GetComponent<RectTransform>().position = new Vector2(900, 200 - (30 * index));
                btn.GetComponentInChildren<Text>().text = rplayer.m_track.m_altInstrumentations[n].name;
                but.onClick.RemoveAllListeners();

                but.onClick.AddListener(() => rplayer.SwitchInstrumentation(index));

            }
        }



        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            GameObject tgl;
            int index = i;
            tgl = Instantiate(toggleprefab, canvas.transform);
            tgl.name = "mutetoggle" + i;
            Toggle tog = tgl.GetComponent<Toggle>();
            tgl.GetComponent<RectTransform>().position = new Vector2(100 + (25 * i), 230);
            tgl.GetComponentInChildren<Text>().text = i.ToString();
            tog.onValueChanged.RemoveAllListeners();

            tog.onValueChanged.AddListener((val) => rplayer.ToggleMute(val,index) );

        }

        //core.engine.SetPlaying(false);
        //for (int i = 0; i < 16; i++)
        //{
        //    /*
        //                static char* defaultDurationStrings[GE_DUR_PULSE_GEN_MAX_DURS] =
        //    {
        //        "1/1", "1/2", "1/4", "1/8", "1/16", "1/32",
        //        "1/1.", "1/2.", "1/4.", "1/8.", "1/16.", "1/32.",
        //        "1/1T", "1/2T", "1/4T", "1/8T", "1/16T", "1/32T",
        //    };*/

        //    //core.engine.SetParamValue(Engine.EventObject.PulseGen, i, "selecteddurations", "tfffffffffffffffff");
        //    core.engine.ScheduleParam(Engine.OSCParam.pul, -1, i, "selecteddurations", "ffftfffffffffftftt");
        //    core.engine.ScheduleParam(Engine.OSCParam.pit, -1, i, "minpitch", 0);
        //    core.engine.ScheduleParam(Engine.OSCParam.pit, -1, i, "maxpitch", 127);
        //    //core.engine.SetParamValue(Engine.EventObject.PitchGen, i,)
        //}




        // MAKE SURE PERCUSSION DOES NOT TRANSPOSE
        //for (int i = 0; i < currentSection.tracks.Count; i++)
        //{
        //    if (currentSection.tracks[i].isPercussion)
        //    {
        //        core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "uselocalscale", true);
        //        core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "localscale", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]");
        //        core.engine.SetParamValue(Engine.EventObject.PulseGen, i, "selecteddurations", "tttttfffffffffffff");
        //    }
        //}



        //for (int i = 0; i < 16; i++)
        //{
        //    if (i < 3) continue;
        //    core.engine.MapSlider("Slider 1", "pul", i, "velocity");
        //    core.engine.MapSliderVia("Slider 2", "pul", i, "main");
        //    core.engine.MapSliderVia("Slider 3", "pit", i, "main");
        //    if (i != 12 && i != 13)
        //        core.engine.MapSlider("Slider 4", "pit", i, "ctranspose");
        //    else
        //    {
        //        if (i == 12)
        //            core.engine.MapSlider("Slider 5", "pit", i, "ctranspose");
        //        else
        //            core.engine.MapSlider("Slider 6", "pit", i, "ctranspose");
        //    }

        //    core.engine.SetSliderValue(3, 1);
        //    core.engine.SetSliderValue(4, 0.5f);
        //    core.engine.SetSliderValue(5, 0.5f);

        //    core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "ctranspose.mapmin", -12);
        //    core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "ctranspose.mapmax", 12);
        //    core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "ctransposequant", false);
        //    // core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "ctransposequant", "true");

        //}



       // StartCoroutine(DelayStart());
    }

    //IEnumerator DelayStart()
    //{
        
    //    if (currentSection != null)
    //    {
    //        Debug.Log(currentSection.setupMessages.Count);
    //        for (int i = 0; i < currentSection.setupMessages.Count; i++)
    //        {
    //            core.engine.Enqueue(currentSection.setupMessages[i].data);
    //        }
    //    }
    //    yield return new WaitForSeconds(1);
    //    core.engine.SetCurrentBeat(-2);






    //    StartCoroutine(SetTransposeSlot());
    //    StartCoroutine(SetVelocity());


    //    play = true;
    //}

    void Update()
    {
        if (tempotext)
        {
            tempotext.text = core.engine.GetTempo().ToString();
            partText.text = rplayer.m_currentSection.name.ToString();
        }
    }

    IEnumerator UpdateProgress(Slider progresSlider, Text sliderName)
    {
        var colr = new Color(1f, 0.24f, 0.8f, 1);
        while (true)
        {
            yield return new WaitForSeconds(0.05f);
            progresSlider.value = (float)((rplayer.m_beat / (rplayer.m_currentSection.stopBeat - rplayer.m_currentSection.startBeat)) % (rplayer.m_currentSection.stopBeat - rplayer.m_currentSection.startBeat));

            for (int i = 0; i < sectionbuttons.Count; i++)
            {
                if (rplayer.m_currentSection == rplayer.m_sections[i / 3] || rplayer.m_currentSection == rplayer.m_sectionsHalf[i / 3] || rplayer.m_currentSection == rplayer.m_sectionsDouble[i / 3] )
                    sectionbuttons[i].GetComponent<Image>().color = colr;
                else
                    sectionbuttons[i].GetComponent<Image>().color = Color.white;
            }

            for (int i = 0; i < motifbuttons.Count; i++)
            {
                if (rplayer.m_currentmotif == rplayer.m_motifs[i / 3] || rplayer.m_currentmotif == rplayer.m_motifsHalf[i / 3] || rplayer.m_currentmotif == rplayer.m_motifsDouble[i / 3])
                    motifbuttons[i].GetComponent<Image>().color = colr;
                else
                    motifbuttons[i].GetComponent<Image>().color = Color.white;
            }
        }
    }



    IEnumerator SetTransposeSlot()
    {
            yield return new WaitForDivision(core, 4);
            rplayer.currentTranspose = transpose;
            core.engine.ScheduleTransposeSlot(Mathf.Round((float)beat), transpose + 7);

            //for (int i = 0; i < currentSection.tracks.Count; i++)
            //{
            //    if (currentSection.tracks[i].isPercussion)
            //    {
            //        core.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, beat - 0.15f, i, "mtranspose", -transpose);
            //    }
            //}       

    }

    public void SetScale()
    {
        core.engine.SetScaleForSlot(0, root, scaleText.text);
    }

    public void SetRoot(System.Single r)
    {
        root = (int)r;

        rootText.text = getRootName((int)r);
    }

    public void SetTempo(System.Single t)
    {
        tempo = t;
        core.engine.SetTempo(tempo);
    }

    public void SetVelocity(System.Single v)
    {
        velocity = v;
        StartCoroutine(SetVelocity());
    }

    public void SetQuantVal(System.Single q)
    {
        if (q == 0) q = 0.25f;
        else
            q = q * 0.25f;
        rplayer.m_quantVal = q;
    }


    public void SetSpeed(System.Single q)
    {

        rplayer.m_speedscale = q/4;
    }



    public void SetTransposeSlot(System.Single t)
    {
        transpose = (int)t;
        functionText.text = getFunctionName((int)t);
        rplayer.SetTransposeSlot(t);
    }

    public void SetChromaticTranspose(System.Single t)
    {
        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            rplayer.m_core.engine.SetParamValue(Engine.EventObject.PitchGen, i,"ctranspose", t);
        }
    }

    public void SetBend(System.Single b)
    {
        bend = b;
    }

    public void SetRanges(System.Single r)
    {
        ranges = r;
    }

    public bool SetFlip(bool b)
    {
        flipRange = b;
        return b;
    }


    public void Flip(bool value)
    {
        if (value == true)
        {
            rplayer.m_flipRange = true;
            flipRange = true;
        }
        else
        {
            rplayer.m_flipRange = false;
            flipRange = false;
        }
    }

    public void Quant(bool value)
    {
        if (value == true)
        {
            rplayer.m_quant = true;
        }
        else
        {
            rplayer.m_quant = false;
        }
    }

    public void Hide(bool value)
    {
        if (value == true)
        {
            canvas.enabled = false;
        }
        else
        {
            canvas.enabled = true;
        }
    }


    IEnumerator SetVelocity()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.05f);
            rplayer.m_core.engine.SetSliderValue(1, velocity);
            //core.engine.SetSliderValue(2, Slider2);
            //core.engine.SetSliderValue(3, Slider3);
            //core.engine.SetSliderValue(4, Slider4);
            //core.engine.SetSliderValue(5, Slider5 + bend);
            //core.engine.SetSliderValue(6, Slider5 - bend);
            // core.engine.SetParamValue(Engine.EventObject.PulseGen, 1, "velocity", 0.4f + (velocity * 0.6f));

        }

    }


    double Remap(double s, double a1, double a2, double b1, double b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }


    double Flip(double num, double min, double max)
    {
        return (max + min) - num;
    }

    public string getRootName(int n)
    {

        string[] names = { "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" };

        return names[n];

    }


    public string getFunctionName(int n)
    {
        string[] functionNames = { "I", "II", "III", "IV", "V", "VI", "VII" };
        n = (n % 7);
        Debug.Log(n);
        if (n < 0) n = n + 7;
        return functionNames[n];
    }


    private void SetPart(int part)
    {
        Debug.Log(part);
        StartCoroutine(SetPartRoutine(part));
    }


    IEnumerator SetPartRoutine(int part)
    {
        //var rplayer = FindObjectOfType<ReactionalPlayer>();

        if (part == 666)
        {
            yield return new WaitForDivision(core, 4);
            rplayer.m_adjustedBeat = Mathf.Round((float)core.engine.GetCurrentBeat());
            rplayer.m_activeId = 1;
            rplayer.m_currentSection = sections[0];
            rplayer.autoPlay = true;
            if (partText) partText.text = "Full track";
            yield break;
        }

        yield return new WaitForDivision(core, 4);
        rplayer.m_adjustedBeat = Mathf.Round((float)core.engine.GetCurrentBeat());
        rplayer.m_activeId = part;
        rplayer.m_currentSection = sections[part - 1];
        if (partText) partText.text = currentSection.name;
        rplayer.autoPlay = false;

    }



    public void SaveCurrentSettingsAsState(string name)
    {
        GestrumentState state = new GestrumentState();
        var saveload = FindObjectOfType<GestrumentStateSaveLoad>();
        state.stateName = name;

        // Save key & scale
        state.AddOSC(core.engine.SetScaleForSlot(0, (float)root, (string)scaleText.text));

        //// Save transpose slot
        state.AddOSC(core.engine.ScheduleTransposeSlot(0, transpose + 7));

        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            //if (rplayer.m_track.m_timelineTracks[i].isPercussion)
            //    state.AddOSC(core.engine.ScheduleParam(Gestrument.Engine.OSCParam.pit, 0, i, "mtranspose", -transpose));
        }

        for(int i = 1; i < 8; i++)
            state.AddOSC(core.engine.ScheduleParam(Engine.OSCParam.eng, 0, 0, "slider"+i, core.engine.GetSliderValue(i)));

        core.engine.GetParamValue(Engine.EventObject.Engine, 0, "transposeslotindex", out int ind);
        state.transposeslot = ind;
        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            core.engine.GetParamValue(Engine.EventObject.PitchGen, i, "ctranspose", out int c);
            state.ctranspose.Add(c);
            core.engine.GetParamValue(Engine.EventObject.PitchGen, i, "mtranspose", out int m);
            state.mtranspose.Add(m);
        }


        state.editorVals.Add("transpose", transpose);
        state.editorVals.Add("velocity", velocity);
        state.editorVals.Add("tempo", (float)tempo);
        state.editorVals.Add("root", root);
       // state.editorVals.Add("ctranspose", )


        UnityEngine.UI.Slider[] slist = FindObjectsOfType<UnityEngine.UI.Slider>();
        for (int i = 0; i < slist.Length; i++)
        {
            state.editorVals.Add("UI "+slist[i].transform.name, slist[i].value);
        }

        UnityEngine.UI.Toggle[] tlist = FindObjectsOfType<UnityEngine.UI.Toggle>();
        for (int i = 0; i < tlist.Length; i++)
        {
            state.editorBools.Add("UI " + tlist[i].transform.name, tlist[i].isOn);
        }

        state.activeSectionId = rplayer.m_currentSection.id;
        if(rplayer.m_currentmotif) state.activeMotifId = rplayer.m_currentmotif.id;
        state.instrumentation = rplayer.instrumentationId;

        saveload.SaveState(state);
               
    }

    public void LoadSettings(string name)
    {
        StartCoroutine(LoadCoroutine(name));
    }

    IEnumerator LoadCoroutine(string name)
    {
        var saveload = FindObjectOfType<GestrumentStateSaveLoad>();
        yield return new WaitForDivision(core, 4);
        GestrumentState state = saveload.LoadState(name);

        //Slider2 = core.engine.GetSliderValue(2);
        //Slider3 = core.engine.GetSliderValue(3);
        //Slider4 = core.engine.GetSliderValue(4);
        //Slider5 = core.engine.GetSliderValue(5);

        float t;
        state.editorVals.TryGetValue("transpose", out t);
        transpose = (int)t;

        state.editorVals.TryGetValue("velocity", out velocity);

        float temp;
        state.editorVals.TryGetValue("tempo", out temp);
        tempo = (double)temp;

        for (int i = 0; i < state.oscMessages.Count; i++)
        {
            if (state.oscMessages[i].Adress == "/slt/eng") {
                scaleText.GetComponentInParent<UnityEngine.UI.InputField>().text = state.oscMessages[i].Data[3].ToString();
               }
        }

        UnityEngine.UI.Slider[] slist = FindObjectsOfType<UnityEngine.UI.Slider>();
        for (int i = 0; i < slist.Length; i++)
        {
            float val;
            state.editorVals.TryGetValue("UI " + slist[i].transform.name, out val);
            slist[i].value = val;
           // state.editorVals.Add("UI " + slist[i].transform.parent.name, slist[i].value);
        }

        UnityEngine.UI.Toggle[] tlist = FindObjectsOfType<UnityEngine.UI.Toggle>();
        for (int i = 0; i < tlist.Length; i++)
        {
            bool val;
            state.editorBools.TryGetValue("UI " + tlist[i].transform.name, out val);
            tlist[i].isOn = val;
        }

        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "ctranspose", state.ctranspose[i]);
            core.engine.SetParamValue(Engine.EventObject.PitchGen, i, "mtranspose", state.mtranspose[i]);
        }
        core.engine.SetParamValue(Engine.EventObject.Engine, 0, "transposeslotindex", state.transposeslot);

        rplayer.instrumentationId = state.instrumentation;
        rplayer.m_track.m_instrumentation = rplayer.m_track.m_altInstrumentations[state.instrumentation].instrumentation;

        for (int i = 0; i < rplayer.m_sections.Count; i++)
        {
            if (rplayer.m_sections[i].id == state.activeSectionId) rplayer.m_currentSection = rplayer.m_sections[i];
            else if(rplayer.m_sectionsHalf[i].id == state.activeSectionId) rplayer.m_currentSection = rplayer.m_sectionsHalf[i];
            else if (rplayer.m_sectionsDouble[i].id == state.activeSectionId) rplayer.m_currentSection = rplayer.m_sectionsDouble[i];
        }


        //StartCoroutine(rplayer.KillCursors(rplayer.m_currentmotif));

        if (state.activeMotifId != -1)
        {

            for (int i = 0; i < rplayer.m_motifs.Count; i++)
            {
                if (rplayer.m_motifs[i].id == state.activeMotifId) rplayer.m_currentmotif = rplayer.m_motifs[i];
                else if (rplayer.m_motifsHalf[i].id == state.activeMotifId) rplayer.m_currentmotif = rplayer.m_motifsHalf[i];
                else if (rplayer.m_motifsDouble[i].id == state.activeMotifId) rplayer.m_currentmotif = rplayer.m_motifsDouble[i];
            }

            StartCoroutine(rplayer.RecallMotif(rplayer.m_currentmotif));
        }

        
    }





    public void MakeDissonant(bool dissonant)
    {
        float dis = 0;
        
        for (int i = 0; i < rplayer.m_track.m_trackCount; i++)
        {
            if (dissonant)
            {
                dis = ((dis + i) / 3) % 2.5f;
            }
            rplayer.m_core.engine.ScheduleParam(Engine.OSCParam.pit, 0, i, "ctranspose", dis);
        }
    }
}




