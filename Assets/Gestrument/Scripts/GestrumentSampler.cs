﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestrumentSampler : MonoBehaviour
{
    public GestrumentSampleBank sampleBank;

    //double globalOffset = 0.05f;//0.125f;

    public int globalTranspose = 0;
    [Range(0, 1)]
    public float setVelocity = 0.8f;

    public float maxVelocity = 1;
    public float minVelocity = 0;

    [Range(0, 1)]
    public float volumeOffset = 1;

    [Range(0, 1)]
    public float vibrato = 0;

    [Range(1, 16)]
    public int polyphony = 4;

    public bool oneShot = false;

    public bool autoPanByPitch = false;
    public float autoPanMin = 21;
    public float autoPanMax = 108;

    public float starttime = 0;

    public bool spatialiseInstrument = false;
    public GameObject spatialParent;

    [Range(0, 8)]
    public float releaseTime = 0;
    int currentVoice = 0;
    int prevVoice = 0;

    AudioSource[] audioSource;
    AudioSource[,] voices = new AudioSource[127, 32];

    public bool fakeRoundRobin = false;
    [Range(0, 7)]
    public int rrRange = 3;

    int roundRobin = 0;

    private IEnumerator coroutine;

    private IEnumerator[] fadeOut;


    void Start()
    {
        var sourceSettings = GetComponent<AudioSource>();
        audioSource = new AudioSource[polyphony];
        fadeOut = new IEnumerator[polyphony];
        for (int i = 0; i < polyphony; i++)
        {
            GameObject go = new GameObject("voice" + i);
            var aS = go.AddComponent<AudioSource>();
            var lpf = go.AddComponent<AudioLowPassFilter>();
            aS.outputAudioMixerGroup = sourceSettings.outputAudioMixerGroup;
            aS.playOnAwake = sourceSettings.playOnAwake;
            aS.loop = sourceSettings.loop;
            aS.priority = sourceSettings.priority;
            aS.spatialBlend = sourceSettings.spatialBlend;
            aS.spatialize = sourceSettings.spatialize;
            aS.dopplerLevel = 0;// sourceSettings.dopplerLevel;
            go.transform.parent = gameObject.transform;
            go.transform.position = new Vector3(0, 0, 0);
            aS.rolloffMode = sourceSettings.rolloffMode;
            aS.minDistance = 5;// sourceSettings.minDistance;
            aS.maxDistance = 200; // sourceSettings.maxDistance;
            aS.GetComponent<AudioLowPassFilter>().enabled = false;

            audioSource[i] = aS;

            if (GetComponent<ADSR>())
            {
                var parentAdsr = GetComponent<ADSR>();
                var adsr = go.AddComponent<ADSR>();
                adsr.AttackTime = parentAdsr.AttackTime;
                adsr.DecayTime = parentAdsr.DecayTime;
                adsr.SustainLevel = parentAdsr.SustainLevel;
                adsr.SustainTime = parentAdsr.SustainTime;
                adsr.ReleaseTime = parentAdsr.ReleaseTime;
                adsr.Enabled = parentAdsr.Enabled;
            }
        }
    }

    public bool StartVoice(int voice, float pitch, float velocity, int instrument, double beat, double tempo = 0, double currentBeat = 0)
    {
        pitch = pitch + globalTranspose;                                // If sampler has a global transpose, then adjust pitch
        
        if (fadeOut[currentVoice] != null)                              // If fadeout couroutine is already active for said voice, kill it
            StopCoroutine(fadeOut[currentVoice]);

       
        voices[voice, instrument] = audioSource[currentVoice] as AudioSource;
        var aS = audioSource[currentVoice];
        aS.clip = null;

        int offset = 0;
        if (fakeRoundRobin)                                             // If fakeRoundRobin, then pick random close note and set pitch offset accordingly. Used to avoid repetitiveness of sound file
            offset = (int)Random.Range(0, rrRange);

        if (sampleBank != null)
            sampleBank.GetSample(pitch, velocity, aS, offset, this);        // SET SAMPLE

        if(aS.clip == false) return false; // if failed, return

        float normal = Mathf.InverseLerp(0, 1, velocity);               // SET SCALED VELOCITY
        velocity = Mathf.Lerp(minVelocity, maxVelocity, normal);
        aS.volume = velocity * volumeOffset;

        if(starttime!=0) aS.time = starttime;

        if (autoPanByPitch)                                             // SET AUTOPAN
        {
            float pan = Mathf.InverseLerp(autoPanMin, autoPanMax, pitch);
            pan = Mathf.Lerp(-1, 1, pan);
            aS.panStereo = pan;
        }


        if (spatialiseInstrument)                                       // SET SPATIALISATION STUFF
            aS.spatialBlend = 1;
        else
            aS.spatialBlend = 0;
        if (spatialParent)
            aS.transform.position = spatialParent.transform.position;



        if (GetComponent<ADSR>() != null)                               // If there is an ADSR envelope script, then update values/trigger.
        {
            UpdateADSR(aS);                      // Only used in inspector to pass values to children
            aS.GetComponent<ADSR>().Trigger(0);
            if (!aS.isPlaying || aS.loop == false) aS.Play();
        }
        else
        {
            aS.loop = false;                        
            if (!oneShot)
            {
                var time = Gestrument.Engine.globalOffset + (beat - currentBeat) * ((60 / tempo));
              //  Debug.Log("s "+beat);
                if (time > 0)
                    aS.PlayScheduled(AudioSettings.dspTime + time); // Schedule playback at the requested beat, converted into actual DSP-time, with a global offset.
            }
            //else
            //    aS.PlayOneShot(aS.clip);
        }

        //LPF
        //var lpf = aS.GetComponent<AudioLowPassFilter>();                // Set audio lowpass filter frequency based on the velocity passed.
        //lpf.cutoffFrequency = Mathf.Pow(10, velocity * 4f + 2.5f);

        //VOICE HANDLING
        prevVoice = currentVoice;
        currentVoice = (currentVoice + 1) % polyphony;
        roundRobin++;
        return true;
    }

    public bool StopVoice(int voice, int instrument, double beat, double tempo = 0, double currentBeat = 0, GestrumentCore core = null)       // When stopping, either stop directly, or start a couroutine which fades out the audio source over time.
    {
        if (core)
        {
            tempo = core.engine.GetTempo();
            currentBeat = core.engine.GetCurrentBeat();
        }
        AudioSource aS = voices[voice, instrument];
        if (!aS) return false;

        if (aS.GetComponent<ADSR>())
            aS.GetComponent<ADSR>().NoteOff();
        else if (aS.isPlaying && oneShot == false)
            if (releaseTime > 0)
            {
                var wait = (Gestrument.Engine.globalOffset + (beat - currentBeat) * ((60 / tempo)));
                fadeOut[prevVoice] = StartFade(aS, wait, releaseTime, 0f);
                StartCoroutine(fadeOut[prevVoice]);
            }
            else
                aS.SetScheduledEndTime(AudioSettings.dspTime + Gestrument.Engine.globalOffset + (beat - currentBeat) * ((60 / tempo)));
        return true;
    }


    public IEnumerator StartFade(AudioSource audioSource, double wait, float duration, float targetVolume)
    {
        yield return new WaitForSecondsRealtime((float)wait);
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        audioSource.Stop();
        yield break;
    }


    public void StopAllVoices()
    {
        StopAllCoroutines();
        for (int i = 0; i < audioSource.Length; i++)
        {
            if (audioSource[i].isPlaying)
                StartCoroutine(StartFade(audioSource[i], 0, 0.3f, 0f));
                    //audioSource[i].Stop();
            }
            
        
    }



    // Nevermind for now

        void UpdateADSR(AudioSource go)                 
    {
        if (GetComponent<ADSR>())
        {
            var parentAdsr = GetComponent<ADSR>();
            var adsr = go.GetComponent<ADSR>();
            adsr.AttackTime = parentAdsr.AttackTime;
            adsr.DecayTime = parentAdsr.DecayTime;
            adsr.SustainLevel = parentAdsr.SustainLevel;
            adsr.SustainTime = parentAdsr.SustainTime;
            adsr.ReleaseTime = parentAdsr.ReleaseTime;
            adsr.Enabled = parentAdsr.Enabled;
        }
    }

    public void ProcessAudioEvent(GestrumentCore.Event engineEvent)                         // Convenience-functions to directly pass Gestrument events into the Start and Stop functions.
    {
        if (engineEvent is GestrumentCore.NoteOnEvent)
        {
            GestrumentCore.NoteOnEvent noteEvent = (GestrumentCore.NoteOnEvent)engineEvent;
            StartVoice(noteEvent.Voice, noteEvent.Pitch, noteEvent.Velocity, noteEvent.Instrument, noteEvent.Beat);
        }
        else if (engineEvent is GestrumentCore.NoteOffEvent)
        {
            GestrumentCore.NoteOffEvent noteEvent = (GestrumentCore.NoteOffEvent)engineEvent;
            StopVoice(noteEvent.Voice, noteEvent.Instrument, noteEvent.Beat);
        }

    }

    // void LoopVoice(AudioSource aS, int n, int p)
    // { //// WIP WIP WIP
    //     NoteData nD = noteData[n];

    //     //aS.PlayOneShot(aS.clip);
    //     //aS.SetScheduledEndTime(AudioSettings.dspTime + nD.loopEnd[p]);
    //     var tS = Instantiate(aS) as AudioSource;
    //     var startClip = AudioClip.Create("start", nD.loopEnd[p], nD.clip[p].channels, nD.clip[p].frequency, false);
    //     var lClip = AudioClip.Create("temp", (nD.loopEnd[p] - nD.loopStart[p]), nD.clip[p].channels, nD.clip[p].frequency, false);
    //     float[] samples = new float[lClip.samples];//(nD.loopEnd[p] - nD.loopStart[p])*nD.clip[p].channels];
    //     float[] startSamples = new float[startClip.samples];
    //     nD.clip[p].GetData(samples, nD.loopStart[p] * nD.clip[p].channels);
    //     nD.clip[p].GetData(startSamples, 0);
    //     lClip.SetData(samples, 0);
    //     startClip.SetData(startSamples, 0);
    //     aS.clip = lClip;
    //     tS.clip = startClip;
    //     tS.loop = false;
    //     tS.PlayScheduled(AudioSettings.dspTime);
    //     aS.loop = true;
    //     aS.PlayScheduled(AudioSettings.dspTime + (double)startClip.samples / (startClip.frequency * (double)aS.pitch));
    //     //aS.PlayScheduled(AudioSettings.dspTime + nD.loopStart[n]);
    //     Destroy(startClip, 3);
    //     Destroy(tS.gameObject, 3);
    //     Destroy(lClip, 3);

    // }
}
