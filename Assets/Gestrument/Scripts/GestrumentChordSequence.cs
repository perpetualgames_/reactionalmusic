﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum rootNote { C, Db, D, Eb, E, F, Gb, G, Ab, A, Bb, B }
public enum chord { major, minor, _7 };



[System.Serializable]
public class ChordSettings
{
   // public int atBeat;
    public rootNote root;
    public chord chord;
}

[CreateAssetMenu(fileName = "New Chord Sequence", menuName = "Gestrument/Chord Sequence", order = 3)]

public class GestrumentChordSequence : ScriptableObject
{
    public int startBeat;
    public int[] changeAtBeat;
    public List<ChordSettings> ChordList = new List<ChordSettings>();
    public int chordID;
}

