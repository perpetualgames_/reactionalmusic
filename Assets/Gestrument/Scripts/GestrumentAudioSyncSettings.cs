﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Audio Sync Asset", menuName = "Gestrument/Audio Sync Asset", order = 2)]



public class GestrumentAudioSyncSettings : ScriptableObject
{
    public AudioClip musicClip;
    public float tempo = 120;
    public timeSignature timesig = timeSignature.FourFour;
    public rootNote root = rootNote.C;
    public double length;
    public int startBeat;
    public int cycleLength;


}

