﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using Gestrument;
using UnityOSC;
using NativeWebSocket;

/// <summary>
///   Gestrument Core
///   Copyright © 2020 Gestrument AB. All rights reserved.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class GestrumentCore : MonoBehaviour
{

    public class Event
    {
        public GestrumentCore Core;
        public double Beat;
        public double CurrentBeat;

        public Event(GestrumentCore core, double beat, double currentBeat)
        {
            this.Core = core;
            this.Beat = beat;
            this.CurrentBeat = currentBeat;
        }
    };

    public class ConfigEvent : Event
    {

        public ConfigEvent(GestrumentCore core)
        : base(core, 0, 0)
        {
        }

    };

    public class NoteOnEvent : Event
    {

        public int Instrument;
        public int Voice;
        public float Pitch;
        public float Velocity;

        public NoteOnEvent(GestrumentCore core, double beat, double currentBeat, int instrument, int voice, float pitch, float velocity)
        : base(core, beat, currentBeat)
        {
            this.Instrument = instrument;
            this.Voice = voice;
            this.Pitch = pitch;
            this.Velocity = velocity;
        }
    };

    public class NoteOffEvent : Event
    {

        public int Instrument;
        public int Voice;
        public float Velocity;

        public NoteOffEvent(GestrumentCore core, double beat, double currentBeat, int instrument, int voice, float velocity)
        : base(core, beat, currentBeat)
        {
            this.Instrument = instrument;
            this.Voice = voice;
            this.Velocity = velocity;
        }
    };

    public class CCEvent : Event
    {

        public int Instrument;
        public int Control;
        public double Value;

        public CCEvent(GestrumentCore core, double beat, double currentBeat, int instrument, int control, double value)
        : base(core, beat, currentBeat)
        {
            this.Instrument = instrument;
            this.Control = control;
            this.Value = value;
        }
    };

    public class SourceValueEvent : Event
    {

        public int Cursor;
        public int Source;
        public double Value;

        public SourceValueEvent(GestrumentCore core, double beat, double currentBeat, int cursor, int source, double value)
        : base(core, beat, currentBeat)
        {
            this.Cursor = cursor;
            this.Source = source;
            this.Value = value;
        }
    };

    public class TempoEvent : Event
    {

        public double Tempo;

        public TempoEvent(GestrumentCore core, double beat, double currentBeat, double tempo)
        : base(core, beat, currentBeat)
        {
            this.Tempo = tempo;
        }
    };


    // In Editor

    public bool DebugOutput = true;
    public bool BroadcastOSC = false;
    public int OSCPort = 9000;

    #region inspector elements
    // public Toggle playToggle;
    //public GameObject cursorPrefab;
    //    [Header("Gestrument preset")]
    //public Preset selectedPreset;
    #endregion

    [Serializable]
    public class UnityEventConfig : UnityEvent<ConfigEvent> { };
    public UnityEventConfig OnConfig;

    [Serializable]
    public class UnityEventNoteOn : UnityEvent<NoteOnEvent> { };
    public UnityEventNoteOn OnNoteOn;

    [Serializable]
    public class UnityEventNoteOff : UnityEvent<NoteOffEvent> { };
    public UnityEventNoteOff OnNoteOff;

    [Serializable]
    public class UnityEventCC : UnityEvent<CCEvent> { };
    public UnityEventCC OnCC;

    [Serializable]
    public class UnityEventSourceValue : UnityEvent<SourceValueEvent> { };
    public UnityEventSourceValue OnSourceValue;

    [Serializable]
    public class UnityEventTempo : UnityEvent<TempoEvent> { };
    public UnityEventTempo OnTempo;


    // DelegateEvents
    public delegate void NoteOn(NoteOnEvent noteOnEvent);
   // public event NoteOn NewNoteOn;



    // Not in editor
    public Engine engine;
    public double sampleRate;
    private byte[] eventBuffer;
    //private UDPPacketIO osc;

    private OSCClient client;
    int[,] voiceID = new int[127, 16];


    bool overRide = false;

    void Awake()
    {

        sampleRate = AudioSettings.outputSampleRate;

        eventBuffer = new byte[4096];

        Engine tmpEngine = new Engine();
        tmpEngine.SetEventsEnabled(Engine.EventType.NoteOn, true);
        tmpEngine.SetEventsEnabled(Engine.EventType.NoteOff, true);

        // Done
        engine = tmpEngine;
        OnConfig.Invoke(new ConfigEvent(this));

        if (DebugOutput)
        {
            engine.SetLogLevel(Engine.LogLevel.Debug);
            engine.OnLog += TestLogCallback;
        }

    }

    private void Start()
    {


        if (BroadcastOSC)
        {
            System.Net.IPAddress adress = new System.Net.IPAddress(new byte[] { 192, 168, 0, 81 });
            client = new OSCClient(adress, 7000);
            client.Connect();
        }
    }


    void TestLogCallback(Engine engine, string message)
    {
        Debug.Log("Log Callback: " + message);
    }


    void OnAudioFilterRead(float[] data, int channels)
    {
        if (engine != null)
        {
            int size = data.Length / channels;
            engine.AudioRender(sampleRate, size);
        }
    }

    void Update()
    {

        if (engine != null && !overRide)
        {
            engine.RunLoop();

            int numBytes;
            int times = 0;
            while ((numBytes = engine.Dequeue(eventBuffer)) > 0 && times < 512)
            {
                times++;
                var msg = UnityOSC.OSCMessage.Unpack(eventBuffer);
                List<object> msgData;
                msgData = msg.Data;

                // if(BroadcastOSC) client.SendRaw(eventBuffer);

                if (msg.Address == "/noteon" && msgData.Count == 5)
                {
                    double beat = Convert.ToDouble(msgData[0]);
//                    Debug.Log("C   " + (beat)); 
                    int instrument = Convert.ToInt32(msgData[1]);
                    int voice = Convert.ToInt32(msgData[2]);
                    float pitch = Convert.ToSingle(msgData[3]);
                    float velocity = Convert.ToSingle(msgData[4]);

                    if (!BroadcastOSC)
                    {
                        var note = new NoteOnEvent(this, beat, beat, instrument, voice, pitch, velocity);
                        OnNoteOn.Invoke(note);
                    }
                    else
                    {

                        voiceID[voice, instrument] = (int)pitch;
                        OSCMessage m = new OSCMessage("/vkb_midi/" + instrument.ToString() + "/note/" + pitch.ToString());
                        m.Append<float>(velocity * 127);
                        m.Pack();

                        StartCoroutine(SendOSCNote(beat,m));


                    }
                    //NewNoteOn(note);

                }

                if (msg.Address == "/noteof" && msgData.Count == 4)
                {
                    double beat = Convert.ToDouble(msgData[0]);
                    int instrument = Convert.ToInt32(msgData[1]);
                    int voice = 0; Convert.ToInt32(msgData[2]);
                    float velocity = Convert.ToSingle(msgData[3]);

                    if (!BroadcastOSC)
                    {
                        var note = new NoteOffEvent(this, beat, beat, instrument, voice, velocity);
                        OnNoteOff.Invoke(note);
                    }
                    else
                    {
                        OSCMessage m = new OSCMessage("/vkb_midi/" + instrument.ToString() + "/note/" + voiceID[voice, instrument].ToString());
                        m.Append<float>(0);
                        m.Pack();

                        StartCoroutine(SendOSCNote(beat,m));
                    }
                }
            }
        }
    }

    IEnumerator SendOSCNote(double beat, OSCMessage m){
        yield return new WaitForSeconds(1f + (float)(beat - GestrumentGlobalSync.GetCurrentBeat()) * ((60 / (float)GestrumentGlobalSync.GetTempo())));
        client.Send(m);
    }

    private void OnApplicationQuit()
    {
        if (BroadcastOSC)
        {
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 127; j++)
                {
                    OSCMessage m = new OSCMessage("/vkb_midi/" + i.ToString() + "/note/" + j.ToString());
                    m.Append<float>(0);
                    m.Pack();

                    client.Send(m);
                }
            }

        }
    }

    //void OnWebSocketReceived(byte[] e)
    //{
    //    int ret = engine.Enqueue(e);
    //    Debug.Log(ret);
    //}

}
