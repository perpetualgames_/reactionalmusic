﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reactional;

public class CubeSpawnerSpatialiser : MonoBehaviour
{

    public GameObject cube;
    List<GameObject> cubes = new List<GameObject>();
    ReactionalPlayer rp;

    public Material pulseMaterial;
    List<Material> pulseMaterials = new List<Material>();
    public int offset = 21;
    public List<Color> colors = new List<Color>();
    public List<Material> StartMaterial = new List<Material>();

    List<Coroutine> coroutines = new List<Coroutine>();

    // Start is called before the first frame update
    void OnEnable()
    {
        rp = FindObjectOfType<ReactionalPlayer>();
        rp.GetComponent<GestrumentCore>().OnNoteOn.AddListener(ProcessAudioEvent);
        StartMaterial.Clear();
        pulseMaterials.Clear();
        colors.Clear();
        cubes.Clear();
        coroutines.Clear();

        for (int i = 0; i < rp.m_samplers.Count; i++)
        {
            if (rp.m_samplers[i] == null) continue;
            var c = Instantiate(cube,gameObject.transform);
            var check = i % 2;
            if (check == 0)
                c.transform.position = new Vector3(i  - (rp.m_samplers.Count), 0, 5 +i*2);
            else
                c.transform.position = new Vector3(-i + (rp.m_samplers.Count), 0, 5 +i*2);
            rp.m_samplers[i].transform.parent = c.transform;
            rp.m_samplers[i].spatialiseInstrument = true;
            rp.m_samplers[i].spatialParent = c;
            StartMaterial.Add(new Material(c.GetComponent<MeshRenderer>().material));
            pulseMaterials.Add(new Material(pulseMaterial));
            cubes.Add(c);
            Color clr = new Color();
            clr = Random.ColorHSV(0f,1f,1f,1f,1f,1f);
            //clr.b = 1f / (i);
            colors.Add(clr);
            coroutines.Add(StartCoroutine(Pulse(0,0,0)));
        }

    }

    private void OnDisable()
    {
        rp.GetComponent<GestrumentCore>().OnNoteOn.RemoveListener(ProcessAudioEvent);

        for (int i = 0; i < rp.m_samplers.Count; i++)
        {
            if (rp.m_samplers[i] == null) continue;
            rp.m_samplers[i].GetComponent<AudioSource>().spatialBlend = 0;
            rp.m_samplers[i].transform.parent = rp.gameObject.transform;
            rp.m_samplers[i].spatialParent = rp.gameObject;
            rp.m_samplers[i].spatialiseInstrument = false;

            Destroy(cubes[i].gameObject,0.2f);

        }
    }

    // Update is called once per frame
    void Update()
    {
       // gameObject.transform.rotation = new Quaternion(Time.deltaTime / 10, Time.deltaTime / 12, 0, 0);
    }

    void NoteOnChecker(int voice, float pitch, float velocity, int instrument, double beat)
    {

        //if (gO.instrumentSettings.Count > 0 && (gO.instrumentSettings.Count) > instrument)  // Check Core instruments output so to visualize all notes of same sampler
        //    instrument = gO.instrumentSettings[instrument].output;
        var inst = rp.m_track.m_instrumentation[instrument];
        var note = (int)pitch - offset;
        if (cubes[inst] != null) //&& instruments.Contains(instrument))
        {
            if(coroutines[inst] != null) StopCoroutine(coroutines[inst]);
            coroutines[inst] = StartCoroutine(Pulse(beat, note, inst));
            //StartCoroutine(Pulse(beat, note, inst));
        }

    }

    void NoteOffChecker(int voice, double beat, int instrument)
    {

    }

    IEnumerator Pulse(double beat, int key, int instrument)
    {
        //yield return new WaitForBeat(gO.core, beat+0.15f);
        cubes[instrument].transform.localScale = new Vector3(2, 2, 2);
        var t = 0f;
        var time = 0.2f;
        var rend = cubes[instrument].GetComponent<MeshRenderer>();

        pulseMaterials[instrument].color = colors[instrument];
        while (t < 1)
        {
            t += Time.deltaTime / time;
            rend.material.Lerp(pulseMaterials[instrument], StartMaterial[instrument], t);
            cubes[instrument].transform.localScale = new Vector3(2-t, 2-t, 2-t);
            yield return null;
        }
    }


    public void ProcessAudioEvent(GestrumentCore.Event engineEvent)
    {
        if (engineEvent is GestrumentCore.NoteOnEvent)
        {
            GestrumentCore.NoteOnEvent noteEvent = (GestrumentCore.NoteOnEvent)engineEvent;
            NoteOnChecker(noteEvent.Voice, noteEvent.Pitch, noteEvent.Velocity, noteEvent.Instrument, noteEvent.Beat);
        }
        else if (engineEvent is GestrumentCore.NoteOffEvent)
        {
            GestrumentCore.NoteOffEvent noteEvent = (GestrumentCore.NoteOffEvent)engineEvent;
            NoteOffChecker(noteEvent.Voice, noteEvent.Beat, noteEvent.Instrument);
        }

    }

}
