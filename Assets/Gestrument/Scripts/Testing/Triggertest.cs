﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggertest : MonoBehaviour
{

    public string parameterName;

    public UnityEngine.UI.Text savename;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayMotif()
    {
        Reactional.Request.Motif(1);
    }

    public void PlayStinger()
    {
        Reactional.Request.Stinger(1);
    }

    public void SetParam(System.Single v)
    {
        Reactional.Request.SetParameter("myParam", v);
    }

    public void SaveState()
    {
        Reactional.Request.SaveState(savename.text);
    }

    public void LoadState()
    {
        Reactional.Request.LoadState(savename.text);
    }

}



