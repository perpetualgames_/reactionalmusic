using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using UnityOSC;
using UnityEngine;
namespace Gestrument
{

    public class Engine
    {

#if UNITY_IOS
        public const string DLL_NAME = "__Internal";
#elif UNITY_STANDALONE || UNITY_EDITOR
        public const string DLL_NAME = "GE_Core";
#else
        public const string DLL_NAME = "libGE_Core.so";
#endif

        public enum LogLevel : int
        {
            None,
            Error,
            Warning,
            Info,
            Debug,
            Max
        };

        // Types used in events
        public enum EventObject : int
        {
            None,
            Engine,
            Cursor,
            Instrument,
            PulseGen,
            PitchGen,
            Max
        };

        // Parameter types
        public enum ParamType : int
        {

            None,
            Boolean,
            Integer,
            Float,
            String,
            Enum,
            Max
        };

        // Event types
        public enum EventType : int
        {

            None,
            NoteOn,
            NoteOff,
            Cursor,
            SetParam,
            Map,
            MapVia,
            Slot,
            Max
        };

        // OSC Params
        public enum OSCParam
        {
            eng,
            ins,
            cur,
            pul,
            pit
        };

        // Parameter info
        public class ParamInfo
        {
            public ParamType Type;
            public string Name;
            public bool RealtimeSafe;
            public string Description;
        };

        //
        // Log.
        //

        // Get the current log level.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern LogLevel GE_GetLogLevel();

        // Set the current log level.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void GE_SetLogLevel(LogLevel level);

        // Used for log callbacks.
        public delegate void GE_LogCallback(IntPtr message, IntPtr arg);

        // Set the current log level.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void GE_SetLogCallback(GE_LogCallback callback, IntPtr arg);

        //
        // Engine functions.
        //

        // Creates a new engine instance.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_Engine_New(IntPtr config);

        // Frees the engine instance.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void GE_Engine_Free(IntPtr engine);

        // Call from main thread.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void GE_Engine_ProcessRunLoop(IntPtr engine);

        // Dequeue (read) OSC event.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_Engine_DequeueOSC(IntPtr engine, bool compat, int maxSize, [MarshalAs(UnmanagedType.LPArray)] byte[] data);

        // Enqueue (write) OSC event.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_Engine_EnqueueOSC(IntPtr engine, int size, [MarshalAs(UnmanagedType.LPArray)] byte[] data);

        // Get events enabled flag.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool GE_Engine_GetEventsEnabled(IntPtr engine, EventType eventType);

        // Set events enabled flag.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_Engine_SetEventsEnabled(IntPtr engine, EventType eventType, bool enabled);

        // Used for event callbacks.
        public delegate void GE_EventCallback(IntPtr engine, IntPtr ev, IntPtr arg);

        // Set the event callback function.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void GE_Engine_SetEventCallback(IntPtr engine, GE_EventCallback callback, IntPtr arg);

        // Convert GE_Event to OSC.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_Engine_ToOSC(IntPtr engine, IntPtr ev, bool compat, UIntPtr size, [MarshalAs(UnmanagedType.LPArray)] byte[] data);

        // Get current beat.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double GE_Engine_GetCurrentBeat(IntPtr engine);

        // Set current beat.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void GE_Engine_SetCurrentBeat(IntPtr engine, double beat);

        //
        // Audio thread exclusive functions.
        //

        // Used to render events.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool GE_Engine_Render(IntPtr engine, double sampleRate, int numFrames);

        //
        // Component API.
        //

        // Get a component based on an object enum and index.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_Engine_GetComponent(IntPtr engine, EventObject objenum, int index);

        // Get event object enum from component.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern EventObject GE_Component_GetEventObject(IntPtr component);

        // Get component type name.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_Component_GetType(IntPtr component);

        // Get the param table.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_Component_GetParamTable(IntPtr component);

        //
        // ParamTable API.
        //

        // Get the number of parameters the table has.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_GetLength(IntPtr table);

        // Get the number of parameters the table has.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_ParamTable_GetParamAt(IntPtr table, int index);

        // Find parameter index.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_FindParamIndex(IntPtr table, [MarshalAs(UnmanagedType.LPStr)] string name);

        // Set boolean parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_SetBooleanAt(IntPtr table, int index, bool paramValue);

        // Get boolean parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_GetBooleanAt(IntPtr table, int index, out bool paramValue);

        // Set integer parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_SetIntegerAt(IntPtr table, int index, long paramValue);

        // Get integer parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_GetIntegerAt(IntPtr table, int index, out long paramValue);

        // Set float parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_SetFloatAt(IntPtr table, int index, double paramValue);

        // Get float parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_GetFloatAt(IntPtr table, int index, out double paramValue);

        // Set string parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_SetStringAt(IntPtr table, int index, [MarshalAs(UnmanagedType.LPStr)] string paramValue);

        // Get string parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_GetStringAt(IntPtr table, int index, IntPtr size, [MarshalAs(UnmanagedType.LPStr)] StringBuilder paramValue);

        // Set enum parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_SetEnumAt(IntPtr table, int index, int paramValue);

        // Get enum parameter value.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GE_ParamTable_GetEnumAt(IntPtr table, int index, out int paramValue);

        //
        // ParamInfo API.
        //

        // Get the parameter type.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern ParamType GE_ParamInfo_GetType(IntPtr info);

        // Get parameter name.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_ParamInfo_GetName(IntPtr info);

        // Get parameter RT flag.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool GE_ParamInfo_GetRealtimeSafe(IntPtr info);

        // Get parameter description.
        [DllImport(Engine.DLL_NAME, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GE_ParamInfo_GetDescription(IntPtr info);

        // Public callback.
        public delegate void OnEventCallback(Engine engine, int size, byte[] osc);

        public event OnEventCallback OnEvent;

        // Public callback.
        public delegate void OnLogCallback(Engine engine, string message);

        public event OnLogCallback OnLog;

        public static float globalOffset = 0.05f;
        public bool OSCCompatibility = true;
        protected IntPtr cPointer;
        byte[] internalBuffer;
        public bool recordMessages;
        System.Collections.Generic.List<byte[]> recordedMessagesList = new System.Collections.Generic.List<byte[]>();

        static string ConvertToString(IntPtr intptr)
        {
            if (intptr == IntPtr.Zero)
                return "";
            return Marshal.PtrToStringAnsi(intptr);
        }

        // Called by the GE_Engine_Render(), possibly from the audio thread.
        static void EventCallback(IntPtr engine, IntPtr ev, IntPtr arg)
        {
            if (arg != IntPtr.Zero && ev != IntPtr.Zero)
            {
                GCHandle handle = GCHandle.FromIntPtr(arg);
                Engine e = (Engine)handle.Target;
                UIntPtr maxSize = (UIntPtr)e.internalBuffer.Length;
                Console.WriteLine(e + "   " + ev + "  " + maxSize + "  " + e.internalBuffer);
                int size = Engine.GE_Engine_ToOSC(engine, ev, e.OSCCompatibility, maxSize, e.internalBuffer);
                if (size > 0 && e.OnEvent != null)
                    e.OnEvent(e, size, e.internalBuffer);
            }
        }

        // Called when log is output.
        static void LogCallback(IntPtr message, IntPtr arg)
        {
            if (arg != IntPtr.Zero && message != IntPtr.Zero)
            {
                GCHandle handle = GCHandle.FromIntPtr(arg);
                Engine e = (Engine)handle.Target;
                if (e.OnLog != null)
                    e.OnLog(e, ConvertToString(message));
            }
        }

        IntPtr GetComponent(EventObject eventObject, int objectIndex)
        {
            if (eventObject == EventObject.Engine)
                return cPointer;
            if (cPointer != IntPtr.Zero)
                return GE_Engine_GetComponent(cPointer, eventObject, objectIndex);
            return IntPtr.Zero;
        }

        IntPtr GetParamTable(EventObject eventObject, int objectIndex)
        {
            IntPtr comp = GetComponent(eventObject, objectIndex);
            if (comp != IntPtr.Zero)
                return GE_Component_GetParamTable(comp);
            return IntPtr.Zero;
        }

        public Engine()
        {
            internalBuffer = new byte[8192];
            cPointer = GE_Engine_New(IntPtr.Zero);
            GCHandle handle = GCHandle.Alloc(this, GCHandleType.Normal);
            GE_SetLogCallback(LogCallback, GCHandle.ToIntPtr(handle));
            GE_Engine_SetEventCallback(cPointer, EventCallback, GCHandle.ToIntPtr(handle));
        }

        ~Engine()
        {
            GE_SetLogCallback(null, IntPtr.Zero);
            GE_Engine_SetEventCallback(cPointer, null, IntPtr.Zero);
            if (cPointer != IntPtr.Zero)
                GE_Engine_Free(cPointer);
        }

        public string GetEventObjectName(EventObject eventObject, int objectIndex)
        {
            IntPtr comp = GetComponent(eventObject, objectIndex);
            if (comp != IntPtr.Zero)
                return ConvertToString(GE_Component_GetType(comp));
            return "";
        }

        public EventObject GetEventObject(EventObject eventObject, int objectIndex)
        {
            IntPtr comp = GetComponent(eventObject, objectIndex);
            if (comp != IntPtr.Zero)
                return GE_Component_GetEventObject(comp);
            return EventObject.None;
        }

        //
        // Params
        //

        public ParamInfo GetParamInfo(EventObject eventObject, int objectIndex, int paramIndex)
        {
            ParamInfo ret = new ParamInfo();
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table != IntPtr.Zero)
            {
                IntPtr info = GE_ParamTable_GetParamAt(table, paramIndex);
                if (info != IntPtr.Zero)
                {
                    ret.Type = GE_ParamInfo_GetType(info);
                    ret.Name = ConvertToString(GE_ParamInfo_GetName(info));
                    ret.RealtimeSafe = GE_ParamInfo_GetRealtimeSafe(info);
                    ret.Description = ConvertToString(GE_ParamInfo_GetDescription(info));
                }
            }
            return ret;
        }

        public ParamInfo GetParamInfo(EventObject eventObject, int objectIndex, string name)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table != IntPtr.Zero)
            {
                int paramIndex = GE_ParamTable_FindParamIndex(table, name);
                return GetParamInfo(eventObject, objectIndex, paramIndex);
            }
            return new ParamInfo();
        }

        public ParamInfo[] GetParams(EventObject eventObject, int objectIndex)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return new ParamInfo[0];
            int numParams = GE_ParamTable_GetLength(table);
            ParamInfo[] infos = new ParamInfo[numParams];
            for (int i = 0; i < numParams; i++)
            {
                infos[i] = GetParamInfo(eventObject, objectIndex, i);
            }
            return infos;
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, int paramIndex, bool paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetBooleanAt(table, paramIndex, paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, string paramName, bool paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetBooleanAt(table, GE_ParamTable_FindParamIndex(table, paramName), paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, int paramIndex, out bool paramValue)
        {
            paramValue = false;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetBooleanAt(table, paramIndex, out paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, string paramName, out bool paramValue)
        {
            paramValue = false;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetBooleanAt(table, GE_ParamTable_FindParamIndex(table, paramName), out paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, int paramIndex, long paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetIntegerAt(table, paramIndex, paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, string paramName, long paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetIntegerAt(table, GE_ParamTable_FindParamIndex(table, paramName), paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, int paramIndex, out long paramValue)
        {
            paramValue = 0;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetIntegerAt(table, paramIndex, out paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, string paramName, out long paramValue)
        {
            paramValue = 0;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetIntegerAt(table, GE_ParamTable_FindParamIndex(table, paramName), out paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, int paramIndex, double paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetFloatAt(table, paramIndex, paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, string paramName, double paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetFloatAt(table, GE_ParamTable_FindParamIndex(table, paramName), paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, int paramIndex, out double paramValue)
        {
            paramValue = 0;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetFloatAt(table, paramIndex, out paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, string paramName, out double paramValue)
        {
            paramValue = 0;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetFloatAt(table, GE_ParamTable_FindParamIndex(table, paramName), out paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, int paramIndex, string paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetStringAt(table, paramIndex, paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, string paramName, string paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetStringAt(table, GE_ParamTable_FindParamIndex(table, paramName), paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, int paramIndex, out string paramValue)
        {
            paramValue = "";
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            int size = GE_ParamTable_GetStringAt(table, paramIndex, (IntPtr)0, null);
            if (size > 0)
            {
                StringBuilder sb = new StringBuilder(size + 1);
                size = GE_ParamTable_GetStringAt(table, paramIndex, (IntPtr)size + 1, sb);
                if (size > 0)
                    paramValue = sb.ToString();
            }
            return size;
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, string paramName, out string paramValue)
        {
            paramValue = "";
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GetParamValue(eventObject, objectIndex, GE_ParamTable_FindParamIndex(table, paramName), out paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, int paramIndex, int paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetEnumAt(table, paramIndex, paramValue);
        }

        public int SetParamValue(EventObject eventObject, int objectIndex, string paramName, int paramValue)
        {
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_SetEnumAt(table, GE_ParamTable_FindParamIndex(table, paramName), paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, int paramIndex, out int paramValue)
        {
            paramValue = 0;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GE_ParamTable_GetEnumAt(table, paramIndex, out paramValue);
        }

        public int GetParamValue(EventObject eventObject, int objectIndex, string paramName, out int paramValue)
        {
            paramValue = 0;
            IntPtr table = GetParamTable(eventObject, objectIndex);
            if (table == IntPtr.Zero)
                return -1;
            return GetParamValue(eventObject, objectIndex, GE_ParamTable_FindParamIndex(table, paramName), out paramValue);
        }


        //
        // Main thread functions.
        //

        // Call from main thread.
        public void RunLoop()
        {
            if (cPointer != IntPtr.Zero)
                GE_Engine_ProcessRunLoop(cPointer);
        }

        //
        // Events.
        //

        // Dequeue (read) OSC event in main thread.
        public int Dequeue(byte[] buffer)
        {
            if (cPointer != IntPtr.Zero)
            {
                if (buffer == null)
                    return Engine.GE_Engine_DequeueOSC(cPointer, OSCCompatibility, 0, null);
                return Engine.GE_Engine_DequeueOSC(cPointer, OSCCompatibility, buffer.Length, buffer);
            }
            return -1;
        }

        // Enqueue (write) OSC event in main thread.
        public int Enqueue(byte[] buffer)
        {
            if (recordMessages) { recordOSCMessages(buffer); }
            if (cPointer != IntPtr.Zero)
            {
                if (buffer != null && buffer.Length > 0)
                    return Engine.GE_Engine_EnqueueOSC(cPointer, buffer.Length, buffer);
                return 0;
            }
            return -1;
        }

        void recordOSCMessages(byte[] buffer)
        {
            recordedMessagesList.Add(buffer);
        }

        // Get events enabled flag.
        public bool GetEventsEnabled(EventType eventType)
        {
            if (cPointer != IntPtr.Zero)
                return Engine.GE_Engine_GetEventsEnabled(cPointer, eventType);
            return false;
        }

        // Set events enabled flag.
        public void SetEventsEnabled(EventType eventType, bool enabled)
        {
            if (cPointer != IntPtr.Zero)
                Engine.GE_Engine_SetEventsEnabled(cPointer, eventType, enabled);
        }

        // Get log level.
        public LogLevel GetLogLevel()
        {
            if (cPointer != IntPtr.Zero)
                return Engine.GE_GetLogLevel();
            return LogLevel.None;
        }

        // Set events enabled flag.
        public void SetLogLevel(LogLevel level)
        {
            if (cPointer != IntPtr.Zero)
                Engine.GE_SetLogLevel(level);
        }

        //
        // Audio thread functions.
        //

        // Used to render events.
        public bool AudioRender(double sampleRate, int numFrames)
        {
            if (cPointer != IntPtr.Zero)
                return GE_Engine_Render(cPointer, sampleRate, numFrames);
            return false;
        }

        public int AudioDequeueOSC(int maxSize, byte[] buffer)
        {
            if (cPointer != IntPtr.Zero)
            {
                // THIS DOES NOT EXIST IN CORE YET!
                return 0;
            }
            return 0;
        }


        public void SetCursor(int cursorNum, float x, float y, float z)
        {
            OSCMessage msg = new OSCMessage("/crs");
            msg.Address = "/crs";
            msg.Append<float>(0);
            msg.Append<int>(0);
            msg.Append<int>(1);
            msg.Append<float>(x);
            msg.Append<float>(y);
            msg.Append<float>(z);

            Enqueue(msg.BinaryData);
        }

        public void MapSlider(string sliderName, string destination, int instrument, string name )
        {
            OSCMessage msg = new OSCMessage("/map/eng");
            msg.Address = "/map/eng";
            msg.Append<int>(0);
            msg.Append<string>(sliderName);
            msg.Append<string>(destination); //"pul"
            msg.Append<int>(instrument);
            msg.Append<string>(name); // "velocity"

            Enqueue(msg.BinaryData);
        }


        public void Map(string adress, int index, string source, string destination, int instrument, string name)
        {
            OSCMessage msg = new OSCMessage(adress);
            msg.Address = adress;
            msg.Append<int>(index);
            msg.Append<string>(source);
            msg.Append<string>(destination); //"pul"
            msg.Append<int>(instrument);
            msg.Append<string>(name); // "velocity"

            Enqueue(msg.BinaryData);
        }


        public void MapSliderVia(string sliderName, string destination, int instrument, string name)
        {
            OSCMessage msg = new OSCMessage("/mapv/eng");
            msg.Address = "/mapv/eng";
            msg.Append<int>(0);
            msg.Append<string>(sliderName);
            msg.Append<string>(destination); //"pul"
            msg.Append<int>(instrument);
            msg.Append<string>(name); // "velocity"

            Enqueue(msg.BinaryData);
        }



        public float GetCursorX(int cursor)
        {
            double posX;
            GetParamValue(EventObject.Cursor, cursor, "x", out posX);
            return (float)posX;
        }

        public float GetCursorY(int cursor)
        {
            double posY;
            GetParamValue(EventObject.Cursor, cursor, "y", out posY);
            return (float)posY;
        }

        public void SetCursorX(int cursor, double value)
        {
            SetParamValue(EventObject.Cursor, cursor, "x", value);
        }

        public void SetCursorY(int cursor, double value)
        {
            SetParamValue(EventObject.Cursor, cursor, "y", value);
        }

        public void SetCursorZ(int cursor, double value)
        {
            SetParamValue(EventObject.Cursor, cursor, "z", value);
        }

        public void SetCursorGate(int cursor, bool value)
        {
            SetParamValue(EventObject.Cursor, cursor, "gate", value);
        }

        public double GetCurrentBeat()
        {
            if (cPointer != IntPtr.Zero)
                return GE_Engine_GetCurrentBeat(cPointer);
            return 0;
        }

        public void SetCurrentBeat(double beat)
        {
            if (cPointer != IntPtr.Zero)
                GE_Engine_SetCurrentBeat(cPointer, beat);
        }

        public void SetInstrumentCursor(int instrument, int cursor)
        {
            SetParamValue(EventObject.Instrument, instrument, "cursor", cursor);
        }

        public void SetSliderValue(int sliderNumber, double value)
        {
            SetParamValue(EventObject.Engine, 0, "slider" + sliderNumber, value);
        }

        public float GetSliderValue(int sliderNumber)
        {
            double sliderVal;
            GetParamValue(EventObject.Engine, 0, "slider" + sliderNumber, out sliderVal);
            return (float)sliderVal;
        }

        public void SetPlaying(bool playing) { SetParamValue(EventObject.Engine, 0, "playing", playing); }

        public bool GetPlaying()
        {
            GetParamValue(EventObject.Engine, 0, "playing", out bool playing);
            return playing;
        }

        public void SetTempo(double tempo) { SetParamValue(EventObject.Engine, 0, "tempo", tempo); }


        public double GetTempo()
        {
            double tempo;
            GetParamValue(EventObject.Engine, 0, "tempo", out tempo);
            return tempo;
        }

        public int NumCursors
        {
            get
            {
                GetParamValue(EventObject.Engine, 0, "numcursors", out int numCursors);
                return numCursors;
            }   // get method
            set { SetParamValue(EventObject.Engine, 0, "numcursors", value); }
        }

        public int NumInstruments
        {
            get
            {
                GetParamValue(EventObject.Engine, 0, "numinstruments", out int numInstruments);
                return numInstruments;
            }   // get method
            set { SetParamValue(EventObject.Engine, 0, "numinstruments", value); }
        }


        public byte[] SetScaleForSlot(int slot, float root, string scale)
        {
            OSCMessage msg = new OSCMessage("/slt/eng");
            msg.Address = "/slt/eng";
            msg.Append<string>("scale");
            msg.Append<int>(slot);
            msg.Append<float>(root);
            msg.Append<string>(scale);
            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }


        public void SetValueForTransposeSlot(int slot, float value)
        {
            OSCMessage msg = new OSCMessage("/slt/eng");
            msg.Address = "/slt/eng";
            msg.Append<string>("transpose");
            msg.Append<int>(slot);
            msg.Append<float>(value);
            Enqueue(msg.BinaryData);
        }

        public byte[] ScheduleTransposeSlot(double beat, int slot)
        {
            OSCMessage msg = new OSCMessage("/slt/eng");
            msg.Address = "/sp/eng";
            msg.Append<double>(beat);
            msg.Append<int>(0);
            msg.Append<string>("transposeslotindex");
            msg.Append<int>(slot);


            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }

        public byte[] ScheduleScaleSlot(double beat, int slot)
        {
            OSCMessage msg = new OSCMessage("/slt/eng");
            //msg.Address = "/sp/eng";
            msg.Append<double>(beat);
            msg.Append<int>(0);
            msg.Append<string>("transposeslotindex");
            msg.Append<int>(slot);


            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }

        public void ScheduleRelPitchIndex(double beat, int instrument, int index)
        {
            OSCMessage msg = new OSCMessage("/sp/pit");
            msg.Address = "/sp/pit";
            msg.Append<double>(beat);
            msg.Append<int>(instrument);
            msg.Append<string>("index");
            msg.Append<int>(index);


            Enqueue(msg.BinaryData);
        }

        public void ScheduleParam_PitchGen(float beat, int instrument, string parameter, int value)
        {
            OSCMessage msg = new OSCMessage("/sp/pit");
            msg.Address = "/sp/pit";
            msg.Append<float>(beat);
            msg.Append<int>(instrument);
            msg.Append<string>(parameter);
            msg.Append<int>(value);
            Enqueue(msg.BinaryData);
        }

        public byte[] ScheduleParam(OSCParam param, double beat, int index, string parameter, float value)
        {
            OSCMessage msg = new OSCMessage("/sp/" + param.ToString());
            msg.Append<double>(beat);
            msg.Append<int>(index);
            msg.Append<string>(parameter);
            msg.Append<float>(value);
            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }

        public byte[] ScheduleParam(OSCParam param, double beat, int index, string parameter, bool value)
        {
            OSCMessage msg = new OSCMessage("/sp/" + param.ToString());
            msg.Append<double>(beat);
            msg.Append<int>(index);
            msg.Append<string>(parameter);
            msg.Append<bool>(value);
            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }

        public byte[] ScheduleParam(OSCParam param, float beat, int index, string parameter, int value)
        {
            OSCMessage msg = new OSCMessage("/sp/" + param.ToString());
            msg.Append<float>(beat);
            msg.Append<int>(index);
            msg.Append<string>(parameter);
            msg.Append<int>(value);
            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }

        public byte[] ScheduleParam(OSCParam param, float beat, int index, string parameter, string value)
        {
            string address = "/sp/" + param.ToString();
            OSCMessage msg = new OSCMessage(address);
            msg.Address = address;
            msg.Append<float>(beat);
            msg.Append<int>(index);
            msg.Append<string>(parameter);
            msg.Append<string>(value);
            Enqueue(msg.BinaryData);
            return msg.BinaryData;
        }

        public void ScheduleCursor(int index, double beat, int gate, double x, double y,double z)
        {
            var msg = new OSCMessage("/crs");
            msg.Append<double>(beat);
            msg.Append<int>(index);
            msg.Append<int>(gate);
            msg.Append<double>(x);
            msg.Append<double>(y);
            msg.Append<double>(z);
            Enqueue(msg.BinaryData);
        }


        public void LoadG2Preset(string path)
        {
            OSCMessage msg = new OSCMessage("/sp/eng");
            msg.Address = "/sp/eng";
            msg.Append<float>(0);
            msg.Append<int>(0);
            msg.Append<string>("g2preset");
            msg.Append<string>(path);

            Enqueue(msg.BinaryData);
        }

        public void SetPulseGen(int index, string generator)
        {
            SetParamValue(EventObject.Instrument, index, "pulse", generator);
        }

        public void SetPulsePlayMode(int instrument, string playmode)
        {
            SetParamValue(EventObject.PulseGen, instrument, "playmode", playmode);
        }

        public void SetPitchGen(int index, string generator)
        {
            SetParamValue(EventObject.Instrument, index, "pitch", generator);
        }

        public double GetCursorValue(int index, string value)
        {
            GetParamValue(EventObject.Cursor, index, value, out double paramValue);
            return paramValue;
        }

        public bool GetCursorGate(int index)
        {
            GetParamValue(EventObject.Cursor, index, "gate", out bool paramValue);
            return paramValue;
        }


        public void LoadPulseMIDI(int instrument, string path)
        {
            OSCMessage msg = new OSCMessage("/sp/pul");
            msg.Address = "/sp/pul";
            msg.Append<float>(0);
            msg.Append<int>(instrument);
            msg.Append<string>("midipath");
            msg.Append<string>(path);

            Enqueue(msg.BinaryData);
        }

        public void LoadPitchMIDI(int instrument, string path)
        {
            OSCMessage msg = new OSCMessage("/sp/pit");
            msg.Address = "/sp/pit";
            msg.Append<float>(0);
            msg.Append<int>(instrument);
            msg.Append<string>("midipath");
            msg.Append<string>(path);

            Enqueue(msg.BinaryData);
        }


         
        public double Remap(double s, double a1, double a2, double b1, double b2)
        {
            return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
        }

    };

};
