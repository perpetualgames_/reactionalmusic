﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestrumentAdvancedParameter : MonoBehaviour
{
    public string m_parameterName;
    //public AnimationClip curve;

    //[SerializeField]
    //public GestrumentParameterAsset parameterAsset;

    private float m_parameterValue;

    //public float ParameterValue
    //{
    //    get { return m_parameterValue; }
    //    set
    //    {
    //        m_parameterValue = value;
    //        UpdateParameter(value);
    //    }
    //}

    //private GestrumentCore core;

    //private void Awake()
    //{
    //    core = FindObjectOfType<GestrumentCore>();
    //}

    //// Start is called before the first frame update
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //public void UpdateParameter(GestrumentParameterAsset parameterAsset, float parameterValue)
    //{
    //    if (!parameterAsset) return;

    //    for (int i = 0; i < parameterAsset.gestrumentSubParameters.Count; i++)
    //    {
    //        var gsp = parameterAsset.gestrumentSubParameters[i];

    //        if (parameterValue >= gsp.inVal && parameterValue <= gsp.outVal)
    //        {
    //            double beat = Mathf.Ceil((float)core.engine.GetCurrentBeat() * gsp.quant) / gsp.quant;

    //            if (!gsp.isActive)
    //            {
    //                for (int j = 0; j < gsp.cursorSequenceAssets.Count; j++)
    //                {
    //                    SequenceScheduler(gsp.cursorSequenceAssets[j], beat);
    //                }
    //            }

                
    //            for (int j = 0; j < gsp.setParameterList.Count; j++)
    //            {
    //                if (!gsp.isActive || gsp.setParameterList[j].isContinous)
    //                    ParseSetParameter(gsp.setParameterList[j], parameterValue, beat, gsp.inVal, gsp.outVal);
    //            }


    //            gsp.isActive = true;
    //        }
    //        else 
    //        {
    //            gsp.isActive = false;
    //        }
    //    }

    //}



    //void ParseSetParameter(SPParameter param, float value, double beat, float inVal, float outVal)
    //{
    //    if (param.remap) value = (float)core.engine.Remap(value, inVal, outVal, param.minVal, param.maxVal);
    //    Debug.Log(value);
    //    core.engine.ScheduleParam(param.paramType, beat, param.index, param.parameterName, value);
    //}
     


    //private void OnValidate()
    //{
    //    if (parameterAsset)
    //        m_parameterName = parameterAsset.parameterName;
    //}
     


    //public void SequenceScheduler(CursorSequenceAsset sq, double beat)
    //{
    //    var st = sq.cursorArrayContainer[0].steps;
    //    for (int i = 0; i < st.Count; i++)
    //    {
    //        double b;

    //        b = st[i].pos[0] + beat;


    //        var g = st[i].pos[3];
    //        var x = st[i].pos[1];
    //        var y = st[i].pos[2];
    //        var z = st[i].pos[4] / 127f;

    //        // Legato overlap hack
    //        //if (i > 1 && g == 0 && i < st.Count + 1)
    //        //    if (y != st[i - 1].pos[2] && st[i + 1].pos[3] != 1)
    //        //        continue;


    //        // Schedule movement
    //        core.engine.ScheduleCursor(sq.trackNumber, b, (int)g, x, y, z);


    //    }
    //}
}
    





[System.Serializable]
public class GestrumentSubParameter
{
    public float inVal;
    public float outVal;
    public int quant = 4;
    public List<string> states = new List<string>();
    public List<CursorSequenceAsset> cursorSequenceAssets = new List<CursorSequenceAsset>();
    public List<SPParameter> setParameterList = new List<SPParameter>();
    public bool isActive = true;
}

[System.Serializable]
public class SPParameter 
{
    public bool isContinous = true;
    public Gestrument.Engine.OSCParam paramType;
    public int index;
    public string parameterName;
    public bool remap = false;
    public float minVal;
    public float maxVal;
}

