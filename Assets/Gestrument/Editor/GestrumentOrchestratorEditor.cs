﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(GestrumentOrchestrator))]
public class GestrumentOrchestratorEditor : Editor
{

    SerializedProperty m_instruments;

    public override void OnInspectorGUI()
    {

        var gO = target as GestrumentOrchestrator;

        EditorGUI.BeginChangeCheck();
        gO.settings = (GestrumentSettings)EditorGUILayout.ObjectField("Settings", gO.settings, typeof(GestrumentSettings), true);
        if (EditorGUI.EndChangeCheck())
        {
            if (gO.settings && Application.isPlaying == false)
            {
                gO.UpdateSettingsFileVars();
            }
        }

        GUILayout.BeginHorizontal();
        gO.cursorCount = EditorGUILayout.IntField("Cursors", gO.cursorCount);
        gO.instrumentCount = EditorGUILayout.IntField("Instruments", gO.instrumentCount);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);


        gO.totalPolyphony = EditorGUILayout.IntField("Total Polyphony", gO.totalPolyphony);
        // GUILayout.BeginHorizontal();
        //        gO.instruments = (GameObject)EditorGUILayout.ObjectField(gO.instruments,"Samplers",true);

        m_instruments = serializedObject.FindProperty("instruments");



        for (int i = 0; i < gO.instruments.Count; i++)
        {
            GameObject arraysp = gO.instruments[i];
            gO.instruments[i] = (GameObject)EditorGUILayout.ObjectField("Instrument", arraysp, typeof(GameObject), true);
        }

        //for (int i = 0; i < gO.manualInstrumentRouting.Length; i++)
        //{
        //    int arraysp = gO.manualInstrumentRouting[i];
        //    gO.manualInstrumentRouting[i] = EditorGUILayout.IntField("Manual Routing", arraysp);
        //}

        //EditorGUILayout.PropertyField(m_instruments);

    }
}
