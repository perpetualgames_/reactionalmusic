﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;
// using System.IO;
// using System.Text.RegularExpressions;


// public class CreateSamplerFromSfz : EditorWindow
// {
//     Rect windowRect = new Rect(20, 20, 120, 50);

//     [MenuItem("Tools/Gestrument/Create Sampler From Sfz")]
//     public static void OpenSamplerCreator()
//     {
//         var w = GetWindow<CreateSamplerFromSfz>();
//         w.Show();
//     }

//     string outputFolder = "Assets/Gestrument/Instruments/";
//     string sfzName;

//     string importPath = "";

//     List<string> filesToProcess = new List<string>();

//     void OnGUI()
//     {
//         GUIStyle guiStyle = new GUIStyle(); //create a new variable
//         guiStyle.fontSize = 50;
//         GUILayout.Label("GESTRUMENT", guiStyle);
//         guiStyle.fontSize = 20;
//         GUILayout.Label("   Sfz to UnitySampler converter", guiStyle);
//         GUILayout.Space(20);
//         GUILayout.Label("Please make sure you have selected");
//         GUILayout.Label("the Sfz file before proceeding");
//         GUILayout.Space(20);
//         GUILayout.Label("Output folder");
//         outputFolder = EditorGUILayout.TextField(outputFolder);

//         if (GUILayout.Button("Import Sfz and Samples from disk"))
//         {
//             filesToProcess.Clear();
//             importPath = "";

//             importPath = EditorUtility.OpenFilePanel("Load Sfz", "", "sfz");
//             var importName = Path.GetFileName(importPath);
//             var importFolder = Path.GetDirectoryName(importPath);

//             var destinationPath = Application.dataPath + "/Gestrument/Sampler/" + sfzName + "/" + importName;
//             sfzName = Path.GetFileNameWithoutExtension(importPath);

//             if (!Directory.Exists(Path.GetDirectoryName(destinationPath)))
//             {
//                 Directory.CreateDirectory(Path.GetDirectoryName(destinationPath));
//                 AssetDatabase.Refresh();
//             }

//             FileUtil.CopyFileOrDirectory(importPath, destinationPath);
//             AssetDatabase.Refresh();


//             filesToProcess.Add("Assets/Gestrument/Sampler/" + importName);


//             var read = new StreamReader(destinationPath);
//             var fileContents = read.ReadToEnd();
//             read.Close();
//             var lines = fileContents.Split("\n"[0]);

//             foreach (string line in lines)
//             {
//                 if (line.Contains("sample="))
//                 {
//                     int index = line.IndexOf("sample=");
//                     string str = line.TrimEnd('\r', '\n');
//                     str = str.Remove(0, index + 7);
//                     str = str.Replace(@"\", @"/");
//                     string[] file = Regex.Split(str, ".wav");
//                     var sampleDir = Path.GetDirectoryName(importFolder + "/" + sfzName + "/" + file[0]);
//                     sampleDir = Path.GetFileName(sampleDir);


//                     if (sampleDir != sfzName)
//                     {
//                         if (!Directory.Exists(Path.GetDirectoryName(destinationPath) + "/" + sampleDir))
//                         {
//                             Directory.CreateDirectory(Path.GetDirectoryName(destinationPath) + "/" + sampleDir);
//                             AssetDatabase.Refresh();
//                         }
//                     }

//                     if (File.Exists(Path.GetDirectoryName(destinationPath) + "/" + file[0] + ".wav") == false)
//                     {
//                         FileUtil.CopyFileOrDirectory(importFolder + "/" + file[0] + ".wav", Path.GetDirectoryName(destinationPath) + "/" + file[0] + ".wav");

//                         AssetDatabase.Refresh();
//                     }
//                 }
//             }
//             AssetDatabase.Refresh();
//         }


//         if (GUILayout.Button("M A G I C"))
//         {
//             filesToProcess.Clear();
//             string path = "Assets";

//             foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
//             {
//                 path = AssetDatabase.GetAssetPath(obj);
//                 var attr = File.GetAttributes(path);
//                 sfzName = obj.name;

//                 filesToProcess.Add(path);
//             }

//             foreach (var p in filesToProcess)
//             {
//                 CreateSfzPrefab(p);
//             }
//         }
//     }

//     void CreateSfzPrefab(string p)
//     {
//         var source = new StreamReader(p);
//         var fileContents = source.ReadToEnd();
//         source.Close();
//         var lines = fileContents.Split("\n"[0]);

//         string prefabPath = outputFolder + sfzName + ".prefab";

//         if (!Directory.Exists(Path.GetDirectoryName(outputFolder)))             // Check if Prefab folder exists, otherwise create it.
//         {
//             Directory.CreateDirectory(Path.GetDirectoryName(outputFolder));
//             AssetDatabase.Refresh();
//         }


//         GameObject obj = new GameObject("test");                                // Create temporary GameObject in scene and set it up with components
//         var sampler = obj.AddComponent<GestrumentSampler>() as GestrumentSampler;
//         var aS = obj.AddComponent<AudioSource>() as AudioSource;
//         aS.playOnAwake = false;
//         sampler.releaseTime = 0.2f;
//         sampler.noteData.Add(new NoteData());


//         int lo = 0, hi = 0, center = 0, seq_length = 0, seq_position = 0, loopStart = 0, loopEnd = 0, transpose = 0, hivel = 0, lovel = 0;

//         AudioClip addClip = null;
//         //foreach (string line in lines)
//         for (int i = 0; i < lines.Length; i++)
//         {
//             var line = lines[i];
//             //  Debug.Log(i);

//             if (line.Contains("seq_length="))                                   // If sfz contains seq_lenght (round robin), check and create extra NoteData instances
//             {
//                 var index = line.IndexOf("seq_length=");
//                 var l = line;
//                 l = l.Remove(0, index + 11);

//                 int.TryParse(l, out seq_length);

//                 if (sampler.noteData.ToArray().Length < seq_length)
//                     for (int j = 0; j < seq_length; i++) sampler.noteData.Add(new NoteData());
//             }

//             if (line.Contains("seq_position="))
//             {
//                 var index = line.IndexOf("seq_position=");
//                 var l = line;
//                 l = l.Remove(0, index + 13);
//                 int.TryParse(l, out seq_position);
//                 seq_position = seq_position - 1;
//             }

//             if (line.Contains("hivel="))
//             {
//                 var index = line.IndexOf("hivel=");
//                 var l = line;
//                 l = l.Remove(0, index + 6);
//                 string[] numbers = Regex.Split(l, @"\D+");
//                 int.TryParse(numbers[0], out hivel);
//             }

//             if (line.Contains("hivel="))
//             {
//                 var index = line.IndexOf("hivel=");
//                 var l = line;
//                 l = l.Remove(0, index + 6);
//                 string[] numbers = Regex.Split(l, @"\D+");
//                 int.TryParse(numbers[0], out lovel);
//             }

//             if (line.Contains("loop_start="))
//             {
//                 var index = line.IndexOf("loop_start=");
//                 var l = line;
//                 l = l.Remove(0, index + 11);
//                 string[] numbers = Regex.Split(l, @"\D+");
//                 int.TryParse(numbers[0], out loopStart);
//             }

//             if (line.Contains("loop_end="))
//             {
//                 var index = line.IndexOf("loop_end=");
//                 var l = line;
//                 l = l.Remove(0, index + 9);
//                 string[] numbers = Regex.Split(l, @"\D+");
//                 int.TryParse(numbers[0], out loopEnd);
//             }


//             if (line.Contains("lokey="))
//             {
//                 var index = line.IndexOf("lokey=");
//                 var l = line;
//                 l = l.Remove(0, index + 6);
//                 //l = l.Remove(3, l.Length - 3);

//                 lo = -1;//NoteNameToMidi(l);
//                 if (lo == -1)
//                 {
//                     string[] numbers = Regex.Split(l, @"\D+");
//                     int.TryParse(numbers[0], out lo);
//                 }
//             }

//             if (line.Contains("hikey="))
//             {
//                 var index = line.IndexOf("hikey=");
//                 var l = line;
//                 l = l.Remove(0, index + 6);
//                 //l = l.Remove(3, l.Length - 3);

//                 hi = -1;//NoteNameToMidi(l);
//                 if (hi == -1)
//                 {
//                     string[] numbers = Regex.Split(l, @"\D+");
//                     int.TryParse(numbers[0], out hi);
//                 }
//             }

//             if (line.Contains("pitch_keycenter="))
//             {
//                 var index = line.IndexOf("pitch_keycenter=");
//                 var l = line;
//                 l = l.Remove(0, index + 16);
//                 //l = l.Remove(3, l.Length - 3);

//                 center = -1; //NoteNameToMidi(l);
//                 if (center == -1)
//                 {
//                     string[] numbers = Regex.Split(l, @"\D+");
//                     int.TryParse(numbers[0], out center);
//                 }
//             }

//             if (line.Contains(" key="))
//             {
//                 var index = line.IndexOf(" key=");
//                 var l = line;
//                 l = l.Remove(0, index + 5);
//                 center = NoteNameToMidi(l);
//                 if (center == -1)
//                 {
//                     string[] numbers = Regex.Split(l, @"\D+");
//                     int.TryParse(numbers[0], out center);
//                 }
//             }

//             if (line.Contains("transpose="))
//             {
//                 var index = line.IndexOf("transpose=");
//                 var l = line;
//                 l = l.Remove(0, index + 10);
//                 //string numbers;
//                 //Regex.Match(l,@"^-?\d*\.{0,1}\d+$").Result(numbers);
//                 Regex rgxNumber = new Regex("([-+]{0,1}[0-9]+)");
//                 Match mNumber = rgxNumber.Match(l);
//                 string result = mNumber.Groups[1].Value;
//                 int.TryParse(result, out transpose);
//                 // Debug.Log(transpose);
//             }


//             if (line.Contains("sample="))
//             {
//                 int index = line.IndexOf("sample=");
//                 string str = line;
//                 str = line.TrimEnd('\r', '\n');
//                 str = str.Remove(0, index + 7);
//                 string[] file = Regex.Split(str, ".wav");
//                 var clipPath = (Path.GetDirectoryName(p) + "/" + file[0] + ".wav");
//                 addClip = AssetDatabase.LoadAssetAtPath<AudioClip>(clipPath) as AudioClip;

//             }

//             if (line.Contains("<region>") || line.Contains("<group>") || i == lines.Length - 1 && addClip != null)
//             {         // If new region about to begin, or end of file; define note settings
//                 if (hivel - lovel != 0)
//                 {
//                     if (hi - lo == 0)
//                     {
//                         for (int j = lovel; j < hivel; j++)
//                         {
//                             // sampler.velNoteData.noteData[j].notes[hi] = center;
//                             // sampler.velNoteData.noteData[j].clip[hi] = addClip as AudioClip;
//                             // sampler.velNoteData.noteData[j].transpose[hi] = transpose;
//                             // sampler.velNoteData.noteData[j].loopStart[hi] = loopStart;
//                             // sampler.velNoteData.noteData[j].loopEnd[hi] = loopEnd;
//                         }
//                     }
//                 }
//                 else if (hi - lo == 0)
//                 {
//                     sampler.noteData[seq_position].notes[hi] = center;
//                     sampler.noteData[seq_position].clip[hi] = addClip as AudioClip;
//                     sampler.noteData[seq_position].transpose[hi] = transpose;
//                     sampler.noteData[seq_position].loopStart[hi] = loopStart;
//                     sampler.noteData[seq_position].loopEnd[hi] = loopEnd;
//                     Debug.Log(hi);
//                 }
//                 else
//                 {
//                     for (int j = lo; j <= hi; j++)
//                     {
//                         sampler.noteData[seq_position].notes[j] = center;
//                         sampler.noteData[seq_position].clip[j] = addClip as AudioClip;
//                         sampler.noteData[seq_position].transpose[j] = transpose;
//                         sampler.noteData[seq_position].loopStart[j] = loopStart;
//                         sampler.noteData[seq_position].loopEnd[j] = loopEnd + 1;
//                     }
//                 }
//                 transpose = 0; lo = 0; hi = 0; center = 0; loopStart = 0; loopEnd = 0; addClip = null;
//             }
//         }


//         PrefabUtility.SaveAsPrefabAsset(obj, prefabPath);
//         DestroyImmediate(obj);
//         AssetDatabase.Refresh();

//     }

//     int NoteNameToMidi(string n)
//     {
//         n = n.ToLower();
//         string[] note = Regex.Split(n, @"\d");
//         string oct = Regex.Match(n, @"\d").Value;
//         string[] names = { "c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b" };
//         var pos = System.Array.IndexOf(names, note[0]);

//         int octave;
//         int.TryParse(oct, out octave);
//         var midinote = pos + ((octave + 1) * 12);

//         return midinote;
//     }

// }


