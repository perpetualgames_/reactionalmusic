﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GestrumentSampler))]
public class GestrumentSamplerEditor : Editor
{
    // Start is called before the first frame update
    bool extraFoldout = false;

    bool debug;

    public override void OnInspectorGUI()
    {
        var gS = target as GestrumentSampler;

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUIStyle guiStyle = new GUIStyle(); //create a new variable
        guiStyle.fontSize = 30;
         GUILayout.Space(10);
        GUILayout.Label("GESTRUMENT", guiStyle);
        guiStyle.fontSize = 15;
        GUILayout.Label("    Basic Unity Sampler", guiStyle);
         GUILayout.Space(20);
        gS.polyphony = EditorGUILayout.IntField("Polyphony", gS.polyphony);
        gS.globalTranspose = EditorGUILayout.IntField("Global Transpose", gS.globalTranspose);

        GUILayout.EndVertical();
        Rect rect = new Rect(0, 0, 128, 128);
        GUILayout.Label((Texture)Resources.Load("GestrumentLogo"));
        GUILayout.EndHorizontal();

        gS.sampleBank = (GestrumentSampleBank)EditorGUILayout.ObjectField("Sample Bank", gS.sampleBank, typeof(GestrumentSampleBank),true);

        EditorGUILayout.MinMaxSlider("Scale Velocity", ref gS.minVelocity, ref gS.maxVelocity, 0, 1f);


        gS.volumeOffset = EditorGUILayout.Slider("Volume Offset", gS.volumeOffset, 0f, 1f);

        if (!gS.oneShot)
        {
           gS.releaseTime = EditorGUILayout.Slider("Release Time", gS.releaseTime,0,5f);
        }


        extraFoldout = EditorGUILayout.Foldout(extraFoldout, "Additional Settings");
        if (extraFoldout)
        {
            gS.autoPanByPitch = EditorGUILayout.Toggle("Auto Pan By Pitch", gS.autoPanByPitch);
            if (gS.autoPanByPitch)
            {
                EditorGUILayout.MinMaxSlider("Range", ref gS.autoPanMin, ref gS.autoPanMax, 0, 127);
            }
            gS.spatialiseInstrument = EditorGUILayout.Toggle("Spatialize Instrument", gS.spatialiseInstrument);
            if (gS.spatialiseInstrument)
            {
                gS.spatialParent = (GameObject)EditorGUILayout.ObjectField("Spatial Parent:", gS.spatialParent, typeof(GameObject), true);
            }
            gS.fakeRoundRobin = EditorGUILayout.Toggle("Fake RoundRobin", gS.fakeRoundRobin);
            if (gS.fakeRoundRobin)
            {
               gS.rrRange = EditorGUILayout.IntField("Round Robin Range", gS.rrRange);

            }

            gS.oneShot = EditorGUILayout.Toggle("One Shot", gS.oneShot);


            debug = EditorGUILayout.Toggle("Debug", debug);
            if (debug)
            {
                DrawDefaultInspector();
            }
        }





    }
}
