﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(GestrumentAudioSync))]
public class GestrumentAudioSyncEditor : Editor
{

    public override void OnInspectorGUI()
    {
        GestrumentAudioSync script = (GestrumentAudioSync)target;

        script.timesig = (timeSignature)EditorGUILayout.EnumPopup("Time Signature", script.timesig);
        script.root = (rootNote)EditorGUILayout.EnumPopup("Root", script.root);
//        script.scale = (scaleModes)EditorGUILayout.EnumPopup("Scale Mode", script.scale);
        script.tempo = EditorGUILayout.FloatField("Tempo",script.tempo);
        script.musicPlayer = (AudioSource)EditorGUILayout.ObjectField("Audio Source",script.musicPlayer,typeof(AudioSource),true);
        script.currentBeat = EditorGUILayout.IntField("Current Beat",script.currentBeat);
        script.startBeat = EditorGUILayout.IntField("Start Beat",script.startBeat);
        script.cycleLength = EditorGUILayout.IntField("Cycle length",script.cycleLength);
    }

}
