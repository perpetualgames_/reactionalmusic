/**
 * @file version.h
 * @brief Version header
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_VERSION_H
#define GE_VERSION_H

#include <stdbool.h>
#include "GE_Core/common.h"

/**
 * @brief Version.
 */
typedef struct
{

    /**
     * @brief Major.
     */
    int major;

    /**
     * @brief Minor.
     */
    int minor;

    /**
     * @brief Patch.
     */
    int patch;

    /**
     * @brief Build info.
     */
    char build_info[512];

    /**
     * @brief Git info.
     */
    char git_info[512];

} GE_Version;


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Get the version.
 * @returns A pointer to @ref GE_VERSION.
 */
const GE_Version* GE_GetVersion(void);

/**
 * @brief Get the just the minor, major and patch version as a string.
 * @returns A string version.
 */
const char* GE_GetVersionString(void);

/**
 * @brief Get the the build info as string.
 * @returns The build info.
 */
const char* GE_GetBuildInfo(void);

/**
 * @brief Get the the GIT info as string.
 * @returns The build info.
 */
const char* GE_GetGITInfo(void);

/**
 * @brief Get the the full version as a string.
 * @returns A string version of the version.
 */
GE_PUBLIC const char* GE_GetFullVersionString(void);

#ifdef __cplusplus
}
#endif

#endif /* GE_ENGINE_H */
