/**
 * @file engine.h
 * @brief GE_Core main context.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @author Peter Gebauer
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * GE_Engine is the main "context" of GE_Core. Most of the functionality GE_Core
 * provides needs access to an instance of GE_Engine.
 *
 * Call @ref GE_Engine_Render to advance the master clock and trigger note events.
 * This function should be called from an audio thread.
 *
 * Call @ref GE_Engine_ProcessRunLoop to process the main event loop.
 * This function should be called from a main thread context, such as a GUI thread.
 *
 * @example tutorial-01-engine.c
 * @example tutorial-02-controls.c
 * @example tutorial-03-generators.c
 * @example tutorial-04-rendering.c
 * @example tutorial-05-scales.c
 *
 */

#ifndef GE_ENGINE_H
#define GE_ENGINE_H

#include "GE_Core/common.h"
#include "GE_Core/component.h"
#include "GE_Core/control_source.h"
#include "GE_Core/instrument.h"
#include "GE_Core/cursor.h"
#include "GE_Core/scale.h"
#include "GE_Core/utils/constants.h"
#include "GE_Core/utils/variant.h"
#include "GE_Core/version.h"
#include "GE_Core/message_queue.h"
#include "GE_Core/utils/thread.h"
#include "GE_Core/notification_center.h"
#include "GE_Core/utils/atomic.h"
#include "GE_Core/utils/heap.h"
#include "GE_Core/event.h"
#include "GE_Core/utils/osc.h"

/**
 * The highest supported state file format version.
 */
#define GE_STATE_FORMAT_VERSION 1

/**
 * The maximum number of transpose slots.
 */
#define GE_MAX_TRANSPOSE_SLOTS 128

/**
 * @brief Invalid event object.
 */
#define GE_ERROR_EVENT_OBJECT -1

/**
 * @brief Invalid event object index.
 */
#define GE_ERROR_OBJECT_INDEX -2

/**
 * @brief Invalid param name.
 */
#define GE_ERROR_PARAM_NAME -3

/**
 * @brief Invalid param index.
 */
#define GE_ERROR_PARAM_INDEX -4

/**
 * @brief Engine ControlSource indexes.
 */
#ifndef GE_ENGINE_SOURCE_ENUM
#define GE_ENGINE_SOURCE_ENUM
typedef enum {
    GE_ENGINE_SOURCE_SLIDER_0,
    GE_ENGINE_SOURCE_SLIDER_1,
    GE_ENGINE_SOURCE_SLIDER_2,
    GE_ENGINE_SOURCE_SLIDER_3,
    GE_ENGINE_SOURCE_SLIDER_4,
    GE_ENGINE_SOURCE_SLIDER_5,
    GE_ENGINE_SOURCE_SLIDER_6,
    GE_ENGINE_SOURCE_SLIDER_7,
    GE_ENGINE_SOURCE_MAX_SLIDERS,
    GE_ENGINE_SOURCE_RANDOM = GE_ENGINE_SOURCE_MAX_SLIDERS,
    GE_ENGINE_SOURCE_SCALE_SLOT,
    GE_ENGINE_SOURCE_TRANSPOSE_SLOT,
    MAX_GE_ENGINE_SOURCE,
} GE_EngineSourceIndex;
#endif

/**
 * @brief Engine ControlInput indexes.
 */
typedef enum {
    GE_ENGINE_INPUT_TEMPO,
    MAX_GE_ENGINE_INPUT
} GE_EngineInputIndex;

/**
 * @see struct GE_EngineConfig
 */
typedef struct GE_EngineConfig GE_EngineConfig;

#ifndef GE_ENGINE_STRUCT
#define GE_ENGINE_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Engine
 */
typedef struct GE_Engine GE_Engine;
#endif

/**
 * Forward declaration.
 * @see struct GE_ERSources
 */
typedef struct GE_ERSources GE_ERSources;

/**
 * Forward declaration.
 * @see struct GE_EngineSync
 */
typedef struct GE_EngineSync GE_EngineSync;

#ifndef GE_ONZERO_STRUCT
#define GE_ONZERO_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Engine
 */
typedef struct GE_OnZero GE_OnZero;
#endif

/**
 * GE_OnZero callback.
 * @see struct GE_OnZero
 */
typedef void (* GE_OnZeroCallback)(void *arg);

/**
 * @brief Forward declaration. @see struct GE_Transpose
 */
typedef struct GE_Transpose GE_Transpose;

/**
 * Callback function for start note events.
 * @param engine The engine.
 * @param instrument The instrument index.
 * @param voice The voice index.
 * @param beat The beat time relative to the engine clock.
 * @param pitch The pitch of the note.
 * @param velocity The velocity of the note (normalized [0..1]).
 * @relates GE_Engine
 */
typedef void (*GE_StartNoteCallback)(GE_Engine *engine, int instrument, int voice, double beat, float pitch, float velocity);

/**
 * Callback function for stop note events.
 * @param engine The engine.
 * @param instrument The instrument index.
 * @param voice The voice index.
 * @param beat The beat time relative to the engine clock.
 * @relates GE_Engine
 */
typedef void (*GE_StopNoteCallback)(GE_Engine *engine, int instrument, int voice, double beat);

/**
 * Callback function for sound controls.
 * @param engine The engine.
 * @param instrument The instrument index.
 * @param soundControl The sound control that triggered the callback.
 * @param index The control index (can be used as MIDI CC number).
 * @param newValue The new value.
 * @param oldValue The old value.
 * @relates GE_Engine
 */
typedef void (*GE_SoundControlCallback)(GE_Engine *engine, int instrument, GE_SoundControl soundControl, int index, double newValue, double oldValue);

/**
 * The realtime thread has received or generated an event from GE_Engine_Render().
 * @param engine The engine.
 * @param size The size of the data (in bytes).
 * @param data The data.
 * @param arg Provided by the engine GE_Engine_SetEventCallback().
 */
typedef void (*GE_EventCallback)(GE_Engine *engine, const GE_Event *event, void *arg);

/**
 * Used to initialize the engine.
 * Setting @ref GE_EngineConfig.numInstruments or
 * @ref GE_EngineConfig.numCursors will create the specified number of
 * instruments/cursor when the engine is created.
 */
struct GE_EngineConfig {

    /**
     * Provide a title, can be NULL.
     */
    const char *title;

    /**
     * The number of cursors to allocate on init.
     */
    int numCursors;

    /**
     * The number of instruments to allocate on init.
     */
    int numInstruments;

    /**
     * Number of instrument-cursor mappings.
     */
    int numInstrumentCursorMappings;

    /**
     * Instrument-cursor mappings, a pointer to an array of 2 integers
     * where the first integer is the instrument index and the second
     * integer is the cursor index.
     */
    int (*instrumentCursorMappings)[2];

    /**
     * The number of scale slots to allocate on init.
     * @note By default there is always 8 scale slots. Use this option if more
     * than 8 slots are needed.
     */
    int numScaleSlots;

    /**
     * A callback for start note events.
     */
    GE_StartNoteCallback startNoteCallback;

    /**
     * A callback for stop note events.
     */
    GE_StopNoteCallback stopNoteCallback;

    /**
     * The sound control callback.
     */
    GE_SoundControlCallback soundControlCallback;

    /**
     * Event callback.
     */
    GE_EventCallback eventCallback;

    /**
     * Used to synchronize one or more engines from another thread.
     */
    GE_EngineSync *sync;

    /**
     * Arbitrary data.
     */
    void *appData;

    /**
     * Component setup.
     */
    GE_Component component;

    /**
     * @brief Enable automagic event generation from a realtime render to main.
     */
    bool eventsEnabled[MAX_GE_EVENT_TYPE];

};

/**
 * Used to synchronize one or more engines from another thread.
 */
struct GE_EngineSync {

    /**
     * Playing yes/no.
     **/
    volatile GE_AtomicBool playing;

    /**
     * Current beat.
     */
    double beat;

    /**
     * Tempo.
     */
    double tempo;

    /**
     * Phase.
     */
    double phase;

};

/**
 * Container for EventRecorder sources.
 */
struct GE_ERSources {
    GE_ControlSource **items;
    int count;
};

struct GE_OnZero {
    int count;
    GE_OnZeroCallback callback;
    void *arg;
};

/**
 * @brief A transposition.
 */
struct GE_Transpose {

    /**
     * @brief The name of the transposition.
     */
    char *name;

    /**
     * @brief The transposition value.
     */
    float value;
};

/**
 * The main GE_Core context.
 */
struct GE_Engine {

    /**
     * Component setup, polymorphic struct.
     */
    GE_Component component;

    /**
     * The number of input frames for the current render cycle.
     * @see GE_Engine_Render
     */
    int bufferFrames;

    /**
     * The current sample rate.
     * @see GE_Engine_Render
     */
    double sampleRate;

    /**
     * Current beat time.
     * The start time of a render cycle.
     */
    double currentBeat;

    /**
     * Next beat time in the engine.
     * The end time of a render cycle.
     */
    double nextBeat;

    /**
     * Used as the initial currentBeat if the engine is synched using an
     * external clock.
     */
    double resetBeat;

    /**
     * The number of beats in a bar.
     * Default is 4.
     */
    int beatsPerBar;

    /**
     * Arbitrary data.
     */
    void *appData;

    /**
     * A callback for start note events.
     */
    GE_StartNoteCallback startNoteCallback;

    /**
     * Arbitrary argument to the start note.
     */
    void *startNoteCallbackArg;
    
    /**
     * A callback for stop note events.
     */
    GE_StopNoteCallback stopNoteCallback;

    /**
     * Arbitrary argument to the start note.
     */
    void *stopNoteCallbackArg;

    /**
     * The sound control callback.
     */
    GE_SoundControlCallback soundControlCallback;

    /**
     * Arbitrary argument to the sound control.
     */
    void *soundControlCallbackArg;

    /**
     * If non-NULL called from the realtime thread (GE_Engine_Render())
     * when ever the render function generates an event.
     */
    GE_EventCallback eventCallback;

    /**
     * Arbitrary argument to the event callback.
     */
    void *eventCallbackArg;

    /**
     * The current host machine's time, platform specific.
     */
    uint64_t currentHostTime;

    /**
     * The number of frames in a beat.
     */
    double framesPerBeat;

    /**
     * The number of scale slots.
     */
    int numScaleSlots;

    /**
     * Array of global scales.
     */
    GE_Scale **scaleSlots;

    /**
     * Array of global transpositions.
     */
    GE_Transpose transposeSlots[GE_MAX_TRANSPOSE_SLOTS];

    /**
     * Pointer to the current scale slot.
     * @ref GE_Engine.scaleSlots
     */
    int currentScaleIndex;

    /**
     * Pointer to the current transpose slot.
     * @ref GE_Engine.transposeSlots
     */
    int currentTransposeIndex;

    /**
     * The number of available instruments.
     */
    int numInstruments;

    /**
     * An array of instrument instances.
     */
    GE_Instrument **instruments;

    /**
     * The number of available cursors.
     */
    int numCursors;

    /**
     * An array of cursor instances.
     */
    GE_Cursor **cursors;

    /**
     * Is the engine currently playing?
     * Can be used to reset the engine clock.
     */
    bool playing;

    /**
     * Local setting for playing.
     */
    bool local_playing;

    /**
     * Internal use only.
     */
    bool rt_playing;

    /**
     * Internal use only.
     */
    bool didReset;

    /**
     * The (MIDI) pitch bend range.
     * Default 2.
     */
    int pitchBendRange;

    /**
     * In beats, 0 is off.
     * Used by EventRecorders.
     */
    int loopQuant;

    /**
     * An array of control sources.
     */
    GE_ControlSource *controlSources[MAX_GE_ENGINE_SOURCE];

    /**
     * An array of control inputs.
     */
    GE_ControlInput *controlInputs[MAX_GE_ENGINE_INPUT];

    /**
     * The generator types registered with the engine.
     */
    const GE_GeneratorType **generatorTypes;

    /**
     * The number of registered generator types.
     */
    int numGeneratorTypes;

    /**
     * Array of recordable ControlSources.
     * Internal use only.
     */
    GE_ERSources recordableSources;

    /**
     * Array of recorded ControlSources to be used on the audio thread.
     * Internal use only.
     */
    GE_ERSources recordedSources;

    /**
     * A user managed title.
     */
    char *title;

    /**
     * Internal use only.
     */
    double eventRecStartBeat;
    double eventRecStartTime;
    GE_MessageQueue messageQueue;
    GE_OnZero ERStateUpdateWait;
    GE_Mutex *renderLock;
    
    /**
     * Notifies that 'playing' changed.
     */
    GE_NotificationCenter isPlayingNotification;
    
    /**
     * Notifies that Engine did load state.
     */
    GE_NotificationCenter didLoadStateNotification;

    /**
     * Used to synchronize the engine with another thread or external program.
     */
    GE_EngineSync *sync;

    /**
     * Last beat from external host.
     */
    double lastSyncBeat;

    /**
     * @brief Output event queue (from audio/render thread to main).
     */
    GE_EventQueue eventQueueOut;

    /**
     * @brief Input event queue (from main to audio/render thread).
     */
    GE_EventQueue eventQueueIn;

    /**
     * @brief Used by the main thread to push events on eventQueueIn.
     */
    GE_BinHeap eventScheduler;

    /**
     * @brief Enable event generation.
     */
    bool eventsEnabled[MAX_GE_EVENT_TYPE];

    /**
     * @brief The path of the last loaded g2preset.
     */
    char *lastG2Preset;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Create a new engine.
 * @param config A configuration for easy setup, pass NULL to use defaults.
 * @returns A new engine or NULL if @p config has unacceptable settings.
 * @relates GE_Engine
 */
GE_PUBLIC GE_Engine* GE_Engine_New(const GE_EngineConfig * config);

/**
 * @brief Free engine.
 * @param engine The engine.
 * @relates GE_Engine
 */
GE_PUBLIC void GE_Engine_Free(GE_Engine *engine);

/**
 * @brief Process the main thread run loop.
 * @note This function should be called periodically from a main thread context.
 * @param engine The engine.
 */
GE_PUBLIC void GE_Engine_ProcessRunLoop(GE_Engine *engine);

/**
 * @brief Check if the engine has events enabled.
 * @param engine The engine.
 * @param eventType The event type to get the status flag for.
 * @returns True if events are enabled.
 */
bool GE_Engine_GetEventsEnabled(const GE_Engine *engine, GE_EventType eventType);

/**
 * @brief Enable events for the engine.
 * @param engine The engine.
 * @param eventType The event type to set the status flag for.
 * @param enabled True if events are enabled.
 */
GE_PUBLIC void GE_Engine_SetEventsEnabled(GE_Engine *engine, GE_EventType eventType, bool enabled);

/**
 * @brief Dequeue the internal event buffer for events pushed from the audio thread.
 * @param engine The engine.
 * @param maxEvents The maximum number of events to return.
 * @param events A list to populate with note events.
 * @returns The number of dequeued events.
 * @note This is should be called in a main thread context.
 *
 * Main <= Realtime.
 */
GE_PUBLIC int GE_Engine_DequeueEvents(GE_Engine *engine, int maxEvents, GE_Event *events);

/**
 * @brief The events are scheduled and eventually moved to the realtime queue.
 * @param engine The engine.
 * @param numEvents The maximum number of events to return.
 * @param events Enqueue these events.
 * @returns The number of enqueued events.
 * @note This is should be called in a main thread context.
 * @note If the buffer is full the actual number of enqueued events may be less than @p numEvents.
 *
 * Main => Scheduler => Realtime.
 *
 * This buffer will be updated when GE_Engine_ProcessRunLoop() is called and
 * the events within a lookahead will be pushed for consumption by the audio
 * thread the next cycle.
 */
int GE_Engine_EnqueueEvents(GE_Engine *engine, int numEvents, const GE_Event *events);

/**
 * @brief The events are queued on the main thread queue.
 * @param engine The engine.
 * @param numEvents The maximum number of events to return.
 * @param events Enqueue these events.
 * @returns The number of enqueued events.
 * @note This is should be called in a realtime thread context.
 * @note If the buffer is full the actual number of enqueued events may be less than @p numEvents.
 *
 * Main <= Realtime.
 */
int GE_Engine_RTEnqueueEvents(GE_Engine *engine, int numEvents, const GE_Event *events);

/**
 * Serialize an event to an OSC message.
 * @param event The event to serialize.
 * @param size The maximum size in bytes to write to @p data, ignored
 * if @p data is NULL.
 * @param[out] data If non-NULL write serialized bytes here.
 * @returns The number serialized bytes or a negative error code on failure.
 * @note If @p data is NULL nothing is serialized and the number of required bytes is returned.
 */
GE_PUBLIC int GE_Engine_ToOSC(GE_Engine *engine, const GE_Event *event, bool compat, size_t size, void *osc);

/**
 * Deserialize an OSC message to an event.
 * @param[out] event If non-NULL write event data here.
 * @param size The maximum size in bytes to read from @p data.
 * @param data Deserialize this OSC data.
 * @returns The number deserialized bytes or a negative error code on failure.
 * @note If @p event is NULL nothing is deserialized and the number of bytes required
 * for the next message is returned.
 */
int GE_Engine_FromOSC(GE_Engine *engine, GE_Event *event, size_t size, const void *osc);

/**
 * @brief Dequeue the internal event buffer for events pushed from the audio thread.
 * @param engine The engine.
 * @param compat Enable strict OSC 1.0 type compatability.
 * @param maxSize The maximum number of bytes to write in @p oscEvent.
 * @param[out] oscEvent Write OSC data here, pass NULL to simply discard the event.
 * @returns The number bytes written to @p oscEvent (or would have been if @p oscEvent is NULL)
 * or a negative error code on failure.
 * @note This function should be called from a main thread context.
 *
 * Main <= Realtime.
 *
 * This is essentially the same as GE_Engine_DequeueEvents(), but will serialize
 * the GE_Event to platform independent OSC data. Also, only one event at a time.
 */
GE_PUBLIC int GE_Engine_DequeueOSC(GE_Engine *engine, bool compat, int maxSize, void *oscEvent);

/**
 * @brief The event is scheduled and eventually moved to the realtime queue.
 * @param engine The engine.
 * @param size The number of available bytes in @p oscEvent.
 * @param oscEvent OSC data.
 * @returns The number bytes actually read from @p oscEvent or a negative error code on failure.
 * @note This function should be called from a main thread context.
 *
 * Main => Scheduler => Realtime.
 * 
 * This is essentially the same as GE_Engine_EnqueueEvents(), but will deserialize
 * the platform independent OSC data to a GE_Event. Also, only one event at a time.
 */
GE_PUBLIC int GE_Engine_EnqueueOSC(GE_Engine *engine, int size, const void *oscEvent);

/**
 * @brief The events are queued on the main thread queue.
 * @param engine The engine.
 * @param size The number of available bytes in @p oscEvent.
 * @param oscEvent OSC data.
 * @returns The number bytes actually read from @p oscEvent or a negative error code on failure.
 * @note This is should be called in a realtime thread context.
 * @note If the buffer is full the actual number of enqueued events may be less than @p numEvents.
 *
 * Main <= Realtime.
 *
 * This is essentially the same as GE_Engine_RTEnqueueEvents(), but will deserialize
 * the platform independent OSC data to a GE_Event. Also, only one event at a time.
 */
int GE_Engine_RTEnqueueOSC(GE_Engine *engine, int size, const void *oscEvent);

/**
 * Render, this function should be called from an audio thread.
 * @param engine The engine.
 * @param sampleRate Sample rate of host audio context.
 * @param numFrames The number of frames to render
 * @returns True if render was run, false if the trylock failed or if @p sampleRate or @p numFrames <= 0.
 * @relates GE_Engine
 */
GE_PUBLIC bool GE_Engine_Render(GE_Engine *engine, double sampleRate, int numFrames);

/**
 * Get start note callback.
 * @param engine The engine.
 * @returns The start note callback, may be NULL for no callback.
 * @relates GE_Engine
 */
GE_StartNoteCallback GE_Engine_GetStartNoteCallback(const GE_Engine *engine);

/**
 * Set start note callback.
 * @param engine The engine.
 * @param cb The start note callback, may be NULL for no callback.
 * @relates GE_Engine
 */
void GE_Engine_SetStartNoteCallback(GE_Engine *engine, GE_StartNoteCallback cb);

/**
 * Get stop note callback.
 * @param engine The engine.
 * @returns The stop note callback, may be NULL for no callback.
 * @relates GE_Engine
 */
GE_StopNoteCallback GE_Engine_GetStopNoteCallback(const GE_Engine *engine);

/**
 * Set stop note callback.
 * @param engine The engine.
 * @param cb The stop note callback, may be NULL for no callback.
 * @relates GE_Engine
 */
void GE_Engine_SetStopNoteCallback(GE_Engine *engine, GE_StopNoteCallback cb);

/**
 * Get the sound control callback.
 * @param engine The engine.
 * @relates GE_Engine
 */
GE_SoundControlCallback GE_Engine_GetSoundControlCallback(const GE_Engine *engine);

/**
 * Set the sound control callback.
 * @param engine The engine.
 * @param callback The callback function, NULL for no callback.
 * @relates GE_Engine
 */
void GE_Engine_SetSoundControlCallback(GE_Engine *engine, GE_SoundControlCallback callback);

/**
 * Get the event callback (and data).
 * @param engine The engine.
 * @param arg If non-NULL return the arbitrary user data pointer.
 * @relates GE_Engine
 */
GE_EventCallback GE_Engine_GetEventCallback(const GE_Engine *engine, void **arg);

/**
 * @brief Set the event callback (and data).
 * @param engine The engine.
 * @param callback The callback function, NULL for no callback.
 * @param arg Arbitrary user data passed to the callback.
 * @relates GE_Engine
 */
GE_PUBLIC void GE_Engine_SetEventCallback(GE_Engine *engine, GE_EventCallback callback, void *arg);

/**
 * Get the playing state of the engine.
 * The engine will reset currentBeat according to resetBeat (default 0).
 * @see GE_Engine.resetBeat
 * @relates GE_Engine
 */
bool GE_Engine_GetPlaying(GE_Engine *engine);

/**
 * Set the playing state of the engine.
 * The engine will reset currentBeat according to resetBeat (default 0).
 * @see GE_Engine.resetBeat
 * @relates GE_Engine
 */
void GE_Engine_SetPlaying(GE_Engine *engine, bool play);

/**
 * Get the tempo.
 * @param engine The engine.
 * @returns The tempo in beats per minute.
 * @relates GE_Engine
 */
GE_PUBLIC double GE_Engine_GetTempo(const GE_Engine *engine);

/**
 * Set the tempo.
 * @param engine The engine.
 * @param bpm Tempo in beats per minute.
 * @relates GE_Engine
 */
GE_PUBLIC void GE_Engine_SetTempo(GE_Engine *engine, double bpm);

/**
 * Get the current beat (engine timeline).
 * @param engine The engine.
 * @returns The current beat.
 * @relates GE_Engine
 */
GE_PUBLIC double GE_Engine_GetCurrentBeat(const GE_Engine *engine);

/**
 * Set the current beat (engine timeline).
 * @param engine The engine.
 * @param currentBeat The current beat.
 * @relates GE_Engine
 */
GE_PUBLIC void GE_Engine_SetCurrentBeat(GE_Engine *engine, double currentBeat);

/**
 * Get the absolute frame offset for a beat in the render cycle.
 * @param engine The engine.
 * @param beat The beat to compare.
 * @returns The frame offset of the beat in the current render cycle.
 * @relates GE_Engine
 */
double GE_Engine_GetFrameOffsetForBeat(GE_Engine *engine, double beat);

/**
 * Get the current sample rate of the Engine.
 * @param engine The engine.
 * @returns The sample rate.
 * @relates GE_Engine
 */
GE_PUBLIC int GE_Engine_GetSampleRate(const GE_Engine *engine);

/**
 * Set the host time.
 * @param engine The engine.
 * @param hostTime The host machine's time.
 * The host time will be used for MIDI callback timestamps.
 * @relates GE_Engine
 */
void GE_Engine_SetHostTime(GE_Engine *engine, uint64_t hostTime);

/**
 * Get the host time.
 * @param engine The engine.
 * @relates GE_Engine
 */
uint64_t GE_Engine_GetHostTime(const GE_Engine *engine);

/**
 * Get the number of control sources.
 * @param engine The engine.
 * @returns The number of control sources the engine has.
 * @relates GE_Engine
 */
int GE_Engine_GetNumControlSources(const GE_Engine *engine);

/**
 * Get a control source from the engine.
 * @param engine The engine.
 * @param index The index of the control source to get.
 * @returns The control source or NULL if @p index was out of range.
 * @relates GE_Engine
 */
GE_ControlSource* GE_Engine_GetControlSource(const GE_Engine *engine, int index);

/**
 * Find a control source index by searching for it's title.
 * @param engine The engine.
 * @param name The name to find for.
 * @returns The control source or NULL if @p name was not found.
 * @relates GE_Engine
 */
int GE_Engine_FindControlSource(const GE_Engine *engine, const char *name);

/**
 * Get the number of control inputs.
 * @param engine The engine.
 * @returns The number of control inputs the engine has.
 * @relates GE_Engine
 */
int GE_Engine_GetNumControlInputs(const GE_Engine *engine);

/**
 * Get a control input from the engine.
 * @param engine The engine.
 * @param index The index of the control input to get.
 * @returns The control input or NULL if @p index was out of range.
 * @relates GE_Engine
 */
GE_ControlInput* GE_Engine_GetControlInput(const GE_Engine *engine, int index);

/**
 * Find a control input index by searching for it's title.
 * @param engine The engine.
 * @param name The name to find for.
 * @returns The control input or NULL if @p name was not found.
 * @relates GE_Engine
 */
int GE_Engine_FindControlInput(const GE_Engine *engine, const char *name);

/**
 * Get the number of instruments the engine has.
 * @param engine The engine.
 * @returns The number of instruments.
 * @relates GE_Engine
 */
int GE_Engine_GetNumInstruments(const GE_Engine *engine);

/**
 * Set the number of instruments the engine should have.
 * @param engine The engine.
 * @param numInstruments The number of instruments.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @relates GE_Engine
 */
void GE_Engine_SetNumInstruments(GE_Engine *engine, int numInstruments);

/**
 * Add a new instrument to the engine.
 * @param engine The engine.
 * @returns The new instrument.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @relates GE_Engine
 */
GE_Instrument* GE_Engine_AppendInstrument(GE_Engine *engine);

/**
 * Insert a new instrument into the engine.
 * @param engine The engine.
 * @param index The index of the insertion point.
 * @returns The new instrument.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 *
 * - If @p index < 0 the instrument will be inserted at zero.
 * - If @p index >= num instruments the new instrument will be appended at the end.
 * @relates GE_Engine
 */
GE_Instrument* GE_Engine_InsertInstrument(GE_Engine *engine, int index);

/**
 * Delete a instrument from the engine.
 * @param engine The engine.
 * @param index The instrument index.
 * @returns The number of removed instruments, zero if @p index is out of range.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @relates GE_Engine
 */
int GE_Engine_RemoveInstrument(GE_Engine *engine, int index);

/**
 * Get a instrument from the engine.
 * @param engine The engine.
 * @param index The instrument index.
 * @returns A pointer to the instrument or NULL if @p index is out of range.
 * @relates GE_Engine
 */
GE_Instrument* GE_Engine_GetInstrument(const GE_Engine *engine, int index);

/**
 * Get the number of cursors the engine has.
 * @param engine The engine.
 * @returns The number of cursors.
 * @relates GE_Engine
 */
int GE_Engine_GetNumCursors(const GE_Engine *engine);

/**
 * Set the number of cursors the engine should have.
 * @param engine The engine.
 * @param numCursors The number of cursors.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @relates GE_Engine
 */
void GE_Engine_SetNumCursors(GE_Engine *engine, int numCursors);

/**
 * Add a new cursor to the engine.
 * @param engine The engine.
 * @returns The new cursor.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @relates GE_Engine
 */
GE_Cursor* GE_Engine_AppendCursor(GE_Engine *engine);

/**
 * Insert a new cursor into the engine.
 * @param engine The engine.
 * @param index The index of the insertion point.
 * @returns The new cursor.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 *
 * - If @p index < 0 the cursor will be inserted at zero.
 * - If @p index >= num cursors the new cursor will be appended at the end.
 * @relates GE_Engine
 */
GE_Cursor* GE_Engine_InsertCursor(GE_Engine *engine, int index);

/**
 * Delete a cursor from the engine.
 * @param engine The engine.
 * @param index The cursor index.
 * @returns The number of removed cursors, zero if @p index is out of range.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @relates GE_Engine
 *
 * All references to the cursor in instruments and all references to the 
 * cursor's control sources in generators will be set to NULL.
 */
int GE_Engine_RemoveCursor(GE_Engine *engine, int index);

/**
 * Get a cursor from the engine.
 * @param engine The engine.
 * @param index The cursor index.
 * @returns A pointer to the cursor or NULL if @p index is out of range.
 * @relates GE_Engine
 */
GE_Cursor* GE_Engine_GetCursor(const GE_Engine *engine, int index);

/**
 * Get arbitrary app data pointer.
 * @param engine The engine.
 * @returns The arbitrary app data pointer.
 * @relates GE_Engine
 */
void* GE_Engine_GetAppData(const GE_Engine *engine);

/**
 * Set arbitrary app data pointer.
 * @param engine The engine.
 * @param appData The arbitrary app data pointer.
 * @relates GE_Engine
 */
void GE_Engine_SetAppData(GE_Engine *engine, void *appData);

/**
 * Return the current state in a dict variant.
 * @param engine The engine.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will never return NULL (even though the variant may be an empty dict).
 *
 * Used to save states.
 * @relates GE_Engine
 */
GE_Variant* GE_Engine_GetState(const GE_Engine *engine);

/**
 * Set the current engine state using a dict variant.
 * @param engine The engine.
 * @param state A dict variant with the state, pass NULL to clear engine.
 * @returns True if the state was set, false on failure.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 *
 * Used to load states.
 * @relates GE_Engine
 */
bool GE_Engine_SetState(GE_Engine *engine, const GE_Variant *state);

/**
 * Load state from a file.
 * @param engine The engine.
 * @param f The file to load the state from.
 * @returns True if the state was loaded, false on error.
 * @relates GE_Engine
 * @note The file must be opened in binary mode.
 */
bool GE_Engine_LoadStateF(GE_Engine *engine, FILE *f);

/**
 * Save state to a file.
 * @param engine The engine.
 * @param f The file to load the state from.
 * @returns True if the state was saved, false on error.
 * @relates GE_Engine
 * @note The file must be opened in binary mode.
 */
bool GE_Engine_SaveStateF(const GE_Engine *engine, FILE *f);

/**
 * Load state from a file.
 * @param engine The engine.
 * @param filename Path to the file.
 * @returns True if the state was loaded, false on error.
 * @relates GE_Engine
 */
bool GE_Engine_LoadState(GE_Engine *engine, const char *filename);

/**
 * Load state from a Gestrument Pro preset file.
 * @param engine The engine.
 * @param filename Path to the file.
 * @returns True if the state was loaded, false on error.
 * @relates GE_Engine
 */
bool GE_Engine_LoadG2Preset(GE_Engine *engine, const char *filename);

/**
 * Save state to a file.
 * @param engine The engine.
 * @param filename Path to the file.
 * @returns True if the state was saved, false on error.
 * @relates GE_Engine
 */
bool GE_Engine_SaveState(const GE_Engine *engine, const char *filename);

/**
 * Set a defaultScale for a scale slot in the engine.
 * @param engine The engine.
 * @param scale A GE_DefaultScale
 * @param index Scale slot index 0 - 255.
 * @return true on success, false if index is out of range or @p scale is invalid.
 * @relates GE_Engine
 */
bool GE_Engine_SetDefaultScaleForSlot(GE_Engine *engine, GE_DefaultScale scale, int index);

/**
 * Get a scale from one of the default scale slots.
 * @param engine The engine.
 * @param index Scale slot index 0 - 255.
 * @return A GE_Scale or NULL if index is out of range.
 * @relates GE_Engine
 */
GE_Scale* GE_Engine_GetScaleForSlot(GE_Engine *engine, int index);

/**
 * Set a scale for one of the default scale slots.
 * @param engine The engine.
 * @param scale The new scale.
 * @param index Scale slot index 0 - 255.
 * @return True if set or false if index is out of range.
 * @relates GE_Engine
 * @note @p scale will be copied to internal scale allocation, 
 *       remember to call GE_Scale_Free() when you're done with it.
 */
bool GE_Engine_SetScaleForSlot(GE_Engine *engine, const GE_Scale *scale, int index);

/**
 * Set the root pitch for a scale.
 * @param engine The engine.
 * @param rootPitch The new root pitch.
 * @param index Scale slot index 0 - 255.
 * @relates GE_Engine
 * @returns True on success, false if index is out of bounds.
 */
bool GE_Engine_SetRootForScaleSlot(const GE_Engine *engine, float rootPitch, int index);

/**
 * Set the root pitch for a scale.
 * @param engine The engine.
 * @param root The root pitch.
 * @param str A string with a default scale, or an array of semitones.
 * @param index Scale slot index 0 - 255.
 * @relates GE_Engine
 * @returns True on success, false if index is out of bounds, or if @p str is invalid.
 */
bool GE_Engine_SetStringScaleForSlot(GE_Engine *engine, float root, const char *str, int index);

/**
 * Returns the scale index controlled by the "ScaleSlot" control source.
 * @param engine The engine.
 * @returns The scale index as seleceted by the "ScaleSlot" control source.
 * @note Value is between 0 and @ref GE_Engine.numScaleSlots.
 */
int GE_Engine_GetCurrentScaleIndex(const GE_Engine *engine);

/**
 * Returns the scale controlled by the "ScaleSlot" control source.
 * @param engine The engine.
 * @returns The scale as seleceted by the "ScaleSlot" control source or NULL if no scale is set.
 */
const GE_Scale* GE_Engine_GetCurrentScale(const GE_Engine *engine);

/**
 * Returns the transpose index controlled by the "ScaleSlot" control source.
 * @param engine The engine.
 * @returns The transpose index as seleceted by the "TranposeSlot" control source.
 * @note Value is between 0 and @ref GE_MAX_TRANSPOSE_SLOTS
 */
int GE_Engine_GetCurrentTransposeIndex(const GE_Engine *engine);

/**
 * Set the name of a transpose slot.
 * @brief Set the name of transposition.
 * @param engine The engine.
 * @param name The name to set.
 * @param index The transpose slot index.
 */
void GE_Engine_SetTransposeName(GE_Engine *engine, char *name, int index);

/**
 * Get the name of a transpose slot.
 * @brief Get the name of transposition.
 * @param engine The engine.
 * @param index The transpose slot index.
 * @returns Name or empty string if not set.
 */
const char* GE_Engine_GetTransposeName(const GE_Engine *engine, int index);

/**
 * Set the value of a transpose slot.
 * @brief Set the value of transposition.
 * @param engine The engine.
 * @param value The value to set.
 * @param index The transpose slot index.
 */
void GE_Engine_SetTransposeValue(GE_Engine *engine, float value, int index);

/**
 * Get the value of a transpose slot.
 * @brief Get the value of transposition.
 * @param transpose The transposition.
 * @param index The transpose slot index.
 * @returns The value.
 */
float GE_Engine_GetTransposeValue(const GE_Engine *engine, int index);

/**
 * Return the number of registered generator types.
 * @param engine The engine.
 * @returns The number of registered generator types.
 * @relates GE_Engine
 */
int GE_Engine_GetNumGeneratorTypes(const GE_Engine *engine);

/**
 * Get a registered generator type.
 * @param engine The engine.
 * @param index The index of the generator type.
 * @returns A pointer to the generator type or NULL if @p index is out of range.
 * @relates GE_Engine
 */
const GE_GeneratorType* GE_Engine_GetGeneratorType(const GE_Engine *engine, int index);

/**
 * Get a registered generator type by name.
 * @param engine The engine.
 * @param name The name of the generator type.
 * @returns A pointer to the generator type or NULL if @p name was not found.
 * @relates GE_Engine
 */
const GE_GeneratorType* GE_Engine_GetGeneratorTypeName(const GE_Engine *engine, const char *name);

/**
 * Find a generator type index by name.
 * @param engine The engine.
 * @param name The name to search for (case-insensetive).
 * @returns The index of the found generator type or -1 if not found.
 * @relates GE_Engine
 */
int GE_Engine_FindGeneratorType(const GE_Engine *engine, const char *name);

/**
 * Register a generator type with the engine.
 * @param engine The engine.
 * @param genType The generator type to register.
 * @returns True if registered, false if there is already a generator type with the same name registered.
 * @relates GE_Engine
 */
bool GE_Engine_AddGeneratorType(GE_Engine *engine, const GE_GeneratorType *genType);

/**
 * Unregister a generator type from the engine.
 * @param engine The engine.
 * @param index The generator to unregister.
 * @returns True if unregistered, false if @p index is out of range.
 * @relates GE_Engine
 */
bool GE_Engine_RemoveGeneratorType(GE_Engine *engine, int index);

/**
 * Start event recording.
 * @param engine The engine.
 * @param time The time the recording started.
 * @relates GE_Engine
 */
void GE_Engine_StartEventRecording(GE_Engine *engine, double time);

/**
 * Saves the current value of ControlSource as new event.
 * @param engine The engine.
 * @param source Pointer to the GE_ControlSource to record.
 * @param time The time of the recorded event.
 * @relates GE_Engine
 */
void GE_Engine_AddEventForControlSource(GE_Engine *engine, GE_ControlSource *source, double time);

/**
 * Finish event recording.
 * @param engine The engine.
 * @param time The time the recording finished.
 * @relates GE_Engine
 */
void GE_Engine_FinishEventRecording(GE_Engine *engine, double time);

/**
 * Clear all event recorders.
 * @param engine The engine.
 * @relates GE_Engine
 */
void GE_Engine_ClearAllEventRecordings(GE_Engine *engine);

/**
 * Clear event recorder for a ControlSource.
 * @param engine The engine.
 * @param src The ControlSource with the EventRecorder to clear.
 * @note This function does nothing if the ControlSource doesn't have an EventRecorder attached.
 * @relates GE_Engine
 */
void GE_Engine_ClearEventRecorderForControlSource(GE_Engine *engine, GE_ControlSource *src);

/**
 * Update the recorded control sources.
 * @param engine The engine.
 */
void GE_Engine_UpdateRecordedControlSources(GE_Engine *engine);

/**
 * Dump debug info (text) to stream.
 * @param engine The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_Engine
 */
void GE_Engine_DebugDump(const GE_Engine *engine, FILE *f, unsigned int indent);

/**
 * This function can be used to run a callback when @ref GE_OnZero.count
 * reaches zero.
 * @param onDone Pointer to GE_OnZero struct.
 * @see GE_OnZero
 * @see GE_OnZeroCallback
 * @relates GE_Engine
 */
void GE_Engine_OnZeroDecrement(GE_OnZero *onDone);

/**
 * Get user managed title from the engine.
 * @param engine The engine to get the title from.
 * @returns The title, can be empty but never NULL.
 * @relates GE_Engine
 */
const char* GE_Engine_GetTitle(const GE_Engine *engine);

/**
 * Set user managed title for the engine.
 * @param engine The engine to set the title for.
 * @param title The title or NULL for no title.
 * @relates GE_Engine
 */
void GE_Engine_SetTitle(GE_Engine *engine, const char *title);

/**
 * Get the sync struct.
 * @param engine The engine.
 * @returns The sync struct pointer or NULL for none.
 * @relates GE_Engine
 */
GE_EngineSync* GE_Engine_GetSync(const GE_Engine *engine);

/**
 * Set the sync struct.
 * @param engine The engine.
 * @param sync A pointer to the sync struct.
 * @relates GE_Engine
 */
void GE_Engine_SetSync(GE_Engine *engine, GE_EngineSync *sync);

/**
 * Get the user info for a control source in the engine.
 * @param engine The engine.
 * @param cursorIndex The cursor index or a negative value get info for the engine sources.
 * @param sourceIndex The index of the control source.
 * @param maxNum The maximum number of users to return.
 * @param[out] retUsers Write user structs here, pass NULL to get the total number of users.
 * @returns The number of returned users or the total number of users if @p retUsers is NULL.
 * @note If @p retUsers is NULL @p maxNum will be ignored.
 */
int GE_Engine_GetControlSourceUsers(const GE_Engine *engine, int cursorIndex, int sourceIndex, int maxNum, GE_ControlSourceUser *retUsers);

/**
 * @brief Get a component from an engine.
 * @param engine The engine.
 * @param object Any valid object except @ref GE_EVENT_OBJECT_ENGINE.
 * @param index The index of the component to get.
 * @returns A pointer to the component or NULL if @p object or @p index are invalid.
 */
GE_PUBLIC GE_Component* GE_Engine_GetComponent(const GE_Engine *engine, GE_EventObject object, int index);

/**
 * @brief Get a GE_ParamTable from a component in the engine.
 * @param engine The engine.
 * @param object Any valid object except @ref GE_EVENT_OBJECT_ENGINE.
 * @param index The index of the component to get.
 * @param err Optional error code.
 * @returns A pointer to the GE_ParamTable or NULL if @p object or @p index are invalid.
 */
GE_PUBLIC const GE_ParamTable* GE_Engine_GetParamTableForEventObject(const GE_Engine *engine, GE_EventObject object, int index, int *err);

/**
 * @brief Get a GE_ParamInfo from a component in the engine.
 * @param engine The engine.
 * @param object Any valid object except @ref GE_EVENT_OBJECT_ENGINE.
 * @param index The index of the component to get.
 * @param paramName The parameter name.
 * @param err Optional error code.
 * @returns A pointer to the GE_ParamInfo or NULL if @p object or @p index are invalid.
 */
const GE_ParamInfo* GE_Engine_GetParamInfo(const GE_Engine *engine, GE_EventObject object, int index, const char* paramName, int *err);

/**
 * @brief Get a GE_ParamInfo from a component in the engine.
 * @param engine The engine.
 * @param object Any valid object except @ref GE_EVENT_OBJECT_ENGINE.
 * @param index The index of the component to get.
 * @param paramIndex The parameter index.
 * @param err Optional error code.
 * @returns A pointer to the GE_ParamInfo or NULL if @p object or @p index are invalid.
 */
const GE_ParamInfo* GE_Engine_GetParamInfoAt(const GE_Engine *engine, GE_EventObject object, int index, int paramIndex, int *err);

//
// Engine sync functions.
//

/**
 * Create a GE_EngineSync struct to be used for external synchronization.
 * @note The GE_EngineSync pointer can be used to synchronize several engines.
 * @note The returned sync has a reference count of zero.
 * @relates GE_EngineSync
 */
GE_EngineSync* GE_EngineSync_New(void);

/**
 * Free engine sync.
 * @param sync The sync to free.
 * @relates GE_EngineSync
 */
void GE_EngineSync_Free(GE_EngineSync *sync);

/**
 * Synchronize data for realtime thread (calling render).
 * @param sync The engine sync.
 * @relates GE_EngineSync
 * @note This operation uses a memory barrier (ACQUIRE).
 */
void GE_EngineSync_SyncRead(GE_EngineSync *sync);

/**
 * Synchronize data for main thread (the one controller the GE_EngineSync data).
 * @param sync The engine sync.
 * @relates GE_EngineSync
 * @note This operation uses a memory barrier (RELEASE).
 */
void GE_EngineSync_SyncWrite(GE_EngineSync *sync);

/**
 * Get the engine sync playing flag.
 * @param sync The engine sync.
 * @returns True if the engine is playing.
 * @relates GE_EngineSync
 */
bool GE_EngineSync_GetPlaying(const GE_EngineSync *sync);

/**
 * Set the engine sync playing flag.
 * @param sync The engine sync.
 * @param playing True if the engine is playing.
 * @relates GE_EngineSync
 */
void GE_EngineSync_SetPlaying(GE_EngineSync *sync, bool playing);

/**
 * Get the engine sync beat.
 * @param sync The engine sync.
 * @returns The beat.
 * @relates GE_EngineSync
 */
double GE_EngineSync_GetBeat(const GE_EngineSync *sync);

/**
 * Set the engine sync beat.
 * @param sync The engine sync.
 * @param beat The beat.
 * @relates GE_EngineSync
 */
void GE_EngineSync_SetBeat(GE_EngineSync *sync, double beat);

/**
 * Get the engine sync tempo.
 * @param sync The engine sync.
 * @returns The tempo.
 * @relates GE_EngineSync
 */
double GE_EngineSync_GetTempo(const GE_EngineSync *sync);

/**
 * Set the engine sync tempo.
 * @param sync The engine sync.
 * @param tempo The tempo.
 * @relates GE_EngineSync
 */
void GE_EngineSync_SetTempo(GE_EngineSync *sync, double tempo);

/**
 * Get the engine sync phase.
 * @param sync The engine sync.
 * @returns The phase.
 * @relates GE_EngineSync
 */
double GE_EngineSync_GetPhase(const GE_EngineSync *sync);

/**
 * Set the engine sync phase.
 * @param sync The engine sync.
 * @param phase The phase.
 * @relates GE_EngineSync
 */
void GE_EngineSync_SetPhase(GE_EngineSync *sync, double phase);

#ifdef __cplusplus
}
#endif

#endif /* GE_ENGINE_H */
