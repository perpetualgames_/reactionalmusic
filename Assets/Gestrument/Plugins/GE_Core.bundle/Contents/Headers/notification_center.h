/**
* @file notification_center.h
* @brief GE_NotificationCenter implementation
* @author Jonatan Liljedahl
* @author David Granström
* @copyright Copyright © 2018 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef NOTIFICATIONCENTER_H
#define NOTIFICATIONCENTER_H

#include "GE_Core/common.h"

typedef struct GE_NotificationCenter GE_NotificationCenter;
typedef void (* GE_NotificationCenterCallback)(GE_NotificationCenter *center, void *sender, void *userData);

typedef struct GE_NotificationCenterObserver {
    GE_NotificationCenterCallback callback;
    void *userData;
    struct GE_NotificationCenterObserver *next;
} GE_NotificationCenterObserver;

struct GE_NotificationCenter {
    GE_NotificationCenterObserver *observersHead;
};

#ifdef __cplusplus
extern "C" {
#endif

void GE_NotificationCenter_Init(GE_NotificationCenter *center);
GE_NotificationCenterObserver* GE_NotificationCenter_AddObserver(GE_NotificationCenter *center, GE_NotificationCenterCallback callback, void *userData);
void GE_NotificationCenter_Emit(GE_NotificationCenter *center, void *sender);

void GE_NotificationCenter_GetAll(GE_NotificationCenter *center, GE_NotificationCenterObserver **list);

void GE_NotificationCenter_RemoveFirst(GE_NotificationCenter *center);
void GE_NotificationCenter_RemoveLast(GE_NotificationCenter *center);
void GE_NotificationCenter_RemoveObserver(GE_NotificationCenter *center, GE_NotificationCenterObserver *observer);
void GE_NotificationCenter_RemoveAll(GE_NotificationCenter *center);
void GE_NotificationCenter_RemoveObserversWithCallback(GE_NotificationCenter *center, GE_NotificationCenterCallback callback);
int GE_NotificationCenter_Count(GE_NotificationCenter *center);

#ifdef __cplusplus
}
#endif

#endif /* NOTIFICATIONCENTER_H */
