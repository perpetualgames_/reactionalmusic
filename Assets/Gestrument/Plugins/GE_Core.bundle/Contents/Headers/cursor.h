/**
 * @file cursor.h
 * @brief A representation of a control point on a surface.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 * 
 * Cursors are used as the main way of interaction in the Gestrument Pro iOS
 * app. They are very well suited for mappings to a UI similiar to of what
 * Gestrument Pro offers. A GE_Cursor is created with a set of GE_ControlSources
 * that can be mapped to GE_ControlInputs.
 *
 * @see GE_Cursor_FindControlSource for more information.
 *
 * The Engine can be configured with a set of cursors using GE_EngineConfig, or
 * they can be appended to the engine using GE_Engine_AppendCursor. The cursors
 * are mapped to Instruments and used to control properties of pulse and pitch
 * generators of the Instrument. One cursor can control several instruments.
 *
 */

#ifndef CURSOR_H
#define CURSOR_H

#include "GE_Core/common.h"
#include "GE_Core/component.h"
#include "GE_Core/control_source.h"
#include "GE_Core/utils/constants.h"
#include "GE_Core/utils/variant.h"

/**
 * Enumerated ControlSource indices.
 */
typedef enum GE_CursorControlSource {

    /**
     * X (horizontal).
     */
    GE_CURSOR_CONTROL_SOURCE_X,

    /**
     * Y (vertical).
     */
    GE_CURSOR_CONTROL_SOURCE_Y,

    /**
     * Z (depth).
     */
    GE_CURSOR_CONTROL_SOURCE_Z,

    /**
     * Pressure.
     */
    GE_CURSOR_CONTROL_SOURCE_PRESSURE,

    /**
     * Radius/pressure.
     */
    GE_CURSOR_CONTROL_SOURCE_GATE,

    /**
     * Speed.
     */
    GE_CURSOR_CONTROL_SOURCE_SPEED,

    /**
     * Max enum.
     **/
    MAX_GE_CURSOR_CONTROL_SOURCE
} GE_CursorControlSource;

/**
 * A cursor used by the engine.
 */
typedef struct GE_Cursor {

    /**
     * Component setup, polymorphic struct.
     */
    GE_Component component;

    /**
     * A user managed title.
     */
    char *title;

    /**
     * The index of the cursor in it's engine array.
     */
    int index;

    /**
     * Arbitrary data.
     */
    void *appData;

    /**
     * Map control sources to an array.
     */
    GE_ControlSource *controlSources[MAX_GE_CURSOR_CONTROL_SOURCE];

    /**
     * For velocity calculation.
     * Internal use only.
     */
    float last_x;

    /**
     * For velocity calculation.
     * Internal use only.
     */
    float last_y;

    /**
     * Internal use only.
     */
    bool touched, hold, locked;
} GE_Cursor;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new cursor.
 * @param index The cursor's index in the engine cursor array.
 * @returns A new cursor.
 * @note Used internally by GE_Engine.
 */
GE_Cursor* GE_Cursor_New(int index);

/**
 * Free a cursor.
 * @param cursor The cursor.
 * @note Used internally by GE_Engine, do not free cursors that belong to an engine.
 * @relates GE_Cursor
 */
void GE_Cursor_Free(GE_Cursor *cursor);

/**
 * Get the index of the cursor.
 * @param cursor The cursor.
 * @returns The index.
 * @relates GE_Cursor
 */
int GE_Cursor_GetIndex(const GE_Cursor *cursor);

/**
 * Reset the cursor to initial values.
 * @param cursor The cursor.
 * @relates GE_Cursor
 */
void GE_Cursor_Reset(GE_Cursor *cursor);

/**
 * Get horizontal position of cursor.
 * @param cursor The cursor.
 * @returns A unit value (0-1) representing the horizontal position of the cursor.
 * @relates GE_Cursor
 */
double GE_Cursor_GetX(const GE_Cursor *cursor);

/**
 * Set the horizontal position of cursor.
 * @param cursor The cursor.
 * @param value A unit value (0-1) representing the horizontal position of the cursor.
 * @relates GE_Cursor
 */
void GE_Cursor_SetX(const GE_Cursor *cursor, double value);

/**
 * Get vertical position of cursor.
 * @param cursor The cursor.
 * @returns A unit value (0-1) representing the vertical position of the cursor.
 * @relates GE_Cursor
 */
double GE_Cursor_GetY(const GE_Cursor *cursor);

/**
 * Set the vertical position of cursor.
 * @param cursor The cursor.
 * @param value A unit value (0-1) representing the vertical position of the cursor.
 * @relates GE_Cursor
 */
void GE_Cursor_SetY(const GE_Cursor *cursor, double value);

/**
 * Get depth (z position) of cursor.
 * @param cursor The cursor.
 * @returns A unit value (0-1) representing the vertical position of the cursor.
 * @relates GE_Cursor
 */
double GE_Cursor_GetZ(const GE_Cursor *cursor);

/**
 * Set the depth (z position) of cursor.
 * @param cursor The cursor.
 * @param value A unit value (0-1) representing the vertical position of the cursor.
 * @relates GE_Cursor
 */
void GE_Cursor_SetZ(const GE_Cursor *cursor, double value);

/**
 * Get gate state of cursor.
 * @param cursor The cursor.
 * @returns True if the gate is open or false if it's closed.
 * @relates GE_Cursor
 */
bool GE_Cursor_GetGate(const GE_Cursor *cursor);

/**
 * Set gate state of cursor.
 * @param cursor The cursor.
 * @param value True for open and false for closed.
 * @relates GE_Cursor
 */
void GE_Cursor_SetGate(const GE_Cursor *cursor, bool value);

/**
 * Get pressure state of cursor.
 * @param cursor The cursor.
 * @returns A unit value (0-1) representing the pressure of the cursor.
 * @relates GE_Cursor
 */
double GE_Cursor_GetPressure(const GE_Cursor *cursor);

/**
 * Set pressure state of cursor.
 * @param cursor The cursor.
 * @param value A unit value (0-1) representing the pressure of the cursor.
 * @relates GE_Cursor
 */
void GE_Cursor_SetPressure(const GE_Cursor *cursor, double value);

/**
 * Get speed state of cursor.
 * @param cursor The cursor.
 * @returns A unit value (0-1) representing the speed of the cursor.
 * @relates GE_Cursor
 */
double GE_Cursor_GetSpeed(const GE_Cursor *cursor);

/**
 * Set speed state of cursor.
 * @param cursor The cursor.
 * @param value A unit value (0-1) representing the speed of the cursor.
 * @relates GE_Cursor
 */
void GE_Cursor_SetSpeed(const GE_Cursor *cursor, double value);

/**
 * Get the number of control sources.
 * @param cursor The cursor.
 * @returns The number of control sources the cursor has.
 * @relates GE_Cursor
 */
int GE_Cursor_GetNumControlSources(const GE_Cursor *cursor);

/**
 * Get a control source from the cursor.
 * @param cursor The cursor.
 * @param index Index of the control source (use @ref GE_CursorControlSource, see note)
 * @returns A pointer to the control source or NULL if @p cursorControlSource is invalid.
 * @note When the string based control source API is implemented the enum will be deprecated.
 * @relates GE_Cursor
 */
GE_ControlSource* GE_Cursor_GetControlSource(const GE_Cursor *cursor, int index);

/**
 * Find a control source index by searching for it's title.
 * @param cursor The cursor.
 * @param title The title to find for.
 * @returns The control source or NULL if @p title was not found.
 * @relates GE_Cursor
 */
int GE_Cursor_FindControlSource(const GE_Cursor *cursor, const char *title);

/**
 * Return the current state in a dict variant.
 * @param cursor The cursor.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will never return NULL (even though the variant may be an empty dict).
 *
 * Used to save states.
 * @relates GE_Cursor
 */
GE_Variant* GE_Cursor_GetState(const GE_Cursor *cursor);

/**
 * Set the current cursor state using a dict variant.
 * @param cursor The cursor.
 * @param state A dict variant with the state, pass NULL to clear cursor.
 * @returns True if the state was set, false on failure.
 *
 * Used to load states.
 * @relates GE_Cursor
 */
bool GE_Cursor_SetState(GE_Cursor *cursor, const GE_Variant *state);

/**
 * Dump debug info (text) to stream.
 * @param cursor The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_Cursor
 */
void GE_Cursor_DebugDump(const GE_Cursor *cursor, FILE *f, unsigned int indent);

/**
 * Get user managed title from the cursor.
 * @param cursor The cursor to get the title from.
 * @returns The title, can be empty but never NULL.
 */
const char* GE_Cursor_GetTitle(const GE_Cursor *cursor);

/**
 * Set user managed title for the cursor.
 * @param cursor The cursor to set the title for.
 * @param title The title or NULL for no title.
 */
void GE_Cursor_SetTitle(GE_Cursor *cursor, const char *title);

#ifdef __cplusplus
}
#endif

#endif /* CURSOR_H */
