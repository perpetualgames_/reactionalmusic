/**
 * @file param.h
 * @brief A typed parameter
 * @author David Granström
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * Implicit parameter value conversion:
 *
 * - Boolean, integer, float and non-string enum values are all easily converted
 * as they would be in normal C.
 * - Conversions to/from strings may cause a call to snprintf.
 */

#ifndef GE_PARAM_H
#define GE_PARAM_H

#include <stdarg.h>

#include "GE_Core/common.h"

/**
 * @brief The default number of buckets.
 */
#define GE_PARAM_TABLE_DEFAULT_BUCKETS 1024

/**
 * @brief Generic param table error.
 */
#define GE_ERROR_PARAM_TABLE -1

/**
 * @brief Param type error.
 */
#define GE_ERROR_PARAM_TABLE_TYPE -2

/**
 * @brief Param duplicate name error.
 */
#define GE_ERROR_PARAM_TABLE_DUP -3

/**
 * @brief The operation does not exist or is not implemented.
 */
#define GE_ERROR_PARAM_TABLE_OP -4

/**
 * @brief The a value passed to the operation was invalid.
 */
#define GE_ERROR_PARAM_TABLE_VALUE -5

/**
 * @brief Macro to check for type validity.
 * @param type_ The type to check.
 * @returns True or false.
 */
#define GE_PARAM_TYPE_VALID(type_) ((type_) > GE_PARAM_TYPE_NONE && (type_) < MAX_GE_PARAM_TYPE)

/**
 * @brief Helper macro to define a param info literal.
 * @param name_ The name of the parameter.
 * @param rtSafe_ True if the parameter is realtime safe to operate on.
 * @param description_ A short human readable description.
 * @param setter_ The setter callback function.
 * @param getter_ The getter callback function.
 * @param def_ The default value.
 */
#define GE_PARAM_DEF_BOOLEAN(name_, rtSafe_, description_, setter_, getter_, def_) {.type=GE_PARAM_TYPE_BOOLEAN, name_, (rtSafe_), (description_), {.pBoolean={.setter=(setter_), .getter=(getter_), .def=(def_)}}}

/**
 * @brief Helper macro to define a param info literal.
 * @param name_ The name of the parameter.
 * @param rtSafe_ True if the parameter is realtime safe to operate on.
 * @param description_ A short human readable description.
 * @param setter_ The setter callback function.
 * @param getter_ The getter callback function.
 * @param def_ The default value.
 * @param min_ The minimum value.
 * @param max_ The maximum value.
 * @note If @p min_ is greater than @p max_ the thing will break!
 */
#define GE_PARAM_DEF_INTEGER(name_, rtSafe_, description_, setter_, getter_, def_, min_, max_) {.type=GE_PARAM_TYPE_INTEGER, name_, (rtSafe_), (description_), {.pInteger={.setter=(setter_), .getter=(getter_), .def=(def_), .max=(max_), .min=(min_)}}}

/**
 * @brief Helper macro to define a param info literal.
 * @param name_ The name of the parameter.
 * @param rtSafe_ True if the parameter is realtime safe to operate on.
 * @param description_ A short human readable description.
 * @param setter_ The setter callback function.
 * @param getter_ The getter callback function.
 * @param def_ The default value.
 * @param min_ The minimum value.
 * @param max_ The maximum value.
 * @note If @p min_ is greater than @p max_ the thing will break!
 */
#define GE_PARAM_DEF_FLOAT(name_, rtSafe_, description_, setter_, getter_, def_, min_, max_) {.type=GE_PARAM_TYPE_FLOAT, name_, (rtSafe_), (description_), {.pFloat={.setter=(setter_), .getter=(getter_), .def=(def_), .max=(max_), .min=(min_)}}}

/**
 * @brief Helper macro to define a param info literal.
 * @param name_ The name of the parameter.
 * @param rtSafe_ True if the parameter is realtime safe to operate on.
 * @param description_ A short human readable description.
 * @param setter_ The setter callback function.
 * @param getter_ The getter callback function.
 * @param def_ The default value.
 * @param max_ The maximum length of the string (excluding the zero terminator).
 * @note If @p min_ is greater than @p max_ the thing will break!
 */
#define GE_PARAM_DEF_STRING(name_, rtSafe_, description_, setter_, getter_, def_, max_) {.type=GE_PARAM_TYPE_STRING, name_, (rtSafe_), (description_), {.pString={.setter=(setter_), .getter=(getter_), .def=(def_), .maxLength=(max_)}}}

/**
 * @brief Helper macro to define a param info literal.
 * @param name_ The name of the parameter.
 * @param rtSafe_ True if the parameter is realtime safe to operate on.
 * @param description_ A short human readable description.
 * @param setter_ The setter callback function.
 * @param getter_ The getter callback function.
 * @param def_ The default value.
 * @param num_ The number of string enumerations.
 * @param max_ The maximum value.
 * @note If @p min_ is greater than @p max_ the thing will break!
 */
#define GE_PARAM_DEF_ENUM(name_, rtSafe_, description_, setter_, getter_, def_, num_, ...) {.type=GE_PARAM_TYPE_ENUM, name_, (rtSafe_), (description_), {.pEnum={.setter=(setter_), .getter=(getter_), .def=(def_), .num=(num_), .strings=(char *[]){__VA_ARGS__}}}}

/**
 * @brief Helper macro to define a param info literal.
 * @param name_ The name of the parameter.
 * @param table_ The subtable.
 * @param paramIndex_ The parameter index in the subtable.
 */
#define GE_PARAM_DEF_TABLE(name_, table_, paramIndex_) {.type=GE_PARAM_TYPE_TABLE, name_, false, "", {.pTable={.table=(table_), .paramIndex=(paramIndex_)}}}

/**
 * @brief Helper macro to define a param info literal.
 */
#define GE_PARAM_DEF_SENTINEL() {0}

/**
 * @brief Helper macro to get the size of a parameter list.
 */
#define GE_PARAM_GET_LENGTH(params_) (sizeof((params_)) / sizeof(GE_ParamInfo))

/**
 * @see struct GE_ParamInfo
 */
typedef struct GE_ParamInfo GE_ParamInfo;

/**
 * @see struct GE_ParamTable
 */
typedef struct GE_ParamTable GE_ParamTable;

/**
 * @see struct GE_ParamTableItem
 */
typedef struct GE_ParamTableItem GE_ParamTableItem;

/**
 * @brief The available parameter types.
 */
typedef enum {

    /**
     * @brief No type / invalid.
     */
    GE_PARAM_TYPE_NONE,

    /**
     * @brief Boolean.
     */
    GE_PARAM_TYPE_BOOLEAN,

    /**
     * @brief Signed 64-bit integer.
     */
    GE_PARAM_TYPE_INTEGER,

    /**
     * @brief 64-bit floating point.
     */
    GE_PARAM_TYPE_FLOAT,

    /**
     * @brief A single string.
     */
    GE_PARAM_TYPE_STRING,

    /**
     * @brief Enumerated value.
     */
    GE_PARAM_TYPE_ENUM,

    /**
     * @brief Use subtable.
     */
    GE_PARAM_TYPE_TABLE,

    /**
     * @brief Max enum.
     */
    MAX_GE_PARAM_TYPE

} GE_ParamType;

/**
 * @brief Param value union.
 * @note String values are not part of this union and are handled separately!
 */
typedef union {

    /**
     * @brief Boolean value.
     * @ref GE_PARAM_TYPE_BOOLEAN
     */
    bool vBoolean;

    /**
     * @brief Integer value.
     * @ref GE_PARAM_TYPE_INTEGER
     */
    int64_t vInteger;

    /**
     * @brief Float value.
     * @ref GE_PARAM_TYPE_FLOAT
     */
    double vFloat;

    /**
     * @brief Enum value.
     * @ref GE_PARAM_TYPE_ENUM
     */
    int vEnum;
 
} GE_ParamValue;

/**
 * @brief A parameter info.
 */
struct GE_ParamInfo {

    /**
     * @brief The param type.
     */
    GE_ParamType type;

    /**
     * @brief The name, including a final zero terminator.
     * @note The name will always be padded with zeroes even if it's
     * terminated before the last char.
     */
    char *name;

    /**
     * @brief True if setting the parameter is realtime-safe.
     */
    bool realtimeSafe;

    /**
     * @brief Parameter description.
     */
    char *description;

    /**
     * @brief Type-specific data.
     */
    union {

        /**
         * @brief Boolean.
         */
        struct {

            /**
             * @brief The setter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param value The value.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*setter)(GE_ParamTable *table, int paramIndex, bool value);

            /**
             * @brief The getter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param[out] retValue On success, write the value here.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*getter)(const GE_ParamTable *table, int paramIndex, bool *retValue);

            /**
             * @brief The default value of the parameter.
             */
            bool def;

        } pBoolean;

        /**
         * @brief Signed 64-bit integer.
         */
        struct {

            /**
             * @brief The setter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param value The value.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*setter)(GE_ParamTable *table, int paramIndex, int64_t value);

            /**
             * @brief The getter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param[out] retValue On success, write the value here.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*getter)(const GE_ParamTable *table, int paramIndex, int64_t *retValue);

            /**
             * @brief The maximum value of the parameter.
             */
            int64_t max;

            /**
             * @brief The minimum value of the parameter.
             */
            int64_t min;

            /**
             * @brief The default value of the parameter.
             */
            int64_t def;

        } pInteger;

        /**
         * @brief 64-bit floating point.
         */
        struct {

            /**
             * @brief The setter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param value The value.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*setter)(GE_ParamTable *table, int paramIndex, double value);

            /**
             * @brief The getter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param[out] retValue On success, write the value here.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*getter)(const GE_ParamTable *table, int paramIndex, double *retValue);

            /**
             * @brief The maximum value of the parameter.
             */
            double max;

            /**
             * @brief The minimum value of the parameter.
             */
            double min;

            /**
             * @brief The default value of the parameter.
             */
            double def;

        } pFloat;

        /**
         * @brief String.
         */
        struct {

            /**
             * @brief The setter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param value The value.
             * @returns 0 on success or a negative error code on failure.
             * @note The GE_ParamInfo.maxLength setting is NOT enforced (unlike the min/max of other types)!
             */
            int (*setter)(GE_ParamTable *table, int paramIndex, const char *value);

            /**
             * @brief The getter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param size The maximum number of bytes to write to @p retValue, including the zero terminator.
             * @param[out] retValue On success, write the value here.
             * @returns The length of the new string (excluding zero terminator) on success or a negative error code on failure.
             * @note Implementations must support receiving a NULL @p retValue in which case @p size is ignored
             * and the length, if successful, is returned without actually writing the string.
             */
            int (*getter)(const GE_ParamTable *table, int paramIndex, size_t size, char *retValue);

            /**
             * @brief The default value of the parameter.
             */
            const char *def;

            /**
             * @brief The maximum length of the string, excluding zero terminator.
             * @note A size of zero disables the max length check.
             */
            size_t maxLength;

        } pString;

        /**
         * @brief Enumeration.
         */
        struct {

            /**
             * @brief The setter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param value The value.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*setter)(GE_ParamTable *table, int paramIndex, int value);

            /**
             * @brief The getter callback.
             * @param table The table.
             * @param paramIndex The index of the parameter.
             * @param[out] retValue On success, write the value here.
             * @returns 0 on success or a negative error code on failure.
             */
            int (*getter)(const GE_ParamTable *table, int paramIndex, int *retValue);

            /**
             * @brief The default value of the parameter.
             */
            int def;

            /**
             * @brief The number of enumerations the parameter has.
             */
            int num;

            /**
             * @brief An array of enumerated strings.
             */
            char **strings;

        } pEnum;

        /**
         * @brief Table.
         */
        struct
        {

            /**
             * @brief The subtable.
             */
            GE_ParamTable *table;

            /**
             * @brief The index of the parameter.
             */
            int paramIndex;

        } pTable;

    } p;

};

/**
 * @brief Used to associate dynamic data with a parameter
 * and keep track of where in the hash map it is.
 */
struct GE_ParamTableItem
{

    /**
     * @brief The bucket index of the parameter.
     */
    int bucketIndex;

    /**
     * @brief The item index of the parameter.
     */
    int itemIndex;

    /**
     * @brief The parameter info.
     */
    GE_ParamInfo paramInfo;

    /**
     * @brief User set title.
     */
    char *title;

};

/**
 * @brief A parameter table.
 */
struct GE_ParamTable {

    /**
     * @brief The number of buckets.
     */
    int numBuckets;

    /**
     * @brief The buckets.
     */
    struct
    {

        /**
         * @brief The number of item indices in the bucket.
         */
        int length;

        /**
         * @brief Item indices.
         */
        int *indices;

    } *buckets;

    /**
     * @brief Total length (number of items) in the table.
     */
    int length;

    /**
     * @brief An array of table items.
     */
    GE_ParamTableItem *items;

    /**
     * @brief Arbitrary data to associate with the parameter.
     */
    void *userData;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Get a string version of the param type.
 * @param type The param type.
 * @returns A pointer to the param type string or a pointer to an empty string if @p type is invalid.
 */
const char *GE_ParamType_GetString(GE_ParamType type);

/**
 * @brief Check if get/set operations are realtime safe for parameter and value type. 
 * @param param The param to test.
 * @param type The value type to test.
 * @returns true if param is real time safe otherwise false.
 */
bool GE_Param_IsRTSafe(const GE_ParamInfo *param, GE_ParamType type);

/**
 * @brief Initialize a param table.
 * @param table The table struct to initialize.
 * @param numBuckets The number of buckets or <= 0 for @ref GE_PARAM_TABLE_DEFAULT_BUCKETS.
 * @param numParams The number of parameters to expect in @p params or a negative value to scan for a sentinel.
 * @param params The parameters, if @p numParams is negative it will be counted until an invalid type sentinel is found.
 * @returns The number of added parameters on success or a negative error code on failure.
 *
 * Error codes:
 *
 *  - @ref GE_PARAM_TABLE_ERROR_DUP if there is a duplicate parameter name.
 *  - @ref GE_PARAM_TABLE_ERROR_TYPE if a parameter has an invalid type.
 */
int GE_ParamTable_Init(GE_ParamTable *table, int numBuckets, int numParams, const GE_ParamInfo *params);

/**
 * @brief Deinitialize a param table.
 * @param table The table to deinitialize.
 */
void GE_ParamTable_Deinit(GE_ParamTable *table);

/**
 * @brief Get the number of parameters the table has.
 * @param table The table.
 * @returns The number of parameters in the table.
 */
GE_PUBLIC int GE_ParamTable_GetLength(const GE_ParamTable *table);

/**
 * @brief Add more parameters to the table.
 * @param table The table.
 * @param numParams The number of parameters to expect in @p params or a negative value to scan for a sentinel.
 * @param params The parameters, if @p numParams is negative it will be counted until an invalid type sentinel is found.
 * @returns The number of added parameters on success or a negative error code on failure.
 *
 * Error codes:
 *
 *  - @ref GE_PARAM_TABLE_ERROR_DUP if there is a duplicate parameter name.
 *  - @ref GE_PARAM_TABLE_ERROR_TYPE if a parameter has an invalid type.
 */
int GE_ParamTable_AddParams(GE_ParamTable *table, int numParams, const GE_ParamInfo *params);

/**
 * @brief Count and return the number of collisions in the table.
 * @param table The table.
 * @returns The number of collisions.
 */
int GE_ParamTable_GetNumCollisions(const GE_ParamTable *table);

/**
 * @brief Find the index for a parameter by name.
 * @param table The table.
 * @param name The name of the parameter.
 * @returns The parameter index on success or @ref GE_PARAM_TABLE_ERROR if @p name was not found.
 */
GE_PUBLIC int GE_ParamTable_FindParamIndex(const GE_ParamTable *table, const char *name);

/**
 * @brief Get parameter info by name.
 * @param table The table.
 * @param name The name of the parameter.
 * @returns A pointer to the parameter info or NULL if @p name was not found.
 */
GE_PUBLIC const GE_ParamInfo *GE_ParamTable_GetParam(const GE_ParamTable *table, const char *name);

/**
 * @brief Get parameter info by index.
 * @param table The table.
 * @param index The parameter index.
 * @returns A pointer to the parameter info or NULL if @p index was out of range.
 */
GE_PUBLIC const GE_ParamInfo *GE_ParamTable_GetParamAt(const GE_ParamTable *table, int index);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetBoolean(GE_ParamTable *table, const char *name, bool value);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetBooleanAt(GE_ParamTable *table, int index, bool value);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetBoolean(const GE_ParamTable *table, const char *name, bool *retValue);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetBooleanAt(const GE_ParamTable *table, int index, bool *retValue);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetInteger(GE_ParamTable *table, const char *name, int64_t value);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetIntegerAt(GE_ParamTable *table, int index, int64_t value);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetInteger(const GE_ParamTable *table, const char *name, int64_t *retValue);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetIntegerAt(const GE_ParamTable *table, int index, int64_t *retValue);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetFloat(GE_ParamTable *table, const char *name, double value);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetFloatAt(GE_ParamTable *table, int index, double value);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetFloat(const GE_ParamTable *table, const char *name, double *retValue);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetFloatAt(const GE_ParamTable *table, int index, double *retValue);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The value to set.
 * @returns The length of the set string (excluding zero terminator  on success,
 * @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetString(GE_ParamTable *table, const char *name, const char *value);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The value to set.
 * @returns The length of the set string on success,
 * @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetStringAt(GE_ParamTable *table, int index, const char *value);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param format The format of the value to set.
 * @param args The arguments to format.
 * @returns The length of the set string (excluding zero terminator  on success,
 * @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_SetStringVA(GE_ParamTable *table, const char *name, const char *format, va_list args);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param format The format of the value to set.
 * @param args The arguments to format.
 * @returns The length of the set string on success,
 * @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_SetStringAtVA(GE_ParamTable *table, int index, const char *format, va_list args);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param format The format of the value to set.
 * @param ... The arguments to format.
 * @returns The length of the set string (excluding zero terminator  on success,
 * @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_SetStringV(GE_ParamTable *table, const char *name, const char *format, ...) GE_PRINTF_FORMAT(3, 4);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param format The format of the value to set.
 * @param ... The arguments to format.
 * @returns The length of the set string on success,
 * @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_SetStringAtV(GE_ParamTable *table, int index, const char *format, ...)  GE_PRINTF_FORMAT(3, 4);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param size The maximum number of bytes to write to @p retValue, including the zero terminator.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns String length (excluding zero terminator) on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found.
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetString(const GE_ParamTable *table, const char *name, size_t size, char *retValue);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param size The maximum number of bytes to write to @p retValue, including the zero terminator.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns String length (excluding zero terminator) on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found.
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetStringAt(const GE_ParamTable *table, int index, size_t size, char *retValue);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetEnum(GE_ParamTable *table, const char *name, int value);

/**
 * @brief Set the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetEnumAt(GE_ParamTable *table, int index, int value);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetEnum(const GE_ParamTable *table, const char *name, int *retValue);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetEnumAt(const GE_ParamTable *table, int index, int *retValue);

/**
 * @brief Set a normalized value (0-1) for the parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The normalized value to set.
 * @returns 0 on success, @ref GE_ERROR_PARAM_TABLE if @p name was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetNormalized(GE_ParamTable *table, const char *name, double value);

/**
 * @brief Set a normalized value (0-1) for the parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The normalized value to set.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_SetNormalizedAt(GE_ParamTable *table, int index, double value);

/**
 * @brief Get a normalized value (0-1) for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param[out] retValue On success, write the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetNormalized(const GE_ParamTable *table, const char *name, double *retValue);

/**
 * @brief Get a normalized value (0-1) for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param[out] retValue On success, write the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
GE_PUBLIC int GE_ParamTable_GetNormalizedAt(const GE_ParamTable *table, int index, double *retValue);

/**
 * @brief Set the title for a parameter.
 * @param table The table.
 * @param name The name of the parameter.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_ERROR_PARAM_TABLE if @p name was not found.
 */
GE_PUBLIC int GE_ParamTable_SetTitle(GE_ParamTable *table, const char *name, const char *title);

/**
 * @brief Set the title for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param value The value to set.
 * @returns 0 on success, @ref GE_ERROR_PARAM_TABLE if @p index was out of range.
 */
GE_PUBLIC int GE_ParamTable_SetTitleAt(GE_ParamTable *table, int index, const char *title);

/**
 * @brief Get the title for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @returns A pointer to the title string or NULL if @p name was not found.
 */
GE_PUBLIC const char *GE_ParamTable_GetTitle(const GE_ParamTable *table, const char *name);

/**
 * @brief Get the title for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @returns A pointer to the title string or NULL if @p index was out of range.
 */
GE_PUBLIC const char *GE_ParamTable_GetTitleAt(const GE_ParamTable *table, int index);

/**
 * @brief Reset a parameter to it's default value.
 * @param table The table.
 * @param name The parameter name.
 * @returns 0 on success or @ref GE_PARAM_ERROR_TABLE if @p name was not found.
 */
GE_PUBLIC int GE_ParamTable_ResetDefault(GE_ParamTable *table, const char *name);

/**
 * @brief Reset a parameter to it's default value.
 * @param table The table.
 * @param name The parameter name.
 * @returns 0 on success or @ref GE_PARAM_ERROR_TABLE if @p name was not found.
 */
GE_PUBLIC int GE_ParamTable_ResetDefaultAt(GE_ParamTable *table, int index);

/**
 * @brief Reset all parameters to their defaults.
 * @param table The table.
 */
GE_PUBLIC void GE_ParamTable_ResetDefaults(GE_ParamTable *table);

/**
 * @brief Set value.
 * @param table The table.
 * @param name The name of the parameter.
 * @param type The value type.
 * @param value The value.
 * @returns 0 on success, @ref GE_ERROR_PARAM_TABLE if @p name was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_SetValue(GE_ParamTable *table, const char *name, GE_ParamType type, GE_ParamValue value);

/**
 * @brief Set value.
 * @param table The table.
 * @param index The index of the parameter.
 * @param type The value type.
 * @param value The value.
 * @returns 0 on success, @ref GE_ERROR_PARAM_TABLE if @p index was not found 
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_SetValueAt(GE_ParamTable *table, int index, GE_ParamType type, GE_ParamValue value);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param name The parameter name.
 * @param[out] retType On success, copy the type here, NULL to omit.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p name was not found
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_GetValue(const GE_ParamTable *table, const char *name, GE_ParamType *retType, GE_ParamValue *retValue);

/**
 * @brief Get the value for a parameter.
 * @param table The table.
 * @param index The parameter index.
 * @param[out] retType On success, copy the type here, NULL to omit.
 * @param[out] retValue On success, copy the value here, NULL to omit.
 * @returns 0 on success, @ref GE_PARAM_TABLE_ERROR if @p index is out of range
 * or @ref GE_PARAM_TABLE_ERROR_TYPE if the type is incompatible with the parameter.
 */
int GE_ParamTable_GetValueAt(const GE_ParamTable *table, int index, GE_ParamType *retType, GE_ParamValue *retValue);

/**
 * @brief Dump the table.
 * @param table The table.
 * @param f The stream to dump to.
 * @param indent Number of spaces to indent.
 */
GE_PUBLIC void GE_ParamTable_DebugDump(GE_ParamTable *table, FILE *f, int indent);

//
// Helpers to get info from the ParamInfo.
//

/**
 * @brief Get the name from parameter info.
 * @param info The info.
 * @returns The name of the info.
 */
GE_PUBLIC const char *GE_ParamInfo_GetName(const GE_ParamInfo *info);

/**
 * @brief Get the description from parameter info.
 * @param info The info.
 * @returns The description of the info.
 */
GE_PUBLIC const char *GE_ParamInfo_GetDescription(const GE_ParamInfo *info);

/**
 * @brief Get the type from parameter info.
 * @param info The info.
 * @returns The type of the info.
 */
GE_PUBLIC GE_ParamType GE_ParamInfo_GetType(const GE_ParamInfo *info);

/**
 * @brief Get realtime safe flag from parameter info.
 * @param info The info.
 * @returns The realtime safe flag.
 */
GE_PUBLIC bool GE_ParamInfo_GetRealtimeSafe(const GE_ParamInfo *info);

#ifdef __cplusplus
}
#endif

#endif /* GE_PARAM_H */
