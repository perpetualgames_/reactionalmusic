/**
 * @file seq_pulse_gen.h
 * @brief GE_SeqPulseGen
 * @author Peter Gebauer
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_SEQ_PULSE_GEN_H
#define GE_SEQ_PULSE_GEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/pulse_gen.h"
#include "GE_Core/utils/sequencer.h"
#include "GE_Core/utils/constants.h"

#define GE_SEQ_PULSE_GEN_MAX_VOICES 128
#define GE_SEQ_PULSE_GEN_MAX_GATE_EVENTS 1024


/**
 * @see struct GE_SeqPulseGenType
 */
typedef struct GE_SeqPulseGenType GE_SeqPulseGenType;

/**
 * @see struct GE_SeqPulseGen
 */
typedef struct GE_SeqPulseGen GE_SeqPulseGen;

/**
 * A SeqPulseGenType.
 */
struct GE_SeqPulseGenType {
    GE_PulseGenType super;
};

/**
 * @brief Pulses need voices.
 */
typedef struct GE_PulseVoice {
    bool active;
    double beat;
    double duration;
    double startBeat;
    double endBeat;
    float onVelocity;
    float offVelocity;
    int index;
} GE_PulseVoice;

struct GE_SeqPulseGen {

    /**
     * Inherited struct.
     */
    GE_PulseGen super;

    /**
     * The sequencer.
     */
    GE_Sequencer *sequencer;

    /**
     * @brief Keep track of active gates.
     */
    GE_PulseVoice voices[GE_SEQ_PULSE_GEN_MAX_VOICES];

    /**
     * @brief Keep track of active gates.
     */
    GE_GateEvent gateEvents[GE_SEQ_PULSE_GEN_MAX_GATE_EVENTS];

    /**
     * @brief Number of gate events.
     */
    int numGateEvents;

    /**
     * @brief Loop, yes or no, sir?
     */
    bool loop;

    /**
     * @brief Add this to currentIndex for each new gate event. Can be negative.
     * @note If this is zero then absolute indexing is assumed and currentIndex is ignored.
     * @note The play rate control input can flip the sign of the step if the rate is negative.
     */
    int indexStepLength;

    /**
     * @brief The current pulse index if not playing by delta beat.
     */
    int currentPulseIndex;

    /**
     * @brief The path to the last opened MIDI file.
     */
    char *midiPath;

    /**
     * @brief The velocity control input overrides the sequencer velocity.
     */
    bool velocityOverride;

    /**
     * @brief Control input for legato probability.
     */
    GE_ControlInput *control_legato;

    /**
     * @brief Control input for playback rate.
     */
    GE_ControlInput *control_rate;

    /**
     * @brief Control input velocity multiplier or override.
     * @see If velocityOverride is true this is an override, otherwise a multiplier.
     */
    GE_ControlInput *control_velocity;

    /**
     * @brief Control input quantize.
     */
    GE_ControlInput *control_quant;

    /**
     * @brief Interal use only.
     */
    bool didTrig;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * A GE_SeqPulseGenType.
 */
extern GE_PUBLIC const GE_SeqPulseGenType GE_SEQ_PULSE_GEN_TYPE;

/**
 * @brief Get the sequencer owned by the generator.
 * @param generator The sequence pulse generator.
 * @returns The sequencer owned by the generator, DO NOT DEINITIALIZE.
 */
GE_Sequencer* GE_SeqPulseGen_GetSequencer(const GE_SeqPulseGen *generator);

/**
 * @brief Clear all tracks in the sequencer.
 * @param generator The sequence pulse generator.
 * @note This will clear the last read MIDI path returned from GE_SeqPulseGen_GetMIDIPath().
 * FIXME: lock mutex
 */
void GE_SeqPulseGen_Clear(GE_SeqPulseGen *generator);

/**
 * @brief Read a MIDI file (clears all previous tracks in the sequencer).
 * @param generator The sequence pulse generator.
 * @param path The path to the MIDI file.
 * @returns Zero on success or a negative error code on failure.
 * @note If the call fails the sequencer will be cleared along with the MIDI path returned from GE_SeqPulseGen_GetMIDIPath().
 * FIXME: lock mutex
 */
int GE_SeqPulseGen_ReadMIDIPath(GE_SeqPulseGen *generator, const char *path);

/**
 * @brief Get the name of the last read MIDI file.
 * @param generator The sequence pulse generator.
 * @returns The path to the last read MIDI file or NULL if no file was read.
 */
const char* GE_SeqPulseGen_GetMIDIPath(const GE_SeqPulseGen *generator);

/**
 * @brief Get the number of events.
 * @param generator The sequence pulse generator.
 * @returns The number of events.
 */
int GE_SeqPulseGen_GetNumEvents(const GE_SeqPulseGen *generator);

/**
 * @brief Get the number of tracks.
 * @param generator The sequence pulse generator.
 * @returns The number of tracks.
 */
int GE_SeqPulseGen_GetNumTracks(const GE_SeqPulseGen *generator);

/**
 * @brief Get the name of a track in the generator's sequencer.
 * @param generator The sequence pulse generator.
 * @param trackIndex The index of the track to get the name for.
 * @returns A track name or NULL if @p trackIndex is invalid.
 */
const char* GE_SeqPulseGen_GetTrackName(const GE_SeqPulseGen *generator, int trackIndex);

/**
 * @brief Check if the track is enabled.
 * @param generator The sequence pulse generator.
 * @param trackIndex The index of the track to check.
 * @returns True if the track is enabled, false if not or if @p trackIndex is invalid.
 */
bool GE_SeqPulseGen_GetIsTrackEnabled(const GE_SeqPulseGen *generator, int trackIndex);

/**
 * @brief Set if the track is enabled.
 * @param generator The sequence pulse generator.
 * @param trackIndex The index of the track to check.
 * @param enabled True to enable, false to disable.
 * @returns True if the track state was set, false if @p trackIndex is invalid.
 */
void GE_SeqPulseGen_SetIsTrackEnabled(GE_SeqPulseGen *generator, int trackIndex, bool enabled);

/**
 * @brief Check if the sequencer is looped.
 * @param generator The sequence pulse generator.
 * @returns True if the sequencer is looped, false if not.
 */
bool GE_SeqPulseGen_GetLoop(const GE_SeqPulseGen *generator);

/**
 * @brief Set the loop flag.
 * @param generator The sequence pulse generator.
 * @param loop True if the sequencer should be loop, false if not.
 */
void GE_SeqPulseGen_SetLoop(GE_SeqPulseGen *generator, bool loop);

/**
 * @brief Get the current beat.
 * @param generator The sequence pulse generator.
 * @returns The current beat.
 */
double GE_SeqPulseGen_GetCurrentBeat(const GE_SeqPulseGen *generator);

/**
 * @brief Get the current beat.
 * @param generator The sequence pulse generator.
 * @param beat The new current beat.
 */
void GE_SeqPulseGen_SetCurrentBeat(GE_SeqPulseGen *generator, double beat);

/**
 * @brief Reset the generator's states. This will not generate any gate off events.
 * @param generator The sequence pulse generator.
 */
void GE_SeqPulseGen_Reset(GE_SeqPulseGen *generator);

/**
 * @brief Get the index step length (i.e step current index by this many).
 * @returns The index step length, if zero that means absolute indices are used.
 */
int GE_SeqPulseGen_GetIndexStepLength(const GE_SeqPulseGen *generator);

/**
 * @brief Get the index step length (i.e step current index by this many).
 * @param generator The sequence pulse generator.
 * @param indexStepLength The index step length, if zero that means absolute indices are used.
 */
void GE_SeqPulseGen_SetIndexStepLength(GE_SeqPulseGen *generator, int indexStepLength);

/**
 * @brief Get the flag for control input velocity override.
 * @returns If true the control input "velocity" overrides velocity from the sequencer.
 * If false the control input is a multiplier.
 */
int GE_SeqPulseGen_GetVelocityOverride(const GE_SeqPulseGen *generator);

/**
 * @brief Set the flag for control input velocity override.
 * @param generator The sequence pulse generator.
 * @param velocityOverride If true the control input "velocity" overrides velocity from the sequencer.
 * If false the control input is a multiplier.
 */
void GE_SeqPulseGen_SetVelocityOverride(GE_SeqPulseGen *generator, bool velocityOverride);

#ifdef __cplusplus
}
#endif

#endif /* SEQ_PULSE_GEN_H */
