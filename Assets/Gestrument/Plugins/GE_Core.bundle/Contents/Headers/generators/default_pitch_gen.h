/**
 * @file default_pitch_gen.h
 * @brief Generate pitches from a scale.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 * 
 * GE_DefaultPitchGen produces pitch values according to the scale pointed to by
 * GE_Engine currentScaleIndex, or a by using a localScale exclusive to the
 * instance of the GE_DefaultPitchGen.
 *
 * @see GE_ScalePitchGen for information about localScale.
 */

#ifndef GE_DEFAULT_PITCH_GEN_H
#define GE_DEFAULT_PITCH_GEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/scale_pitch_gen.h"

/**
 * @see struct GE_DefaultPitchGenType
 */
typedef struct GE_DefaultPitchGenType GE_DefaultPitchGenType;

/**
 * @see struct GE_DefaultPitchGen
 */
typedef struct GE_DefaultPitchGen GE_DefaultPitchGen;

/**
 * GE_DefaultPitchGenType.
 */
struct GE_DefaultPitchGenType {
    GE_ScalePitchGenType super;
};

/**
 * GE_DefaultPitchGen.
 */
struct GE_DefaultPitchGen {

    /**
     * @brief Polymorphic extension of @ref GE_ScalePitchGen.
     */
    GE_ScalePitchGen super;

    /**
     * @brief Force a new note when transpose changes.
     */
    bool forceTransTrig;

    /**
     * @brief Round transpose to nearest integral value.
     */
    bool transQuant;

    /**
     * Internal use only.
     */
    int lastIndex;

    /**
     * Internal use only.
     */
    int transIndex;

    /**
     * Internal use only.
     */
    float transPitch;
    
    /**
     * @brief Change chromatic transposition.
     */
    GE_ControlInput *control_ctranspose;

    /**
     * @brief Change modal transposition.
     */
    GE_ControlInput *control_mtranspose;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The GE_PitchGenType type.
 */
extern GE_PUBLIC const GE_DefaultPitchGenType GE_DEFAULT_PITCH_GEN_TYPE;

/**     
 * Get transpose quantize.
 * @param generator Pointer to a GE_DefaultPitchGen.
 * @returns Transpose quantize flag.
 */
bool GE_DefaultPitchGen_GetTransQuant(const GE_DefaultPitchGen *generator);

/**
 * Set transpose quantize.
 * This will round chromatic transposition to nearest integral value.
 * @param generator Pointer to a GE_DefaultPitchGen.
 * @param transQuant Transpose quantize flag.
 */
void GE_DefaultPitchGen_SetTransQuant(GE_DefaultPitchGen *generator, bool transQuant);

/**
 * Get transpose force trig.
 * Force a new note when transpose changes.
 * @param generator Pointer to a GE_DefaultPitchGen.
 * @returns Force transpose trig flag.
 */
bool GE_DefaultPitchGen_GetForceTransTrig(const GE_DefaultPitchGen *generator);

/**
 * Set transpose force trig.
 * @param generator Pointer to a GE_DefaultPitchGen.
 * @param transTrig Force transpose trig flag.
 */
void GE_DefaultPitchGen_SetForceTransTrig(GE_DefaultPitchGen *generator, bool transTrig);

#ifdef __cplusplus
}
#endif

#endif /* GE_DEFAULT_PITCH_GEN_H */
