/**
 * @file scale_pitch_gen.h
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_SCALE_PITCH_GEN_H
#define GE_SCALE_PITCH_GEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/pitch_gen.h"
#include "GE_Core/scale.h"
#include "GE_Core/utils/constants.h"

/**
 * @see struct GE_ScalePitchGenType
 */
typedef struct GE_ScalePitchGenType GE_ScalePitchGenType;

/**
 * @see struct GE_ScalePitchGen
 */
typedef struct GE_ScalePitchGen GE_ScalePitchGen;

/**
 * @brief A type for all pitch generators using scales.
 */
struct GE_ScalePitchGenType {

    /**
     * @brief Polymorphic extension of @ref GE_PitchGenType.
     */
    GE_PitchGenType super;

};

/**
 * @brief A pitch generator using scales.
 */
struct GE_ScalePitchGen {

    /**
     * @brief Polymorphic extension of @ref GE_PitchGen.
     */
    GE_PitchGen super;

    /**
     * @brief Optional local scale.
     * This scale can be used to override the current global scale.
     */
    GE_Scale *localScale;

    /**
     * @brief Raw scales.
     * @note Internal use only.
     * The calculated scale degrees as floating point MIDI notes.
     */
    GE_RawScale *rawScales;

    /**
     * @brief Minimum pitch the generator can produce.
     */
    float minPitch;

    /**
     * @brief Maximum pitch the generator can produce.
     */
    float maxPitch;

    /**
     * @brief Use local scale.
     * Set this flag to override the current global scale.
     * @see GE_ScalePitchGen.localScale
     */
    bool useLocalScale;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief A scale pitch generator type.
 */
extern GE_PUBLIC const GE_ScalePitchGenType GE_SCALE_PITCH_GEN_TYPE;

/**
 * Updates the GE_RawScale representation of the current scale slots.
 * @param generator The generator.
 * @see GE_Scale_MakeRawScale
 * @relates GE_ScalePitchGen
 */
void GE_ScalePitchGen_UpdateScales(GE_ScalePitchGen *generator);

/**
 * Return the scale currently used by this generator.
 * @param generator The generator.
 * @return A GE_Scale
 * @relates GE_ScalePitchGen
 */
GE_Scale* GE_ScalePitchGen_GetCurrentScale(GE_ScalePitchGen *generator);

/**
 * Return the raw scale currently used by this generator.
 * @param generator The generator.
 * @return A GE_RawScale
 * @relates GE_ScalePitchGen
 */
GE_RawScale* GE_ScalePitchGen_GetCurrentRawScale(GE_ScalePitchGen *generator);

/**
 * Set the minimum pitch for the current scale.
 * @param generator The generator.
 * @param minPitch The minimum (lowest) pitch.
 */
void GE_ScalePitchGen_SetMinPitch(GE_ScalePitchGen *generator, float minPitch);

/**
 * Get the minimum pitch for the current scale.
 * @param generator The generator.
 * @return The minimum (lowest) pitch.
 */
float GE_ScalePitchGen_GetMinPitch(GE_ScalePitchGen *generator);

/**
 * Set the maximum pitch for the current scale.
 * @param generator The generator.
 * @param maxPitch The maximum (highest) pitch.
 */
void GE_ScalePitchGen_SetMaxPitch(GE_ScalePitchGen *generator, float maxPitch);

/**
 * Get the maximum pitch for the current scale.
 * @param generator The generator.
 * @return The maximum (highest) pitch.
 */
float GE_ScalePitchGen_GetMaxPitch(GE_ScalePitchGen *generator);

/**
 * Set the minimum and maximum pitch.
 * @param generator The generator.
 * @param minPitch The minimum pitch.
 * @param maximum The maximum (highest) pitch.
 */
void GE_ScalePitchGen_SetRange(GE_ScalePitchGen *generator, float minPitch, float maxPitch);

/**
 * Get the minimum and maximum pitch.
 * @param generator The generator.
 * @param[out] minPitch The minimum pitch.
 * @param[out] maximum The maximum (highest) pitch.
 */
void GE_ScalePitchGen_GetRange(const GE_ScalePitchGen *generator, float *minPitch, float *maxPitch);

/**
 * Use the local scale for this generator.
 * @param generator The generator.
 * @param useLocalScale if true use the local scale, if false use the global scale slots.
 */
void GE_ScalePitchGen_UseLocalScale(GE_ScalePitchGen *generator, bool useLocalScale);

/**
 * See if this generator is using its local scale or not.
 * @param generator The generator.
 * @return true if local scale is used otherwise false.
 */
bool GE_ScalePitchGen_IsUsingLocalScale(GE_ScalePitchGen *generator);

/**
 * Set the local scale for this generator.
 * Use GE_ScalePitchGen_UseLocalScale to enable.
 * @param generator The generator.
 * @param localScale A GE_DefaultScale to be used as the local scale.
 */
void GE_ScalePitchGen_SetLocalScale(GE_ScalePitchGen *generator, GE_DefaultScale localScale);

/**
 * Get the local scale for this generator.
 * Use GE_ScalePitchGen_UseLocalScale to enable.
 * @param generator Pointer to a GE_ScalePitchGen.
 * @return The local scale.
 */
GE_Scale* GE_ScalePitchGen_GetLocalScale(GE_ScalePitchGen *generator);

/**
 * Map a midi note number to a scale degree.
 * @param generator The generator.
 * @param midinote The midi note to map. 
 * @returns A scale degree. If the midi note is not found -1 is returned.
 */
int GE_ScalePitchGen_NoteToDegree(const GE_ScalePitchGen *generator, float midinote);

#ifdef __cplusplus
}
#endif

#endif /* SCALE_PITCHGEN_H */
