/**
 * @file chord_pitch_gen.h
 * @brief A generator which produces chords.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 * 
 * GE_ChordPitchGen, in contrast to GE_DefaultPitchGen, generates chords instead of single pitches.
 *
 * Control inputs:
 *      - Transpose absolute (+/- 12) semitones.
 */

#ifndef GE_CHORD_PITCHGEN_H
#define GE_CHORD_PITCHGEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/pitch_gen.h"
#include "GE_Core/chord.h"
#include "GE_Core/utils/constants.h"
#include "GE_Core/utils/array.h"

#define kMaxChords 32

/**
 * @see struct GE_RawChord
 */
typedef struct GE_RawChord GE_RawChord;

/**
 * @see struct GE_ChordListNode
 */
typedef struct GE_ChordListNode GE_ChordListNode;

/**
 * @see struct GE_ChordPitchGenType
 */
typedef struct GE_ChordPitchGenType GE_ChordPitchGenType;

/**
 * @see struct GE_ChordPitchGen
 */
typedef struct GE_ChordPitchGen GE_ChordPitchGen;

/**
 * Calculated raw pitches.
 * @note Internal use only.
 */
struct GE_RawChord {
    float pitches[GE_MAX_VOICES];
    int numPitches;
};

struct GE_ChordPitchGenType {
    GE_PitchGenType super;
};

/**
 * Plays chords.
 */
struct GE_ChordPitchGen {
    /**
     * @brief Polymorphic extension of @ref GE_PitchGen.
     */
    GE_PitchGen super;
    int lastIndex;
    int numChords;
    GE_RawChord chordData[kMaxChords];
    GE_Array chords;
    float transPitch;
    bool forceTransTrig;
    bool transQuant;

    /**
     * @brief Change chromatic transposition.
     */
    GE_ControlInput *transpose;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief A chord generator type.
 */
extern GE_PUBLIC const GE_ChordPitchGenType GE_CHORD_PITCH_GEN_TYPE;

/**
 * Append a new chord to the generator.
 * @param obj Pointer to a GE_ChordPitchGen
 * @param chord The chord to append.
 * @relates GE_ChordPitchGen
 */
void GE_ChordPitchGen_AppendChord(GE_ChordPitchGen *obj, GE_Chord *chord);

#ifdef __cplusplus
}
#endif

#endif /* GE_CHORD_PITCHGEN_H */
