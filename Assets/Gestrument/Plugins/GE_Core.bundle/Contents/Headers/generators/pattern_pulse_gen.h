/**
 * @file pattern_pulse_gen.h
 * @brief Generate pulses from a pattern
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * GE_PatternPulseGen can be used as a step sequencer. @see GE_PatternStep for
 * information about the properties of a step. A maximum of 64 steps can be
 * defined.
 */

#ifndef GE_PATTERN_PULSEGEN_H
#define GE_PATTERN_PULSEGEN_H

#include <stdbool.h>
#include "GE_Core/generators/dur_pulse_gen.h"

#define kMaxPatternSteps 64

/**
 * @see struct GE_PatternStep
 */
typedef struct GE_PatternStep GE_PatternStep;

/**
 * @see struct GE_PatternPulseGenType
 */
typedef struct GE_PatternPulseGenType GE_PatternPulseGenType;

/**
 * @see struct GE_PatternPulseGen
 */
typedef struct GE_PatternPulseGen GE_PatternPulseGen;

/**
 * @brief Defines a single step in the pattern.
 */
struct GE_PatternStep {

    /**
     * The probability of the step triggering a pulse or not.
     */
    float probability;

    /**
     * Step velocity.
     */
    float velocity;

    /**
     * Should the step be a tie to the previous step.
     */
    bool tie;

};

/**
 * @brief Currently simply extends dur pulse generator type.
 */
struct GE_PatternPulseGenType
{

    /**
     * @brief Polymorphic extension.
     */
    GE_DurPulseGenType super;

};

/**
 * @brief Steps through patterns of pulses.
 */
struct GE_PatternPulseGen
{

    /**
     * @brief Polymorphic extension.
     */
    GE_DurPulseGen super;

    /**
     * @brief The pattern steps.
     */
    GE_PatternStep steps[kMaxPatternSteps];

    /**
     * @brief FIXME
     */
    GE_PatternStep *step;

    /**
     * @brief The number of steps.
     */
    int numSteps;

    /**
     * @brief The current step.
     */
    int currentStep;

    /**
     * @brief FIXME
     */
    bool resetOnGate;

    /**
     * @brief Store the last phase to detect change.
     */
    double lastPhase;

    /**
     * @brief Store the last duration to detect change.
     */
    double lastDur;

    /**
     * @brief Velocity.
     */
    GE_ControlInput *control_velocity;

    /**
     * @brief FIXME
     */
    GE_ControlInput *control_probtilt;

    /**
     * @brief Legato probability.
     */
    GE_ControlInput *control_legatoprob;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The pattern pulse generator type.
 */
extern GE_PUBLIC const GE_PatternPulseGenType GE_PATTERN_PULSE_GEN_TYPE;

/**
 * Get pointer to steps array.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param[out] numSteps The number of active steps.
 * @return Pointer to first element in steps array.
 * @relates GE_PatternPulseGen
 */
GE_PatternStep* GE_PatternPulseGen_GetSteps(GE_PatternPulseGen *generator, int *numSteps);

/**
 * @brief Set the pattern sequence.
 * @param generator Pointer to GE_PatternPulseGen.
 * @param steps Array of @ref GE_PatternStep 
 * @param numSteps Number of steps in @p steps.
 * @relates GE_PatternPulseGen
 */
void GE_PatternPulseGen_SetSteps(GE_PatternPulseGen *generator, const GE_PatternStep *steps, int numSteps);

/**
 * Clear the current pattern sequence, but keep the current sequence length.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @relates GE_PatternPulseGen
 */
void GE_PatternPulseGen_ClearSteps(GE_PatternPulseGen *generator);

/**
 * Set the probability for step at index.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param index Step index.
 * @param value Probability value.
 * @relates GE_PatternPulseGen
 */
void GE_PatternPulseGen_SetStepProbability(GE_PatternPulseGen *generator, int index, float value);

/**
 * Set the velocity for step at index.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param index Step index.
 * @param value Velocity value.
 * @relates GE_PatternPulseGen
 */
void GE_PatternPulseGen_SetStepVelocity(GE_PatternPulseGen *generator, int index, float value);

/**
 * Set tie for step at index.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param index Step index.
 * @param tie Tie value.
 * @relates GE_PatternPulseGen
 */
void GE_PatternPulseGen_SetStepTie(GE_PatternPulseGen *generator, int index, bool tie);

/**
 * Get the probability for step at index.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param index Step index.
 * @return The probability or -1 if index is out of range.
 * @relates GE_PatternPulseGen
 */
float GE_PatternPulseGen_GetStepProbability(GE_PatternPulseGen *generator, int index);

/**
 * Get the velocity for step at index.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param index Step index.
 * @return The velocity or -1 if index is out of range.
 * @relates GE_PatternPulseGen
 */
float GE_PatternPulseGen_GetStepVelocity(GE_PatternPulseGen *generator, int index);

/**
 * Get tie for step at index.
 *
 * @param generator Pointer to GE_PatternPulseGen.
 * @param index Step index.
 * @return The tie value or -1 if index is out of range.
 * @relates GE_PatternPulseGen
 */
int GE_PatternPulseGen_GetStepTie(GE_PatternPulseGen *generator, int index);

/**
 * Set resetOnGate flag. The pattern will be restarted if
 * this generator's main control gate is triggered.
 * @param generator Pointer to GE_PatternPulseGen.
 * @param value Reset flag value.
 * @relates GE_PatternPulseGen
 */
void GE_PatternPulseGen_SetResetOnGate(GE_PatternPulseGen *generator, bool value);

/**
 * Get the resetOnGate flag value.
 * @see GE_PatternPulseGen_SetResetOnGate for more information.
 * @param generator Pointer to GE_PatternPulseGen.
 * @returns Value of the resetOnGate flag.
 * @relates GE_PatternPulseGen
 */
bool GE_PatternPulseGen_GetResetOnGate(GE_PatternPulseGen *generator);

#ifdef __cplusplus
}
#endif

#endif /* GE_PATTERN_PULSEGEN_H */
