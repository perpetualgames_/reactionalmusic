/**
 * @file rel_pitch_gen.h
 * @brief Generate pitches relative to a scale.
 * @author David Granström
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 * 
 * GE_RelPitchGen produces pitch values according to the scale pointed to by
 * GE_Engine currentScaleIndex, or a by using a localScale exclusive to the
 * instance of the GE_RelPitchGen.
 *
 * @see GE_ScalePitchGen for information about localScale.
 */

#ifndef GE_RELATIVE_CHORD_PITCH_GEN_H
#define GE_RELATIVE_CHORD_PITCH_GEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/scale_pitch_gen.h"

#define GE_MAX_DEGREES 128

/**
 * @see struct GE_RelPitchGenType
 */
typedef struct GE_RelPitchGenType GE_RelPitchGenType;

/**
 * @see struct GE_RelPitchGen
 */
typedef struct GE_RelPitchGen GE_RelPitchGen;

/**
 * @see struct GE_RawDegree
 */
typedef struct GE_RawDegrees GE_RawDegrees;

/**
 * GE_RelPitchGenType.
 */
struct GE_RelPitchGenType {
    GE_ScalePitchGenType super;
};

/**
 * Container for degrees.
 * @note For internal use only.
 */
struct GE_RawDegrees
{
    float degrees[GE_MAX_DEGREES];
    size_t length;
};

/**
 * GE_RelPitchGen.
 */
struct GE_RelPitchGen {

    /**
     * @brief Polymorphic extension of @ref GE_ScalePitchGen.
     */
    GE_ScalePitchGen super;

    /**
     * Container for degrees.
     * @note For internal use only.
     */
    GE_RawDegrees degreeData[GE_MAX_DEGREES];

    /**
     * Number of indexable degrees.
     * @note For internal use only.
     */
    int numDegreeData;

    /**
     * @brief Force a new note when transpose changes.
     */
    bool forceTransTrig;

    /**
     * @brief Round transpose to nearest integral value.
     */
    bool ctransposeQuant;
    
    /**
     * @brief Change chromatic transposition.
     */
    GE_ControlInput *control_ctranspose;

    /**
     * @brief Change modal transposition.
     */
    GE_ControlInput *control_mtranspose;

    /**
     * @brief Set the current degrees index.
     * The range of this input is 0 - @ref GE_MAX_DEGREES - 1.
     */
    GE_ControlInput *control_degrees_index;

    /**
     * @brief Set inversion of degrees.
     * Values +/- degrees size will be clamped.
     * @ref GE_RelPitchGen_GetDegrees
     */
    GE_ControlInput *control_inversion;

    /**
     * @brief Set to true to use absolute degrees indexing (bypass main ctrl input).
     */
    bool absoluteIndexing;

    /**
     * @brief Internal use only.
     */
    int degreesLastIndex;
    double mtranspose;
    double ctranspose;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The GE_PitchGenType type.
 */
extern const GE_RelPitchGenType GE_REL_PITCH_GEN_TYPE;

/**
 * Append degrees to the end of the degree array.
 * @param generator The pitchgen.
 * @param degrees Array of scale degrees.
 * @param length The length of @p degrees
 * @note The decimal part of a @p degrees value will chromatically transpose
 * the scale degreee. For instance a chord such as {0,1.9,4} played in a
 * major scale will lower the third to a minor third.
 * @returns True if success, false if @ref GE_MAX_DEGREES has been reached.
 */
bool GE_RelPitchGen_AppendDegrees(GE_RelPitchGen *generator, const float *degrees, size_t length);

/**
 * Remove degrees from the end of the degree array.
 * @param generator The pitchgen.
 * @param n The number of degrees to remove. Negative values will remove all degrees.
 */
void GE_RelPitchGen_RemoveDegrees(GE_RelPitchGen *generator, int n);

/**
 * Insert/Modify degrees at index.
 * @note In contrast to @ref GE_RelPitchGen_AppendDegrees this function will
 * *not* work with the default control input. Use @ref absoluteIndexing to
 * choose the degrees instead.
 * @param generator The pitchgen.
 * @param index The index in which to place the degrees (will overwrite if any degrees are already stored at index).
 * @param degrees Array of scale degrees.
 * @param length The length of @p degrees
 */
bool GE_RelPitchGen_InsertDegrees(GE_RelPitchGen *generator, int index, const float *degrees, size_t length);

/**
 * Delete degrees at index.
 * @note In contrast to @ref GE_RelPitchGen_RemoveDegrees this function will
 * *not* work with the default control input. Use @ref absoluteIndexing to
 * choose the degrees instead.
 * @param generator The pitchgen.
 * @param index The degrees index to delete.
 */
void GE_RelPitchGen_DeleteDegrees(GE_RelPitchGen *generator, int index);

/**
 * Get the size of degrees array.
 * @param generator The pitchgen.
 * @returns The size of the degrees array.
 */
size_t GE_RelPitchGen_GetDegreesSize(const GE_RelPitchGen *generator);

/**
 * Get the size of degrees array.
 * @param generator The pitchgen.
 * @param index The index to get.
 * @param[out] degrees The degrees to get.
 * @param maxLength Maximum size to get.
 * @returns The size of the degrees at index.
 */
size_t GE_RelPitchGen_GetDegrees(const GE_RelPitchGen *generator, int index, float *degrees, size_t maxLength);

/**
 * Set quantization for ctranspose.
 * Rounds to nearest integral value.
 * @param generator The sequence pitch generator.
 * @param quant True enables quantization and off disables.
 */
void GE_RelPitchGen_SetTransposeQuant(GE_RelPitchGen *generator, bool quant);

/**
 * Get quantization for ctranspose.
 * Rounds to nearest integral value.
 * @param generator The sequence pitch generator.
 * @returns True if quantization is enables or false if disabled.
 */
bool GE_RelPitchGen_GetTransposeQuant(const GE_RelPitchGen *generator);

/**
 * Set transpose force trig.
 * @param generator The generator.
 * @param force Force transpose trig flag.
 */
void GE_RelPitchGen_SetForceTransTrig(GE_RelPitchGen *generator, bool force);

/**
 * Get transpose force trig.
 * Force a new note when transpose changes.
 * @param generator The generator.
 * @returns Force transpose trig flag.
 */
bool GE_RelPitchGen_GetForceTransTrig(const GE_RelPitchGen *generator);

/**
 * Set absolute indexing mode.
 * @param generator The generator.
 * @param absoluteIndexing True means that @ref control_degrees_index will be used instead of the main ctrl input.
 */
void GE_RelPitchGen_SetAbsoluteIndexing(GE_RelPitchGen *generator, bool absoluteIndexing);

/**
 * Get absolute indexing mode.
 * @param generator The generator.
 * @returns The absoluteIndexing mode.
 */
bool GE_RelPitchGen_GetAbsoluteIndexing(const GE_RelPitchGen *generator);

#ifdef __cplusplus
}
#endif

#endif /* GE_RELATIVE_CHORD_PITCH_GEN_H */
