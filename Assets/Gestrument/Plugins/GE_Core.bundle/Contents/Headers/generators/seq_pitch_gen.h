/**
 * @file seq_pitch_gen.h
 * @brief GE_SeqPitchGen
 * @author David Granström
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_SEQ_PITCH_GEN_H
#define GE_SEQ_PITCH_GEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/scale_pitch_gen.h"
#include "GE_Core/utils/sequencer.h"
#include "GE_Core/utils/constants.h"

/**
 * @see struct GE_SeqPitchGenType
 */
typedef struct GE_SeqPitchGenType GE_SeqPitchGenType;

/**
 * @see struct GE_SeqPitchGen
 */
typedef struct GE_SeqPitchGen GE_SeqPitchGen;

/**
 * A SeqPitchGenType.
 */
struct GE_SeqPitchGenType {
    GE_ScalePitchGenType super;
};

struct GE_SeqPitchGen {

    /**
     * Inherited struct.
     */
    GE_ScalePitchGen super;

    /**
     * The sequencer.
     */
    GE_Sequencer *sequencer;

    /**
     * @brief Loop.
     */
    bool loop;

    /**
     * @brief The path to the last opened MIDI file.
     */
    char *midiPath;

    /**
     * Chromatic transposition.
     */
    GE_ControlInput *control_ctranspose;

    /**
     * Modal transposition.
     */
    GE_ControlInput *control_mtranspose;

    /**
     * Round the value of ctranspose to integral numbers.
     */
    bool ctransposeQuant;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * A GE_SeqPitchGenType.
 */
extern GE_PUBLIC const GE_SeqPitchGenType GE_SEQ_PITCH_GEN_TYPE;

/**
 * @brief Get the sequencer owned by the generator.
 * @param generator The sequence pitch generator.
 * @returns The sequencer owned by the generator, DO NOT DEINITIALIZE.
 */
GE_Sequencer* GE_SeqPitchGen_GetSequencer(const GE_SeqPitchGen *generator);

/**
 * @brief Clear all tracks in the sequencer.
 * @param generator The sequence pitch generator.
 * FIXME: lock mutex
 */
void GE_SeqPitchGen_Clear(GE_SeqPitchGen *generator);

/**
 * @brief Read a MIDI file (clears all previous tracks in the sequencer).
 * @param generator The sequence pitch generator.
 * @param path The path to the MIDI file.
 * @returns Zero on success or a negative error code on failure.
 * FIXME: lock mutex
 */
int GE_SeqPitchGen_ReadMIDIPath(GE_SeqPitchGen *generator, const char *path);

/**
 * @brief Get the name of the last read MIDI file.
 * @param generator The sequence pitch generator.
 * @returns The path to the last read MIDI file or NULL if no file was read.
 */
const char* GE_SeqPitchGen_GetMIDIPath(const GE_SeqPitchGen *generator);

/**
 * @brief Get the number of events.
 * @param generator The sequence pitch generator.
 * @returns The number of events.
 */
int GE_SeqPitchGen_GetNumEvents(const GE_SeqPitchGen *generator);

/**
 * @brief Get the number of tracks.
 * @param generator The sequence pitch generator.
 * @returns The number of tracks.
 */
int GE_SeqPitchGen_GetNumTracks(const GE_SeqPitchGen *generator);

/**
 * @brief Check if the track is enabled.
 * @param generator The sequence pitch generator.
 * @param trackIndex The index of the track to check.
 * @returns True if the track is enabled, false if not or if @p trackIndex is invalid.
 */
bool GE_SeqPitchGen_GetIsTrackEnabled(const GE_SeqPitchGen *generator, int trackIndex);

/**
 * @brief Set if the track is enabled.
 * @param generator The sequence pitch generator.
 * @param trackIndex The index of the track to check.
 * @param enabled True to enable, false to disable.
 * @returns True if the track state was set, false if @p trackIndex is invalid.
 */
void GE_SeqPitchGen_SetIsTrackEnabled(GE_SeqPitchGen *generator, int trackIndex, bool enabled);

/**
 * @brief Check if the sequencer is looped.
 * @param generator The sequence pitch generator.
 * @returns True if the sequencer is looped, false if not.
 */
bool GE_SeqPitchGen_GetLoop(const GE_SeqPitchGen *generator);

/**
 * @brief Set the loop flag.
 * @param generator The sequence pitch generator.
 * @param loop True if the sequencer should be loop, false if not.
 */
void GE_SeqPitchGen_SetLoop(GE_SeqPitchGen *generator, bool loop);

/**
 * @brief Reset the generator's states. This will not generate any gate off events.
 * @param generator The sequence pitch generator.
 */
void GE_SeqPitchGen_Reset(GE_SeqPitchGen *generator);

/**
 * @brief Check if the sequencer is finished (has reached the end of the sequence).
 * @param generator The sequence pitch generator.
 * @returns True if the sequencer is finished or false if not.
 * @note If the SeqPitchGen has loop set to true this function always returns false.
 */
bool GE_SeqPitchGen_GetIsFinished(const GE_SeqPitchGen *generator);

/**
 * Set quantization for ctranspose.
 * Rounds to nearest integral value.
 * @param generator The sequence pitch generator.
 * @param quant True enables quantization and off disables.
 */
void GE_SeqPitchGen_SetTransposeQuant(GE_SeqPitchGen *generator, bool quant);

/**
 * Get quantization for ctranspose.
 * Rounds to nearest integral value.
 * @param generator The sequence pitch generator.
 * @returns True if quantization is enables or false if disabled.
 */
bool GE_SeqPitchGen_GetTransposeQuant(const GE_SeqPitchGen *generator);

#ifdef __cplusplus
}
#endif

#endif /* SEQ_PITCH_GEN_H */
