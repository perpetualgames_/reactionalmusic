/**
 * @file pitch_gen.h
 * @brief GE_PitchGenBase header
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * All pitch generators must extend this type.
 */

#ifndef GE_PITCH_GEN_H
#define GE_PITCH_GEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/generator.h"
#include "GE_Core/scale.h"

/**
 * @brief Selects how to index pitches.
 */
typedef enum
{

    /**
     * @brief The index does nothing.
     */
    GE_PITCH_INDEXING_NONE,

    /**
     * @brief The index is a sequence index.
     */
    GE_PITCH_INDEXING_SEQUENCE,

    /**
     * @brief The index is a pulse index.
     */
    GE_PITCH_INDEXING_PULSE,

    /**
     * @brief Max enum.
     */
    MAX_GE_PITCH_INDEXING

} GE_PitchIndexing;

/**
 * @see struct GE_PitchGenType
 */
typedef struct GE_PitchGenType GE_PitchGenType;

/**
 * @see struct GE_PitchGen
 */
typedef struct GE_PitchGen GE_PitchGen;

/**
 * @brief A type for all pitch generators.
 */
struct GE_PitchGenType {

    /**
     * @brief Polymorphic extension of @ref GE_GeneratorType.
     */
    GE_GeneratorType super;

    /**
     * @brief All pitch generators produce pitches.
     */
    const float* (*Process)(GE_PitchGen *generator, int *numPitches, bool legato, int index);

};

/**
 * @brief The base for all pitch generators.
 *
 * This may be considered abstract since the base pitch generator does nothing.
 */
struct GE_PitchGen {

    /**
     * @brief Polymorphic extension of @ref GE_Generator.
     */
    GE_Generator super;

    /**
     * @brief How pitches are indexed when process is called.
     */
    GE_PitchIndexing pitchIndexing;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief A pitch generator type.
 */
extern GE_PUBLIC const GE_PitchGenType GE_PITCH_GEN_TYPE;

/**
 * @brief Generate pitches from a sequence or pulse index.
 * @param generator The generator.
 * @param[out] numPitches Write the number of pitches, NULL to omit.
 * @param legato Legato state.
 * @param index The sequence or pulse index.
 */
const float* GE_PitchGen_Process(GE_PitchGen *generator, int *numPitches, bool legato, int index);

/**
 * @brief Set the pitch indexing.
 * @param generator The generator.
 * @param indexing The pitch indexing.
 */
void GE_PitchGen_SetPitchIndexing(GE_PitchGen *generator, GE_PitchIndexing indexing);

/**
 * @brief Get the pitch indexing.
 * @param generator The generator.
 * @returns The pitch indexing.
 */
GE_PitchIndexing GE_PitchGen_GetPitchIndexing(const GE_PitchGen *generator);

/**
 * @brief Set the range of the pitch generator.
 * @param generator The pitch generator.
 * @param minPitch The minimum pitch.
 * @param maxPitch The maximum (highest) pitch.
 * @returns True if successful, false if the pitch generator does not support
 * modifiying the pitch range.
 */
bool GE_PitchGen_SetRange(GE_PitchGen *generator, float minPitch, float maxPitch);

/**
 * @brief Get the range of the pitch generator.
 * @param generator The pitch generator.
 * @param[out] minPitch The minimum pitch.
 * @param[out] maxPitch The maximum (highest) pitch.
 * @returns True if successful, false if the pitch generator does not support
 * pitch range.
 */
bool GE_PitchGen_GetRange(const GE_PitchGen *generator, float *minPitch, float *maxPitch);

/**
 * @brief Helper function to convert the fraction of a transpose value to a semitone.
 * @param degree A degree.
 * @param[out] Integral part of degree.
 * @returns The decimal part of @p degree converted to a semitone.
 */
float GE_PitchGen_GetAccidental(float degree, float *outDegree);

#ifdef __cplusplus
}
#endif

#endif /* PITCHGEN_BASE_H */
