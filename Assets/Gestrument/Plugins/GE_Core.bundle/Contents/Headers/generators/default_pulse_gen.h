/**
 * @file default_pulse_gen.h
 * @brief A pulse generator.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 * 
 * GE_DefaultPulseGen produces pulses (gates) according to note durations.
 * @see GE_DurPulseGen for information about default durations.
 */

#ifndef PULSEGEN_H
#define PULSEGEN_H

#include "GE_Core/common.h"
#include "GE_Core/generators/dur_pulse_gen.h"
#include "GE_Core/control_input.h"

#define kMaxPatternSteps 64

/**
 * @see struct GE_DefaultPulseGenType
 */
typedef struct GE_DefaultPulseGenType GE_DefaultPulseGenType;

/**
 * @see struct GE_DefaultPulseGen
 */
typedef struct GE_DefaultPulseGen GE_DefaultPulseGen;

/**
 * @brief The type just extends dur pulse generator for now.
 */
struct GE_DefaultPulseGenType
{

    /**
     * @brief Polymorphic extension.
     */
    GE_DurPulseGenType super;

};

/**
 * @brief Pulses with quantization and controls for legato, pause, phase offset and velocity.
 */
struct GE_DefaultPulseGen
{

    /**
     * @brief Polymorphic extension.
     */
    GE_DurPulseGen super;

    /**
     * @brief Store the last used phase to detect changes.
     */
    double lastPhase;

    /**
     * @brief FIXME: lastPhase "0"?
     */
    double lastPhase0;

    /**
     * @brief Store the last duration to detect changes.
     */
    double lastDur;

    /**
     * @brief FIXME
     */
    double newOffset;

    /**
     * @brief Store last beat to get the number of beats since last call to process.
     */
    double lastBeat;

    /**
     * @brief FIXME
     */
    int lastIndex;

    /**
     * @brief Quantize the offset.
     */
    double offsetQuant;

    /**
     * @brief FIXME
     */
    int offsetQuantIndex;

    /**
     * @brief FIXME
     */
    bool additive;

    /**
     * @brief Legato probability.
     */
    GE_ControlInput *control_legatoprob;

    /**
     * @brief Pause proability.
     */
    GE_ControlInput *control_pauseprob;

    /**
     * @brief Phase offset.
     */
    GE_ControlInput *control_phaseofs;

    /**
     * @brief Velocity.
     */
    GE_ControlInput *control_velocity;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The default pulse generator type.
 */
extern GE_PUBLIC const GE_DefaultPulseGenType GE_DEFAULT_PULSE_GEN_TYPE;

/**
 * Get the offset quant index.
 * @param generator The generator.
 * @returns The offset quantization index.
 * 
 * FIXME: why would you want to get this? What is it used for?
 */
int GE_DefaultPulseGen_GetOffsetQuantIndex(GE_DefaultPulseGen *generator);

/**
 * Set offset quant index.
 * @param generator Pointer to a DefaultPulseGen
 * @param offsetQuantIndex The new index.
 *
 * FIXME: why would you want to set this? What is it used for?
 */
void GE_DefaultPulseGen_SetOffsetQuantIndex(GE_DefaultPulseGen *generator, int offsetQuantIndex);

#ifdef __cplusplus
}
#endif

#endif /* PULSEGEN_H */
