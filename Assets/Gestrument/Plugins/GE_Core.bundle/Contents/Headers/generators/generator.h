/**
 * @file generator.h
 * @brief Base of all generators.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @author Peter Gebauer
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_GENERATOR_H
#define GE_GENERATOR_H

#include "GE_Core/common.h"
#include "GE_Core/component.h"
#include "GE_Core/control_input.h"

/**
 * @brief Calls GE_GeneratorType_CanCast() with a type cast to (GE_GeneratorType*) so you don't have to.
 * @param generator_type_ptr_ A pointer to the generator type object.
 * @param to_generator_type_ptr_ Can we cast to this type?
 * @returns True or false.
 */
#define GE_GENERATOR_TYPE_CAN_CAST(generator_type_ptr_, to_generator_type_ptr_) GE_GeneratorType_CanCast((const GE_GeneratorType *)(generator_type_ptr_), (const GE_GeneratorType *)(to_generator_type_ptr_))

/**
 * @brief Calls GE_Generator_GetType() with a type cast to (GE_Generator*) so you don't have to.
 * @param generator_ Get the type from this generator.
 * @returns The generator type pointer as `GE_GeneratorType*`.
 */
#define GE_GENERATOR_GET_TYPE(generator_) GE_Generator_GetType((const GE_Generator *)(generator_))

/**
 * @brief Calls GE_Geneator_IsKindOf() to check for compatibility and type casts a generator type to void*.
 * @param generator_ The generator.
 * @param generator_type_ptr_ A pointer to the generator type object.
 * @returns The generator type as `void*` or NULL if @p generator_ is not compatible with @p generator_type_ptr_.
 */
#define GE_GENERATOR_CHECK_TYPE(generator_, generator_type_ptr_) (GE_Generator_IsKindOf((const GE_Generator*)(generator_), (const GE_GeneratorType*)(generator_type_ptr_)) ? (void*)GE_Generator_GetType((const GE_Generator*)(generator_)) : NULL)

/**
 * @brief Calls GE_Geneator_IsKindOf() to check for compatibility and type casts a generator instance to void*.
 * @param generator_ The generator.
 * @param generator_type_ptr_ A pointer to the generator type object.
 * @returns The generator as `void*` or NULL if @p generator_ is not compatible with @p generator_type_ptr_.
 */
#define GE_GENERATOR_IS_KIND_OF(generator_, generator_type_ptr_) (GE_Generator_IsKindOf((const GE_Generator*)(generator_), (const GE_GeneratorType*)(generator_type_ptr_)) ? (void*)(generator_) : NULL)

/**
 * @brief Helper macro to access a param table.
 * @param generator Pointer to a GE_Generator.
 */
#define GE_GENERATOR_GET_PARAM_TABLE(generator_) (&((GE_Generator*)(generator_))->component.params)

#ifndef GE_INSTRUMENT_STRUCT
#define GE_INSTRUMENT_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Instrument
 */
typedef struct GE_Instrument GE_Instrument;
#endif

/**
 * @see struct GE_GeneratorInputDef
 */
typedef struct GE_GeneratorInputDef GE_GeneratorInputDef;

/**
 * @see struct GE_GeneratorType
 */
typedef struct GE_GeneratorType GE_GeneratorType;

/**
 * @see struct GE_Generator
 */
typedef struct GE_Generator GE_Generator;

/**
 * @brief Constructor callback.
 * @param generator The generator instance.
 */
typedef void (*GE_GeneratorInit)(GE_Generator *generator);

/**
 * @brief Destructor callback.
 * @param generator The generator instance.
 */
typedef void (*GE_GeneratorDeinit)(GE_Generator *generator);

/**
 * @brief Debug dump callback.
 * @param generator The generator instance.
 * @param f File handle to redirect debug output
 * @param indent Number of spaces to indent output.
 */
typedef void (*GE_GeneratorDebugDump)(const GE_Generator *generator, FILE *f, unsigned int indent);

/**
 * @brief Get state callback.
 * @param generator The generator instance.
 * @param extendState An existing state to modify or NULL for an empty state.
 * @returns The current state.
 */
typedef GE_Variant* (*GE_GeneratorGetState)(const GE_Generator *generator, GE_Variant *extendState);

/**
 * @brief Set state callback.
 * @param generator The generator instance.
 * @param state The state to set.
 * @returns True if state was successfully set or false if there was an error.
 */
typedef bool (*GE_GeneratorSetState)(GE_Generator *generator, const GE_Variant *state);

/**
 * @brief Used by generators to define control inputs statically.
 */
struct GE_GeneratorInputDef {

    /**
     * @brief A name that must be unique per component, used to identify the control input.
     */
    const char *name;

    /**
     * @brief A human readable title for the control input.
     */
    const char *title;

    /**
     * @brief Minimum value.
     */
    double minValue;

    /**
     * @brief Maximum value.
     */
    double maxValue;

    /**
     * @brief A default value.
     */
    double defaultValue;

    /**
     * @brief Quantization of the value.
     */
    double quant;

    /**
     * @brief String formatting.
     */
    GE_ControlInputValueFormatter formatter;

    /**
     * @brief The control input pointer member offset.
     */
    size_t member_offset;

};

/**
 * @brief Basis for all generator types.
 *
 * Use polymorphism by extending this struct for generator type implementations.
 */
struct GE_GeneratorType
{

    /**
     * @brief Point to a super type struct.
     */
    const GE_GeneratorType *super;

    /**
     * @brief Unique type name, used to instance generators by type name.
     */
    const char *name;

    /**
     * @brief Brief description of what the generator is.
     */
    const char *description;

    /**
     * @brief The instance size.
     */
    size_t instance_size;

    /**
     * @brief Constructor callback.
     */
    GE_GeneratorInit Init;

    /**
     * @brief Destructor callback.
     */
    GE_GeneratorDeinit Deinit;

    /**
     * @brief Get state callback.
     */
    GE_GeneratorGetState GetState;

    /**
     * @brief Set state callback.
     */
    GE_GeneratorSetState SetState;

    /**
     * @brief Debug dump callback.
     */
    GE_GeneratorDebugDump DebugDump;

};

/**
 * @brief The base generator struct.
 * @note This has no implementation and is considered abstract.
 *
 * Use polymorphism by extending this struct for generator instance implementations.
 */
struct GE_Generator {

    /**
     * @brief Component setup, polymorphic struct.
     */
    GE_Component component;

    /**
     * @brief The generator type, NULL if base GE_Generator (dummy/empty).
     */
    const GE_GeneratorType *type;

    /**
     * @brief The owning instrument.
     */
    GE_Instrument *instrument;

    /**
     * @brief Number of control inputs.
     */
    int numControlInputs;

    /**
     * @brief The control inputs.
     */
    GE_ControlInput **controlInputs;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the generator type's super type.
 * @param type The generator type.
 * @returns The super type or NULL.
 */
const GE_GeneratorType* GE_GeneratorType_GetSuper(const GE_GeneratorType *type);

/**
 * Get the generator type name.
 * @param type The generator type.
 * @returns The type name.
 */
const char* GE_GeneratorType_GetName(const GE_GeneratorType *type);

/**
 * Get the generator type description.
 * @param type The generator type.
 * @returns The type description.
 */
const char* GE_GeneratorType_GetDescription(const GE_GeneratorType *type);

/**
 * Check if a type can be type casted.
 * @param type The generator type.
 * @param to_type The type we want to cast to.
 * @returns True if possible, false if not.
 */
bool GE_GeneratorType_CanCast(const GE_GeneratorType *type, const GE_GeneratorType *to_type);

/**
 * @brief Create a new generator instance.
 * @param type The generator type.
 * @param instrument The owning instrument.
 * @note Used internally by @ref GE_Instrument.
 */
GE_Generator* GE_GeneratorType_CreateInstance(const GE_GeneratorType *type, GE_Instrument *instrument);

/**
 * @brief Free generator.
 * @param generator The generator to free.
 * @note Used internally by @ref GE_Instrument, do not call manually!
 */
void GE_Generator_Free(GE_Generator *generator);

/**
 * @brief Get the generator type.
 * @param generator The generator.
 * @returns A pointer to the type.
 */
const GE_GeneratorType* GE_Generator_GetType(const GE_Generator *generator);

/**
 * @brief Get the generator type name.
 * @param generator The generator.
 * @returns The generator type name, never NULL.
 *
 * Convenience function.
 */
const char* GE_Generator_GetTypeName(const GE_Generator *generator);

/**
 * @brief Get the base (top) type of the generator.
 * @param generator The generator.
 * @returns The base (top) type of the generator.
 */
const GE_GeneratorType* GE_Generator_GetBaseType(const GE_Generator *generator);

/**
 * @brief Check generator type compatibility.
 * @param generator The generator.
 * @param type The type to check for.
 * @returns True if @p generator is compatible with @p type.
 */
bool GE_Generator_IsKindOf(const GE_Generator *generator, const GE_GeneratorType *type);

/**
 * @brief Check generator type compatibility.
 * @param generator The generator.
 * @param typeName The name of the type to check for.
 * @returns True if @p generator is compatible with @p typeName.
 */
bool GE_Generator_IsKindOfName(const GE_Generator *generator, const char *typeName);

/**
 * @brief Get the main control input.
 * @param generator The generator.
 * @returns The control input, NULL if the generator has no inputs.
 */
GE_ControlInput* GE_Generator_GetMainControlInput(const GE_Generator *generator);

/**
 * Get the number of control inputs (main not counted).
 * @param generator The generator.
 * @returns The number of control inputs.
 * @relates GE_Generator
 */
int GE_Generator_GetNumControlInputs(const GE_Generator *generator);

/**
 * Update ControlInput sources for this generator.
 * @note If the generator is not attached to an instrument this function does nothing.
 * @param generator The generator.
 */
void GE_Generator_UpdateControlInputSources(const GE_Generator *generator);

/**
 * Get a control input.
 * @param generator The generator.
 * @param index The index of the control input.
 * @returns The control input or NULL if @p index is out of range.
 * @note To get the main control input use GE_Generator_GetMainControlInput().
 */
GE_ControlInput* GE_Generator_GetControlInput(const GE_Generator *generator, int index);

/**
 * Find the index of a control input.
 * @param generator The generator.
 * @param name The name of the control input.
 * @returns The index of the control input or -1 if @p name was not found.
 * @note To get the main control input, pass NULL or use GE_Generator_GetMainControlInput().
 */
int GE_Generator_FindControlInput(const GE_Generator *generator, const char *name);

/**
 * Append and configure a control input manually.
 * @param generator The generator.
 * @param name A name for identification.
 * @param title Title for user display.
 * @param minValue Minimum value.
 * @param maxValue Maximum value.
 * @param defaultValue Default value.
 * @param quant The granularity of the value, 0.01 equals two decimal. 0.0 means don't quantize.
 * @param formatter Function pointer to format the value as string, or NULL.
 * @note Used by extending generators, do not call unless you are implementing a generator!
 * @relates GE_Generator
 */
GE_ControlInput* GE_Generator_AddControlInput(
    GE_Generator *generator,
    const char *name,
    const char *title,
    double minValue,
    double maxValue,
    double defaultValue,
    double quant,
    GE_ControlInputValueFormatter formatter
);

/**
 * Configure a control input and append it to the internal array of control inputs.
 * @param generator The generator.
 * @param defs A zero terminated (a def with NULL or empty name) array of generator input defs.
 * @returns The number of added control inputs.
 * @note Used by extending generators, do not call unless you are implementing a generator!
 *
 * The function will iterate all @p defs until a def with NULL or empty name, that's the
 * zero termination.
 */
int GE_Generator_AddControlInputs(GE_Generator *generator, const GE_GeneratorInputDef *defs);

/**
 * Get the state of a generator.
 *
 * A lot of generators have unique parameters/settings which are not captured
 * by Instrument_GetState. Generators can implement this function to capture
 * their individual state.
 *
 * @param generator Pointer to a GE_Generator.
 * @param extendState The state to extend, will return a new dict if @p extendState is NULL.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note The returned dict must be freed.
 */
GE_Variant* GE_Generator_GetState(const GE_Generator *generator, GE_Variant *extendState);

/**
 * Set the state of a generator.
 * Sets the generator's specific state.
 * @param generator Pointer to a GE_Generator.
 * @param state The state to set.
 * @returns true on success, false on fail.
 */
bool GE_Generator_SetState(GE_Generator *generator, const GE_Variant *state);

/**
 * Dump debug info (text) to stream.
 * @param generator The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 */
void GE_Generator_DebugDump(const GE_Generator *generator, FILE *f, unsigned int indent);

#ifdef __cplusplus
}
#endif

#endif
