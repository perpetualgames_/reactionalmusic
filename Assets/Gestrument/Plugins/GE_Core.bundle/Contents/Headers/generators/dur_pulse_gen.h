/**
 * @file dur_pulse_gen.h
 * @brief GE_DurPulseGen
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_DUR_PULSE_GEN_H
#define GE_DUR_PULSE_GEN_H

#define GE_DUR_PULSE_GEN_MAX_DURS 18

#include "GE_Core/common.h"
#include "GE_Core/generators/pulse_gen.h"

/**
 * @see struct GE_DurPulseGenType
 */
typedef struct GE_DurPulseGenType GE_DurPulseGenType;

/**
 * @see struct GE_DurPulseGen
 */
typedef struct GE_DurPulseGen GE_DurPulseGen;

/**
 * A DurPulseGenType.
 */
struct GE_DurPulseGenType {
    GE_PulseGenType super;
};

struct GE_DurPulseGen {

    /**
     * Inherited struct.
     */
    GE_PulseGen super;

    /**
     * The number of selected durations.
     */
    int numDurations;

    /**
     * List of duration indices.
     */
    int durationIndexes[GE_DUR_PULSE_GEN_MAX_DURS];

    /**
     * The currently selected durations.
     * Used as a filter (mask) to select durations from the durations array.
     * @see GE_DurPulseGen_SetSelectedDurations
     */
    bool selectedDurations[GE_DUR_PULSE_GEN_MAX_DURS];

    /**
     * Initialized with defaultDurationStrings.
     * @see GE_DurPulseGen_SetSelectedDurations
     */
    char *durationStrings[GE_DUR_PULSE_GEN_MAX_DURS];

    /**
     * Calculated from duration string representation.
     */
    double durations[GE_DUR_PULSE_GEN_MAX_DURS];

    /**
     * The minimum duration of the durations array.
     * @see GE_DurPulseGen.durations
     */
    double minDur;

    /**
     * The maximum duration of the durations array.
     * @see GE_DurPulseGen.durations
     */
    double maxDur;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * A GE_DurPulseGenType.
 */
extern GE_PUBLIC const GE_DurPulseGenType GE_DUR_PULSE_GEN_TYPE;

/**
 * FIXME: describe
 */
void GE_DurPulseGen_GetDuration(GE_PulseGen *generator, int *durIndexOut, double *durOut);

/**
 * Select durations from the list of default durations.
 *
 * This is the order of the default durations.
 *
 * "1/1", "1/2", "1/4", "1/8", "1/16", "1/32",
 * "1/1.", "1/2.", "1/4.", "1/8.", "1/16.", "1/32.",
 * "1/1T", "1/2T", "1/4T", "1/8T", "1/16T", "1/32T",
 *
 * @param generator Pointer to GE_PulseGen
 * @param selected Array of booleans used to filter the default durations list.
 * @param numSelected Length of @p selected.
 * @returns true on success otherwise false.
 */
bool GE_DurPulseGen_SetSelectedDurations(GE_PulseGen *generator, const bool *selected, int numSelected);

#ifdef __cplusplus
}
#endif

#endif /* DUR_PULSE_GEN_H */
