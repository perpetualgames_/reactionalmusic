/**
* @file component.h
* @brief Component base struct for managing references and destructors.
* @author Peter Gebauer
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
* Adds a few functions for the tree-like types.
*
* This is a base for structs that are memory managed
* by the engine (including the engine struct itself).
*
* To make bindings for a memory managed language easier
* the component struct provides a base to hold references 
* and call callbacks.
*
* TODO / FIXME:
*
* - Component_Init/Deinit for Engine, Instrument, Cursor, Control Input, Control Source and Generator.
* - Fast hash table for custom size elements.
*/

#ifndef GE_COMPONENT_H
#define GE_COMPONENT_H

#include "GE_Core/common.h"
#include "GE_Core/param.h"
#include "GE_Core/event.h"

/**
 * @brief Component type max length, including zero terminator.
 */
#define GE_COMPONENT_TYPE_LENGTH 32

/**
 * @brief Maximum number of parameters.
 */
#define GE_COMPONENT_MAX_PARAMS 128

/**
 * @see struct GE_Component
 */
typedef struct GE_Component GE_Component;

/**
 * @see struct GE_ComponentConfig
 */
typedef struct GE_ComponentConfig_ GE_ComponentConfig;

/**
 * @brief A callback called from GE_Component_Deinit().
 * @param component The component.
 */
typedef void (*GE_ComponentDestructor)(GE_Component *component);

/**
 * @brief Callback to get the event object for a component.
 * @param component The component.
 * @returns The event object.
 */
typedef GE_EventObject (*GE_ComponentEventObjectGetter)(const GE_Component *component);

/**
 * @brief Used to initialize a component.
 */
struct GE_ComponentConfig_ {

    /**
     * The type, must be zero terminated.
     */
    char type[GE_COMPONENT_TYPE_LENGTH];

    /**
     * @brief The number of parameters.
     */
    int numParams;

    /**
     * @brief A pointer to an array of param infos, the length must match numParams.
     * @note Set to NULL if numParams is zero.
     */
    const GE_ParamInfo *params;

    /**
     * @brief Get the event object for the component.
     */
    GE_ComponentEventObjectGetter getEventObject;

};

/**
 * @brief The base struct for components.
 */
struct GE_Component {

    /**
     * The type, must be zero terminated.
     */
    char type[GE_COMPONENT_TYPE_LENGTH];

    /**
     * Used to hold external references.
     * @note Investigate if the OSC API obsoletes these bindings refs.
     */
    void *ref;

    /**
     * A callback, must be called from the destructor of all components.
     * @note Investigate if the OSC API obsoletes these bindings refs.
     */
    GE_ComponentDestructor destructor;

    /**
     * Any arbitrary data.
     */
    void *userData;

    /**
     * @brief Parameters used by the component.
     */
    GE_ParamTable params;

    /**
     * @brief Get the event object for the component.
     */
    GE_ComponentEventObjectGetter getEventObject;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initializes the component data.
 * @param component The component.
 * @param config A component config.
 * @returns Zero on success or a negative error code on failure.
 */
int GE_Component_Init(GE_Component *component, const GE_ComponentConfig *config);

/**
 * Deinitializes the component data.
 * @param component The component.
 * @param type The type name.
 * @returns Zero on success or a negative error code on failure.
 */
void GE_Component_Deinit(GE_Component *component);

/**
 * Get the type for a component.
 * @param component The component.
 * @returns The type (never NULL).
 */
GE_PUBLIC const char* GE_Component_GetType(const GE_Component *component);

/**
 * Set the type for a component.
 * @param component The component.
 * @param type The component type.
 * @note If type is NULL it will be set to empty string.
 */
void GE_Component_SetType(GE_Component *component, const char *type);

/**
 * Get the reference pointer for a component.
 * @param component The component.
 * @returns The reference pointer or NULL if no reference pointer is set.
 */
void* GE_Component_GetRef(const GE_Component *component);

/**
 * Set the reference pointer for a component.
 * @param component The component.
 * @param ref The reference pointer or NULL for no reference pointer.
 */
void GE_Component_SetRef(GE_Component *component, void *ref);

/**
 * Get the data pointer for a component.
 * @param component The component.
 * @returns The data pointer or NULL if no data pointer is set.
 */
void* GE_Component_GetUserData(const GE_Component *component);

/**
 * Set the data pointer for a component.
 * @param component The component.
 * @param data The data pointer or NULL for no data pointer.
 */
void GE_Component_SetUserData(GE_Component *component, void *data);

/**
 * Get the callback pointer for a component.
 * @param component The component.
 * @returns The callback pointer or NULL if no callback pointer is set.
 */
GE_ComponentDestructor GE_Component_GetDestructorCallback(const GE_Component *component);

/**
 * Set the callback pointer for a component.
 * @param component The component.
 * @param callback The callback pointer or NULL for no callback pointer.
 */
void GE_Component_SetDestructorCallback(GE_Component *component, GE_ComponentDestructor callback);

/**
 * @brief Get the event object from a component.
 * @param component The component.
 * @returns The event object.
 */
GE_PUBLIC GE_EventObject GE_Component_GetEventObject(const GE_Component *component);

/**
 * @brief Stupid glue for bindings.
 * @param component The component.
 * @returns The param table.
 */
GE_PUBLIC GE_ParamTable* GE_Component_GetParamTable(GE_Component *component);

#ifdef __cplusplus
}
#endif

#endif /* GE_CHORD_H */
