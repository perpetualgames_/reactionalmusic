/**
 * @file message_queue.h
 * @brief A bidirectional queue for use between main and realtime threads.
 * @author David Granström
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * Used to convey data and executable code between two threads; the main thread and the realtime/audo thread.
 * 
 * Main thread writes to `realtimeThreadBuffer`, the realtime thread reads and process read items.
 * When realtime has processed an item succesfully it is then pushed back to `mainThreadBuffer`.
 *
 * Realtime thread can also write directly to `mainThreadBuffer`.
 */

#ifndef GE_MESSAGE_QUEUE_H
#define GE_MESSAGE_QUEUE_H

#include "GE_Core/common.h"
#include "GE_Core/utils/circular_buffer.h"
#include "GE_Core/utils/mutex.h"
#include "GE_Core/utils/logger.h"

/**
 * Get the data pointer for a @ref GE_MessageQueueItem.
 * @param item A pointer to the item.
 * @returns A void pointer to where the item data starts.
 * @note To know the size of the data, look at `item->length`.
 */
#define GE_MESSAGE_QUEUE_ITEM_DATA(item) ((void*)((char*)(item) + sizeof(GE_MessageQueueItem)))

/**
 * @see struct GE_MessageQueue
 */
typedef struct GE_MessageQueue GE_MessageQueue;

/**
 * @see struct GE_MessageQueueItem
 */
typedef struct GE_MessageQueueItem GE_MessageQueueItem;

/**
 * Process an item.
 * @param queue The queue the item came from.
 * @param item The item being processed.
 * @param arg Arbitrary data passed to the function.
 */
typedef void (*GE_MessageQueueItemProcessor)(GE_MessageQueue *queue, GE_MessageQueueItem *item, void *arg);

/**
 * Deinitialize an item that has not been processed when the queue is deinitialized.
 * @param queue The queue the item came from.
 * @param item The item being deinitialized.
 * 
 * Normally a deinit would be handled in fProcess, but since there might
 * be remaining, unprocessed items when the queue is deinitialized
 * this function should deinit those items.
 */
typedef void (*GE_MessageQueueItemDeinit)(GE_MessageQueue *queue, GE_MessageQueueItem *item);

/**
 * Items are placed in the queue buffers.
 */
struct GE_MessageQueueItem {

    /**
     * Callback to process the item.
     */
    GE_MessageQueueItemProcessor fProcess;

    /**
     * Callback for unprocessed items when the queue is deinitialized.
     */
    GE_MessageQueueItemDeinit fDeinit;

    /**
     * The number of bytes of data (excluding the GE_MessageQueueItem struct itself).
     */
    size_t length;
};

/**
 * The message queue that handles bidirectional thread buffers.
 */
struct GE_MessageQueue {

    /**
     * Realtime to main thread messages.
     */
    GE_CircularBuffer mainThreadBuffer;

    /**
     * Main to realtime thread messages.
     */
    GE_CircularBuffer realtimeThreadBuffer;

    /**
     * A mutex for locking when updating values such as `holdRealtimeProcessing`.
     * @note Realtime thread will use trylock.
     */
    GE_Mutex *mutex;

    /**
     * True if realtime processing is on hold.
     */
    bool holdRealtimeProcessing;

    /**
     * Allocated memory to handle message queue items on the main thread.
     */
    GE_MessageQueueItem *mainThreadItem;

    /**
     * Allocated memory to handle message queue items on the realtime thread.
     */
    GE_MessageQueueItem *realtimeThreadItem;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialize a message queue.
 * @param queue The message queue to initialize.
 * @param mainBufferSize The size of the buffer with messages for the main thread to process.
 * @param realtimeBufferSize The size of the buffer with messages for the realtime thread to process.
 */
void GE_MessageQueue_Init(GE_MessageQueue *queue, size_t mainBufferSize, size_t realtimeBufferSize);

/**
 * Deinitialize a message queue.
 * @param queue Deinitialize this queue.
 */
void GE_MessageQueue_Deinit(GE_MessageQueue *queue);

/**
 * Process messages in the main thread.
 * @param queue The queue.
 * @param arg Aribtrary data passed to the processor callback.
 * @returns The number of processed messages.
 * @note This function should only be called in the main thread.
 */
int GE_MessageQueue_ProcessOnMainThread(GE_MessageQueue *queue, void *arg);

/**
 * Process messages in the realtime thread.
 * @param queue The queue.
 * @param arg Aribtrary data passed to the processor callback.
 * @returns The number of processed messages or a negative error code on error.
 * @note This function should be called from the realtime/audio thread.
 *
 * The two error codes that may be returned are:
 * - -1 if `realtimeThreadBuffer` doesn't have enough bytes for the message size.
 * - -2 if `mainThreadBuffer` does not have enough space to accomodate a response.
 */
int GE_MessageQueue_ProcessOnRealtimeThread(GE_MessageQueue *queue, void *arg);

/**
 * Check if the queue is holding (not processing) in the realtime thread.
 * @param queue The queue.
 * @returns True or false.
 * @note This function should only be called from the main thread.
 */
bool GE_MessageQueue_GetHoldRealtimeProcessing(const GE_MessageQueue *queue);

/**
 * Set holding (not processing) in the realtime thread.
 * @param queue The queue.
 * @param hold True or false.
 * @note This function should only be called from the main thread.
 */
void GE_MessageQueue_SetHoldRealtimeProcessing(GE_MessageQueue *queue, bool hold);

/**
 * Send a message from realtime to main thread.
 * @param queue The queue.
 * @param fMainThreadProcess A function to execute on the main thread, can be NULL.
 * @param fDeinit Called for unprocessed items when the queue is deinitializing.
 * @param length The size of the data.
 * @param data Pointer to the data, will be copied.
 * @returns True on success, false if `mainThreadBuffer` does not have the required space.
 * @note This function should only be called in the realtime thread.
 * @note sizeof(GE_MessageQueueItem) will be added to @p length by this function.
 */
bool GE_MessageQueue_SendMessageToMainThread(GE_MessageQueue *queue,
                                             GE_MessageQueueItemProcessor fMainThreadProcess,
                                             GE_MessageQueueItemDeinit fDeinit,
                                             size_t length,
                                             const void *data);

/**
 * Send a message from main to realtime thread.
 * @param queue The queue.
 * @param fRealtimeThreadProcess A function to execute on the realtime thread, can be NULL.
 * @param fDeinit Called for unprocessed items when the queue is deinitializing.
 * @param length The size of the data.
 * @param data Pointer to the data, will be copied.
 * @returns True on success, false if `realtimeThreadBuffer` does not have the required space.
 * @note This function should only be called in the main thread.
 * @note sizeof(GE_MessageQueueItem) will be added to @p length by this function.
 */
bool GE_MessageQueue_SendMessageToRealtimeThread(GE_MessageQueue *queue,
                                                 GE_MessageQueueItemProcessor fRealtimeThreadProcess,
                                                 GE_MessageQueueItemDeinit fDeinit,
                                                 size_t length,
                                                 const void *data);

#ifdef __cplusplus
}
#endif

#endif /* GE_MESSAGE_QUEUE_H */
