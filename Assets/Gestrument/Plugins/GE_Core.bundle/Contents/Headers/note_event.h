/**
* @file note_event.h
* @brief GE_NoteEvent header
* @author Peter Gebauer
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_NOTE_EVENT_H
#define GE_NOTE_EVENT_H

#include "GE_Core/common.h"

/**
 * @see struct GE_NoteEvent_s
 */
typedef struct GE_NoteEvent_s GE_NoteEvent;


/**
 * A note event.
 */
struct GE_NoteEvent_s {

    /**
     * Timestamp.
     */
    double timestamp;

    /**
     * Pitch.
     */
    float pitch;

    /**
     * Velocity.
     */
    float velocity;

    /**
     * Duration.
     */
    double duration;

    /**
     * For midi upper 8 bits is track, lower 8 is channel.
     */
    uint16_t channel;

};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif
