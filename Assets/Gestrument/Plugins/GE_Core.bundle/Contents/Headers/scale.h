/**
 * @file scale.h
 * @brief A representation of scale.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * GE_Scale is used to calculate pitch values for pitch generators. The Engine
 * has a set of default scale slots which can be used to change scale globally
 * for all pitch generators.
 * @see GE_Engine_SetDefaultScaleForSlot
 * @see GE_Engine_SetScaleForSlot
 *
 * GE_Core comes with a set of default scales which can be used to create GE_Scale instances.
 * See @ref GE_DefaultScale and @ref GE_Scale_NewFromDefault @ref GE_Scale_SetFromDefault for more information.
 *
 * It is also possible to import external tunings (including microtonal) and
 * use them as scales, @see the GE_Scale_LoadFile function.
 */

#ifndef SCALE_H
#define SCALE_H

#include "GE_Core/common.h"
#include "GE_Core/utils/variant.h"

#define SCALE_MAX_SIZE 1024

typedef struct GE_RawScale GE_RawScale;
typedef struct GE_Scale GE_Scale;

/**
* Default scales.
*/
typedef enum GE_DefaultScale {

    /**
     * Ionian scale.
     */
    GE_DEFAULT_SCALE_IONIAN,

    /**
     * Dr Dorian scale.
     */
    GE_DEFAULT_SCALE_DORIAN,

    /**
     * Phrygian scale.
     */
    GE_DEFAULT_SCALE_PHRYGIAN,

    /**
     * Lydian scale.
     */
    GE_DEFAULT_SCALE_LYDIAN,

    /**
     * Mixolydian scale.
     */
    GE_DEFAULT_SCALE_MIXOLYDIAN,

    /**
     * Aeolian scale.
     */
    GE_DEFAULT_SCALE_AEOLIAN,

    /**
     * Locrian scale.
     */
    GE_DEFAULT_SCALE_LOCRIAN,

    /**
     * Chromatic.
     */
    GE_DEFAULT_SCALE_CHROMATIC, // 7

    /**
     * Wholetone.
     */
    GE_DEFAULT_SCALE_WHOLETONE,

    /**
     * Major pentatonic.
     */
    GE_DEFAULT_SCALE_MAJOR_PENTATONIC,

    /**
     * Minor pentatonic.
     */
    GE_DEFAULT_SCALE_MINOR_PENTATONIC,

    /**
     * Harmonic minor.
     */
    GE_DEFAULT_SCALE_HARMONIC_MINOR,

    /**
     * Max enum.
     */
    MAX_GE_DEFAULT_SCALE

} GE_DefaultScale;

/**
 * Supported scale formats.
 * @ref GE_Scale_LoadFile will automatically detect the format based on the file extension.
 * Valid extension are: .midi/.mid, .scl and .kit.
 */
typedef enum {

    /**
     * Invalid format.
     */
    GE_SCALE_FORMAT_INVALID = -1,

    /**
     * Scala format.
     */
    GE_SCALE_FORMAT_SCALA = 0,

    /**
     * A Standard MIDI File.
     */
    GE_SCALE_FORMAT_MIDI,

    /**
     * Kit format.
     */
    GE_SCALE_FORMAT_KIT,

    /**
     * Max enum.
     */
    MAX_GE_SCALE_FORMAT

} GE_ScaleFormat;

/**
 * The scale type.
 */
typedef enum {

    /**
     * A scale constructed by intervals in cents.
     */
    GE_SCALE_TYPE_INTERVALS,

    /**
     * A scale used for drum mappings.
     */
    GE_SCALE_TYPE_KIT,

    /**
     * A scale imported from a MIDI file.
     * Uses MIDI pitches as scale degrees.
     */
    GE_SCALE_TYPE_RAW,

    /**
     * Max enum.
     */
    GE_MAX_SCALE_TYPE

} GE_ScaleType;

/**
 * Raw scales contains pitches.
 */
struct GE_RawScale {

    /**
     * The pitches.
     */
    float pitches[SCALE_MAX_SIZE];

    /**
     * The number of pitches.
     */
    int count;

};

/**
 * A scale.
 */
struct GE_Scale {
    
    /**
     * Type of scale.
     */
    GE_ScaleType type;

    /**
     * Name of this scale.
     * Will default to "Untitled" if not set explictly.
     */
    char *name;

    /**
     * URL to scale data
     */
    char *url;

    /**
     * Scale data in cents.
     */
    float data[SCALE_MAX_SIZE];

    /**
     * Arbitrary data.
     */
    void *appData;

    /**
     * Number of pitches in the scale.
     */
    int count;

    /**
     * Root note of the scale as a MIDI note number (can be floating point).
     */
    float root;

    /**
     * Global scale index assigned by the engine, don't change this manually!
     * @see GE_Engine_Init
     */
    int index;

    /**
     * Used internally by GE_SCALE_TYPE_KIT and GE_SCALE_TYPE_RAW
     */
    int trim_begin;

    /**
     * Used internally by GE_SCALE_TYPE_KIT and GE_SCALE_TYPE_RAW
     */
    int trim_len;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get a human readable name of a default scale.
 * @param defaultScale The default scale.
 * @returns A human redable name or an empty string if @p defaultScale is invalid.
 */
const char* GE_GetDefaultScaleName(GE_DefaultScale defaultScale);

/**
 * Get the pitches for a default scale.
 * @param defaultScale The default scale.
 * @param maxPitches The maximum number of pitches to return.
 * @param[out] pitches Return pitches here, NULL to get the number of available pitches.
 * @returns The number of returned (or available) pitches or -1 if @p defaultScale is invalid.
 * @note If @p pitches is NULL @p maxPitches will be ignored.
 */
int GE_GetDefaultScalePitches(GE_DefaultScale defaultScale, int maxPitches, float *pitches);

/**
 * Find a default scale by name (case insensetive).
 * @param name The name of the scale to find.
 * @returns The default scale enum or a negative value if @p name was not found.
 */
int GE_FindDefaultScale(const char *name);

/**
 * Create a new (empty) Scale.
 *
 * @return Pointer to a GE_Scale
 */
GE_Scale* GE_Scale_New(void);

/**
 * Create a new scale using a default scale.
 *
 * @see GE_DefaultScales
 * @param defaultScale A default scale enum
 * @return Pointer to a GE_Scale
 */
GE_Scale* GE_Scale_NewFromDefault(GE_DefaultScale defaultScale);

/**
 * Create a new scale from an array of semitones.
 *
 * @param semitones An array of floats representing semitones.
 *        @note First pitch (root) is already implied.
 * @param length The length of @p semitones
 * @return Pointer to a GE_Scale or NULL on error.
 * @relates GE_Scale
 */
GE_Scale* GE_Scale_NewFromSemitones(float *semitones, int length);

/**
 * Deallocate a GE_Scale.
 * @relates GE_Scale
 */
void GE_Scale_Free(GE_Scale *scale);

/**
 * Clone a scale.
 *
 * @param scale Pointer to a GE_Scale to clone.
 * @returns The cloned scale.
 */
GE_Scale* GE_Scale_Clone(const GE_Scale *scale);

/**
 * Set scale using a default scale.
 *
 * @see GE_DefaultScales
 * @param scale Pointer to GE_Scale.
 * @param defaultScale A default scale enum
 * @return Success or fail.
 * @relates GE_Scale
 */
bool GE_Scale_SetFromDefault(GE_Scale *scale, GE_DefaultScale defaultScale);

/**
 * Set scale from an array of semitones.
 *
 * @param scale The scale.
 * @param semitones An array of floats representing semitones.
 *        @note First pitch (root) is already implied.
 * @param length The length of @p semitones
 * @return Pointer to a GE_Scale or NULL on error.
 * @relates GE_Scale
 */
void GE_Scale_SetFromSemitones(GE_Scale *scale, const float *semitones, int length);

/**
 * Copy a scale.
 * @param scale The scale.
 * @param source The scale to copy from.
 */
void GE_Scale_Copy(GE_Scale *scale, const GE_Scale *source);

/**
 * Set name of scale.
 *
 * @note Usually called from constructor functions.
 *
 * @param scale Pointer to GE_Scale
 * @param str Name of scale
 * @relates GE_Scale
 */
void GE_Scale_SetName(GE_Scale *scale, const char *str);

/**
 * Get the name of the current scale.
 *
 * @return Name of the scale or an empty string.
 * @relates GE_Scale
 */
char* GE_Scale_GetName(const GE_Scale *scale);

/**
 * Load pitch data from a file on disk.
 *
 * @see GE_ScaleFormats for supported formats.
 *
 * @param scale Pointer to GE_Scale
 * @param path A file path
 * @relates GE_Scale
 */
bool GE_Scale_LoadFile(GE_Scale *scale, const char *path);

/**
 * Load pitch data from a stream.
 *
 * @see GE_ScaleFormats for supported formats.
 *
 * @param scale Pointer to GE_Scale
 * @param stream A stream
 * @param format Scale file format
 *
 * @return true on success false on error
 * @relates GE_Scale
 */
bool GE_Scale_LoadStream(GE_Scale *scale, FILE *stream, const GE_ScaleFormat format);

/**
 * This function is called by pitch generators to calculate absolute pitch steps.
 *
 * @see GE_DefaultPitchGen
 * @relates GE_Scale
 */
void GE_Scale_MakeRawScale(GE_Scale *scale, GE_RawScale *raw, float min, float max);

/**
 * Return the current state in a dict variant.
 * @param scale Pointer to a GE_Scale.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will never return NULL (even though the variant may be an empty dict).
 *
 * Used to save states.
 * @relates GE_Scale
 */
GE_Variant* GE_Scale_GetState(const GE_Scale *scale);

/**
 * Set the current state using a dict variant.
 * @param scale Pointer to a GE_Scale.
 * @param state A dict variant with the state, pass NULL to reset the ControlInput to default values.
 * @returns True if the state was set, false on failure.
 *
 * Used to load states.
 * @relates GE_Scale
 */
bool GE_Scale_SetState(GE_Scale *scale, const GE_Variant *state);

/**
 * Set the root pitch of a scale.
 * @param scale The scale.
 * @param rootPitch The new root pitch.
 * @relates GE_Scale
 */
void GE_Scale_SetRootPitch(GE_Scale *scale, float rootPitch);

/**
 * Get the root pitch of a scale.
 * @param scale The scale.
 * @returns The root pitch.
 * @relates GE_Scale
 */
float GE_Scale_GetRootPitch(const GE_Scale *scale);

/**
 * Dump debug info (text) to stream.
 * @param scale The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_Scale
 */
void GE_Scale_DebugDump(const GE_Scale *scale, FILE *f, unsigned int indent);

/**
 * Reset a scale to its default values.
 * @param scale The scale.
 * @param type The type to reset to.
 * @relates GE_Scale
 */
void GE_Scale_ClearWithType(GE_Scale *scale, GE_ScaleType type);

/**
 * Set the scale using a scientific pitch notation for root and then a default scale name.
 * @param scale The scale to set.
 * @param s The string to set from.
 * @returns True if the scale was set or false if @p s is invalid.
 * @relates GE_Scale
 *
 * Notation is "<ROOT>/<SCALE>" where root and scale are separated by a slash.
 * 
 * If root is omitted C4 is default, if scale is omitted Chromatic is default.
 *
 * Example strings:
 * - "C5/ionian"
 * - "F#/lydian"
 * - "D/minor penatonic"
 *
 * etc.
 */
bool GE_Scale_SetFromString(GE_Scale *scale, const char *s);

#ifdef __cplusplus
}
#endif

#endif /* SCALE_H */
