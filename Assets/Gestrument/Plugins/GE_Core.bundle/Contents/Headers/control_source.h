/**
 * @file control_source.h
 * @brief Defines a source of control.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 * 
 * GE_ControlSource is mostly used together with GE_ControlInput to control
 * properties of generators. 
 * 
 * GE_Engine provides a set of default ControlSources
 * that can be mapped to arbitrary properties, such as transpose properties of
 * pitch generators or legato probabilty of pulse generators.
 */

#ifndef GE_CONTROL_SOURCE_H
#define GE_CONTROL_SOURCE_H

#include "GE_Core/common.h"
#include "GE_Core/component.h"
#include "GE_Core/event_recorder.h"

/**
 * @brief Used to get a list of users of a control source.
 */
typedef struct GE_ControlSourceUser {

    /**
     * @brief Instrument index.
     */
    int instrumentIndex;

    /**
     * @brief 0 = Pulse, 1 = Pitch.
     */
    int generatorIndex;

    /**
     * @brief The index of the input user.
     */
    int inputIndex;

} GE_ControlSourceUser;

/**
 * A control source is primarely used by @ref GE_ControlInput.
 */
typedef struct GE_ControlSource {

    /**
     * Component setup, polymorphic struct.
     */
    GE_Component component;

    /**
     * The current value.
     */
    double value;

    /**
     * The maximum value.
     */
    double maxValue;

    /**
     * A function that returns a value (e.g. a random value source)
     */
    double (*valueBlock)(void);
    int updateCount;

    /**
     * The ControlSource name.
     */
    char *name;
    char *title;
    char *description;

    /**
     * Indicates if this source should be read only on triggered condition.
     * Useful for controlling normally continiuous ControlInputs like Volume
     * with fluctuating ControlSources like Random.
     */
    bool triggerable;
    bool locked;
    GE_EventRecorder *eventRecorder;
} GE_ControlSource;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new control source.
 * @param name A per-owner unique name.
 * @param description A short human readable description.
 * @returns A new control source.
 * @note The default maximum value is 1.
 */
GE_ControlSource* GE_ControlSource_New(const char *name, const char *description);

/**
 * Create a new control source.
 * @param name A per-owner unique name.
 * @param description A short human readable description.
 * @param maxValue The maximum value, zero or negative means no limit.
 * @returns A new control source.
 */
GE_ControlSource* GE_ControlSource_NewWithMax(const char *name, const char *description, double maxValue);

/**
 * Free a control source.
 * @param source The control source.
 * @relates GE_ControlSource
 */
void GE_ControlSource_Free(GE_ControlSource *source);

/**
 * Get a value from a control source.
 * @param source The control source.
 * @returns The value.
 * @relates GE_ControlSource
 */
double GE_ControlSource_GetValue(GE_ControlSource *source);

/**
 * Set a value for a control source.
 * @param source The control source.
 * @param value The value.
 * @note Some control sources will ignore this.
 * @relates GE_ControlSource
 */
void GE_ControlSource_SetValue(GE_ControlSource *source, double value);

/**
 * Get the maximum value the control source allows.
 * @param source The control source.
 * @returns The maximum value.
 * @relates GE_ControlSource
 */
double GE_ControlSource_GetMaxValue(const GE_ControlSource *source);

/**
 * UI related function.
 * Remove?
 * @relates GE_ControlSource
 */
void GE_ControlSource_SetValueAndUpdate(GE_ControlSource *source, double val);

/**
 * UI related function.
 * Remove?
 * @relates GE_ControlSource
 */
int GE_ControlSource_GetUpdateCount(const GE_ControlSource *source);

/**
 * Set the callback for returning a value.
 * @param source The control source.
 * @param valueBlock A pointer to the callback function.
 * @relates GE_ControlSource
 */
void GE_ControlSource_SetValueBlock(GE_ControlSource *source, double (*valueBlock)(void));

/**
 * Dump debug info (text) to stream.
 * @param source The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_ControlSource
 */
void GE_ControlSource_DebugDump(const GE_ControlSource *source, FILE *f, unsigned int indent);

/**
 * Get user managed title from the control source.
 * @param source The control source to get the title from.
 * @returns The title, can be empty but never NULL.
 */
const char* GE_ControlSource_GetTitle(const GE_ControlSource *source);

/**
 * Set user managed title for the control source.
 * @param source The control source to set the title for.
 * @param title The title or NULL for no title.
 */
void GE_ControlSource_SetTitle(GE_ControlSource *source, const char *title);

/**
 * Get the per-cursor (or engine) unique control source name.
 * @param source The control source to get the name from.
 * @returns The name, can be empty but never NULL.
 * @note Names are unique within each cursor and the engine.
 */
const char* GE_ControlSource_GetName(const GE_ControlSource *source);

/**
 * Get a human readable and short description.
 * @param source The control source to get the description from.
 * @returns The description, can be empty but never NULL.
 */
const char* GE_ControlSource_GetDescription(const GE_ControlSource *source);

/**
 * Get the triggerable status.
 * @param source The control source.
 * @returns The triggerable status.
 */
bool GE_ControlSource_GetTriggerable(const GE_ControlSource *source);

/**
 * Set the triggerable status.
 * @param source The control source.
 * @param triggerable Set the source to triggerable if true.
 */
void GE_ControlSource_SetTriggerable(GE_ControlSource *source, bool triggerable);

#ifdef __cplusplus
}
#endif

#endif /* GE_CONTROL_SOURCE_H */
