/**
* @file chord.h
* @brief Chord functions.
* @author David Granström
* @copyright Copyright (c) 2017 Jonatan Liljedahl.
* This source code is released under the MIT License.
* @note This file is part of Gestrument Engine Core.
*/
/*
MIT License

Copyright (c) 2017 Jonatan Liljedahl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef GE_CHORD_H
#define GE_CHORD_H

#include "GE_Core/common.h"
#include "GE_Core/utils/variant.h"

/**
 * Default chord enums.
 */
typedef enum GE_DefaultChord {
    GE_DEFAULT_CHORD_MAJOR,
    GE_DEFAULT_CHORD_MINOR,
    GE_DEFAULT_CHORD_7TH,
    GE_DEFAULT_CHORD_MAJOR_7TH,
    GE_DEFAULT_CHORD_MINOR_7TH,
    GE_DEFAULT_CHORD_6TH,
    GE_DEFAULT_CHORD_MINOR_6TH,
    GE_DEFAULT_CHORD_DIMINISHED,
    GE_DEFAULT_CHORD_DIMINISHED_7TH,
    GE_DEFAULT_CHORD_HALF_DIMINISHED_7TH,
    GE_DEFAULT_CHORD_AUGMENTED,
    GE_DEFAULT_CHORD_7TH_s5,
    GE_DEFAULT_CHORD_9TH,
    GE_DEFAULT_CHORD_7TH_s9,
    GE_DEFAULT_CHORD_MAJOR_9TH,
    GE_DEFAULT_CHORD_ADDED_9TH,
    GE_DEFAULT_CHORD_MINOR_9TH,
    GE_DEFAULT_CHORD_MINOR_ADD_9TH,
    GE_DEFAULT_CHORD_11TH,
    GE_DEFAULT_CHORD_MINOR_11TH,
    GE_DEFAULT_CHORD_7TH_s11,
    GE_DEFAULT_CHORD_MAJOR_7TH_s11,
    GE_DEFAULT_CHORD_13TH,
    GE_DEFAULT_CHORD_MAJOR_13TH,
    GE_DEFAULT_CHORD_MINOR_13TH,
    GE_DEFAULT_CHORD_SUSPENDED_4TH,
    GE_DEFAULT_CHORD_SUSPENDED_2ND,
    GE_DEFAULT_CHORD_UNISON,
    GE_DEFAULT_CHORD_SEMITONE_INTERVAL,
    GE_DEFAULT_CHORD_WHOLE_TONE_INTERVAL,
    GE_DEFAULT_CHORD_MINOR_3RD_INTERVAL,
    GE_DEFAULT_CHORD_MAJOR_3RD_INTERVAL,
    GE_DEFAULT_CHORD_PERFECT_4TH_INTERVAL,
    GE_DEFAULT_CHORD_TRITONE_INTERVAL,
    GE_DEFAULT_CHORD_PERFECT_5TH_INTERVAL,
    GE_DEFAULT_CHORD_MINOR_6TH_INTERVAL,
    GE_DEFAULT_CHORD_MAJOR_6TH_INTERVAL,
    GE_DEFAULT_CHORD_MINOR_7TH_INTERVAL,
    GE_DEFAULT_CHORD_MAJOR_7TH_INTERVAL,
    GE_DEFAULT_CHORD_PERFECT_OCTAVE,

    /**
     * Max enum.
     */
    MAX_GE_DEFAULT_CHORD
} GE_DefaultChord;

typedef struct GE_Chord GE_Chord;

struct GE_Chord {
    char *name;
    char *formula;
    int root;
    int numPitches;
    float *pitches;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get a human readable name of a default chord.
 * @param defaultChord The default chord.
 * @returns A human redable name or an empty string if @p defaultChord is invalid.
 */
const char* GE_GetDefaultChordName(GE_DefaultChord defaultChord);

/**
 * Get the formula for a default chord.
 * @param defaultChord The default chord.
 * @returns The formula or an empty string if @p defaultChord is invalid.
 */
const char* GE_GetDefaultChordFormula(GE_DefaultChord defaultChord);

/**
 * Find a default chord by name (case insensetive).
 * @param name The name of the chord to find.
 * @returns The default chord enum or a negative value if @p name was not found.
 */
int GE_FindDefaultChord(const char *name);

/**
 * Create a new (empty) chord instance.
 * @return Pointer to a GE_Chord.
 */
GE_Chord * GE_Chord_New();

/**
 * Create a new chord using a default chord formula.
 * @param defaultChord The default chord.
 * @param root The root pitch.
 * @return Pointer to a GE_Chord.
 */
GE_Chord * GE_Chord_NewFromDefault(GE_DefaultChord defaultChord, int root);

/**
 * Free a chord.
 *
 * @param chord Pointer to GE_Chord.
 */
void GE_Chord_Free(GE_Chord *chord);

/**
 * Get the name of the chord.
 *
 * @param chord Pointer to GE_Chord.
 */
const char* GE_Chord_GetName(const GE_Chord *chord);

/**
 * Set the name of the chord.
 *
 * @param chord Pointer to GE_Chord.
 * @param name The chord name.
 */
void GE_Chord_SetName(GE_Chord *chord, const char *name);

/**
 * Get the formula of the chord.
 *
 * @param chord Pointer to GE_Chord.
 */
const char* GE_Chord_GetFormula(const GE_Chord *chord);

/**
 * Set the formula of the chord.
 *
 * @param chord Pointer to GE_Chord.
 * @param formula The chord formula.
 * @returns True if the formula was updated, false if the formula is invalid.
 */
bool GE_Chord_SetFormula(GE_Chord *chord, const char *formula);

/**
 * Get the root of the chord.
 *
 * @param chord Pointer to GE_Chord.
 */
int GE_Chord_GetRoot(const GE_Chord *chord);

/**
 * Set the root of the chord.
 *
 * @param chord Pointer to GE_Chord.
 * @param root The chord root pitch.
 */
void GE_Chord_SetRoot(GE_Chord *chord, int root);

/**
 * Get the number of pitches the chord has.
 *
 * @param chord Pointer to a GE_Chord.
 * @return The number of pitches.
 */
int GE_Chord_GetNumPitches(const GE_Chord *chord);

/**
 * Get a pointer to the pitches.
 *
 * @param chord Pointer to a GE_Chord.
 * @return The pitches or NULL if the chord has no pitches.
 */
const float* GE_Chord_GetPitches(const GE_Chord *chord);

/**
 * Get the current state of chord.
 * @param chord Pointer to a GE_Chord.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will always return a dict, never NULL.
 */
GE_Variant * GE_Chord_GetState(GE_Chord *chord);

/**
 * Set the state of chord.
 * @param chord Pointer to a GE_Chord.
 * @param state The state to set.
 * @return True on success, otherwise false.
 */
bool GE_Chord_SetState(GE_Chord *chord, const GE_Variant *state);

/**
 * Dump debug info (text) to stream.
 * @param chord The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_Chord
 */
void GE_Chord_DebugDump(const GE_Chord *chord, FILE *f, unsigned int indent);

#ifdef __cplusplus
}
#endif

#endif /* GE_CHORD_H */
