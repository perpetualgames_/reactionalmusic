/**
 * @file instrument.h
 * @brief Representation of an instrument to be used with the engine.
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * GE_Instrument is a representation of a Instrument which is used internally
 * by the engine. The engine can be configured with a number of Instruments at
 * creation by using an GE_EngineConfig. An instrument has a pulse generator
 * and a pitch generator which are used to produce note on/off events. The
 * generators can be controlled, and even switched out to other types to alter
 * the instruments behaviour.
 */

#ifndef GE_INSTRUMENT_H
#define GE_INSTRUMENT_H

#include "GE_Core/common.h"
#include "GE_Core/component.h"
#include "GE_Core/cursor.h"
#include "GE_Core/generators/default_pitch_gen.h"
#include "GE_Core/generators/default_pulse_gen.h"
#include "GE_Core/utils/constants.h"
#include "GE_Core/utils/variant.h"

/**
 * Defines the various sound controls.
 */
typedef enum GE_SoundControl {

    /**
     * Sound control for volume.
     */
    GE_SOUND_CONTROL_VOLUME,

    /**
     * Sound control for panning.
     */
    GE_SOUND_CONTROL_PAN,

    /**
     * Sound control for modulation.
     */
    GE_SOUND_CONTROL_MODULATION,

    /**
     * Sound control for reverb dry/wet.
     */
    GE_SOUND_CONTROL_REVERB,

    /**
     * Sound control for distortion amount.
     */
    GE_SOUND_CONTROL_DISTORTION,

    /**
     * Custom sound control 1.
     */
    GE_SOUND_CONTROL_CUSTOM1,

    /**
     * Custom sound control 2.
     */
    GE_SOUND_CONTROL_CUSTOM2,

    /**
     * Custom sound control 3.
     */
    GE_SOUND_CONTROL_CUSTOM3,

    /**
     * Custom sound control 4.
     */
    GE_SOUND_CONTROL_CUSTOM4,

    /**
     * Max number of sound controls.
     */
    MAX_GE_SOUND_CONTROL

} GE_SoundControl;

#define GE_SOUND_CONTROL_NO_VALUE (NAN)

/**
 * @see struct GE_Voice
 */
typedef struct GE_Voice GE_Voice;

#ifndef GE_ENGINE_STRUCT
#define GE_ENGINE_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Engine
 */
typedef struct GE_Engine GE_Engine;
#endif

#ifndef GE_INSTRUMENT_STRUCT
#define GE_INSTRUMENT_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Instrument
 */
typedef struct GE_Instrument GE_Instrument;
#endif

/**
 * A voice used by the instrument.
 */
struct GE_Voice {

    /**
     * True means active.
     */
    bool active;

    /**
     * Used to detect pitch-changed as well as know which MIDI note to turn off.
     */
    float pitch;

    /**
     * Beat time of note off. Used by SeqGen.
     */
    double beat; // TODO: remove

    /**
     * The tag of the gate that initiated this voice
     */
    int gate_tag;
};

/**
 * An instrument as mapped by the engine.
 */
struct GE_Instrument {

    /**
     * Component setup, polymorphic struct.
     */
    GE_Component component;

    /**
     * The index of the instrument in the engine's instrument array.
     */
    int index;

    /**
     * The owning engine.
     */
    GE_Engine *engine;

    /**
     * The cursor used by the instrument.
     */
    GE_Cursor *cursor;

    /**
     * The pulse generator used by the instrument.
     */
    GE_PulseGen *pulseGen;

    /**
     * The pitch generator used by the instrument.
     */
    GE_PitchGen *pitchGen;

    /**
     * Voices.
     */
    GE_Voice voices[GE_MAX_VOICES]; // voice tracking

    /**
     * Internal use only.
     */
    uint32_t trigCounter;

    /**
     * Current host time.
     */
    uint64_t midiTime;

    /**
     * The MIDI channel.
     */
    int midiChannel;

    /**
     * A user managed title.
     */
    char *title;

    /**
     * Sound controls.
     */
    GE_ControlInput *soundControls[MAX_GE_SOUND_CONTROL];

    /**
     * Keep track of previous sound control inputs.
     * @note These are only updated if soundControlCallback is != NULL.
     */
    double soundControlValues[MAX_GE_SOUND_CONTROL];

    /**
     * The control indices (can be MIDI CC numbers).
     */
    int soundControlIndices[MAX_GE_SOUND_CONTROL];

    /**
     * Control to mute the instrument.
     * No new notes will be started if the instrument is muted.
     */
    GE_ControlInput *control_mute;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new instrument.
 * @param engine Pointer to GE_Engine.
 * @param index The instrument's index in the engine instrument array.
 * @returns A new instrument.
 * @note Used internally by GE_Engine.
 */
GE_Instrument* GE_Instrument_New(GE_Engine *engine, int index);

/**
 * Free a instrument.
 * @param instr The instrument.
 * @note Used internally by GE_Engine, do not free instruments that belong to an engine.
 */
void GE_Instrument_Free(GE_Instrument *instr);

/**
 * Get the index number of a sound control.
 * @param instr The instrument.
 * @param control The sound control enum.
 * @returns The sound control index.
 */
int GE_Instrument_GetSoundControlIndex(GE_Instrument *instr, GE_SoundControl control);

/**
 * Set the index number of a sound control.
 * @param instr The instrument.
 * @param control The sound control enum.
 * @param index The new sound control index number.
 */
void GE_Instrument_SetSoundControlIndex(GE_Instrument *instr, GE_SoundControl control, int index);

/**
 * Get the number of sound controls.
 * @param instr The instrument.
 * @returns The number of sound controls.
 */
int GE_Instrument_GetNumSoundControls(const GE_Instrument *instr);

/**
 * Find a sound control.
 * @param instr The Instrument.
 * @param name The name to find.
 * @returns The sound control index or -1 if @p name was not found.
 */
int GE_Instrument_FindSoundControl(const GE_Instrument *instr, const char *name);

/**
 * Get a control source.
 * @param instr The Instrument.
 * @param index The index of the control source to get.
 * @returns The control source or NULL if @p index was out of range.
 */
GE_ControlInput* GE_Instrument_GetSoundControl(const GE_Instrument *instr, int index);

/**
 * Get the index of the instrument.
 * @param instrument The instrument.
 * @returns The index.
 */
int GE_Instrument_GetIndex(const GE_Instrument *instrument);

/**
 * Process the instrument.
 * @param instr The instrument.
 * @note Used internally by GE_Engine, do not process instruments that belong to an engine.
 */
void GE_Instrument_Process(GE_Instrument *instr);

/**
 * Get the cursor used by the instrument
 * @param instr The instrument.
 * @returns The cursor or NULL if the instrument has no cursor.
 */
GE_Cursor* GE_Instrument_GetCursor(const GE_Instrument *instr);

/**
 * Map a cursor to the Pulse and Pitch generators of this Instrument.
 * @param instr The instrument.
 * @param cursor Instance of GE_Cursor
 *
 * Will use Cursor X for pulse generator and Cursor Y to pitch generator.
 */
void GE_Instrument_SetCursor(GE_Instrument *instr, GE_Cursor *cursor);

/**
 * Get Pulse generator for this instrument.
 * @param instr The instrument.
 * @returns A pulse generator or NULL if the instrument has no pulse generator.
 */
GE_PulseGen* GE_Instrument_GetPulseGen(const GE_Instrument *instr);

/**
 * Set Pulse generator for this instrument.
 * @param instr The instrument.
 * @param gentype The generator type.
 * @returns The set generator or NULL if @p gentype was invalid or is not a pulse generator type.
 * @note The returned generator is memory managed by the instrument, do not free manually.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 */
GE_PulseGen* GE_Instrument_SetPulseGenType(GE_Instrument *instr, const GE_GeneratorType *gentype);

/**
 * Set Pulse generator for this instrument.
 * @param instr The instrument.
 * @param name The name of the generator.
 * @returns The set generator or NULL if @p name was not found or the found generator is not a pulse generator.
 * @note The returned generator is memory managed by the instrument, do not free manually.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 *
 * The available types are registered with the engine using GE_Engine_AddGeneratorType().
 * You can iterate all registered engine types using GE_Engine_GetNumGeneratorTyps()
 * and GE_Engine_GetGeneratorType().
 */
GE_PulseGen* GE_Instrument_SetPulseGen(GE_Instrument *instr, const char *name);

/**
 * Get Pitch generator for this instrument.
 * @param instr The instrument.
 * @returns A pitch generator or NULL if the instrument has no pitch generator.
 */
GE_PitchGen* GE_Instrument_GetPitchGen(const GE_Instrument *instr);

/**
 * Set Pitch generator for this instrument.
 * @param instr The instrument.
 * @param gentype The generator type.
 * @returns The set generator or NULL if @p gentype was invalid or is not a pitch generator type.
 * @note The returned generator is memory managed by the instrument, do not free manually.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 */
GE_PitchGen* GE_Instrument_SetPitchGenType(GE_Instrument *instr, const GE_GeneratorType *gentype);

/**
 * Set Pitch generator for this instrument.
 * @param instr The instrument.
 * @param name The name of the generator.
 * @returns The set generator or NULL if @p name was not found or the found generator is not a pitch generator.
 * @note If the instrument has no cursor this function does nothing and returns NULL.
 * @note The returned generator is memory managed by the instrument, do not free manually.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 * @see GE_Instrument_SetCursor()
 *
 * The available types are registered with the engine using GE_Engine_AddGeneratorType().
 * You can iterate all registered engine types using GE_Engine_GetNumGeneratorTyps()
 * and GE_Engine_GetGeneratorType().
 */
GE_PitchGen* GE_Instrument_SetPitchGen(GE_Instrument *instr, const char *name);

/**
 * Return the current state in a dict variant.
 * @param instr The instrument.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will never return NULL (even though the variant may be an empty dict).
 *
 * Used to save states.
 * @relates GE_Instrument
 */
GE_Variant* GE_Instrument_GetState(const GE_Instrument *instr);

/**
 * Set the current instrument state using a dict variant.
 * @param instr The instrument.
 * @param state A dict variant with the state, pass NULL to clear instrument.
 * @returns True if the state was set, false on failure.
 * @note Render lock will be in effect, render may fail on trylock while the function is executed.
 *
 * Used to load states.
 * @relates GE_Instrument
 */
bool GE_Instrument_SetState(GE_Instrument *instr, const GE_Variant *state);

/**
 * Get a ControlSource from its identifier.
 * @param instr The instrument.
 * @param name The ControlSource name used as identifier.
 * @returns Pointer to GE_ControlSource or NULL if not found.
 */
GE_ControlSource* GE_Instrument_GetControlSourceByName(const GE_Instrument *instr, const char *name);

/**
 * Dump debug info (text) to stream.
 * @param instr The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 */
void GE_Instrument_DebugDump(const GE_Instrument *instr, FILE *f, unsigned int indent);

/**
 * Get user managed title from the instrument.
 * @param instrument The instrument to get the title from.
 * @returns The title, can be empty but never NULL.
 */
const char* GE_Instrument_GetTitle(const GE_Instrument *instrument);

/**
 * Set user managed title for the instrument.
 * @param instrument The instrument to set the title for.
 * @param title The title or NULL for no title.
 */
void GE_Instrument_SetTitle(GE_Instrument *instrument, const char *title);

/**
 * Get the channel currently used by the instrument.
 * @param instrument The instrument to get the channel from.
 * @returns The channel.
 */
int GE_Instrument_GetMIDIChannel(const GE_Instrument *instrument);

/**
 * Set the channel currently used by the instrument.
 * @param instrument The instrument to set the channel for.
 * @param channel The channel.
 */
void GE_Instrument_SetMIDIChannel(GE_Instrument *instrument, int channel);

/**
 * Start note.
 * @note Used internally by instrument, do not call this manually!
 * @param instr The instrument.
 * @param voiceIndex The voice index.
 * @param beat The beat time relative to the engine clock.
 * @param pitch MIDI note number of the pitch as a floating point number (supports non-equal temperament pitches).
 * @param velocity Note velocity as a floating point number (normalized [0..1]).
 */
void GE_Instrument_StartNote(GE_Instrument *instr, int voiceIndex, double beat, float pitch, float velocity);

/**
 * Stop note.
 * @note Used internally by instrument, do not call this manually!
 * @param instr The instrument.
 * @param voiceIndex The voice index.
 * @param beat The beat time relative to the engine clock.
 */
void GE_Instrument_StopNote(GE_Instrument *instr, int voiceIndex, double beat);

#ifdef __cplusplus
}
#endif

#endif /* _GE_INSTRUMENT_H */
