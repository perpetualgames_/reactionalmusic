/**
 * @file control_input.h
 * @brief GE_ControlInput header
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright © 2016 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * GE_ControlInput are used by generators to access and modify their
 * parameters. Think of them as "knobs" on a synthesizer.
 *
 * A GE_ControlInput can be coupled with a GE_ControlSource which will be used
 * internally by the ControlSource to produce a (possibly scaled) output value.
 * One example would be to use one of the ControlSources provided by GE_Engine,
 * "Slider 1", and map that source to a pitch generators "Transpose in scale"
 * ControlInput.
 *
 * @see GE_Generator_FindControlInput and GE_Generator_GetControlInput for more
 * information.
 * 
 */

#ifndef CONTROL_INPUT_H
#define CONTROL_INPUT_H

#include "GE_Core/common.h"
#include "GE_Core/component.h"
#include "GE_Core/control_source.h"
#include "GE_Core/utils/variant.h"

#ifndef GE_ENGINE_STRUCT
#define GE_ENGINE_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Engine
 */
typedef struct GE_Engine GE_Engine;
#endif

#ifndef GE_INSTRUMENT_STRUCT
#define GE_INSTRUMENT_STRUCT
/**
 * Forward declaration.
 * @see struct GE_Instrument
 */
typedef struct GE_Instrument GE_Instrument;
#endif

/**
 * @see struct GE_ControlInput
 */
typedef struct GE_ControlInput GE_ControlInput;

/**
 * Used to format the value of a control input.
 * @param ctrl The control input.
 * @param[out] buf Return the formatted string here.
 * @param size The maximum size of the returned string, including zero terminator.
 * @param withUnit Add a unit suffix.
 * @returns The length of the returned string, excluding zero terminator.
 * @relates GE_ControlInput
 */
typedef size_t (*GE_ControlInputValueFormatter)(const GE_ControlInput *ctrl, char *buf, size_t size, bool withUnit);

typedef enum {

    /**
     * @brief Use the minimum value of GE_ControlInput for the viaSource multiplication.
     */
    GE_VIA_ZERO_MIN,

    /**
     * @brief Use the value between min/max of GE_ControlInput for the viaSource multiplication.
     */
    GE_VIA_ZERO_MID,

    /**
     * @brief Use the maximum value of GE_ControlInput for the viaSource multiplication.
     */
    GE_VIA_ZERO_MAX,

    /**
     * @brief Max enum.
     */
    MAX_VIA_ZERO_POINT,

} GE_ViaZeroPoint;

struct GE_ControlInput {

    /**
     * Component setup, polymorphic struct.
     */
    GE_Component component;

    /**
     * A name that is unique per generator.
     */
    char *name;

    /**
     * A human readable and very short description.
     */
    char *description;

    /**
     * Minimum value.
     */
    double min;

    /**
     * Maximum value.
     */
    double max;

    /**
     * Real value, between min and max.
     */
    double value;

    /**
     * 0 - 1, maps linearly.
     */
    double normValue;

    /**
     * Quantization granularity.
     */
    double quant;

    /**
     * The default value.
     */
    double defaultValue;

    /**
     * ControlSource, NULL for manual control (min/max is ignored).
     */
    GE_ControlSource *source;

    /**
     * Multiply via another ControlSource, default NULL.
     */
    GE_ControlSource *viaSource;

    /**
     * Pointer to format function.
     * @see GE_ControlInputValueFormatter
     */
    GE_ControlInputValueFormatter valueFormatter;

    double mapMin;
    double mapMid;
    double mapMax;
    GE_ViaZeroPoint viaZeroPoint;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new ControlInput.
 * @param name String used for identification.
 * @param description A human readable, short description.
 * @param minValue Minimum value.
 * @param maxValue Maximum value.
 * @param defaultValue Default value.
 * @param quant The granularity of the value, 0.01 equals two decimal. 0.0 means don't quantize.
 * @param formatter Function pointer to format the value as string, or NULL.
 * @return Pointer to GE_ControlInput.
 * @note If @p minValue > @p maxValue they will be flipped.
 */
GE_ControlInput* GE_ControlInput_New(const char *name, const char *description, double minValue, double maxValue, double defaultValue, double quant, GE_ControlInputValueFormatter formatter);

/**
 * Free a ControlInput.
 * @param ctrl Pointer to GE_ControlInput.
 * @relates GE_ControlInput
 */
void GE_ControlInput_Free(GE_ControlInput *ctrl);

/**
 * Get the source of a ControlInput.
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The control source or NULL if the input has no source.
 * @relates GE_ControlInput
 */
GE_ControlSource* GE_ControlInput_GetSource(const GE_ControlInput *ctrl);

/**
 * Set the source of a ControlInput.
 * @param ctrl Pointer to GE_ControlInput.
 * @param source Pointer to GE_ControlSource.
 * @relates GE_ControlInput
 */
void GE_ControlInput_SetSource(GE_ControlInput *ctrl, GE_ControlSource *source);

/**
 * Get the via source of a ControlInput.
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The control via source or NULL if the input has no source.
 * @relates GE_ControlInput
 */
GE_ControlSource* GE_ControlInput_GetViaSource(const GE_ControlInput *ctrl);

/**
 * Set another ControlSource which will be used to multiply the original source.
 *
 * @param ctrl Pointer to GE_ControlInput.
 * @param viaSource Pointer to GE_ControlSource.
 * @relates GE_ControlInput
 */
void GE_ControlInput_SetViaSource(GE_ControlInput *ctrl, GE_ControlSource *viaSource);

/**
 * Get the min and max range of a ControlInput.
 * @param ctrl Pointer to GE_ControlInput.
 * @param[out] minimum Minimum value, NULL to omit.
 * @param[out] maximum Maximum value, NULL to omit.
 * @relates GE_ControlInput
 */
void GE_ControlInput_GetRange(const GE_ControlInput *ctrl, double *minimum, double *maximum);

/**
 * Get the minimum value.
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The minimum value.
 * @relates GE_ControlInput
 */
double GE_ControlInput_GetMinValue(const GE_ControlInput *ctrl);

/**
 * Get the maximum value.
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The maximum value.
 * @relates GE_ControlInput
 */
double GE_ControlInput_GetMaxValue(const GE_ControlInput *ctrl);

/**
 * Get the default value.
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The default value.
 * @relates GE_ControlInput
 */
double GE_ControlInput_GetDefaultValue(const GE_ControlInput *ctrl);

/**
 * Set the default value.
 * @param ctrl Pointer to GE_ControlInput.
 * @param value The default value.
 * @relates GE_ControlInput
 */
void GE_ControlInput_SetDefaultValue(GE_ControlInput *ctrl, double value);

/**
 * Reset the control input to its default value.
 *
 * @param ctrl Pointer to GE_ControlInput.
 * @relates GE_ControlInput
 */
void GE_ControlInput_ResetToDefault(GE_ControlInput *ctrl);

/**
 * Returns the quantization granularity.
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The quant.
 * @relates GE_ControlInput
 */
double GE_ControlInput_GetQuant(const GE_ControlInput *ctrl);

/**
 * Set the quantization granularity.
 * @param ctrl Pointer to GE_ControlInput.
 * @param quant The default value.
 * @relates GE_ControlInput
 */
void GE_ControlInput_SetQuant(GE_ControlInput *ctrl, double quant);

/**
 * Get the current value.
 *
 * @param ctrl Pointer to GE_ControlInput.
 * @returns The value.
 * @relates GE_ControlInput
 */
double GE_ControlInput_GetValue(const GE_ControlInput *ctrl);

/**
 * Get the current value from a "triggerable" source.
 * @see GE_ControlSource for more information.
 *
 * @param ctrl Pointer to GE_ControlInput.
 * @param trig If true, get the new value. If false return the last value.
 * @returns The value.
 * @relates GE_ControlInput
 */
double GE_ControlInput_GetValueTriggered(GE_ControlInput *ctrl, bool trig);

/**
 * Set the current value.
 * @param ctrl The control input.
 * @param value The new value.
 * @relates GE_ControlInput
 */
void GE_ControlInput_SetValue(GE_ControlInput *ctrl, double value);

/**
 * Recalculate the @ref mapMid value based on the current min and max values.
 * @relates GE_ControlInput
 */
void GE_ControlInput_CenterMapMid(GE_ControlInput *ctrl);

/**
 * Returns a formatted value.
 * @param ctrl The control input to format the value for.
 * @param buf A char buffer
 * @param size The maximum size of the string including zero terminator.
 * @param withUnit Add a unit suffix.
 * @returns The length of the returned string, excluding zero terminator.
 * @note If GE_ControlInput->valueFormatter is non-NULL it will override the default.
 * @see GE_ControlInput_SetValueFormatter() and GE_ControlInput_GetValueFormatter()
 *
 * This function is suitable to use as a function pointer for GE_ControlInput->valueFormatter.
 *
 * The default is to return a raw value without conversion or unit suffixes.
 * @relates GE_ControlInput
 */
size_t GE_ControlInput_GetFormattedValue(const GE_ControlInput *ctrl, char *buf, size_t size, bool withUnit);

/**
 * Get the function overriding GE_GetControLInput_GetFormattedValue().
 * @param ctrl The control input.
 * @returns A pointer to the override function or NULL for default implementation.
 * @relates GE_ControlInput
 */
GE_ControlInputValueFormatter GE_ControlInput_GetValueFormatter(const GE_ControlInput *ctrl);

/**
 * Set the function overriding GE_GetControLInput_GetFormattedValue().
 * @param ctrl The control input.
 * @param formatter A pointer to the override function or NULL for default implementation.
 * @relates GE_ControlInput
 */
void GE_ControlInput_SetValueFormatter(GE_ControlInput *ctrl, GE_ControlInputValueFormatter formatter);

/**
 * Formats value to string, cast to signed integer.
 * @param ctrl The control input to format the value for.
 * @param buf A char buffer
 * @param size The maximum size of the string including zero terminator.
 * @param withUnit Not used for this formatter.
 * @see GE_ControlInput_SetValueFormatter()
 *
 * This function is suitable to use as a function pointer for GE_ControlInput->valueFormatter
 * @relates GE_ControlInput
 */
size_t GE_ControlInput_SignedIntegerStringFormatter(const GE_ControlInput *ctrl, char *buf, size_t size, bool withUnit);

/**
 * Formats value to a percentage string, use withUnits to add a % sign.
 * @param ctrl The control input to format the value for.
 * @param buf A char buffer
 * @param size The maximum size of the string including zero terminator.
 * @param withUnit Add a '%' sign to the format string. 
 * @see GE_ControlInput_SetValueFormatter()
 *
 * This function is suitable to use as a function pointer for GE_ControlInput->valueFormatter
 * @relates GE_ControlInput
 */
size_t GE_ControlInput_PercentageStringFormatter(const GE_ControlInput *ctrl, char *buf, size_t size, bool withUnit);

/**
 * Dump debug info (text) to stream.
 * @param ctrl The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_ControlInput
 */
void GE_ControlInput_DebugDump(const GE_ControlInput *ctrl, FILE *f, unsigned int indent);

/**
 * Return the current state in a dict variant.
 * @param ctrl Pointer to a GE_ControlInput.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will never return NULL (even though the variant may be an empty dict).
 *
 * Used to save states.
 * @relates GE_ControlInput
 */
GE_Variant * GE_ControlInput_GetState(const GE_ControlInput *ctrl);

/**
 * Set the current state using a dict variant.
 * @param ctrl Pointer to a GE_ControlInput.
 * @param instr Pointer to a GE_Instrument.
 * @param state A dict variant with the state, pass NULL to reset the ControlInput to default values.
 * @returns True if the state was set, false on failure.
 *
 * Used to load states.
 * @relates GE_ControlInput
 */
bool GE_ControlInput_SetState(GE_ControlInput *ctrl, GE_Instrument *instr, const GE_Variant *state);

/**
 * Get the per-cursor (or engine) unique control input name.
 * @param input The control input to get the name from.
 * @returns The name, can be empty but never NULL.
 * @note Names are unique within each cursor and the engine.
 */
const char* GE_ControlInput_GetName(const GE_ControlInput *input);

/**
 * Get a human readable and short description.
 * @param input The control input to get the description from.
 * @returns The description, can be empty but never NULL.
 */
const char* GE_ControlInput_GetDescription(const GE_ControlInput *input);

/**
 * Set the "zero point" value for the viaSource multiplication.
 * @param input The control input.
 * @param zeroPoint The 
 */
void GE_ControlInput_SetViaZeroPoint(GE_ControlInput *input, GE_ViaZeroPoint zeroPoint);

/**
 * Get the "zero point" value for the viaSource multiplication.
 * @param input The control input.
 * @returns The current zeroPoint enum value.
 */
GE_ViaZeroPoint GE_ControlInput_GetViaZeroPoint(const GE_ControlInput *input);

#ifdef __cplusplus
}
#endif

#endif /* CONTROL_INPUT_H */
