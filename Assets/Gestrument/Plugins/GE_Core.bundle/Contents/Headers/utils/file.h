/**
* @file file.h
* @brief Utility functions for working with files.
* @author David Granström
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_FILE_H
#define GE_FILE_H

#include "GE_Core/common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Read a text file.
 *
 * The caller is responsible for deallocating the result.
 *
 * @param file The file stream to read from.
 * @param[out] result File content.
 * @returns The number of chars read or negative value on error.
 */
int GE_File_Read(FILE *file, char **result);

/**
 * Read lines from a text file.
 *
 * The caller is responsible for deallocating the result.
 * @see GE_String_ArrayFree
 *
 * @param file The file stream to read from.
 * @param[out] result An array of strings, one for each line.
 * @returns The number of lines read or negative value on error.
 */
int GE_File_ReadLines(FILE *file, char ***result);

#ifdef __cplusplus
}
#endif

#endif /* GE_FILE_H */
