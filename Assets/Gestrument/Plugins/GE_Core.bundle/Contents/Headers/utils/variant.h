/**
 * @file variant.h
 * @brief GE_Variant header
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_VARIANT_H
#define GE_VARIANT_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "GE_Core/common.h"

/**
 * Define a literal NULL variant.
 */
#define GE_VARIANT_LITERAL_NULL() ((GE_Variant){GE_VARIANT_TYPE_NULL, {0}})

/**
 * Define a literal integer variant.
 * @param value Starting value.
 */
#define GE_VARIANT_LITERAL_INTEGER(value) ((GE_Variant){GE_VARIANT_TYPE_INTEGER, {.vInteger = value }})

/**
 * Define a literal real variant.
 * @param value Starting value.
 */
#define GE_VARIANT_LITERAL_REAL(value) ((GE_Variant){GE_VARIANT_TYPE_REAL, {.vReal = value }})

/**
 * Define a literal boolean variant.
 * @param value Starting value.
 */
#define GE_VARIANT_LITERAL_BOOLEAN(value) ((GE_Variant){GE_VARIANT_TYPE_BOOLEAN, {.vBoolean = value }})

/**
 * Define a literal string variant.
 * @param value Starting value.
 */
#define GE_VARIANT_LITERAL_STRING(value) ((GE_Variant){GE_VARIANT_TYPE_STRING, {.vString = value }})

/**
 * Define a literal data variant.
 * @param size The size of the data.
 * @param data The data.
 */
#define GE_VARIANT_LITERAL_DATA(size, data) ((GE_Variant){GE_VARIANT_TYPE_DATA, {.vData = {size, data}}})

/**
 * Define a literal list variant.
 * @param value Starting value.
 */
#define GE_VARIANT_LITERAL_LIST(value) ((GE_Variant){GE_VARIANT_TYPE_LIST, {0}})

/**
 * Define a literal dict variant.
 * @param value Starting value.
 */
#define GE_VARIANT_LITERAL_DICT(value) ((GE_Variant){GE_VARIANT_TYPE_DICT, {0}})

/**
 * Get a human readable name of a variant type.
 * @param t The variant type to get the name for.
 * @returns A pointer to the type name or an empty string if @p t is invalid.
 */
#define GE_VARIANT_TYPE_NAME(t) ((t) >= GE_VARIANT_TYPE_NONE && (t) < MAX_GE_VARIANT_TYPE ? GE_VARIANT_TYPE_NAMES[(t)] : "")

/**
 * How to format GE_VariantInteger using printf etc.
 */
#define GE_VARIANT_INTEGER_FORMAT PRId64

/**
 * Max variant integer value.
 */
#define GE_VARIANT_INTEGER_MAX INT64_MAX

/**
 * Min variant integer value.
 */
#define GE_VARIANT_INTEGER_MIN INT64_MIN

/**
 * The number of buckets to use for the dictionary.
 */
#define GE_VARIANT_DICT_NUM_BUCKETS 1024

/**
 * What type a variant value is.
 */
typedef enum GE_VariantType {

    /**
     * No value.
     */
    GE_VARIANT_TYPE_NONE,

    /**
     * NULL.
     */
    GE_VARIANT_TYPE_NULL,

    /**
     * `GE_VariantInteger`
     */
    GE_VARIANT_TYPE_INTEGER,

    /**
     * `GE_VariantReal`
     */
    GE_VARIANT_TYPE_REAL,

    /**
     * `bool`
     */
    GE_VARIANT_TYPE_BOOLEAN,

    /**
     * `char *`
     */
    GE_VARIANT_TYPE_STRING,

    /**
     * `void *`
     */
    GE_VARIANT_TYPE_DATA,

    /**
     * `GE_Variant`
     */
    GE_VARIANT_TYPE_LIST,

    /**
     * `GE_Variant`
     */
    GE_VARIANT_TYPE_DICT,

    /**
     * Max enum.
     */
    MAX_GE_VARIANT_TYPE

} GE_VariantType;

/**
 * The integer type.
 */
typedef int64_t GE_VariantInteger;

/**
 * The real type.
 */
typedef double GE_VariantReal;

/**
 * @see struct GE_Variant
 */
typedef struct GE_Variant GE_Variant;

/**
 * @see struct GE_VariantPair
 */
typedef struct GE_VariantPair GE_VariantPair;

/**
 * @see struct GE_VariantIter
 */
typedef struct GE_VariantIter GE_VariantIter;

/**
 * Keep track of a variant value.
 */
struct GE_Variant {

    /**
     * The type of the value.
     */
    GE_VariantType type;

    /**
     * Variant value.
     */
    union {

        /**
         * GE_VARIANT_TYPE_INTEGER
         */
        GE_VariantInteger vInteger;

        /**
         * GE_VARIANT_TYPE_REAL
         */
        GE_VariantReal vReal;

        /**
         * GE_VARIANT_TYPE_BOOLEAN
         */
        bool vBoolean;

        /**
         * GE_VARIANT_TYPE_STRING
         */
        char *vString;

        /**
         * GE_VARIANT_TYPE_DATA
         */
        struct {

            /**
             * The size of the data.
             */
            GE_VariantInteger size;

            /**
             * The data.
             */
            void *data;

        } vData;

        /**
         * GE_VARIANT_TYPE_LIST
         */
        struct {
            
            /**
             * The number of variants.
             */
            GE_VariantInteger length;

            /**
             * Variants.
             */
            GE_Variant *variants;

        } vList;

        /**
         * GE_VARIANT_TYPE_DICT
         */
        struct {

            /**
             * The number of variant pairs.
             */
            GE_VariantInteger length;

            /**
             * Buckets for variants.
             */
            struct {
                
                /**
                 * The number key/variant pairs.
                 **/
                GE_VariantInteger length;

                /**
                 * The string key.
                 */
                char **keys;

                /**
                 * The variants.
                 */
                GE_Variant *variants;

            } *buckets;

        } vDict;

    } v;

};

/**
 * Used to initialize dictionaries.
 */
struct GE_VariantPair {

    /**
     * The key.
     */
    const char *key;

    /**
     * A pointer to the variant.
     */
    GE_Variant variant;

};

/**
 * Used to iterate dictionaries.
 */
struct GE_VariantIter {

    /**
     * Bucket index.
     */
    GE_VariantInteger bucket_index;

    /**
     * List index.
     */
    GE_VariantInteger list_index;

    /**
     * The current key.
     */
    const char *key;

    /**
     * A pointer to the variant.
     */
    GE_Variant *variant;

};


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Get variant type names in human readable form.
 * @param type The type.
 * @returns A pointer to the string or an empty string if @p type is invalid.
 */
const char *GE_VariantType_GetName(GE_VariantType type);

/**
 * Initialize a variant.
 * @param variant The variant struct to initialize as a copy.
 * @param type The type of the variant.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_Init(GE_Variant *variant, GE_VariantType type);

/**
 * Copy a variant.
 * @param variant The variant struct to initialize as a copy.
 * @param original The orignial variant that will be copied.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitCopy(GE_Variant *variant, const GE_Variant *original);

/**
 * Initialize a variant as NULL.
 * @param variant The variant struct to initialize.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitNULL(GE_Variant *variant);

/**
 * Initialize a variant as integer.
 * @param variant The variant struct to initialize.
 * @param value The initial value.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitInteger(GE_Variant *variant, GE_VariantInteger value);

/**
 * Initialize a variant as real.
 * @param variant The variant struct to initialize.
 * @param value The initial value.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitReal(GE_Variant *variant, GE_VariantReal value);

/**
 * Initialize a variant as boolean.
 * @param variant The variant struct to initialize.
 * @param value The initial value.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitBoolean(GE_Variant *variant, bool value);

/**
 * Initialize a variant as string.
 * @param variant The variant struct to initialize.
 * @param value The initial value.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitString(GE_Variant *variant, const char *value);

/**
 * Initialize a variant as data.
 * @param variant The variant struct to initialize.
 * @param size The size of the data.
 * @param value The initial value, NULL to zero the memory.
 * @note The memory of @p value will be copied to the variant.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 */
void GE_Variant_InitData(GE_Variant *variant, GE_VariantInteger size, const void *value);

/**
 * Initialize a variant as list.
 * @param variant The variant struct to initialize.
 * @param length The length of the @p variants array, use a negative value to look for sentinel.
 * @param variants The initial values, if length is negative it must end with a sentinel (zeroed) element.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * 
 * It's OK to pass NULL to @p variants with a @p length > 0, in which case the added
 * variants will be zeroed.
 *
 * If using negative @p length make sure the sentinel element ends the array.
 * Any element with an invalid variant type (including GE_VARIANT_TYPE_NONE) is
 * treated as the sentinel. As such you can end your array with {0}.
 * @relates GE_Variant
 */
void GE_Variant_InitList(GE_Variant *variant, GE_VariantInteger length, const GE_Variant *variants);

/**
 * Initialize a variant as dict.
 * @param variant The variant struct to initialize.
 * @param length The length of the @p variants array, use a negative value to look for sentinel.
 * @param pairs The initial values, if length is negative it must end with a sentinel element.
 * @note @p variant must be deinitialized using GE_Variant_Deinit().
 * @relates GE_Variant
 * 
 * If using negative @p length make sure the sentinel element ends the array.
 * A sentinel is a GE_VariantPair with the variant pointer set to NULL.
 */
void GE_Variant_InitDict(GE_Variant *variant, GE_VariantInteger length, const GE_VariantPair *pairs);

/**
 * Initialize a variant from a binary file.
 * @param variant The variant.
 * @param f Read the binary data from this file.
 * @returns The number of bytes read from @p f or a negative value on error.
 * @relates GE_Variant
 * @note The file must be opened in binary mode.
 */
GE_VariantInteger GE_Variant_InitFromBinaryFile(GE_Variant *variant, FILE *f);

/**
 * Deinitialize a variant.
 * @param variant The variant to deinitialize.
 * @relates GE_Variant
 */
void GE_Variant_Deinit(GE_Variant *variant);

/**
 * Create a new variant.
 * @param type The variant type.
 * @returns A new variant of the specified type that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_New(GE_VariantType type);

/**
 * Copy a variant.
 * @param original The orignial variant that will be copied.
 * @returns A copy of @p original, must be freed using GE_Variant_Free().
 * @relates GE_Variant
 */
GE_Variant* GE_Variant_NewCopy(const GE_Variant *original);

/**
 * Create a new null variant.
 * @returns A new null variant that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_NewNULL(void);

/**
 * Create a new integer variant.
 * @param value The initial value.
 * @returns A new integer variant that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_NewInteger(GE_VariantInteger value);

/**
 * Create a new real variant.
 * @param value The initial value.
 * @returns A new real variant that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_NewReal(GE_VariantReal value);

/**
 * Create a new boolean variant.
 * @param value The initial value.
 * @returns A new boolean variant that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_NewBoolean(bool value);

/**
 * Create a new string variant.
 * @param value The initial value.
 * @returns A new string variant that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_NewString(const char *value);

/**
 * Create a new data variant.
 * @param size The size of the data.
 * @param value The initial value.
 * @returns A new data variant that must be freed using GE_Variant_Free().
 */
GE_Variant* GE_Variant_NewData(GE_VariantInteger size, const void *value);

/**
 * Create a new list variant.
 * @param length The length of the @p variants array, use a negative value to look for sentinel.
 * @param variants The initial values, if length is negative it must end with a sentinel element.
 * @returns A new list variant that must be freed using GE_Variant_Free().
 * @see GE_Variant_InitList() for info on how the arguments work.
 * @see GE_VARIANT_LITERAL_SENTINEL()
 * @relates GE_Variant
 */
GE_Variant* GE_Variant_NewList(GE_VariantInteger length, const GE_Variant *variants);

/**
 * Create a new dict variant.
 * @param length The length of the @p variants array, use a negative value to look for sentinel.
 * @param pairs The initial values, if length is negative it must end with a sentinel element.
 * @returns A new dict variant that must be freed using GE_Variant_Free().
 * 
 * If using negative @p length make sure the sentinel element ends the array.
 * A sentinel is a GE_VariantPair with the variant pointer set to NULL.
 */
GE_Variant* GE_Variant_NewDict(GE_VariantInteger length, const GE_VariantPair *pairs);

/**
 * Create a variant from a binary file.
 * @param f Read the binary data from this file.
 * @param[out] retSize Return the number of read bytes here or a negative number on error.
 * @returns The new variant or NULL on error.
 * @relates GE_Variant
 * @note The file must be opened in binary mode.
 */
GE_Variant* GE_Variant_NewFromBinaryFile(FILE *f, GE_VariantInteger *retSize);

/**
 * Deinitialize and free a variant.
 * @param variant The variant to deinitialize and free.
 * @relates GE_Variant
 */
void GE_Variant_Free(GE_Variant *variant);

/**
 * Serialize a variant to binary format.
 * @param variant The variant.
 * @param f Write the binary data to this file, pass NULL to get the number of required bytes.
 * @returns The number of bytes written to @p f or a negative value on error.
 * @relates GE_Variant
 * @note The file must be opened in binary mode.
 */
GE_VariantInteger GE_Variant_SerializeToBinaryFile(const GE_Variant *variant, FILE *f);

/**
 * Check if the variant has a valid type.
 * @param variant The variant to get the value from.
 * @returns True if the variant is type is valid, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsValid(const GE_Variant *variant);

/**
 * Check if the variant is NULL.
 * @param variant The variant to get the value from.
 * @returns True if the variant is NULL, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsNULL(const GE_Variant *variant);

/**
 * Check if the variant is integer.
 * @param variant The variant to get the value from.
 * @returns True if the variant is integer, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsInteger(const GE_Variant *variant);

/**
 * Check if the variant is real.
 * @param variant The variant to get the value from.
 * @returns True if the variant is real, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsReal(const GE_Variant *variant);

/**
 * Check if the variant is boolean.
 * @param variant The variant to get the value from.
 * @returns True if the variant is boolean, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsBoolean(const GE_Variant *variant);

/**
 * Check if the variant is string.
 * @param variant The variant to get the value from.
 * @returns True if the variant is string, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsString(const GE_Variant *variant);

/**
 * Check if the variant is data.
 * @param variant The variant to get the value from.
 * @returns True if the variant is data, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsData(const GE_Variant *variant);

/**
 * Check if the variant is list.
 * @param variant The variant to get the value from.
 * @returns True if the variant is list, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsList(const GE_Variant *variant);

/**
 * Check if the variant is dict.
 * @param variant The variant to get the value from.
 * @returns True if the variant is dict, false if not.
 * @relates GE_Variant
 */
bool GE_Variant_GetIsDict(const GE_Variant *variant);

/**
 * Return the variant value as integer.
 * @param variant The variant to get the value from.
 * @param[out] retValue If the function returns true the value is returned here, NULL to omit.
 * @returns True for successful conversion, false if the variant can not be converted.
 * @relates GE_Variant
 */
bool GE_Variant_GetInteger(const GE_Variant *variant, GE_VariantInteger *retValue);

/**
 * Return the variant value as real.
 * @param variant The variant to get the value from.
 * @param[out] retValue If the function returns true the value is returned here, NULL to omit.
 * @returns True for successful conversion, false if the variant can not be converted.
 * @relates GE_Variant
 */
bool GE_Variant_GetReal(const GE_Variant *variant, GE_VariantReal *retValue);

/**
 * Return the variant value as boolean.
 * @param variant The variant to get the value from.
 * @param[out] retValue If the function returns true the value is returned here, NULL to omit.
 * @returns True for successful conversion, false if the variant can not be converted.
 * @relates GE_Variant
 */
bool GE_Variant_GetBoolean(const GE_Variant *variant, bool *retValue);

/**
 * Return the variant value as string.
 * @param variant The variant to get the string value from.
 * @returns If the variant is of type GE_VARIANT_TYPE_STRING a pointer to the string will be returned, otherwise NULL.
 * @note The variant string member is NULL a pointer to an empty string is returned.
 * @relates GE_Variant
 */
const char* GE_Variant_GetString(const GE_Variant *variant);

/**
 * Return the variant value as string.
 * @param variant The variant to get the value from.
 * @param size The size of @p retValue (including the zero terminator).
 * @param[out] retValue Return the string here, pass NULL to get the required length.
 * @returns The number of return bytes or zero if the conversion could not be made (or if the string is empty).
 * @relates GE_Variant
 */
GE_VariantInteger GE_Variant_GetAsString(const GE_Variant *variant, GE_VariantInteger size, char *retValue);

/**
 * Return a pointer to data.
 * @param variant The variant to get the value from.
 * @param[out] retData If a valid size is returned return a pointer to the data, NULL to omit.
 * @returns The size of the data or -1 if a data pointer could not be returned.
 * @relates GE_Variant
 * @note This function is only successful if the variant type is either
 *     GE_VARIANT_TYPE_DATA or GE_VARIANT_TYPE_STRING.
 */
GE_VariantInteger GE_Variant_GetData(const GE_Variant *variant, void **retData);

/**
 * Set the variant to the value of another variant.
 * @param variant The variant.
 * @param value The value.
 * @returns True if the value was set or false if a conversion failed.
 * @relates GE_Variant
 */
bool GE_Variant_Set(GE_Variant *variant, const GE_Variant *value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param value The value.
 * @returns True if the value was set or false if a conversion failed.
 * @relates GE_Variant
 */
bool GE_Variant_SetInteger(GE_Variant *variant, GE_VariantInteger value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param value The value.
 * @returns True if the value was set or false if a conversion failed.
 * @relates GE_Variant
 */
bool GE_Variant_SetReal(GE_Variant *variant, GE_VariantReal value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param value The value.
 * @returns True if the value was set or false if a conversion failed.
 * @relates GE_Variant
 */
bool GE_Variant_SetBoolean(GE_Variant *variant, bool value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param value The value.
 * @returns True if the value was set or false if a conversion failed.
 * @relates GE_Variant
 */
bool GE_Variant_SetString(GE_Variant *variant, const char *value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param size The size of the data.
 * @param data The data, will be copied to the variant, NULL will zero the new memory.
 * @returns True if the value was set or false if a conversion failed.
 * @note Any already existing data will be freed.
 * @relates GE_Variant
 */
bool GE_Variant_SetData(GE_Variant *variant, GE_VariantInteger size, const void *data);

/**
 * Compare two variants.
 * @param variant The first variant.
 * @param second The second variant.
 * @returns -1 if @p variant < @p second, 1 if @p variant is > @p second and 0 if equal.
 * @relates GE_Variant
 */
int GE_Variant_Cmp(const GE_Variant *variant, const GE_Variant *second);

/**
 * Get the length of a string, data, list or dict variant.
 * @param variant The variant string, data, list or dict to get the length from.
 * @returns The length of the variant or 0 if the variant is not a string, data, list or dict.
 * @relates GE_Variant
 */
GE_VariantInteger GE_Variant_GetLength(const GE_Variant *variant);

/**
 * Set the length of a string, data or list variant.
 * @param variant The variant list.
 * @param length The new length of the variant.
 * @returns True if the length was set.
 * @note Any newly allocated memory will be spaces for strings, zeroes for data and NULL variants for lists.
 * @note For strings length is excluding the zero terminator.
 * @relates GE_Variant
 */
bool GE_Variant_SetLength(GE_Variant *variant, GE_VariantInteger length);

/**
 * Get the a value from a variant list.
 * @param variant The variant list to get a value from.
 * @param index The index of the variant to get, negative values will wrap from the end.
 * @returns The value or NULL if @p index is out of range or @p variant is not a list.
 * @note DO NOT FREE THE RETURNED VARIANT.
 * @relates GE_Variant
 */
GE_Variant* GE_VariantList_Get(const GE_Variant *variant, GE_VariantInteger index);

/**
 * Check if the variant is NULL.
 * @param variant the variant list to check the value for.
 * @param index The index of the variant to check.
 * @returns True if the variant at @p index is of type GE_VARIANT_TYPE_NULL.
 * @relates GE_Variant
 */
bool GE_VariantList_GetIsNULL(const GE_Variant *variant, GE_VariantInteger index);

/**
 * Return the variant value as integer.
 * @param variant the variant list to get the value from.
 * @param index The index of the variant to get.
 * @param[out] retValue Return the value.
 * @returns True for successful conversion, false if the variant can not be converted.
 * @note If this function returns false @p retValue will be set to 0.
 * @relates GE_Variant
 */
bool GE_VariantList_GetInteger(const GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger *retValue);

/**
 * Return the variant value as real.
 * @param variant the variant list to get the value from.
 * @param index The index of the variant to get.
 * @param[out] retValue Return the value.
 * @returns True for successful conversion, false if the variant can not be converted.
 * @note If this function returns false @p retValue will be set to 0.
 * @relates GE_Variant
 */
bool GE_VariantList_GetReal(const GE_Variant *variant, GE_VariantInteger index, GE_VariantReal *retValue);

/**
 * Return the variant value as boolean.
 * @param variant the variant list to get the value from.
 * @param index The index of the variant to get.
 * @param[out] retValue Return the value.
 * @returns True for successful conversion, false if the variant can not be converted.
 * @note If this function returns false @p retValue will be set to false.
 * @relates GE_Variant
 */
bool GE_VariantList_GetBoolean(const GE_Variant *variant, GE_VariantInteger index, bool *retValue);

/**
 * Return the variant value as string.
 * @param variant The variant to get the string value from.
 * @param index The index of the variant to get.
 * @returns If @p index is valid and the variant is of type GE_VARIANT_TYPE_STRING a pointer to the string will be returned, otherwise NULL.
 * @note The variant string member is NULL a pointer to an empty string is returned.
 * @relates GE_Variant
 */
const char* GE_VariantList_GetString(const GE_Variant *variant, GE_VariantInteger index);

/**
 * Return the variant value as string.
 * @param variant The variant to get the value from.
 * @param index The index of the variant to get.
 * @param size The size of @p retValue (including the zero terminator).
 * @param[out] retValue Return the string here, pass NULL to get the required length.
 * @returns The number of return bytes or zero if the conversion could not be made (or if the string is empty).
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_GetAsString(const GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger size, char *retValue);

/**
 * Return a pointer to data.
 * @param variant The variant to get the value from.
 * @param index The index of the variant to get.
 * @param[out] retData If a valid size is returned return a pointer to the data, NULL to omit.
 * @returns The size of the data or -1 if a data pointer could not be returned.
 * @relates GE_Variant
 * @note This function is only successful if the variant type is either
 *     GE_VARIANT_TYPE_DATA or GE_VARIANT_TYPE_STRING.
 */
GE_VariantInteger GE_VariantList_GetData(const GE_Variant *variant, GE_VariantInteger index, void **retData);

/**
 * Set values in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @param length The number of values to set.
 * @param values The values, will all be copied.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_Set(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger length, const GE_Variant *values);

/**
 * Set a NULL value in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_SetNULL(GE_Variant *variant, GE_VariantInteger index);

/**
 * Set an integer value in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @param value The value.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_SetInteger(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger value);

/**
 * Set a real value in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @param value The value.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_SetReal(GE_Variant *variant, GE_VariantInteger index, GE_VariantReal value);

/**
 * Set a boolean value in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @param value The value.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_SetBoolean(GE_Variant *variant, GE_VariantInteger index, bool value);

/**
 * Set a string value in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @param value The value.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_SetString(GE_Variant *variant, GE_VariantInteger index, const char *value);

/**
 * Set a data value in a variant list.
 * @param variant The variant list to set values for.
 * @param index The index of the variant to set, negative values will wrap from the end.
 * @param size The size of the data.
 * @param data The data, NULL will zero the new memory.
 * @returns The number of set values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_SetData(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger size, const void *data);

/**
 * Insert the values in a variant list.
 * @param variant The variant list to insert values for.
 * @param index The index of where to insert, negative values will wrap from the end.
 * @param length The number of values to insert.
 * @param values The values, will all be copied. Pass NULL to zero the new variants.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_Insert(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger length, const GE_Variant *values);

/**
 * Insert a value to the list.
 * @param variant The variant list to insert values for.
 * @param index Where to insert the value.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertNULL(GE_Variant *variant, GE_VariantInteger index);

/**
 * Insert a value to the list.
 * @param variant The variant list to insert values for.
 * @param index Where to insert the value.
 * @param value The value.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertInteger(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger value);

/**
 * Insert a value to the list.
 * @param variant The variant list to insert values for.
 * @param index Where to insert the value.
 * @param value The value.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertReal(GE_Variant *variant, GE_VariantInteger index, GE_VariantReal value);

/**
 * Insert a value to the list.
 * @param variant The variant list to insert values for.
 * @param index Where to insert the value.
 * @param value The value.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertBoolean(GE_Variant *variant, GE_VariantInteger index, bool value);

/**
 * Insert a value to the list.
 * @param variant The variant list to insert values for.
 * @param index Where to insert the value.
 * @param value The value.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertString(GE_Variant *variant, GE_VariantInteger index, const char *value);

/**
 * Insert a value to the list.
 * @param variant The variant list to insert values for.
 * @param index Where to insert the value.
 * @param size The size of the data.
 * @param data The data.
 * @returns The number of inserted values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertData(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger size, const void *data);

/**
 * Append the values in a variant list.
 * @param variant The variant list to append values for.
 * @param length The number of values to append.
 * @param values The values, will all be copied. Pass NULL to zero the new variants.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_Append(GE_Variant *variant, GE_VariantInteger length, const GE_Variant *values);

/**
 * Append a value to the list.
 * @param variant The variant list to append values for.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_AppendNULL(GE_Variant *variant);

/**
 * Append a value to the list.
 * @param variant The variant list to append values for.
 * @param value The value.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_AppendInteger(GE_Variant *variant, GE_VariantInteger value);

/**
 * Append a value to the list.
 * @param variant The variant list to append values for.
 * @param value The value.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_AppendReal(GE_Variant *variant, GE_VariantReal value);

/**
 * Append a value to the list.
 * @param variant The variant list to append values for.
 * @param value The value.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_AppendBoolean(GE_Variant *variant, bool value);

/**
 * Append a value to the list.
 * @param variant The variant list to append values for.
 * @param value The value.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_AppendString(GE_Variant *variant, const char *value);

/**
 * Append a value to the list.
 * @param variant The variant list to append values for.
 * @param size The size of the data.
 * @param data The data.
 * @returns The number of appended values or 0 if @p variant is not a list.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_AppendData(GE_Variant *variant, GE_VariantInteger size, const void *data);

/**
 * Remove the values in a variant list.
 * @param variant The variant list to remove values for.
 * @param index The index of where to remove, negative values will wrap from the end.
 * @param length The number of values to remove.
 * @returns The number of removed values or 0 if @p variant is not a list.
 * @note All removed values will be deinitialized.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_Remove(GE_Variant *variant, GE_VariantInteger index, GE_VariantInteger length);

/**
 * Insert values from one list into another.
 * @param variant The variant list that values will be inserted into.
 * @param index The index of where to insert, negative values will wrap from the end.
 * @param otherList The list to get values from.
 * @returns The number of inserted values or 0 if @p variant or @p otherList is not a list.
 * @note All inserted values will be copied.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantList_InsertList(GE_Variant *variant, GE_VariantInteger index, const GE_Variant *otherList);

/**
 * Sort the list.
 * @param variant The variant list to sort.
 * @param reverse Reverse the sort.
 * @relates GE_Variant
 */
void GE_VariantList_Sort(GE_Variant *variant, bool reverse);

/**
 * Get the a value from a variant dict.
 * @param variant The variant dict to get a value from.
 * @param key The key to get a value for.
 * @returns The value or NULL if @p key was not found or @p variant is not a dict.
 * @note DO NOT FREE THE RETURNED VARIANT.
 * @relates GE_Variant
 */
GE_Variant* GE_VariantDict_Get(const GE_Variant *variant, const char *key);

/**
 * Check if a variant is NULL.
 * @param variant The variant dict to check a value for.
 * @param key The key to check a value for.
 * @returns True if the value at @p key is of type GE_VARIANT_TYPE_NULL.
 * @relates GE_Variant
 */
bool GE_VariantDict_GetIsNULL(const GE_Variant *variant, const char *key);

/**
 * Get a value from a variant dict.
 * @param variant The variant dict to get a value from.
 * @param key The key to get a value for.
 * @param[out] retValue Return the value here.
 * @returns True if a value was returned or false if the value could not be converted or @p key was not found.
 * @relates GE_Variant
 */
bool GE_VariantDict_GetInteger(const GE_Variant *variant, const char *key, GE_VariantInteger *retValue);

/**
 * Get a value from a variant dict.
 * @param variant The variant dict to get a value from.
 * @param key The key to get a value for.
 * @param[out] retValue Return the value here.
 * @returns True if a value was returned or false if the value could not be converted or @p key was not found.
 * @relates GE_Variant
 */
bool GE_VariantDict_GetReal(const GE_Variant *variant, const char *key, GE_VariantReal *retValue);

/**
 * Get a value from a variant dict.
 * @param variant The variant dict to get a value from.
 * @param key The key to get a value for.
 * @param[out] retValue Return the value here.
 * @returns True if a value was returned or false if the value could not be converted or @p key was not found.
 * @relates GE_Variant
 */
bool GE_VariantDict_GetBoolean(const GE_Variant *variant, const char *key, bool *retValue);

/**
 * Return the variant value as string.
 * @param variant The variant to get the string value from.
 * @param key The key to get the value for.
 * @returns If @p key is found and the variant is of type GE_VARIANT_TYPE_STRING a pointer to the string will be returned, otherwise NULL.
 * @note The variant string member is NULL a pointer to an empty string is returned.
 * @relates GE_Variant
 */
const char* GE_VariantDict_GetString(const GE_Variant *variant, const char *key);

/**
 * Return the variant value as string.
 * @param variant The variant to get the value from.
 * @param key The key to get the value for.
 * @param size The size of @p retValue (including the zero terminator).
 * @param[out] retValue Return the string here, pass NULL to get the required length.
 * @returns The number of return bytes or zero if the conversion could not be made (or if the string is empty).
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_GetAsString(const GE_Variant *variant, const char *key, GE_VariantInteger size, char *retValue);

/**
 * Return a pointer to data.
 * @param variant The variant to get the value from.
 * @param key The key to get the value for.
 * @param[out] retData If a valid size is returned return a pointer to the data, NULL to omit.
 * @returns The size of the data or -1 if a data pointer could not be returned.
 * @relates GE_Variant
 * @note This function is only successful if the variant type is either
 *     GE_VARIANT_TYPE_DATA or GE_VARIANT_TYPE_STRING.
 */
GE_VariantInteger GE_VariantDict_GetData(const GE_Variant *variant, const char *key, void **retData);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @param value The value, will be copied.
 * @returns The number of set values or 0 if @p variant is not a dict.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_Set(GE_Variant *variant, const char *key, const GE_Variant *value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @returns The number of set values.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_SetNULL(GE_Variant *variant, const char *key);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @param value The value.
 * @returns The number of set values.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_SetInteger(GE_Variant *variant, const char *key, GE_VariantInteger value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @param value The value.
 * @returns The number of set values.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_SetReal(GE_Variant *variant, const char *key, GE_VariantReal value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @param value The value.
 * @returns The number of set values.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_SetBoolean(GE_Variant *variant, const char *key, bool value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @param value The value.
 * @returns The number of set values.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_SetString(GE_Variant *variant, const char *key, const char *value);

/**
 * Set the a value in a variant dict.
 * @param variant The variant dict to set a value for.
 * @param key The key to set a value for.
 * @param size The size of the data.
 * @param data The data, NULL will zero the new data.
 * @returns The number of set values.
 * @note @p value will not be freed unless you override fDeinit.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_SetData(GE_Variant *variant, const char *key, GE_VariantInteger size, const void *data);

/**
 * Remove a value in a variant dict.
 * @param variant The variant dict to remove a value for.
 * @param key The key to set a value for.
 * @returns The number of removed values or 0 if @p variant is not a dict.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_Remove(GE_Variant *variant, const char *key);

/**
 * Get the key/value pairs from a variant dict.
 * @param variant The variant dict to get key/value pairs from.
 * @param length The maximum number of pairs to return.
 * @param[out] retPairs Return pairs here, pass NULL to get the number of available pairs.
 * @returns The number of pairs in the returned array.
 * @note The returned variant in the pairs are NOT copies! Do not modify them unless you know what's what.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_GetPairs(const GE_Variant *variant, GE_VariantInteger length, GE_VariantPair *retPairs);

/**
 * Merge a dictionary with another one.
 * @param variant The variant dict to receive the new key/value pairs.
 * @param otherDict The dictionary to get key/value pairs from.
 * @param overwrite Allow overwriting of key/value in @p variant.
 * @returns The number of update or added key/value pairs.
 * @relates GE_Variant
 */
GE_VariantInteger GE_VariantDict_Merge(GE_Variant *variant, const GE_Variant *otherDict, bool overwrite);

/**
 * Set the iterator to the first key/value in the dictionary.
 * @param variant The variant dict to iterate.
 * @param[out] iter The iterator.
 * @returns True if the iterator was set, false if the variant is not a dictionary or has no values.
 * @relates GE_Variant
 */
bool GE_VariantDict_IterFirst(const GE_Variant *variant, GE_VariantIter *iter);

/**
 * Move to the next key/value in the dictionary.
 * @param variant The variant dict to iterate.
 * @param[out] iter The iterator.
 * @returns True if the iterator was set, false if the variant is not a dictionary or has no more values.
 * @relates GE_Variant
 */
bool GE_VariantDict_IterNext(const GE_Variant *variant, GE_VariantIter *iter);

/**
 * Output a debug dump.
 * @param variant The variant.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_Variant
 */
void GE_Variant_DebugDump(const GE_Variant *variant, FILE *f, int indent);

#ifdef __cplusplus
}
#endif

#endif
