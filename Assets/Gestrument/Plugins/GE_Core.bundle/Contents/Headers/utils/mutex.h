/**
* @file utils/mutex.h
* @brief Cross-platform mutex implementation
* @author David Granström
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*/

#ifndef GE_MUTEX_H
#define GE_MUTEX_H

#include "GE_Core/common.h"

/**
 * A cross platform mutex.
 * @struct GE_Mutex
 * 
 * This is an opaque type, the definition depends on the platform:
 *
 * - Windows uses `HANDLE`.
 * - All others use `pthread_mutex_t`.
 */
typedef struct GE_Mutex GE_Mutex;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new GE_Mutex.
 *
 * @return A GE_Mutex on success or NULL on error.
 * @relates GE_Mutex
 */
GE_Mutex* GE_Mutex_New(void);

/**
 * Free a GE_Mutex.
 *
 * @param mutex Pointer to GE_Mutex
 * @return 0 on success non-zero on fail
 * @note On Windows this function always returns 0
 * @relates GE_Mutex
 */
int GE_Mutex_Free(GE_Mutex *mutex);

/**
 * Lock GE_Mutex.
 *
 * @param mutex Pointer to GE_Mutex
 * @return 0 on success non-zero on fail
 * @relates GE_Mutex
 */
int GE_Mutex_Lock(GE_Mutex *mutex);

/**
 * Unlock GE_Mutex.
 *
 * @param mutex Pointer to GE_Mutex
 * @return 0 on success non-zero on fail
 * @note On Windows this function always returns 0
 * @relates GE_Mutex
 */
int GE_Mutex_Unlock(GE_Mutex *mutex);

/**
 * Trylock GE_Mutex.
 *
 * @param mutex Pointer to GE_Mutex
 * @return 0 on success non-zero if mutex is already locked.
 * @relates GE_Mutex
 */
int GE_Mutex_TryLock(GE_Mutex *mutex);

#ifdef __cplusplus
}
#endif

#endif /* GE_MUTEX_H */
