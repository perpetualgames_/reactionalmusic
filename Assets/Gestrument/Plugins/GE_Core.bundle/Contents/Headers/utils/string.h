/**
* @file utils/string.h
* @brief Utility functions for string manipulation
* @author David Granström
* @copyright Copyright © 2018 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_STRING_H
#define GE_STRING_H

#include "GE_Core/common.h"

/**
 * @brief Max array length.
 */
#define GE_STRING_FLOAT_ARRAY_MAX 128

/**
 * @brief Too much nesting
 */
#define GE_STRING_PARSE_ERROR_DEPTH -100

/**
 * @brief [ without ending ]
 */
#define GE_STRING_PARSE_ERROR_UNCLOSED -101

/**
 * @brief Invalid character.
 */
#define GE_STRING_PARSE_ERROR_INVALID -102

/**
 * @brief Missing separator in a list.
 */
#define GE_STRING_PARSE_ERROR_SEPARATOR -103

/**
 * @brief A parsed value is invalid.
 */
#define GE_STRING_PARSE_ERROR_VALUE -104

/**
 * @brief Container for String to float* operations.
 */
typedef struct GE_FloatArray_
{
    float data[GE_STRING_FLOAT_ARRAY_MAX];
    int length;
} GE_FloatArray;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Find the file extension from a path URI.
 *
 * @param uri Path to a file
 * @return Pointer to extension from input URI
 */
const char* GE_String_FileNameExtension(const char *uri);

/**
 * Find the last path component from a path URI.
 *
 * i.e. a file name
 *
 * @param uri A path
 * @return Pointer to the last path component
 */
const char* GE_String_LastPathComponent(const char *uri);

/**
 * Remove a path extension from a filename.
 *
 * Removes everything from the last dot (.) and returns a new string.
 * The return string needs to be freed by its caller.
 *
 * @param filename A file name with an extension
 * @return Pointer to string
 */
char* GE_String_NewNameWithoutExtension(const char *filename);

/**
 * Duplicate string.
 * @param s The string to duplicate.
 * @return A new string copied from @p s.
 */
char* GE_Strdup(const char *s);

/**
 * Reallocate existring string as duplicate of another string.
 * @param s The string to realloc.
 * @param t The string to duplicate, NULL to free @p s and return NULL.
 * @return The reallocated pointer of @p s or NULL if @p t was NULL (in which case @p s was also freed).
 */
char* GE_Strdupr(char *s, const char *t);

/**
 * Get the length of a string, but check at most @p n bytes.
 * @param s The string to get the length of.
 * @param n Check at most this many bytes.
 * @returns The length, excluding the zero terminator.
 */
size_t GE_Strnlen(const char *s, size_t n);

/**
 * Get all lines from a char buffer.
 * 
 * A NULL sentinel value is added to the end of the result array, it is not
 * included in the return value (number of lines).
 * 
 * The caller is responsible for deallocating the result.
 * @see GE_String_ArrayFree
 *
 * @param buffer A string
 * @param numChars Number of chars in @p buffer
 * @param[out] result Array of strings, will be NULL on error or empty file.
 * @returns The number of lines or negative value on error.
 */
int GE_String_SplitLines(char *buffer, int numChars, char ***result);

/**
 * Free an array of strings.
 *
 * @see GE_String_SplitLines
 *
 * @param stringArray Pointer to an array of strings.
 */
void GE_String_ArrayFree(char **stringArray);

/**
 * Case insensitive compare (ASCII only!).
 * @param s1 First string.
 * @param s2 Second string.
 * @returns same as strcmp().
 * @note NULL will compare as even less than an empty string.
 */
int GE_StrcmpI(const char *s1, const char *s2);

/**
 * Substitute all occurrences of a character with a replacement character.
 *
 * @param str The input string.
 * @param sub The char to replace.
 * @param replacement The replacement char.
 * @param[out] output The output string.
 * @return True if replacement was done false otherwise.
 */
bool GE_String_Substitute(const char *str, const char sub, const char replacement, char *output);

/**
 * Check if the string only contains ASCII whitespace or commands.
 * @param s The string to check.
 * @returns True if the string contains only whitespace or commands.
 */
bool GE_StrIsEmpty(const char *s);

/**
 * @brief Check if @p s starts with @p startsWith.
 * @param s The string to check.
 * @param startsWith Starts with this string.
 * @returns True if @p s starts with @p startsWith.
 */
bool GE_StrStartsWith(const char *s, const char *startsWith);

/**
 * Parse intervals as semitones.
 * @param s The string to parse.
 * @param root The root (will be added to the semitones).
 * @param maxSemitones The maximum number of semitones to parse.
 * @param[out] retSemitones Return semitones here, pass NULL to get the number of found intervals.
 * @param[out] retErrorOffset If the function returns a negative value, return error offset here.
 * @returns The number of returned (or found) intervals or -1 on error (see @p retErrorOffset).
 */
int GE_StrParseIntervalsInt(const char *s, int root, int maxSemitones, int *retSemitones, size_t *retErrorOffset);

/**
 * Parse intervals as semitones.
 * @param s The string to parse.
 * @param root The root (will be added to the semitones).
 * @param maxSemitones The maximum number of semitones to parse.
 * @param[out] retSemitones Return semitones here, pass NULL to get the number of found intervals.
 * @param[out] retErrorOffset If the function returns a negative value, return error offset here.
 * @returns The number of returned (or found) intervals or -1 on error (see @p retErrorOffset).
 */
int GE_StrParseIntervalsFloat(const char *s, float root, int maxSemitones, float *retSemitones, size_t *retErrorOffset);

/**
 * Parse scientific pitch notation string to pitch.
 * @param s The string to parse.
 * @returns The pitch or -1 if the notation is invalid.
 */
int GE_StrToPitch(const char *s);

/**
 * Convert a pitch to scientific pitch notation string.
 * @param pitch The pitch.
 * @param sharps Use sharps instead of flats.
 * @param size The maximum size of the returned string (including zero terminator).
 * @param[out] ret_s Return the string here, NULL to get the size.
 * @returns The pitch or -1 if the notation is invalid.
 */
size_t GE_StrFromPitch(int pitch, bool sharps, size_t size, char *ret_s);

/**
 * @brief Convert a double to a string.
 * @param d The value to convert.
 * @param size The size of output string.
 * @param[out] s The output string.
 * @returns The number of bytes written to the output string (including 0 terminator).
 */
int GE_String_DoubleToString(double d, size_t size, char *s);

/**
 * @brief Converts a string to a double value.
 * @param[out] d The output value.
 * @param s The input string.
 * @returns The number of bytes parsed from the string.
 */
int GE_String_DoubleFromString(double *d, const char *s);

/**
 * @brief Converts a GE_FloatArray to a string representation.
 * @param num_arrays The number of input arrays.
 * @param arrays The arrays to convert.
 * @param size The length of output string.
 * @param[out] s The output string.
 * @returns The number of bytes written to the output string (including 0 terminator).
 */
int GE_String_FloatArrayToString(int num_arrays, const GE_FloatArray *arrays, size_t size, char *s);

/**
 * @brief Converts a string to a GE_FloatArray.
 * @param max_arrays The maximum number of output arrays.
 * @param[out] arrays The output array.
 * @param[out] num_arrays The number of converted arrays.
 * @param[out] ret_offset The number of bytes parsed from @p s
 * @param s The input string to parse.
 * @returns The number of bytes parsed from the string or an error code.
 *
 * Valid formats:
 *
 * - A single:
 *     - "1, 2, 3, 4, 5, 6, 7, 8, etc"
 *     - Continue parsing until end of string.
 * - One or more arrays:
 *     - "[1, 2, 3, 4, etc], [5, 6], etc"
 *     - Continue parsing until end of string.
 * - Outer brackets for setting an end:
 *     - "[[1, 2, 3, 4, etc], [1, 2, 3, 4, etc], etc]"
 *     - Stop parsing and return when the final ] is reached.
 * - A single trailing comma is allowed.
 */
int GE_String_FloatArraysFromString(int max_arrays, GE_FloatArray *arrays, int *num_arrays, int *ret_offset, const char *s);

/**
 * @brief Check if a string matches a wildcard expression.
 * @param s The string.
 * @param n Check at most @p n bytes of @p s, -1 for zero terminator.
 * @param wc The wildcard.
 * @returns True if match, false if not.
 */
bool GE_String_MatchWildcard(const char *s, int n, const char *wc);

#ifdef __cplusplus
}
#endif

#endif /* GE_STRING_H */
