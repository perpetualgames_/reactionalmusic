/**
 * @file atomic.h
 * @brief Provide cross-platform atomic support.
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_ATOMIC_H
#define GE_ATOMIC_H

#ifdef __cplusplus
#include <atomic>
using namespace std;
#else
#include <stdatomic.h>
#endif

#include "GE_Core/common.h"

#define GE_ATOMIC_INIT atomic_init

#define GE_MEMORY_ORDER_RELAXED memory_order_relaxed
#define GE_MEMORY_ORDER_CONSUME memory_order_consume
#define GE_MEMORY_ORDER_ACQUIRE memory_order_acquire
#define GE_MEMORY_ORDER_RELEASE memory_order_release
#define GE_MEMORY_ORDER_ACQ_REL memory_order_acq_rel
#define GE_MEMORY_ORDER_SEQ_CST memory_order_seq_cst

#define GE_ATOMIC_LOAD(ptr, memorder) atomic_load_explicit(ptr, memorder)
#define GE_ATOMIC_STORE(ptr, value, memorder) atomic_store_explicit(ptr, value, memorder)
#define GE_ATOMIC_EXCHANGE(ptr, val, ret, memorder) atomic_exchange_explicit(ptr, val, ret, memorder)
#define GE_ATOMIC_CMP_EXCHANGE_WEAK(ptr, expected_ptr, desired_ptr, memorder_success, memorder_fail) atomic_compare_exchange_explicit(ptr, expected_ptr, desired_ptr, true, memorder_success, memorder_fail)
#define GE_ATOMIC_CMP_EXCHANGE_STRONG(ptr, expected_ptr, desired_ptr, memorder_success, memorder_fail) atomic_compare_exchange_explicit(ptr, expected_ptr, desired_ptr, false, memorder_success, memorder_fail)
#define GE_ATOMIC_FETCH_ADD(ptr, val, memorder) atomic_fetch_add_explicit(ptr, val, memorder)
#define GE_ATOMIC_FETCH_SUB(ptr, val, memorder) atomic_fetch_sub_explicit(ptr, val, memorder)
#define GE_ATOMIC_FETCH_AND(ptr, val, memorder) atomic_fetch_and_explicit(ptr, val, memorder)
#define GE_ATOMIC_FETCH_XOR(ptr, val, memorder) atomic_fetch_xor_explicit(ptr, val, memorder)
#define GE_ATOMIC_FETCH_OR(ptr, val, memorder) atomic_fetch_or_explicit(ptr, val, memorder)
#define GE_ATOMIC_FETCH_NAND(ptr, val, memorder) atomic_fetch_nand_explicit(ptr, val, memorder)
#define GE_ATOMIC_BOOL_TEST_AND_SET(ptr, memorder) atomic_test_and_set_explicit(ptr, memorder)
#define GE_ATOMIC_BOOL_CLEAR(ptr, memorder) atomic_clear_explicit(ptr, memorder)
#define GE_ATOMIC_IS_ALWAYS_LOCK_FREE(size, ptr) atomic_always_lock_free(size, ptr)
#define GE_ATOMIC_IS_LOCK_FREE(size, ptr) atomic_is_lock_free(size, ptr)

typedef atomic_bool GE_AtomicBool;
typedef atomic_char GE_AtomicChar;
typedef atomic_uchar GE_AtomicUChar;
typedef atomic_short GE_AtomicShort;
typedef atomic_ushort GE_AtomicUShort;
typedef atomic_int GE_AtomicInt;
typedef atomic_uint GE_AtomicUInt;
typedef atomic_long GE_AtomicLong;
typedef atomic_ulong GE_AtomicULong;
typedef atomic_llong GE_AtomicLongLong;
typedef atomic_ullong GE_AtomicULongLong;
typedef atomic_char16_t GE_AtomicChar16;
typedef atomic_char32_t GE_AtomicChar32;
typedef atomic_wchar_t GE_AtomicWChar;
//typedef atomic_int_least8_t GE_AtomicIntLeast8;
//typedef atomic_uint_least8_t GE_AtomicUIntLeast8;
//typedef atomic_int_least16_t GE_AtomicIntLeast16;
//typedef atomic_uint_least16_t GE_AtomicUIntLeast16;
//typedef atomic_int_least32_t GE_AtomicIntLeast32;
//typedef atomic_uint_least32_t GE_AtomicUIntLeast32;
//typedef atomic_int_least64_t GE_AtomicIntLeast64;
//typedef atomic_uint_least64_t GE_AtomicUIntLeast64;
//typedef atomic_int_fast8_t GE_AtomicIntFast8;
//typedef atomic_uint_fast8_t GE_AtomicUintFast8;
//typedef atomic_int_fast16_t GE_AtomicIntFast16;
//typedef atomic_uint_fast16_t GE_AtomicUIntFast16;
//typedef atomic_int_fast32_t GE_AtomicIntFast32;
//typedef atomic_uint_fast32_t GE_AtomicUIntFast32;
//typedef atomic_int_fast64_t GE_AtomicIntFast64;
//typedef atomic_uint_fast64_t GE_AtomicUIntFast64;
typedef atomic_intptr_t GE_AtomicIntPtr;
typedef atomic_uintptr_t GE_AtomicUIntPtr;
typedef atomic_size_t GE_AtomicSize;
typedef atomic_ptrdiff_t GE_AtomicPtrDiff;
typedef atomic_intmax_t GE_AtomicIntMax;
typedef atomic_uintmax_t GE_AtomicUIntMax;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif
