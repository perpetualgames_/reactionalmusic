/**
* @file qsort_r.h
* @brief qsort_r implementation if target platform doesn't have it.
* @author Peter Gebauer
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*/

#ifndef GE_QSORT_R_H
#define GE_QSORT_R_H

#include "GE_Core/common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Quicksort implementation using Hoare partitioning scheme.
 * @param base The array to sort.
 * @param nmemb The length of the array.
 * @param size The size of each element.
 * @param compar A comparator function.
 * @param arg An argument passed to the comparator function.
 */
void GE_QSortRImplHoare(void * base, size_t nmemb, size_t size, int (*compar)(const void *, const void *, void *), void * arg);

/**
 * Quicksort implementation using Lomuto partitioning scheme (for testing only!).
 * @param base The array to sort.
 * @param nmemb The length of the array.
 * @param size The size of each element.
 * @param compar A comparator function.
 * @param arg An argument passed to the comparator function.
 * @note This is just for testing, Lomuto is SLOWER than Hoare.
 */
void GE_QSortRImplLomuto(void * base, size_t nmemb, size_t size, int (*compar)(const void *, const void *, void *), void * arg);

/**
 * Quicksort that will choose implementation based on availability.
 * @param base The array to sort.
 * @param nmemb The length of the array.
 * @param size The size of each element.
 * @param compar A comparator function.
 * @param arg An argument passed to the comparator function.
 * @note This function should be using GE_QSortRImplHoare() since it's faster than GE_QSortRImplLomuto().
 */
void GE_QSortR(void * base, size_t nmemb, size_t size, int (*compar)(const void *, const void *, void *), void * arg);

#ifdef __cplusplus
}
#endif

#endif /* GE_QSORT_R_H */
