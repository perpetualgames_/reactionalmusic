/**
 * @file heap.h
 * @brief GE_Variant header
 * @author Peter Gebauer
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_HEAP_H
#define GE_HEAP_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "GE_Core/common.h"
#include "GE_Core/event.h"

#define GE_HEAP_DEFAULT_MAX_EVENTS 8192

/**
 * @brief See struct GE_BinHeap_
 */
typedef struct GE_BinHeap_ GE_BinHeap;

/**
 * @brief A binary heap.
 */
struct GE_BinHeap_
{
    int maxEvents;
    int numEvents;
    GE_Event *events;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialize a binary heap.
 * @param[out] heap The struct to initialize.
 * @param maxEvents The maximum number of events the heap can hold,
 * if <= 0 will use @ref GE_HEAP_DEFAULT_MAX_EVENTS.
 * @returns Zero on success or -1 on out of memory.
 * @note @p maxEvents must be a multiple of 2, if not the function will
 * try to find the closest, multiple.
 */
int GE_BinHeap_Init(GE_BinHeap *heap, int maxEvents);

/**
 * @brief Deinitialize a binary heap.
 * @param heap The heap.
 */
void GE_BinHeap_Deinit(GE_BinHeap *heap);

/**
 * @brief Add an event to the heap.
 * @param heap The heap.
 * @param event The event.
 * @returns True if the event was added, false if the heap is full.
 */
bool GE_BinHeap_AddEvent(GE_BinHeap *heap, const GE_Event *event);

/**
 * @brief Get the next available event from the heap.
 * @param heap The heap.
 * @param[out] retEvent Write returned event here, pass NULL to omit.
 * @returns True if the event was available, false if the heap is empty.
 * @note If the function returns false @p retEvent will be zeroed.
 */
bool GE_BinHeap_PeekEvent(GE_BinHeap *heap, GE_Event *retEvent);

/**
 * @brief Pops the next available event off the heap.
 * @param heap The heap.
 * @param[out] retEvent Write returned event here, pass NULL to omit.
 * @returns True if the event was popped, false if the heap is empty.
 * @note If the function returns false @p retEvent will be zeroed.
 */
bool GE_BinHeap_PopEvent(GE_BinHeap *heap, GE_Event *retEvent);

#ifdef __cplusplus
}
#endif

#endif
