/**
 * @file osc.h
 * @brief Low-level OSC serializing and desierializing.
 * @author Peter Gebauer
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * @section supported_osc_types Supported types and their host types
 *
 * | Type | Description |
 * | :--- | :---        |
 * | `i` | Signed, 32-bit integer. |
 * | `f` | IEEE 754, 32-bit floating point. |
 * | `s` | Zero-terminated string. |
 * | `b` | Binary data (BLOB). |
 * | `h` | Signed, 64-bit integer. |
 * | `t` | Timetag (unsigned 64-bit integer). |
 * | `d` | IEEE 754, 64-bit floating point. |
 * | `c` | A single ASCII character. |
 * | `r` | 4 bytes of RGBA data. |
 * | `m` | MIDI Channel Message bytes (port, status, data1 and data2). |
 * | `T` | True. No payload data. |
 * | `F` | False. No payload data. |
 * | `N` | Nil. No payload data. |
 * | `I` | Infinitum. No payload data. |
 * | `u` | Unsigned, 32-bit integer. |
 *
 * Notes:
 *
 * - All serialize values are considered read-only and may be const qualified.
 * - OSC data is always 32-bit aligned (@ref GE_OSC_BYTE_ALIGN).
 * - The specification demands 2-complement signed integers, however this code
 *   does not check or convert integers with 1-complement or other exotic bit
 *   representations.
 * - Array tags are NOT supported at this time (although that might change).
 */

#ifndef GE_OSC_H
#define GE_OSC_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include "GE_Core/common.h"
#include "GE_Core/event.h"

/**
 * @brief Maximum size of an address.
 */
//#define GE_OSC_ADDRESS_MAX 128

/**
 * @brief Maximum size of a type tag.
 */
#define GE_OSC_TYPE_TAG_MAX 128

/**
 * @brief Maximum size of strings "s" type.
 */
#define GE_OSC_STRING_MAX 65536

/**
 * @brief Generic error.
 */
#define GE_ERROR -1

/**
 * @brief Invalid OSC type detected.
 */
#define GE_ERROR_OSC_TYPE -2

/**
 * @brief Not enough bytes to complete serializing/deserializing.
 */
#define GE_ERROR_OSC_SIZE -3

/**
 * @brief Integer Overflow.
 *
 * Some things that may cause this error:
 *
 * - String lengths exceeding a length > INT32_MAX.
 */
#define GE_ERROR_OSC_OVERFLOW -4

/**
 * @brief A bundled message overrun.
 *
 * Returned if a message does not fit the number
 * of bytes specified for it.
 */
#define GE_ERROR_OSC_OVERRUN -5

/**
 * @brief A bundle without proper header.
 */
#define GE_ERROR_OSC_BUNDLE -6

/**
 * @brief Unclosed brackets in typetag or address pattern.
 */
#define GE_ERROR_OSC_BRACKETS -7

/**
 * @brief Invalid value passed from OSC.
 */
#define GE_ERROR_OSC_VALUE -8

/**
 * @brief Invalid OSC address.
 */
#define GE_ERROR_OSC_ADDRESS -9

/**
 * @brief See struct GE_OscArg_.
 */
typedef union GE_OSCValue_ GE_OSCValue;

/**
 * @brief See struct GE_OscArg_.
 */
typedef struct GE_OSCMessage_ GE_OSCMessage;

/**
 * @brief Used when serializing/deserializing OSC message payload.
 */
union GE_OSCValue_
{


    /**
     * @brief Signed 32-bit integer.
     */
    int32_t i;

    /**
     * @brief IEEE 754 32-bit float.
     */
    float f;

    /**
     * @brief String.
     */
    struct
    {

        /**
         * @brief The length of the string, excluding zero terminator.
         */
        int32_t length;

        /**
         * @brief String pointer.
         */
        const char *ptr;

    } s;

    /**
     * @brief Blob.
     */
    struct
    {

        /**
         * @brief Size of the blob data.
         */
        uint32_t size;

        /**
         * @brief Pointer to the blobbel data.
         */
        const void *ptr;

    } b;


    /**
     * @brief Signed 64-bit integer.
     */
    int64_t h;

    /**
     * @brief Timetag, unsigned 64-bit integer (NTP timestamp).
     */
    uint64_t t;

    /**
     * @brief IEEE 754 64-bit float.
     */
    double d;

    /**
     * @brief String.
     */
    struct
    {

        /**
         * @brief The length of the string, excluding zero terminator.
         */
        int32_t length;

        /**
         * @brief String pointer.
         */
        const char *ptr;

    } S;

    /**
     * @brief A single ASCII character.
     */
    char c;

    /**
     * @brief RGBA data.
     */
    struct
    {

        /**
         * @bried Red channel.
         */
        uint8_t r;

        /**
         * @bried Green channel.
         */
        uint8_t g;

        /**
         * @bried Blue channel.
         */
        uint8_t b;

        /**
         * @bried Alpha channel.
         */
        uint8_t a;

    } r;

    /**
     * @brief MIDI Channel Message.
     */
    struct
    {

        /**
         * @brief Status byte.
         */
        uint8_t port;

        /**
         * @brief Status byte.
         */
        uint8_t status;

        /**
         * @brief First data byte.
         */
        uint8_t data1;

        /**
         * @brief Second, optional data byte depending on the MIDI message.
         */
        uint8_t data2;

    } m;

    /**
     * @brief Non-spec unsigned 32-bit integer.
     */
    uint32_t u;

};

/**
 * @brief Struct for OSC messages in a bundle.
 */
struct GE_OSCMessage_
{

    /**
     * @brief The address string length, excluding zero terminator.
     * A negative value will tell encoders to count the string length.
     */
    int32_t address_len;

    /**
     * @brief A pointer to the address string. Can be NULL when deserializing.
     */
    const char *address;

    /**
     * @brief The types string length, excluding zero terminator.
     * A negative value will tell encoders to count the string length.
     */
    int32_t types_len;

    /**
     * @brief A pointer to the types string. Can be NULL when deserializing.
     */
    const char *types;

    /**
     * @brief The maximum number of values to write when deserializing,
     * but it is ignored when serializing since the number of values is
     * determined by the length of `types`.
     * @note If <= 0 no values will be deserialized for the message.
     */
    int maxValues;

    /**
     * @brief A pointer to an array of writable values, matching the
     * length of `types` when serializing and `maxValues` when deserializing.
     * @note If NULL no values will be deserialized for the message.
     */
    GE_OSCValue *values;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Serialize OSC values.
 * @param[out] osc If non-NULL write OSC data here.
 * @param size The maximum number of bytes to write to @p osc,
 * ignored if @p osc is NULL.
 * @param types The types to expect as variadic arguments.
 * @param values An array of the values to serialize.
 * @returns The number of serialized bytes or a negative error code on failure.
 * @note The number of @p values must match the length of @p types even if
 * some of the types (`T`, `F`, `N` and `I`) will ignore the value.
 * @note Array characters (`[` and `]`) are completely ignored.
 * @see GE_OSCDeserializeValues(), GE_OSCSerializeMessage() and GE_OSCSerializeBundle().
 *
 * The number of @p values unions to pass must match the number of
 * specified @p types, even if some types do not have a value.
 * For those types the value may be uninitialized as it is completely
 * ignored.
 *
 * If you need to know the size in bytes for particular values you
 * can pass NULL to @p osc (in which case @p size is ignored) to do
 * a dry run that does not write any serialized data.
 *
 * All @p types and @p values will be interpreted as a single,
 * linear array. The array characters `[` and `]` are simply skipped.
 * There is no validity check for unclosed arrays.
 *
 * The @p types are not serialized by this function,
 * see GE_OSCSerializeMessage() to serialize a complete message.
 */
int64_t
GE_OSCSerializeValues
(
    void *osc,
    size_t size,
    const char *types,
    const GE_OSCValue *values
);

/**
 * @brief Deserialize OSC values.
 * @param osc Deserialize this OSC data.
 * @param size The maximum number of bytes to read from @p osc.
 * @param types The types to expect as variadic arguments.
 * @param maxValues The maximum number of values to write to.
 * @param[out] values If non-NULL write the deserialized values.
 * @returns The number of deserialized bytes or a negative error code on failure.
 * @note The number of @p values must match the length of @p types even if
 * some of the types (`T`, `F`, `N` and `I`) will not write the value, the
 * value is simply zeroed.
 * @note Array characters (`[` and `]`) are completely ignored.
 * @see GE_OSCSerializeValues(), GE_OSCDeserializeMessage() and cGE_OSCDeserializeBundle().
 *
 * If @p values is not NULL the number of @p values unions to pass must match
 * the number of specified @p types, even if some types do not have a value.
 * For those types the value will be zeroed to avoid any accidental use of
 * unintialized memory.
 *
 * If @p values is NULL a dry run is preformed without writing any values
 * from the deserialized @p osc data. It should be noted that unlike
 * GE_OSCSerializeValues() this dry run may still return @ref COSC_ERROR_SIZE.
 *
 * All @p types and @p values will be interpreted as a single,
 * linear array. The array characters `[` and `]` are simply skipped.
 * There is no validity check for unclosed arrays.
 *
 * The @p types are not serialized by this function,
 * see GE_OSCDeserializeMessage() to deserialize a complete message.
 */
int64_t
GE_OSCDeserializeValues
(
    const void *osc,
    size_t size,
    const char *types,
    int maxValues,
    GE_OSCValue *values
);

/**
 * @brief Serialize OSC message.
 * @param[out] osc If non-NULL write OSC data here.
 * @param size The maximum number of bytes to write to @p osc,
 * ignored if @p osc is NULL.
 * @param address_len The length of the @p address, excluding the
 * zero terminator. A negative value will force the function to
 * count the length.
 * @param address The address. It is NOT validated by this function!
 * @param types_len The length of the @p types string, excluding the
 * zero terminator. A negative value will force the function to
 * count the length.
 * @param types The types.
 * @param values The values.
 * @returns The number of serialized bytes or a negative error code on failure.
 * @note The number of @p values must match the length of @p types even if
 * some of the types (`T`, `F`, `N` and `I`) will ignore the value.
 * @see GE_OSCDeserializeMessage(), GE_OSCSerializeValues() and GE_OSCSerializeBundle().
 *
 * For a more detailed description of @p types and @p values
 * see GE_OSCSerializeValues().
 */
int64_t
GE_OSCSerializeMessage
(
    void *osc,
    size_t size,
    int32_t address_len,
    const char *address,
    int32_t types_len,
    const char *types,
    const GE_OSCValue *values
);

/**
 * @brief Deserialize OSC message.
 * @param osc Deserialize this OSC data.
 * @param size The maximum number of bytes to read from @p osc.
 * @param[out] address_len If non-NULL write the @p address string length
 * excluding the zero terminator.
 * @param[out] address If non-NULL write a pointer within @p osc to
 * the address. There is no validation for the returned address.
 * @param[out] types_len If non-NULL write the @p types string length
 * excluding the zero terminator.
 * @param[out] types If non-NULL write a pointer within @p osc to the typetag
 * string.
 * @param maxValues The maximum number of values to write to.
 * @param[out] values If non-NULL write the values.
 * @returns The number of deserialized bytes or a negative error code on failure.
 * @note The number of @p values must match the length of @p types even if
 * some of the types (`T`, `F`, `N` and `I`) will not write the value, the
 * value is simply zeroed.
 * @see GE_OSCSerializeMessage(), GE_OSCDeserializeValues() and cGE_OSCDeserializeBundle().
 *
 * For return arguments they may be uninitialized if this function returns
 * an error.
 *
 * The string pointers return to @p address will always be the same
 * as @p osc and @p types will point to where the types were found in @p osc.
 *
 * For a more detailed description of @p types and @p values
 * see GE_OSCSerializeValues().
 */
int64_t
GE_OSCDeserializeMessage
(
    const void *osc,
    size_t size,
    int32_t *address_len,
    const char **address,
    int32_t *types_len,
    const char **types,
    int maxValues,
    GE_OSCValue *values
);

/**
 * @brief Serialize OSC messages for a bundle.
 * @param[out] osc If non-NULL write OSC data here.
 * @param size The maximum number of bytes to write to @p osc,
 * ignored if @p osc is NULL.
 * @param timetag NTP timetag.
 * @param max_messages The maximum number of messages to serialize.
 * @param messages The messages.
 * @param[out] num_messages If non-NULL write the number of
 * succecssfully serialized messages.
 * @param[out] num_bytes If non-NULL write the number of
 * succecssfully serialized bytes.
 * @returns The number of serialized bytes or a negative error code on failure.
 */
int64_t
GE_OSCSerializeBundle
(
    void *osc,
    size_t size,
    uint64_t timetag,
    int max_messages,
    const GE_OSCMessage *messages,
    int *num_messages,
    int64_t *num_bytes
);

/**
 * @brief Deserialize bundled messages from OSC.
 * @param osc Deserialize this OSC data.
 * @param size The maximum number of bytes to read from @p osc.
 * @param[out] timetag If non-NULL write the timetag here.
 * @param max_messages The maximum number of messages to deserialize.
 * @param[out] messages If non-NULL write deserialized messages.
 * @param[out] num_messages If non-NULL write the number of
 * succecssfully deserialized messages.
 * @param[out] num_bytes If non-NULL write the number of
 * succecssfully deserialized bytes.
 * @returns The number of deserialized bytes or a negative error code on failure.
 */
int64_t
GE_OSCDeserializeBundle
(
    const void *osc,
    size_t size,
    uint64_t *timetag,
    int max_messages,
    GE_OSCMessage *messages,
    int *num_messages,
    int64_t *num_bytes
);

/**
 * @brief Match an an address with a pattern.
 * @param address The address to match with.
 * @param pattern The pattern to match.
 * @returns 1 for a match, 0 for no match or a negative error code.
 */
int
GE_OSCMatchPattern
(
    const char *address,
    const char *pattern
);

/**
 * @brief Validate an address.
 * @param address The address validate.
 * @returns The index of last inspected character.
 *
 * If the entire string is valid the index returned
 * will be the terminating zero of @p address.
 *
 * If the index is not the zero terminator it will
 * be the first invalid character.
 */
int32_t
GE_OSCValidateAddress
(
    const char *address
);

/**
 * @brief Validate a typetag string.
 * @param types The types validate.
 * @returns The index of last inspected character.
 *
 * If the entire string is valid the index returned
 * will be the terminating zero of @p types.
 *
 * If the index is not the zero terminator it will
 * be the first invalid character.
 */
int32_t
GE_OSCValidateTypes
(
    const char *types
);

#ifdef __cplusplus
}
#endif

#endif
