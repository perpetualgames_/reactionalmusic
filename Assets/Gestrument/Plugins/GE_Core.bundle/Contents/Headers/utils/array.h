/**
 * @file array.h
 * @brief Dynamic array.
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * Use of the array:
 *
 * ```
 * GE_Array arr;
 * GE_Array_Init(&arr, sizeof(int));
 *
 * int myInteger = 0;
 * int myIntegers[] = {1, 2, 3};
 *
 * // Add a single integer.
 * GE_Array_Append(&arr, 1, &myInteger);
 *
 * // The array is already a pointer to integers.
 * GE_Array_Append(&arr, 3, myIntegers);
 *
 * for (size_t i = 0; i < GE_Array_GetLength(&arr); i++)
 *     printf("%zu == %d\n", i, *((const int*)GE_Array_GetElementAt(&arr, i)));
 *
 * GE_Array_Deinit(&arr);
 * ```
 *
 * Setting, inserting and appending literals:
 *
 * ```
 * // Append objects.
 * GE_Array_Append(&arr, 3, (int[]){1, 2, 3});
 *
 * // Append pointer to pointers/arrays.
 * GE_Array_Append(&arr, 3, &((char*){"Hello world!"}));
 * ```
 *
 * Memory manage the elements using the array:
 * 
 * ```
 * GE_Array arr;
 * // Using a config to setup the size and function callbacks.
 * GE_ArrayConfig config = {sizeof(MyType), &DeinitMyType};
 * GE_Array_InitConfig(&arr, &config);
 *
 * // Initialize MyType which requires a deinit.
 * MyType myType;
 * MyType_Init(&myType);
 *
 * // Append to array.
 * GE_Array_Append(&arr, 1, &myType);
 *
 * // Will call DeinitMyType.
 * GE_Array_Deinit(&arr);
 * ```
 *
 * Notes on using pointer to pointers:
 *
 * The important thing to remember is that the element itself is the pointer
 * so passing values to GE_Array_Set(), GE_Array_Insert() and GE_Array_Append()
 * needs to be done by a pointer to the pointers.
 *
 * Likewise when dereferencing the returned element pointer from GE_Array_GetElementAt()
 * it will return a pointer to pointers.
 *
 * ```
 * GE_Array arr;
 * GE_Array_Init(&arr, sizeof(char *));
 *
 * const char *myString = "HELLO WORLD!";
 * const char *myStrings[] = {"A", "B", "C"};
 *
 * // Add a single string.
 * GE_Array_Append(&arr, 1, &myString); 
 *
 * // The array is already a pointer to pointers.
 * GE_Array_Append(&arr, 3, myStrings); 
 *
 * for (size_t i = 0; i < GE_Array_GetLength(&arr); i++)
 *     printf("%zu == %s\n", i, *((const char**)GE_Array_GetElementAt(&arr, i))); // Deref pointer to pointers.
 *
 * GE_Array_Deinit(&arr);
 * ```
 *
 * Memory management of pointer to pointers works similarly:
 *
 * ```
 * GE_Array arr;
 * // Config with a deinitializer. You can add your own GE_ArrayDeinitElement implementation.
 * GE_ArrayConfig config = {sizeof(char *), &GE_Array_DeinitElementString, NULL};
 * GE_Array_InitConfig(&arr, &config);
 *
 * // Since the array will try to free removed elements we need to duplicate everything.
 * char *myString = strdup("HELLO WORLD!");
 * char *myStrings[] = {strdup("A"), strdup("B"), strdup("C")};
 *
 * // Add a single string.
 * GE_Array_Append(&arr, 1, &myString); 
 *
 * // The array is already a pointer to pointers.
 * GE_Array_Append(&arr, 3, myStrings); 
 *
 * for (size_t i = 0; i < GE_Array_GetLength(&arr); i++)
 *     printf("%zu == %s\n", i, *((const char**)GE_Array_GetElementAt(&arr, i))); // Deref pointer to pointers.
 *
 * // Voila, all the strings are freed automagically by the array.
 * // Note that GE_Array_Clear(), GE_Array_Set() and GE_Array_Remove() behave the same way,
 * // when ever an element is overwritten/removed and fDeinitElement is non-NULL.
 * GE_Array_Deinit(&arr);
 * ```
 */

#ifndef GE_ARRAY_H
#define GE_ARRAY_H

#include "GE_Core/common.h"

/**
 * @see struct GE_ArrayConfig
 */
typedef struct GE_ArrayConfig GE_ArrayConfig;

/**
 * @see struct GE_Array
 */
typedef struct GE_Array GE_Array;

/**
 * Deinitialize an element in the array.
 */
typedef void (*GE_ArrayDeinitElement)(void *element, void *arg);

/**
 * Used to initialize an array.
 */
struct GE_ArrayConfig {

    /**
     * The size of each element.
     */
    size_t elementSize;

    /**
     * Callback to clean up elements before they are removed from the array.
     * @see GE_Array_DeinitElementString()
     */
    GE_ArrayDeinitElement fDeinitElement;

    /**
     * Passed to @ref GE_ArrayDeinitElement.
     */
    void *argDeinitElement;

};

/**
 * A dynamic array.
 */
struct GE_Array {

    /**
     * The config.
     */
    GE_ArrayConfig config;

    /**
     * The length of the array.
     */
    size_t length;

    /**
     * The elements.
     */
    void *elements;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialize a dynamic array.
 * @param arr The array struct to initialize.
 * @param elementSize The size of each element.
 * @note An @p elementSize of zero will result in undefined behaviour.
 * @relates GE_Array
 */
void GE_Array_Init(GE_Array *arr, size_t elementSize);

/**
 * Initialize a dynamic array.
 * @param arr The array struct to initialize.
 * @param config A config for the array.
 * @note A config->elementSize of zero will result in undefined behaviour.
 * @relates GE_Array
 */
void GE_Array_InitConfig(GE_Array *arr, const GE_ArrayConfig *config);

/**
 * Deinitialize a dynamic array.
 * @param arr The array to deinitialize.
 * @note This will call GE_ArrayDeinitElement() for all elements if arr->fDeinitElement is non-NULL.
 * @relates GE_Array
 */
void GE_Array_Deinit(GE_Array *arr);

/**
 * Remove all elements from the array.
 * @param arr The array.
 * @note This will call GE_ArrayDeinitElement() for all elements if arr->fDeinitElement is non-NULL.
 * @relates GE_Array
 */
void GE_Array_Clear(GE_Array *arr);

/**
 * Get the length of the array.
 * @param arr The array.
 * @returns The number of elements in the array.
 * @relates GE_Array
 */
size_t GE_Array_GetLength(const GE_Array *arr);

/**
 * Set the length of the array.
 * @param arr The array.
 * @param length The new length.
 * @note This will call GE_ArrayDeinitElement() for all truncated elements if arr->fDeinitElement is non-NULL.
 * @note If new elements are added they will be zeroed.
 * @relates GE_Array
 */
void GE_Array_SetLength(GE_Array *arr, size_t length);

/**
 * Get elements from the array.
 * @param arr The array.
 * @param index Where to start getting elements.
 * @param num The number of elements to get.
 * @param[out] retElements Return elements here.
 * @returns The number of returned elements, zero if @p index is out of range or @p num is zero.
 * @note The array will NOT clone @p retElements or any of their data, it's just a simple memcpy.
 *
 * The function may return < @p num events if there are no more elements
 * to get from the specified @p index.
 * @relates GE_Array
 */
size_t GE_Array_Get(const GE_Array *arr, size_t index, size_t num, void *retElements);

/**
 * Set elements in the array.
 * @param arr The array.
 * @param index Where to start setting elements.
 * @param num The number of elements to set.
 * @param elements The elements to set.
 * @returns The number of set elements, zero if @p index is out of range or @p num is zero.
 * @note This will call GE_ArrayDeinitElement() for all overwritten elements if arr->fDeinitElement is non-NULL.
 * @note The array will NOT clone @p elements or any of their data, it's just a simple memcpy.
 *
 * The function may return < @p num events if there are no more elements
 * to set from the specified @p index.
 * @relates GE_Array
 */
size_t GE_Array_Set(GE_Array *arr, size_t index, size_t num, const void *elements);

/**
 * Insert elements into the array.
 * @param arr The array.
 * @param index Where to start inserting elements.
 * @param num The number of elements to insert.
 * @param elements The elements to insert.
 * @returns The number of set elements, zero if @p index is out of range or @p num is zero.
 * @note The array will NOT clone @p elements or any of their data, it's just a simple memcpy.
 * @relates GE_Array
 */
size_t GE_Array_Insert(GE_Array *arr, size_t index, size_t num, const void *elements);

/**
 * Append elements to the array.
 * @param arr The array.
 * @param num The number of elements to append.
 * @param elements The elements to append.
 * @returns The number of appended elements, zero if @p num is zero.
 * @note The array will NOT clone @p elements or any of their data, it's just a simple memcpy.
 * @relates GE_Array
 */
size_t GE_Array_Append(GE_Array *arr, size_t num, const void *elements);

/**
 * Remove elements from the array.
 * @param arr The array.
 * @param index Where to start removing elements.
 * @param num The number of elements to remove.
 * @returns The number of removed elements, zero if @p index is out of range or @p num is zero.
 * @note This will call GE_ArrayDeinitElement() for all removed elements if arr->fDeinitElement is non-NULL.
 * @note The array will NOT clone @p elements or any of their data, it's just a simple memcpy.
 *
 * The function may return < @p num events if there are no more elements
 * to remove from the specified @p index.
 * @relates GE_Array
 */
size_t GE_Array_Remove(GE_Array *arr, size_t index, size_t num);

/**
 * Return the pointer to element at an index.
 * @param arr The array.
 * @param index The element to get the pointer to.
 * @returns A pointer to the element or NULL if @p index is out of range.
 * @relates GE_Array
 */
const void* GE_Array_GetElementAt(const GE_Array *arr, size_t index);

/**
 * Can be used as @ref GE_ArrayDeinitElement callback for strings.
 * @param element A pointer to the element to deinitialize.
 * @param arg Passed from config->argDeinitElement.
 */
void GE_Array_DeinitElementString(void *element, void *arg);
    
#ifdef __cplusplus
}
#endif

#endif /* GE_ARRAY_H */
