/**
* @file utils/interpolation.h
* @brief Interpolation algos.
* @author Peter Gebauer
* @copyright Copyright © 2020 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*/

#ifndef GE_INTERPOLATION_H
#define GE_INTERPOLATION_H

#include "GE_Core/common.h"

/**
 * @brief Helper macro to get a string representation from an interpolation type enum.
 * @param interpolationType The interpolation type enum.
 * @returns A `const char*` pointer to the name, will be an empty string if @p interpolationType is invalid.
 */
#define GE_INTERPOLATION_TYPE_STRING(interpolationType) ((interpolationType) >= GE_INTERPOLATION_TYPE_NONE && (interpolationType) < MAX_GE_INTERPOLATION_TYPE ? GE_INTERPOLATION_TYPE_STRINGS[(interpolationType)] : "")

/**
 * @brief The interpolation type.
 */
enum GE_InterpolationType
{

    /**
     * @brief No interpolation.
     */
    GE_INTERPOLATION_TYPE_NONE,

    /**
     * @brief Linear interpolation.
     */
    GE_INTERPOLATION_TYPE_LINEAR,

    /**
     * @brief Square interpolation.
     */
    GE_INTERPOLATION_TYPE_SQUARE,

    /**
     * @brief Inverse square interpolation.
     */
    GE_INTERPOLATION_TYPE_INV_SQUARE,

    /**
     * @brief Cubic interpolation.
     */
    GE_INTERPOLATION_TYPE_CUBIC,

    /**
     * @brief Inverse cubic interpolation.
     */
    GE_INTERPOLATION_TYPE_INV_CUBIC,

    /**
     * @brief Cubic Bezier curve.
     * @note NOT IMPLEMENTED YET
     */
    GE_INTERPOLATION_TYPE_CURVE,

    /**
     * @brief Max enum.
     */
    MAX_GE_INTERPOLATION_TYPE

};

/**
 * @see enum GE_InterpolationType
 */
typedef enum GE_InterpolationType GE_InterpolationType;

/**
 * @see struct GE_Interpolation
 */
typedef struct GE_Interpolation GE_Interpolation;

/**
 * @brief Hold's interpolation data.
 */
struct GE_Interpolation
{

    /**
     * @brief The interpolation type.
     */
    GE_InterpolationType type;

    /**
     * @brief Type-specific data.
     */
    union {

        /**
         * @ref GE_INTERPOLATION_TYPE_CURVE
         */
        struct {

            /**
             * @brief The first handle for parametric interpolations.
             * @note Valid handle values are between 0.0 and 1.0.
             */
            double handle1[2];

            /**
             * @brief The second handle for parametric interpolations.
             * @note Valid handle values are between 0.0 and 1.0.
             */
            double handle2[2];

        } curve;

    } i;

};

/**
 * @brief String representations of the interpolation type enum.
 */
extern GE_PUBLIC const char *GE_INTERPOLATION_TYPE_STRINGS[MAX_GE_INTERPOLATION_TYPE];

/**
 * @brief Interpolate between two values.
 * @param interpolation Holds the type and settings for the interpolation, NULL for linear.
 * @param start The start value (factor <= 0.0).
 * @param end The end value (factor >= 1.0).
 * @param factor A value between 0.0 and 1.0.
 * @returns The interpolated value.
 * @note GE_INTERPOLATION_TYPE_CURVE is NOT IMPLEMENTED YET
 */
double GE_Interpolate(const GE_Interpolation *interpolation, double start, double end, double factor);

/**
 * @brief Debug dump for an interpolation.
 * @param interpolation The interpolation to dump.
 * @param f The output stream.
 * @param indent The indentation (number of spaces).
 */
void GE_Interpolation_DebugDump(const GE_Interpolation *interpolation, FILE *f, unsigned int indent);

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* GE_SEQUENCER_H */
