/**
 * @file g2preset.h
 * @brief Convert between Gestrument 2 and GE Core states.
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_G2PRESET_H
#define GE_G2PRESET_H

#include <stdio.h>
#include "GE_Core/common.h"
#include "GE_Core/utils/plist.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Generic Gestrument 2 preset error.
 */
#define GE_G2PRESET_ERROR -100

/**
 * The property list is invalid for a Gestrument 2 preset.
 */
#define GE_G2PRESET_ERROR_PLIST -101

/**
 * Convert a Property List variant to a Gestrument 2 preset.
 * @param plist The property list.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns The Gestrument 2 preset or NULL on error.
 */
GE_Variant* GE_G2Preset_FromPropertyList(const GE_Variant *plist, int *retError);

/**
 * Convert a Gestrument 2 preset to a GE core state.
 * @param preset The preset to convert.
 * @returns The GE Core state.
 */
GE_Variant* GE_G2Preset_ToState(const GE_Variant *preset);

/**
 * Read a Gestrument 2 preset.
 * @param f The file to read from.
 * @param[out] retSize Return the deserialized (file) size.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns The Gestrument 2 preset or NULL on error.
 * @see GE_G2Preset_ToState()
 */
GE_Variant* GE_G2Preset_ReadFile(FILE *f, size_t *retSize, int *retError);

/**
 * Read a Gestrument 2 preset.
 * @param path The path to the file.
 * @param[out] retSize Return the deserialized (file) size.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns The Gestrument 2 preset or NULL on error.
 * @see GE_G2Preset_ToState()
 */
GE_Variant* GE_G2Preset_ReadPath(const char *path, size_t *retSize, int *retError);

/**
 * Read a Gestrument 2 preset and return a GE Core state.
 * @param f The file to read from.
 * @param[out] retSize Return the deserialized (file) size.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns The GE Core state or NULL on error.
 */
GE_Variant* GE_G2Preset_ReadFileAsState(FILE *f, size_t *retSize, int *retError);

/**
 * Read a Gestrument 2 preset and return a GE Core state.
 * @param path The path to the file.
 * @param[out] retSize Return the deserialized (file) size.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns The GE Core state or NULL on error.
 */
GE_Variant* GE_G2Preset_ReadPathAsState(const char *path, size_t *retSize, int *retError);

#ifdef __cplusplus
}
#endif

#endif
