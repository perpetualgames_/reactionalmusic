/**
* @file math.h
* @brief Math functions
* @author David Granström
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_MATH_H
#define GE_MATH_H

#include "GE_Core/common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Return a pseudo-random number.
 * @returns A psuedo-random value between 0 and 1.
 */
double GE_Random(void);

/**
 * Return a pseudo-random number.
 * @returns A psuedo-random value between INT_MIN and INT_MAX.
 */
int GE_RandomInt(void);

/**
 * Return a pseudo-random number.
 * @returns A psuedo-random value between 0 and UINT_MAX.
 */
unsigned int GE_RandomUint(void);

/**
 * @brief Helper function to check if two floating point values are near.
 * @param a Value A.
 * @param b Value B.
 * @param epsilon Largest diff to accept as "equal".
 * @returns True or false.
 */
bool GE_DoubleNear(double a, double b, double epsilon);

/**
 * @brief Helper function to check if two floating point values are near.
 * @param a Value A.
 * @param b Value B.
 * @param epsilon Largest diff to accept as "equal".
 * @returns True or false.
 */
bool GE_FloatNear(float a, float b, float epsilon);

/**
 * @brief Helper function to clamp a value.
 * @param v The value.
 * @param minimum The minimum value.
 * @param maximum The maximum value.
 * @returns A value >= @p minimum and <= @p maximum.
 * @note If @p minimum > @p maximum they will be automatically swapped.
 */
float GE_FloatClamp(float v, float minimum, float maximum);

/**
 * @brief Helper function to clamp a value.
 * @param v The value.
 * @param minimum The minimum value.
 * @param maximum The maximum value.
 * @returns A value >= @p minimum and <= @p maximum.
 */
double GE_DoubleClamp(double v, double minimum, double maximum);

/**
 * @brief Helper function to clamp a value.
 * @param v The value.
 * @param minimum The minimum value.
 * @param maximum The maximum value.
 * @returns A value >= @p minimum and <= @p maximum.
 */
int GE_IntClamp(int v, int minimum, int maximum);

/**
 * @brief Helper function to modulo a value as always positive.
 * @param v The value.
 * @param maximum The maximum value.
 * @returns A value >= 0 and < @p maximum.
 * @note If @p maximum is negative the sign will be inversed.
 * @note If @p maximum is zero the function simply returns zero.
 */
float GE_FloatMod(float v, float maximum);

/**
 * @brief Helper function to modulo a value as always positive.
 * @param v The value.
 * @param maximum The maximum value.
 * @returns A value >= 0 and < @p maximum.
 * @note If @p maximum is negative the sign will be inversed.
 * @note If @p maximum is zero the function simply returns zero.
 */
double GE_DoubleMod(double v, double maximum);

/**
 * @brief Helper function to modulo a value as always positive.
 * @param v The value.
 * @param maximum The maximum value.
 * @returns A value >= 0 and < @p maximum.
 * @note If @p maximum is negative the sign will be inversed.
 * @note If @p maximum is zero the function simply returns zero.
 */
int GE_IntMod(int v, int maximum);

/**
 * @brief Helper function to handle looping floats.
 * @param v The value.
 * @param maximum The maximum value.
 * @param loop True to use a modulo instead of a clamp.
 * @returns The clamped or modulo value.
 */
float GE_FloatLoop(float v, float maximum, bool loop);

/**
 * @brief Helper function to handle looping doubles.
 * @param v The value.
 * @param maximum The maximum value.
 * @param loop True to use a modulo instead of a clamp.
 * @returns The clamped or modulo value.
 */
double GE_DoubleLoop(double v, double maximum, bool loop);

/**
 * @brief Helper function to modulo a value as always positive.
 * @param v The value.
 * @param maximum The maximum value.
 * @param loop True to use a modulo instead of a clamp.
 * @returns The clamped or modulo value.
 * @note The clamped minimum value is -1.
 */
int GE_IntLoop(int v, int maximum, bool loop);


#ifdef __cplusplus
}
#endif

#endif /* GE_MATH_H */
