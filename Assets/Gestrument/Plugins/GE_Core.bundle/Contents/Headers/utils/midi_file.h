/**
* @file midi_file.h
* @brief GE_MidiFile header
* @author Peter Gebauer
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_MIDI_FILE_H
#define GE_MIDI_FILE_H

#include <limits.h>
#include "GE_Core/common.h"
#include "GE_Core/note_event.h"

/**
 * @brief Get human readable type name.
 * @param eventType The event type.
 * @returns The name or an empty string if @p eventType is invalid.
 */
#define GE_MIDI_EVENT_TYPE_STRING(eventType) ((eventType) >= GE_MIDI_EVENT_TYPE_NONE && (eventType) < MAX_GE_MIDI_EVENT_TYPE ? GE_MIDI_EVENT_TYPE_STRINGS[(eventType)] : "")

/**
 * @brief Largest value for a VLQ.
 */
#define GE_MIDI_FILE_MAX_VLQ 0x200000

/**
 * An IO error during fread().
 */
#define GE_MIDI_FILE_ERROR_IO -1

/**
 * Invalid header chunk.
 */
#define GE_MIDI_FILE_ERROR_HEADER -2

/**
 * Invalid track chunk.
 */
#define GE_MIDI_FILE_ERROR_TRACK -3

/**
 * Invalid VLQ.
 */
#define GE_MIDI_FILE_ERROR_VLQ -4

/**
 * Invalid event.
 */
#define GE_MIDI_FILE_ERROR_EVENT -5

/**
 * Unexpected EOF or end of track.
 */
#define GE_MIDI_FILE_ERROR_EOF -6

/**
 * A buffer overflowed.
 */
#define GE_MIDI_FILE_ERROR_OVERFLOW -7

/**
 * we don't support that!
 */
#define GE_MIDI_FILE_ERROR_UNSUPPORTED -8

/**
 * @see struct GE_MIDIFileEvent
 */
typedef struct GE_MIDIFileEvent GE_MIDIFileEvent;

/**
 * @brief MIDI event types.
 */
typedef enum {

    /**
     * @brief Invalid event.
     */
    GE_MIDI_EVENT_TYPE_NONE,

    /**
     * @brief Note off (0x80).
     */
    GE_MIDI_EVENT_TYPE_NOTE_OFF,

    /**
     * @brief Note on (0x90).
     */
    GE_MIDI_EVENT_TYPE_NOTE_ON,

    /**
     * @brief Track name (0xff, 0x03, len).
     */
    GE_MIDI_EVENT_TYPE_TRACK_NAME,

    /**
     * @brief End of track (0xff, 0x2f, 0x00).
     */
    GE_MIDI_EVENT_TYPE_EOT,

    /**
     * @brief Set tempo (0xff, 0x51, 0x03).
     */
    GE_MIDI_EVENT_TYPE_TEMPO,

    /**
     * @brief Set time signature (0xff, 0x58, 0x04).
     */
    GE_MIDI_EVENT_TYPE_TIME_SIG,

    /**
     * @brief Set key signature (0xff, 0x59, 0x04).
     */
    GE_MIDI_EVENT_TYPE_KEY_SIG,

    /**
     * @brief Max enum.
     */
    MAX_GE_MIDI_EVENT_TYPE

} GE_MIDIEventType;

/**
 * @brief MIDI file formats.
 */
typedef enum {

    /**
     * @brief Single multi-channel track.
     */
    GE_MIDI_FILE_FORMAT_SINGLE,

    /**
     * @brief One or more simultaneously played tracks.
     */
    GE_MIDI_FILE_FORMAT_SIMULTANEOUS,

    /**
     * @brief One or more independently played tracks.
     */
    GE_MIDI_FILE_FORMAT_INDEPENDENT,

    MAX_GE_MIDI_FILE_FORMAT

} GE_MIDIFileFormat;

/**
 * @brief This new MIDI API will replace the old one.
 */
typedef struct GE_MIDIEvent
{

    /**
     * @brief Event type.
     */
    GE_MIDIEventType type;

    /**
     * @brief Delta/timestamp.
     */
    uint32_t delta;

    /**
     * @brief Type specific data.
     */
    union {

        /**
         * @ref GE_MIDI_EVENT_TYPE_NOTE_OFF.
         */
        struct {
            uint8_t channel;
            uint8_t key;
            uint8_t velocity;
        } note_off;

        /**
         * @ref GE_MIDI_EVENT_TYPE_NOTE_OFF.
         */
        struct {
            uint8_t channel;
            uint8_t key;
            uint8_t velocity;
        } note_on;

        /**
         * @ref GE_MIDI_EVENT_TYPE_TRACK_NAME.
         */
        char *track_name;

        /**
         * @ref GE_MIDI_EVENT_TYPE_TEMPO.
         * @note Not implemented yet.
         */
        uint32_t tempo;

        /**
         * @ref GE_MIDI_EVENT_TYPE_TIME_SIG.
         * @note Not implemented yet.
         */
        struct {
            uint8_t numerator;
            uint8_t denominator;
        } time_sig;

        /**
         * @ref GE_MIDI_EVENT_TYPE_KEY_SIG.
         * @note Not implemented yet.
         */
        struct {
            uint8_t sharps_flats;
            bool minor;
        } key_sig;

    } e;

} GE_MIDIEvent;

/**
 * @brief A MIDI track has an array of MIDI events.
 */
typedef struct GE_MIDITrack
{
    char *name;
    int numEvents;
    GE_MIDIEvent *events;
} GE_MIDITrack;

/**
 * @brief A MIDI file has a list of tracks and some info.
 */
typedef struct GE_MIDIFile
{
    GE_MIDIFileFormat format;
    uint16_t ticksPerQuarter;
    uint8_t smtpeFormat;
    uint8_t ticksPerFrame;
    int numTracks;
    GE_MIDITrack *tracks;
} GE_MIDIFile;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Human readable names for event types.
 * @param type The type.
 * @returns A pointer to a name string or an empty string if @p type is invalid.
 */
const char *GE_MIDIEventType_GetName(GE_MIDIEventType type);

/**
 * Read note events from a MIDI file.
 * @param f The file stream to read from.
 * @param fail True to fail if erronous events are encountered.
 * @param[out] ret_events Return a newly allocated array, will be NULL on zero and unmodified on error. Passing NULL is permitted.
 * @param[out] ret_size Return the number of bytes read from the stream, may be NULL to omit.
 * @returns The number of returned events or a negative value on error.
 * @deprecated Start using GE_MIDIFile_Read() instead.
 */
int GE_MidiFile_ReadStream(FILE * f, bool fail, GE_NoteEvent ** ret_events, size_t * ret_size);

/**
 * Read events from a Standard MIDI File.
 * @param f The file stream to read from.
 * @param[out] retError If the function returns NULL an error code will be written here.
 * @param[out] retSize If the function returns non-NULL the number of read bytes will be written here.
 * @returns A new MIDI file or NULL on error.
 * @see GE_MIDIFile_Free()
 */
GE_MIDIFile* GE_MIDIFile_Read(FILE * f, int *retError, size_t *retSize);

/**
 * Read events from a Standard MIDI File.
 * @param path The path to read from.
 * @param[out] retError If the function returns NULL an error code will be written here.
 * @param[out] retSize If the function returns non-NULL the number of read bytes will be written here.
 * @returns A new MIDI file or NULL on error.
 * @see GE_MIDIFile_Free()
 */
GE_MIDIFile* GE_MIDIFile_ReadPath(const char *path, int *retError, size_t *retSize);

/**
 * @brief Free a MIDI file, all it's tracks and their events.
 * @param midiFile The MIDI file to free.
 */
void GE_MIDIFile_Free(GE_MIDIFile *midiFile);

#ifdef __cplusplus
}
#endif

#endif
