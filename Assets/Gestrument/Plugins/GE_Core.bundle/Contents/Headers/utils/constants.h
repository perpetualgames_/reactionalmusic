//
//  constants.h
//  Gestrument Engine Core
//
//  Created by David Granström 2018-06-25
//  Copyright © 2018 Gestrument AB. All rights reserved.
//

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define GE_MAX_VOICES 128

#endif /* CONSTANTS_H */
