/**
 * @file circular_buffer.h
 * @brief A single producer, single consumer, thread safe circular buffer.
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_CIRCULAR_BUFFER_H
#define GE_CIRCULAR_BUFFER_H

#include "GE_Core/common.h"
#include "GE_Core/utils/atomic.h"

/**
 * Returned by some functions to indicate that a buffer is empty.
 */
#define GE_CIRCULAR_BUFFER_ERR_EMPTY -1

/**
 * Returned by some functions to indicate that a buffer is full.
 */
#define GE_CIRCULAR_BUFFER_ERR_FULL -2

/**
 * @see struct GE_CircularBuffer
 */
typedef struct GE_CircularBuffer GE_CircularBuffer;

/**
 * A single producer, single consumer, thread safe circular buffer.
 */
struct GE_CircularBuffer {

    /**
     * The data.
     */
    void *data;

    /**
     * The size (number of bytes).
     */
    size_t size;

    /**
     * Tail index.
     */
    size_t tail;

    /**
     * Head index.
     */
    size_t head;

    /**
     * The number of pending bytes.
     */
    GE_AtomicSize count;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialize circular buffer.
 * @param buffer The buffer struct to initialize.
 * @param size The (fixed max size) size in bytes.
 * @relates GE_CircularBuffer
 *
 * This circular buffer is a single producer, single consumer and thread safe.
 */
void GE_CircularBuffer_Init(GE_CircularBuffer *buffer, size_t size);

/**
 * Deinitialize circular buffer.
 * @param buffer The buffer to deinitialize.
 * @relates GE_CircularBuffer
 */
void GE_CircularBuffer_Deinit(GE_CircularBuffer *buffer);

/**
 * Get the size of the buffer.
 * @param buffer The buffer.
 * @returns The buffer size (in bytes).
 * @relates GE_CircularBuffer
 */
size_t GE_CircularBuffer_GetSize(const GE_CircularBuffer *buffer);

/**
 * Get the tail size.
 * @param buffer The buffer.
 * @returns The number of available bytes at the tail.
 * @relates GE_CircularBuffer
 */
size_t GE_CircularBuffer_GetTailSize(const GE_CircularBuffer *buffer);

/**
 * Copy bytes from the buffer tail.
 * @param buffer The buffer.
 * @param offset Offset from tail.
 * @param size The number of bytes to copy.
 * @param[out] data Copy to this pointer.
 * @returns The number of copied bytes.
 * @note If there is not enough bytes available this function will truncate the copied bytes.
 * @relates GE_CircularBuffer
 */
size_t GE_CircularBuffer_CopyFromTail(GE_CircularBuffer *buffer, size_t offset, size_t size, void *data);

/**
 * Advance the tail of the buffer.
 * @param buffer The buffer.
 * @param size The number of bytes to advance.
 * @relates GE_CircularBuffer
 */
void GE_CircularBuffer_AdvanceTail(GE_CircularBuffer *buffer, size_t size);

/**
 * Get the head size.
 * @param buffer The buffer.
 * @returns The number of available bytes at the head.
 * @relates GE_CircularBuffer
 */
size_t GE_CircularBuffer_GetHeadSize(const GE_CircularBuffer *buffer);

/**
 * Copy bytes to the buffer head.
 * @param buffer The buffer.
 * @param offset Offset from tail.
 * @param size The number of bytes to copy.
 * @param[out] data Copy from this pointer.
 * @returns The number of copied bytes.
 * @note If there is not enough bytes available this function will truncate the copied data.
 * @relates GE_CircularBuffer
 */
size_t GE_CircularBuffer_CopyToHead(GE_CircularBuffer *buffer, size_t offset, size_t size, const void *data);

/**
 * Advance the head of the buffer.
 * @param buffer The buffer.
 * @param size The number of bytes to advance.
 * @relates GE_CircularBuffer
 */
void GE_CircularBuffer_AdvanceHead(GE_CircularBuffer *buffer, size_t size);

/**
 * Copy the tail of one buffer to the head of another.
 * @param buffer Copy from this buffer tail.
 * @param size The number of bytes to copy.
 * @param dst_buffer Copy to this buffer head.
 * @returns Zero on success and a negative error code on failure.
 * @note A @p size greater than either buffer's size will result in undefined behaviour.
 * @relates GE_CircularBuffer
 *
 * The returned error codes may be one of the following:
 * - @ref GE_CIRCULAR_BUFFER_ERR_EMPTY (@p buffer is empty)
 * - @ref GE_CIRCULAR_BUFFER_ERR_FULL (@p dst_buffer is full - can't accomodate @p size more bytes)
 */
int GE_CircularBuffer_CopyToBuffer(const GE_CircularBuffer *buffer, size_t size, GE_CircularBuffer *dst_buffer);

/**
 * Output a debug dump.
 * @param buffer The circular buffer.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_CircularBuffer
 */
void GE_CircularBuffer_DebugDump(const GE_CircularBuffer *buffer, FILE *f, int indent);

#ifdef __cplusplus
}
#endif

#endif
