/**
 * @file plist.h
 * @brief Property list read/write for GE_Variant.
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_PLIST_H
#define GE_PLIST_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "GE_Core/utils/variant.h"

/**
 * Generic error.
 * @note A GE_PLIST_ERROR must be > -100, see @ref GE_G2PRESET_ERROR.
 */
#define GE_PLIST_ERROR -1

/**
 * Error reading/writing file.
 * @note A GE_PLIST_ERROR must be > -100, see @ref GE_G2PRESET_ERROR.
 */
#define GE_PLIST_ERROR_IO -2

/**
 * Invalid data while serializing/deserializing.
 * @note A GE_PLIST_ERROR must be > -100, see @ref GE_G2PRESET_ERROR.
 */
#define GE_PLIST_ERROR_INVALID -3

/**
 * Unsupported version.
 * @note A GE_PLIST_ERROR must be > -100, see @ref GE_G2PRESET_ERROR.
 */
#define GE_PLIST_ERROR_UNSUPPORTED -4

/**
 * Not enough size.
 * @note A GE_PLIST_ERROR must be > -100, see @ref GE_G2PRESET_ERROR.
 */
#define GE_PLIST_ERROR_SIZE -5

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Deserialize a binary property list to a variant.
 * @param size The size of the data.
 * @param data Read data here.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns A variant instance or NULL on error.
 */
GE_Variant* GE_BinaryPList_Deserialize(size_t size, const void *data, int *retError);

/**
 * Deserialize a binary property list to a variant.
 * @param f The stream to deserialize from.
 * @param[out] retSize Return the deserialized (file) size.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns A variant instance or NULL on error.
 */
GE_Variant* GE_BinaryPList_DeserializeFile(FILE *f, size_t *retSize, int *retError);

/**
 * Deserialize a binary property list to a variant.
 * @param path The path to the file.
 * @param[out] retSize Return the deserialized (file) size.
 * @param[out] retError Return error code here (will be 0 on success).
 * @returns A variant instance or NULL on error.
 */
GE_Variant* GE_BinaryPList_DeserializePath(const char *path, size_t *retSize, int *retError);

#ifdef __cplusplus
}
#endif

#endif /* GE_ENGINE_H */
