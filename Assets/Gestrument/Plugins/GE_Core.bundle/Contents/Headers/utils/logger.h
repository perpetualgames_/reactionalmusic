/**
* @file logger.h
* @brief Logger macro for debugging
* @author David Granström
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_LOGGER_H
#define GE_LOGGER_H

#include "GE_Core/common.h"
#include <stdarg.h>

/**
 * Get the string representation for a log level.
 * @param l_ The log level.
 * @returns The log level string or NULL if the log level is invalid.
 */
#define GE_LOG_LEVEL_STRING(l_) ((l_) >= GE_LOG_LEVEL_NONE && (l_) < MAX_GE_LOG_LEVEL ? GE_LOG_LEVEL_STRINGS[(l_)] : NULL)

/**
 * Used to override fprintf in GE_LogMessageV()
 * @param message This could be good news or bad news.
 * @param arg Arbitrary user data.
 */
typedef void (*GE_LogCallback)(const char *message, void *arg);

/**
 * Log levels.
 */
typedef enum GE_LogLevel {

    /**
     * Nothing is logged.
     */
    GE_LOG_LEVEL_NONE,

    /**
     * These are used to report serious errors such as faulty configurations.
     */
    GE_LOG_LEVEL_ERROR,

    /**
     * Used to report "soft" errors and warnings such as file not found.
     */
    GE_LOG_LEVEL_WARNING,

    /**
     * Information that might be useful, but not overly verbose.
     */
    GE_LOG_LEVEL_INFO,

    /**
     * Include all debug info available.
     */
    GE_LOG_LEVEL_DEBUG,

    /**
     * Max enum.
     */
    MAX_GE_LOG_LEVEL
} GE_LogLevel;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Log a message.
 * @param level The level to log for.
 * @param fmt The format.
 * @param args Variable arguments.
 * @note A newline will be added automatically after the formatted message.
 */
void GE_LogMessageV(GE_LogLevel level, const char *fmt, va_list args);

/**
 * Log a message.
 * @param level The level to log for.
 * @param fmt The format.
 * @note A newline will be added automatically after the formatted message.
 */
void GE_LogMessage(GE_LogLevel level, const char *fmt, ...) GE_PRINTF_FORMAT(2, 3);

/**
 * Log an error message.
 * @param fmt The format.
 * @note A newline will be added automatically after the formatted message.
 */
void GE_LogError(const char *fmt, ...) GE_PRINTF_FORMAT(1, 2);

/**
 * Log an warning message.
 * @param fmt The format.
 * @note A newline will be added automatically after the formatted message.
 */
void GE_LogWarning(const char *fmt, ...) GE_PRINTF_FORMAT(1, 2);

/**
 * Log an info message.
 * @param fmt The format.
 * @note A newline will be added automatically after the formatted message.
 */
void GE_LogInfo(const char *fmt, ...) GE_PRINTF_FORMAT(1, 2);

/**
 * Log an debug message.
 * @param fmt The format.
 * @note A newline will be added automatically after the formatted message.
 */
void GE_LogDebug(const char *fmt, ...) GE_PRINTF_FORMAT(1, 2);

/**
 * Wrapper to get the log level.
 * @returns The level to log for.
 */
GE_LogLevel GE_GetLogLevel(void);

/**
 * Wrapper to set the log level.
 * @param level The level to log for.
 */
void GE_SetLogLevel(GE_LogLevel level);

/**
 * Wrapper to get the log level string.
 * @param level The level to get the string for.
 * @returns The level string or an empty string if @p level is invalid.
 */
const char* GE_GetLogLevelString(GE_LogLevel level);

/**
 * Set log stream.
 * @param f The stream.
 */
void GE_SetLogStream(FILE *f);

/**
 * Get log stream.
 * @returns The file stream.
 */
FILE* GE_GetLogStream(void);

/**
 * Set the log callback.
 * @param callback The log callback.
 * @param arg Arbitrary user data.
 */
void GE_SetLogCallback(GE_LogCallback callback, void *arg);

/**
 * Get the log callback.
 * @returns The log callback.
 */
GE_LogCallback GE_GetLogCallback(void);

#ifdef __cplusplus
}
#endif

#endif /* LOGGER_H */
