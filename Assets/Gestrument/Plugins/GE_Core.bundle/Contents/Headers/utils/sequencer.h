/**
 * @file utils/sequencer.h
 * @brief Simple event sequencer.
 * @author Peter Gebauer
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 *
 * - add multiple events with same beat from same track.
 * - ProcessMultiple/Jump with zero delta should return same events again, update docs.
 */

#ifndef GE_SEQUENCER_H
#define GE_SEQUENCER_H

#include "GE_Core/common.h"
#include "GE_Core/utils/interpolation.h"
#include "GE_Core/utils/mutex.h"

/**
 * @brief The maximum number of generated events the sequencer can return
 * in a single call to any of the GE_Sequencer_ProcessBeats().
 */
#define GE_SEQUENCER_MAX_GENERATED_EVENTS 1024

/**
 * @brief Beats are near if 1/100000th close.
 */
#define GE_BEAT_EPSILON 0.00001

/**
 * @brief Helper macro to get a string representation from an event type enum.
 * @param eventType The event type enum.
 * @returns A `const char*` pointer to the name, will be an empty string if @p eventType is invalid.
 */
#define GE_SEQ_EVENT_TYPE_STRING(eventType) ((eventType) >= GE_SEQ_EVENT_TYPE_NONE && (eventType) < MAX_GE_SEQ_EVENT_TYPE ? GE_SEQ_EVENT_TYPE_STRINGS[(eventType)] : "unknown")

/**
 * @brief Note event literal.
 */
#define GE_SEQ_EVENT_HEAD(type, beat) (type), 0, 0, 0, 0, (beat), 0

/**
 * @brief Note event literal.
 */
#define GE_NOTE_EVENT(beat, pitch, duration, onVelocity, offVelocity) {GE_SEQ_EVENT_HEAD(GE_SEQ_EVENT_TYPE_NOTE, (beat)), {.e_note = {(pitch), (duration), (onVelocity), (offVelocity)}}}

/**
 * @brief Sequencer event types.
 */
enum GE_SeqEventType
{

    /**
     * @brief No event.
     */
    GE_SEQ_EVENT_TYPE_NONE,

    /**
     * @brief Note (ON with duration).
     */
    GE_SEQ_EVENT_TYPE_NOTE,

    /**
     * @brief End of track.
     */
    GE_SEQ_EVENT_TYPE_EOT,

    /**
     * @brief Max enum.
     */
    MAX_GE_SEQ_EVENT_TYPE

};

/**
 * @see enum GE_SeqEventType
 */
typedef enum GE_SeqEventType GE_SeqEventType;

/**
 * @see enum GE_SeqEvent
 */
typedef struct GE_SeqEvent GE_SeqEvent;

/**
 * @see struct GE_SeqTrack
 */
typedef struct GE_SeqTrack GE_SeqTrack;

/**
 * @see struct GE_SeqPulse
 */
typedef struct GE_SeqPulse GE_SeqPulse;

/**
 * @see struct GE_Sequencer
 */
typedef struct GE_Sequencer GE_Sequencer;

/**
 * @brief A sequencer event.
 */
struct GE_SeqEvent
{

    /**
     * @brief The event type.
     */
    GE_SeqEventType type;

    /**
     * @brief The index of the track that generated the event.
     */
    int trackIndex;

    /**
     * @brief The index of the event in a track.
     */
    int eventIndex;

    /**
     * @brief The index of the event in the sequencer.
     */
    int sequencerIndex;

    /**
     * @brief What pulse the event is part of.
     */
    int pulseIndex;

    /**
     * @brief The beat.
     */
    double beat;

    /**
     * @brief The beat offset.
     * @note Set when generating the event.
     */
    double beatOffset;

    /**
     * @brief Type-specific data.
     */
    union {

        /**
         * @ref GE_SEQ_EVENT_TYPE_NOTE
         */
        struct {

            /**
             * @brief Pitch.
             */
            float pitch;

            /**
             * @brief Duration.
             */
            double duration;

            /**
             * @brief Normalized velocity (0-1).
             */
            float onVelocity;

            /**
             * @brief Normalized velocity (0-1).
             */
            float offVelocity;

            /**
             * @brief For MIDI compatibility.
             */
            signed char channel;

        } e_note;

    } e;

};

/**
 * @brief Base type for all tracks.
 */
struct GE_SeqTrack
{

    /**
     * @brief A descriptive name.
     */
    char *name;

    /**
     * @brief Enabled or disabled.
     */
    bool isEnabled;

    /**
     * @brief The number of events.
     */
    int numEvents;

    /**
     * @brief events.
     */
    GE_SeqEvent *events;

    /**
     * @brief The length in beats of the longest track in the sequencer, including the track's offset.
     */
    double length;

};

/**
 * @brief A pulse.
 */
struct GE_SeqPulse
{

    /**
     * @brief Where in the pulse array the pulse lives.
     */
    int pulseIndex;

    /**
     * @brief The number of events.
     */
    int numEvents;

    /**
     * @brief The events.
     */
    const GE_SeqEvent **events;

    /**
     * @brief The index of the first event in the pulse.
     */
    int firstSequencerIndex;

    /**
     * @brief The index of the first event in the pulse.
     */
    int lastSequencerIndex;

    
};

/**
 * @brief The sequencer.
 */
struct GE_Sequencer
{

    /**
     * @brief A mutex used for locking the sequencer.
     */
    GE_Mutex *mutex;

    /**
     * @brief A descriptive name.
     */
    char *name;

    /**
     * @brief The number of tracks of each type.
     */
    int numTracks;

    /**
     * @brief The tracks for each type.
     */
    GE_SeqTrack *tracks;

    /**
     * @brief The tempo.
     */
    double tempo;

    /**
     * @brief The length in beats of the longest track in the sequencer, including the track's offset.
     */
    double length;

    /**
     * @brief The total number of events for all tracks.
     */
    int numEvents;

    /**
     * @brief Event pointers for all events in all tracks.
     */
    const GE_SeqEvent **events;

    /**
     * @brief Current beat.
     */
    double currentBeat;

    /**
     * @brief Current index for the current beat.
     */
    int currentIndex;

    /**
     * @brief The number of events generated from calling GE_Sequencer_Process*() family of functions.
     */
    int numGeneratedEvents;

    /**
     * @brief Events generated from calling GE_Sequencer_Process*() family of functions.
     */
    GE_SeqEvent *generatedEvents;

    /**
     * @brief The number of pulses.
     */
    int numPulses;

    /**
     * @brief The pulses.
     */
    GE_SeqPulse *pulses;

};

#ifdef __cplusplus
extern "C" {
#endif

//
// Event API
//

/**
 * @brief If the event has a duration member it can be a pulse.
 * @param seqEvent The sequence event.
 * @returns True if the event has duration.
 */
bool GE_SeqEvent_HasDuration(const GE_SeqEvent *seqEvent);

/**
 * @brief Return the beat + duration for the sequence event.
 * @param seqEvent The sequence event.
 * @returns The end beat.
 */
double GE_SeqEvent_EndBeat(const GE_SeqEvent *seqEvent);

/**
 * @brief Return a duration for the sequence event.
 * @param seqEvent The sequence event.
 * @returns The duration in beats, will be zero if the event has no duration.
 */
double GE_SeqEvent_Duration(const GE_SeqEvent *seqEvent);

/**
 * @brief Return on velocity for the sequence event.
 * @param seqEvent The sequence event.
 * @returns The normalized on velocity (0-1) or zero if the event has no on velocity.
 */
double GE_SeqEvent_OnVelocity(const GE_SeqEvent *seqEvent);

/**
 * @brief Return off velocity for the sequence event.
 * @param seqEvent The sequence event.
 * @returns The normalized off velocity (0-1) or zero if the event has no off velocity.
 */
double GE_SeqEvent_OffVelocity(const GE_SeqEvent *seqEvent);

/**
 * @brief Debug dump for a sequencer event.
 * @param event The event to dump.
 * @param f The output stream.
 * @param indent The indentation (number of spaces).
 */
void GE_SeqEvent_DebugDump(const GE_SeqEvent *event, FILE *f, unsigned int indent);

//
// Sequencer API
//

/**
 * @brief Initialize sequencer.
 * @param sequencer The sequencer.
 */
void GE_Sequencer_Init(GE_Sequencer *sequencer);

/**
 * @brief Initialize sequencer as a copy of another sequencer.
 * @param sequencer The sequencer.
 * @param source The sequencer to copy.
 */
int GE_Sequencer_InitCopy(GE_Sequencer *sequencer, const GE_Sequencer *source);

/**
 * @brief Deinitialize sequencer.
 * @param sequencer The sequencer.
 */
void GE_Sequencer_Deinit(GE_Sequencer *sequencer);

/**
 * @brief Lock the sequencer's mutex.
 * @param sequencer The sequencer.
 * @returns Zero on success or -1 on failure.
 * @note Recursive locks are supported.
 */
int GE_Sequencer_Lock(const GE_Sequencer *sequencer);

/**
 * @brief Try to lock the sequencer's mutex.
 * @param sequencer The sequencer.
 * @returns Zero on success or -1 on failure.
 * @note Recursive locks are supported.
 */
int GE_Sequencer_TryLock(const GE_Sequencer *sequencer);

/**
 * @brief Unlock the sequencer's mutex.
 * @param sequencer The sequencer.
 * @returns Zero on success or -1 on failure.
 */
int GE_Sequencer_Unlock(const GE_Sequencer *sequencer);

/**
 * @brief Get the name of the sequencer.
 * @param sequencer The sequencer.
 * @returns The name, never NULL.
 */
const char* GE_Sequencer_GetName(const GE_Sequencer *sequencer);

/**
 * @brief Set the name of the sequencer.
 * @param sequencer The sequencer.
 * @param name The name, NULL for no name.
 */
void GE_Sequencer_SetName(GE_Sequencer *sequencer, const char *name);

/**
 * @brief Get the number of tracks the sequencer has.
 * @param sequencer The sequencer.
 * @returns The number of tracks the sequencer has.
 */
int GE_Sequencer_GetNumTracks(const GE_Sequencer *sequencer);

/**
 * @brief Set the number of tracks the sequencer should have.
 * @param sequencer The sequencer.
 * @param num The number of tracks the sequencer should have.
 */
void GE_Sequencer_SetNumTracks(GE_Sequencer *sequencer, int num);

/**
 * @brief Append tracks to the sequencer.
 * @param sequencer The sequencer.
 * @param num The number of tracks to append.
 * @returns The number of appended track or -1 on error.
 */
int GE_Sequencer_AppendTracks(GE_Sequencer *sequencer, int num);

/**
 * @brief Insert tracks into the sequencer.
 * @param sequencer The sequencer.
 * @param trackIndex The index to insert the new tracks at.
 * @param num The number of tracks to insert.
 * @returns The number of inserted tracks or -1 on error.
 */
int GE_Sequencer_InsertTracks(GE_Sequencer *sequencer, int trackIndex, int num);

/**
 * @brief Remove tracks from the sequencer.
 * @param sequencer The sequencer.
 * @param trackIndex The index of where to start removing tracks.
 * @param num The number of tracks to remove or a negative number to remove all remaining tracks.
 * @returns The number of removed tracks -1 on error.
 */
int GE_Sequencer_RemoveTracks(GE_Sequencer *sequencer, int trackIndex, int num);

/**
 * @brief Get the name of a track.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @returns The name or NULL if @p trackIndex is invalid.
 */
const char* GE_Sequencer_GetTrackName(const GE_Sequencer *sequencer, int trackIndex);

/**
 * @brief Set the name of a track.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @param name The new name.
 *
 * If @p trackIndex is invalid this function does nothing.
 */
void GE_Sequencer_SetTrackName(GE_Sequencer *sequencer, int trackIndex, const char *name);

/**
 * @brief Get the enabled flag for a track.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @returns True if enabled, false if not or if @p trackIndex is invalid.
 *
 * Only enabled tracks are processed by the sequencer.
 */
bool GE_Sequencer_GetIsTrackEnabled(const GE_Sequencer *sequencer, int trackIndex);

/**
 * @brief Set the enabled flag for a track.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @param isEnabled True for enabled.
 *
 * Only enabled tracks are processed by the sequencer.
 *
 * If @p trackIndex is invalid this function does nothing.
 */
void GE_Sequencer_SetIsTrackEnabled(GE_Sequencer *sequencer, int trackIndex, bool isEnabled);

/**
 * @brief Get the length (in beats) of a track.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @returns The length in beats.
 *
 * This returns the beat of the last event, 0 if no events.
 *
 * The length of the track is recalculated when events are
 * added, removed or modified.
 */
double GE_Sequencer_GetTrackLength(const GE_Sequencer *sequencer, int trackIndex);

/**
 * @brief Get the number of events to track has.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @returns The number events the track has. If @p trackIndex is invalid 0 is returned.
 */
int GE_Sequencer_GetNumTrackEvents(const GE_Sequencer *sequencer, int trackIndex);

/**
 * @brief Get events array from a sequencer track.
 * @param sequencer The sequencer.
 * @param trackIndex The track index.
 * @returns The event array or NULL if @p trackIndex is invalid.
 */
const GE_SeqEvent* GE_Sequencer_GetTrackEvents(const GE_Sequencer *sequencer, int trackIndex);

/**
 * @brief Append events to a track.
 * @param sequencer The sequencer.
 * @param trackIndex The index of the track.
 * @param num The number of events to append.
 * @param events The events to append.
 * @returns The index of the first appended event or -1 if @p trackIndex is invalid.
 * @note The offsets in the @p events will be treated as *relative* from the last event in the track.
 */
int GE_Sequencer_AppendTrackEvents(GE_Sequencer *sequencer, int trackIndex, int num, const GE_SeqEvent *events);

/**
 * @brief Add events using absolute offsets.
 * @param sequencer The sequencer.
 * @param trackIndex The index of the track.
 * @param num The number of events to add.
 * @param events The events to add.
 * @returns The number of added events -1 on error.
 * @note The offsets in the events will be treated as *absolute* offsets.
 *
 * If you are looking to append events to the end of the track
 * you should use the faster GE_Sequencer_AppendTrackEvents().
 */
int GE_Sequencer_AddTrackEvents(GE_Sequencer *sequencer, int trackIndex, int num, const GE_SeqEvent *events);

/**
 * @brief Remove events from a track.
 * @param sequencer The sequencer.
 * @param trackIndex The index of the track.
 * @param index The index where to start removing events.
 * @param num The number of events to remove, a negative value will
 * remove all remaining events after @p trackIndex.
 * @returns The number of removed events.
 */
int GE_Sequencer_RemoveTrackEvents(GE_Sequencer *sequencer, int trackIndex, int index, int num);

/**
 * @brief Merge all events from one track to another.
 * @param sequencer The sequencer.
 * @param dstTrackIndex The index of the track to add events to.
 * @param srcTrackIndex The index of the track to add events from.
 * @return The number merged events.
 */
int GE_Sequencer_MergeTracks(GE_Sequencer *sequencer, int dstTrackIndex, int srcTrackIndex);

/**
 * @brief Get the length (beats) of the longest track in the sequencer.
 * @param sequencer The sequencer.
 * @returns The length (beats) of the longest track in the sequencer.
 *
 * The length of the sequencer is recalculated when events are
 * added, removed or modified.
 */
double GE_Sequencer_GetLength(const GE_Sequencer *sequencer);

/**
 * @brief Search the beat and return the track's index.
 * @param sequencer The sequencer.
 * @param beat The beat.
 * @returns The index of the first event at the @p beat or -1 if there are no events.
 *
 * If @p offset is before the first event 0 is returned.
 *
 * If @p offset is after the last event then num events - 1 is returned.
 */
int GE_Sequencer_GetIndexForBeat(const GE_Sequencer *sequencer, double beat);

/**
 * @brief Get the current beat of the sequencer.
 * @param sequencer The sequencer.
 * @returns The current beat.
 */
double GE_Sequencer_GetCurrentBeat(const GE_Sequencer *sequencer);

/**
 * @brief Update the current beat without generating any events.
 * @param sequencer The sequencer.
 * @param beat The current beat.
 * @note The current index will be updated to the first event matching the beat.
 * @note The beat will be clamped to 0 and GE_Sequencer_GetLength().
 *
 * Preforms a binary search on the event array to find set the current index.
 */
void GE_Sequencer_SetCurrentBeat(GE_Sequencer *sequencer, double beat);

/**
 * @brief Get the current index of the sequencer.
 * @param sequencer The sequencer.
 * @returns The current index.
 */
int GE_Sequencer_GetCurrentIndex(const GE_Sequencer *sequencer);

/**
 * @brief Update the current index without generating any events.
 * @param sequencer The sequencer.
 * @param index The current index.
 * @note The current beat will be updated to that of the event at @p index.
 * @note The @p index will be clamped between 0 and GE_Sequencer_GetNumEvents() - 1.
 *
 * Sets the current beat to the beat of the event found at @p index.
 */
void GE_Sequencer_SetCurrentIndex(GE_Sequencer *sequencer, int index);

/**
 * @brief Get the tempo in beats per minute.
 * @param sequencer The sequencer.
 * @returns The tempo in beats per minute.
 * @note The tempo can be changed by events in the sequencer.
 * @note 
 */
double GE_Sequencer_GetTempo(const GE_Sequencer *sequencer);

/**
 * @brief Set the tempo in beats per minute.
 * @param sequencer The sequencer.
 * @param tempo The tempo in beats per minute.
 * @note The tempo can be changed by events in the sequencer.
 */
void GE_Sequencer_SetTempo(GE_Sequencer *sequencer, double tempo);

/**
 * @brief Get the number of events the sequencer has.
 * @param sequencer The sequencer.
 * @returns The number of events.
 */
int GE_Sequencer_GetNumEvents(const GE_Sequencer *sequencer);

/**
 * @brief Get events from the sequencer.
 * @param sequencer The sequencer.
 * @returns A pointer to the event pointers.
 * @note The last event is always a NULL sentinel.
 */
const GE_SeqEvent** GE_Sequencer_GetEvents(const GE_Sequencer *sequencer);

/**
 * @brief Get the number of pulses the sequencer has.
 * @param sequencer The sequencer.
 * @returns The number of pulses.
 */
int GE_Sequencer_GetNumPulses(const GE_Sequencer *sequencer);

/**
 * @brief Get pulses from the sequencer.
 * @param sequencer The sequencer.
 * @returns A pointer to the pulse pointers.
 * @note The last pulse is always a NULL sentinel.
 */
const GE_SeqPulse* GE_Sequencer_GetPulses(const GE_Sequencer *sequencer);

/**
 * @brief Generate events for a number of @p beats and update both current beat and index.
 * @param sequencer The sequencer.
 * @param beats The number of beats to process, negative values move backwards in time.
 * @param loop True if looping is allowed.
 * @param[out] numEvents Return the number of generated events here, NULL to omit.
 * @returns A pointer to the generated events or NULL if no events were generated.
 *
 * This would normally be called by a pulse generator to step forward/backwards in time
 * and generate events for the time window between the current beat and the
 * new target beat (current + @p beats).
 *
 * If @p beats is non-zero the current beat and index will be updated.
 *
 * If @p beats is zero this function does nothing and returns 0.
 *
 * The current beat will be clamped between 0 and GE_Sequencer_GetLength() unless @p loop is true.
 * If @p loop is true current beats will wrap around and loop.
 *
 * Note that while events will be generated for all tracks, you have to check manually if the event belongs
 * to a enabled or disabled track.
 */
const GE_SeqEvent* GE_Sequencer_ProcessBeats(GE_Sequencer *sequencer, double beats, bool loop, int *numEvents);

/**
 * @brief Generates events for a single pulse.
 * @param sequencer The sequencer.
 * @param pulseIndex The index of the pulse to process.
 * @param[out] numEvents Return the number of generated events here, NULL to omit.
 * @returns A pointer to the generated events or NULL if no events were generated.
 */
const GE_SeqEvent* GE_Sequencer_ProcessPulse(GE_Sequencer *sequencer, int pulseIndex, int *numEvents);

/**
 * @brief Read a stream and append tracks to the sequencer.
 * @param sequencer The sequencer.
 * @param f The stream to read from.
 * @param[out] readSize Write the number of read bytes here, NULL to omit.
 * @returns The number of appended tracks or a negative error code on failure.
 * @note The returned error codes are the same as from GE_MIDIFile_Read().
 */
int GE_Sequencer_ImportTracksFromFile(GE_Sequencer *sequencer, FILE *f, size_t *readSize);

/**
 * @brief Read a stream and append tracks to the sequencer.
 * @param sequencer The sequencer.
 * @param path A path to a MIDI file.
 * @param[out] readSize Write the number of read bytes here, NULL to omit.
 * @returns The number of appended tracks or a negative error code on failure.
 * @note The returned error codes are the same as from GE_MIDIFile_Read().
 */
int GE_Sequencer_ImportTracksFromPath(GE_Sequencer *sequencer, const char *path, size_t *readSize);

/**
 * @brief Debug dump for a sequencer.
 * @param sequencer The sequencer to dump.
 * @param f The output stream.
 * @param indent The indentation (number of spaces).
 */
void GE_Sequencer_DebugDump(const GE_Sequencer *sequencer, FILE *f, unsigned int indent);

#ifdef __cplusplus
}
#endif

#endif /* GE_SEQUENCER_H */
