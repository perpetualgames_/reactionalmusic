/**
* @file time.h
* @brief Time related functions.
* @author Peter Gebauer
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
*/

#ifndef GE_TIME_H
#define GE_TIME_H

#include <stdint.h>
#include "GE_Core/common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the current timestamp.
 * @returns Current timestamp (in seconds) or a negative value on error.
 * @note On Windows the resolution is milliseconds, on all other platforms it is nanoseconds.
 * @note This function is not exact, it only guarantees that subsequent calls are >= previous call.
 */
double GE_GetTime(void);

/**
 * Sleep @p seconds (or fractions thereof).
 * @returns The remaining time if the sleep was interrupted or a negative value on error.
 * @note On Windows the resolution is milliseconds, on all other platforms it is nanoseconds.
 * @note The exactness of the sleep is undefined, but specifying milliseconds (or longer) should be quite reliable.
 */
double GE_Sleep(double seconds);

#ifdef __cplusplus
}
#endif


#endif /* LOGGER_H */
