/**
 * @file utils/thread.h
 * @brief Cross-platform thread implementation
 * @author Peter Gebauer
 * @copyright Copyright © 2019 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_THREAD_H
#define GE_THREAD_H

#include <stdint.h>
#include "GE_Core/common.h"

/**
 * @see struct GE_Thread
 */
typedef struct GE_Thread GE_Thread;

/**
 * Function executed by a thread.
 * @param thread The executing thread.
 */
typedef void (*GE_ThreadFunc)(GE_Thread *thread);

/**
 * A cross platform thread.
 */
struct GE_Thread {

    /**
     * The thread ID, set on creation.
     */
    uint64_t id;

    /**
     * Keep a reference to the thread function.
     */
    GE_ThreadFunc threadFunc;

    /**
     * Keep a reference to the arbitrary data argument.
     */
    void *arg;

    /**
     * Implementation specific data.
     *
     * - Windows uses `HANDLE`.
     * - All others use `pthread_t`.
     *
     * 64 bytes should suffice for all platforms.
     */
    unsigned char impl[64];

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the thread ID of the calling thread.
 * @return The current thread ID.
 */
uint64_t GE_GetCurrentThreadID(void);

/**
 * Create a new thread.
 * @param thread The thread struct to initialize.
 * @param threadFunc Function executed by the thread.
 * @param arg Arbitrary data passed to the @p threadFunc.
 * @return Zero on succes and a negative error code on failure.
 * @relates GE_Thread
 */
int GE_Thread_Start(GE_Thread *thread, GE_ThreadFunc threadFunc, void *arg);

/**
 * Join and discard a thread.
 * @param thread Pointer to the thread.
 * @return Zero on succes and a negative error code on failure.
 * @note Any use of @p thread after a successful call results in undefined behaviour.
 * @relates GE_Thread
 */
int GE_Thread_Join(GE_Thread *thread);

/**
 * Get the ID of a thread.
 * @param thread Pointer to the thread.
 * @return The thread ID.
 * @relates GE_Thread
 */
uint64_t GE_Thread_GetID(const GE_Thread *thread);

/**
 * Give another thread a chance to do something.
 * @return 0 on success or a negative error code on failure.
 * @relates GE_Thread
 */
int GE_Thread_Yield(void);

#ifdef __cplusplus
}
#endif

#endif /* GE_THREAD_H */
