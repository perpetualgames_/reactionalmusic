/**
* @file common.h
* @brief GE_Core common includes
* @author David Granström
* @copyright Copyright © 2019 Gestrument AB. All rights reserved.
* @note This file is part of Gestrument Engine Core.
*
* A header which includes common C library headers.
*/

#ifndef GE_COMMON_H
#define GE_COMMON_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#if defined(__GNUC__) || defined(__clang__)
#define GE_PRINTF_FORMAT(string_index_, first_to_check_) __attribute__ ((__format__ (__printf__, string_index_, first_to_check_)))
#else
#define GE_PRINTF_FORMAT(string_index_, first_to_check_)
#endif

#ifdef _WIN32
#  ifdef _WIN64
#    define PRI_SIZET PRIu64
#  else
#    define PRI_SIZET PRIu32
#  endif
#else
#  define PRI_SIZET "zu"
#endif

#define GE_DEBUG_DUMP_INDENT 2

/**
 * A small helper macro to return a pointer if non-NULL, otherwise alternative.
 * @param ptr The pointer.
 * @param alt Return this if @p ptr is NULL.
 */
#define GE_NON_NULL(ptr, alt) ((void*)(ptr) != NULL ? (ptr) : (alt))

/**
 * Visibility support.
 */
#ifdef _WIN32
#ifdef GE_BUILD_API
#define GE_PUBLIC __declspec(dllexport)
#else // GE_BUILD_API
#define GE_PUBLIC
#endif // GE_BUILD_API
#else // _WIN32
#define GE_PUBLIC
#endif // _WIN32
    
#endif /* GE_COMMON_H */
