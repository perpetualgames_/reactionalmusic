/**
 * @file event_recorder.h
 * @brief ControlSource recorder
 * @author Jonatan Liljedahl
 * @author David Granström
 * @copyright Copyright (c) 2017 Jonatan Liljedahl.
 * @note This file is part of Gestrument Engine Core.
 * This source code is released under the MIT License.
 */

/*
MIT License

Copyright (c) 2017 Jonatan Liljedahl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef GE_EVENT_RECORDER_H
#define GE_EVENT_RECORDER_H

#include "GE_Core/common.h"
#include "GE_Core/utils/variant.h"
#include "GE_Core/message_queue.h"

/**
 * The number of bytes for each GE_EREvent in it's serialized form.
 */
#define GE_EVENT_SERIAL_SIZE 12

typedef struct GE_EventRecorder GE_EventRecorder;
typedef struct GE_EREvent GE_EREvent;
typedef struct GE_EREventList GE_EREventList;

#define GE_EVENT_RECORDER_BLOCK_SIZE 64

struct GE_EREvent {
    double time;
    float value;
};

struct GE_EREventList {
    GE_EREvent *events;
    int length;
};

struct GE_EventRecorder {
    // recording
    bool recording;
    GE_EREventList recList;
    size_t allocLength;
    // playback
    GE_EREventList playList;
    int playPos;
    double playTime;
    double duration;
    /**
     * Start phase relative duration.
     */
    double phase;
    // private
    GE_EREventList oldList;
    bool touched;
    float lastValue;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new EventRecorder.
 */
GE_EventRecorder * GE_EventRecorder_New(void);

/**
 * Free EventRecorder.
 */
void GE_EventRecorder_Free(GE_EventRecorder *recorder);

/**
 * Add a event to the recording.
 * Will also set the recording flag to true.
 * @param recorder The EventRecorder.
 * @param time Timestamp of event.
 * @param value The value to record.
 * @relates GE_EventRecorder
 */
void GE_EventRecorder_AddEventAt(GE_EventRecorder *recorder, double time, float value);

/**
 * Replay recorded values until @p time.
 * @param recorder The EventRecorder.
 * @param time Play back values until this time is reached.
 * @param[out] outVal The recorded value.
 * @returns True if there were any events until time, returns false if there was no event and thus no update of the value.
 * @relates GE_EventRecorder
 */
bool GE_EventRecorder_ValueUntilTime(GE_EventRecorder *recorder, double time, float *outVal);

/**
 * Clear the playlist.
 */
void GE_EventRecorder_StopPlay(GE_EventRecorder *recorder);
void GE_EventRecorder_ClearWithMsgQueue(GE_EventRecorder *recorder, GE_MessageQueue *queue);

/**
 * Reset.
 */
void GE_EventRecorder_Reset(GE_EventRecorder *recorder);

bool GE_EventRecorder_FinishRecordingAt(GE_EventRecorder *recorder, double time); // first call this
bool GE_EventRecorder_SwapRecording(GE_EventRecorder *recorder, double duration, float phase); // then this on audio thread
void GE_EventRecorder_DoneRecording(GE_EventRecorder *recorder); // and this in completion callback

/**
 * Return the current state in a dict variant.
 * @param recorder Pointer to a GE_EventRecorder.
 * @returns A dict variant with the current state, must be freed using GE_Variant_Free().
 * @note Will never return NULL (even though the variant may be an empty dict).
 * @relates GE_EventRecorder
 */
GE_Variant * GE_EventRecorder_GetState(const GE_EventRecorder *recorder);

/**
 * Set the current state using a dict variant.
 * Do not call directly while audio thread is running.
 * @param recorder Pointer to a GE_Scale.
 * @param state A dict variant with the state, pass NULL to clear and reset.
 * @returns True if the state was set, false on failure.
 * @relates GE_EventRecorder
 */
bool GE_EventRecorder_SetState(GE_EventRecorder *recorder, const GE_Variant *state);

/**
 * Dump debug info (text) to stream.
 * @param recorder The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 * @relates GE_EventRecorder
 */
void GE_EventRecorder_DebugDump(const GE_EventRecorder *recorder, FILE *f, unsigned int indent);



/**
 * Helper function to serialize byte data from EREvents to the event recorder state.
 * @param length The number of events.
 * @param events The events to serialize.
 * @param[out] data Write the serialized bytes here, must be at least @p length * GE_EVENT_SERIAL_SIZE bytes.
 */
void GE_EREvent_Serialize(int length, const GE_EREvent *events, void *data);

/**
 * Helper function to deserialize byte data from an event recorder state into EREvents.
 * @param size The size of the data to deserialize.
 * @param data The data to deserialize.
 * @param[out] events Write deserialized events here, must be at least @p size / GE_EVENT_SERIAL_SIZE events long.
 */
void GE_EREvent_Deserialize(size_t size, const void *data, GE_EREvent *events);

#ifdef __cplusplus
}
#endif

#endif /* GE_EVENT_RECORDER_H */
