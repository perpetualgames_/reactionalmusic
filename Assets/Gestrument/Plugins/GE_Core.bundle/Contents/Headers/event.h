/**
 * @file event.h
 * @brief Event system for core.
 * @author Peter Gebauer
 * @copyright Copyright © 2020 Gestrument AB. All rights reserved.
 * @note This file is part of Gestrument Engine Core.
 */

#ifndef GE_EVENT_H
#define GE_EVENT_H

#include "GE_Core/utils/atomic.h"
#include "GE_Core/param.h"

#define GE_EVENT_QUEUE_DEFAULT_MAX 8192

/**
 * @brief See struct GE_EventQueue_s.
 */
typedef struct GE_EventQueue_s GE_EventQueue;

/**
 * @brief See struct GE_Event_s.
 */
typedef struct GE_Event_s GE_Event;

/**
 * @brief Event types.
 */
typedef enum {

    /**
     * @brief Invalid event / no-op.
     */
    GE_EVENT_TYPE_NONE,

    /**
     * @brief A note on event.
     */
    GE_EVENT_TYPE_NOTE_ON,

    /**
     * @brief A note off event.
     */
    GE_EVENT_TYPE_NOTE_OFF,

    /**
     * @brief Cursor conveniece event.
     */
    GE_EVENT_TYPE_CURSOR,

    /**
     * @brief Set a parameter value.
     */
    GE_EVENT_TYPE_SET_PARAM,

    /**
     * @brief Map a ControlSource.
     */
    GE_EVENT_TYPE_MAP,

    /**
     * @brief Map a ControlSource as viaSource.
     */
    GE_EVENT_TYPE_MAP_VIA,

    /**
     * @brief Map a ControlSource as viaSource.
     */
    GE_EVENT_TYPE_SLOT,

    /**
     * @brief Max enum.
     */
    MAX_GE_EVENT_TYPE,

} GE_EventType;

/**
 * @brief Object destinations.
 */
typedef enum {

    /**
     * @brief None object.
     */
    GE_EVENT_OBJECT_NONE,

    /**
     * @brief Engine object.
     */
    GE_EVENT_OBJECT_ENGINE,

    /**
     * @brief Cursor object.
     */
    GE_EVENT_OBJECT_CURSOR,

    /**
     * @brief Instrument object.
     */
    GE_EVENT_OBJECT_INSTRUMENT,

    /**
     * @brief Pulse generator object.
     */
    GE_EVENT_OBJECT_PULSE_GEN,

    /**
     * @brief Pitch generator object.
     */
    GE_EVENT_OBJECT_PITCH_GEN,

    /**
     * @brief Max enum.
     */
    MAX_GE_EVENT_OBJECT,

} GE_EventObject;

struct GE_Event_s {

    /**
     * @brief The event type.
     */
    GE_EventType type;

    /**
     * @brief The engine beat.
     */
    double beat;

    /**
     * @brief Type specific data.
     */
    union {

        /**
         * @ref GE_EVENT_TYPE_NOTE_ON.
         */
        struct {

            /**
             * @brief The instrument index.
             */
            int instrument;

            /**
             * @brief The voice index.
             */
            int voice;

            /**
             * @brief Pitch.
             */
            float pitch;

            /**
             * @brief Velocity.
             */
            float velocity;

        } noteOn;

        /**
         * @ref GE_EVENT_TYPE_NOTE_OFF.
         */
        struct {

            /**
             * @brief The instrument index.
             */
            int instrument;

            /**
             * @brief The voice index.
             */

            int voice;
            /**
             * @brief Velocity.
             */

            float velocity;

        } noteOff;

        /**
         * @ref GE_EVENT_TYPE_SET_PARAM.
         */
        struct {

            /**
             * @brief The event object.
             */
            GE_EventObject object;

            /**
             * @brief Object index.
             */
            int objectIndex;

            /**
             * @brief Param index.
             */
            int paramIndex;

            /**
             * @brief The parameter type.
             */
            GE_ParamType type;

            /**
             * @brief Parameter type specific data.
             */
            union {

                GE_ParamValue p;

                /**
                 * @brief String value.
                 */
                char *vString;

            } value;

        } setParam;

        /**
         * @ref GE_EVENT_TYPE_MAP.
         * @ref GE_EVENT_TYPE_MAP_VIA.
         */
        struct {

            /**
             * @brief Source object.
             */
            GE_EventObject sourceObject;

            /**
             * @brief Source index.
             */
            int sourceIndex;

            /**
             * @brief Source name.
             */
            char *sourceName;

            /**
             * @brief Destination object.
             */
            GE_EventObject destObject;

            /**
             * @brief Destination index.
             */
            int destIndex;

            /**
             * @brief Destination name.
             */
            char *destName;

        } map;

        /**
         * @ref GE_EVENT_TYPE_SLOT.
         */
        struct {

            /**
             * @brief Source object.
             */
            GE_EventObject object;

            /**
             * @brief Slot name.
             */
            char *name;

            /**
             * @brief Slot index.
             */
            int index;

            /**
             * @brief The parameter type.
             */
            GE_ParamType types[2];

            /**
             * @brief The number of parameter types.
             */
            int numTypes;

            /**
             * @brief Parameter type specific data.
             */
            union {

                GE_ParamValue p;

                /**
                 * @brief String value.
                 */
                char *vString;

            } values[2];

        } slot;

        /**
         * @ref GE_EVENT_TYPE_CURSOR.
         */
        struct {

            /**
             * @brief Cursor index.
             */
            int index;

            /**
             * @brief Cursor gate.
             */
            int gate;

            /**
             * @brief Cursor X.
             */
            double x;

            /**
             * @brief Cursor Y.
             */
            double y;

            /**
             * @brief Cursor Z.
             */
            double z;

        } crs;

    } e;

};

/**
 * @brief An event queue.
 */
struct GE_EventQueue_s {
    int maxLength;
    GE_AtomicInt tail;
    GE_AtomicInt head;
    GE_Event *events;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Get event type name.
 * @param type The event type.
 * @returns A human readable string (empty if @p type is invalid).
 */
const char *GE_EventType_GetName(GE_EventType type);

/**
 * @brief Deinitialize an event.
 * @param event The event to deinitialize.
 */
void GE_Event_Deinit(GE_Event *event);

/**
 * @brief Get the string representation of GE_EventObject.
 * @param object An event object.
 */
const char* GE_Event_EventObjectToString(GE_EventObject object);

/**
 * @brief Initialize the queue.
 * @param queue The event queue to initialize.
 * @param maxLength The maximum length of the queue (number of events). 0 or
 * negative value for default size @ref GE_EVENT_QUEUE_DEFAULT_MAX.
 * @returns 0 on success -1 on out of memory.
 */
int GE_EventQueue_Init(GE_EventQueue *queue, int maxLength);

/**
 * @brief Deinitialize the queue.
 * @param queue The event queue to deinitialize.
 */
void GE_EventQueue_Deinit(GE_EventQueue *queue);

/**
 * @brief Get the number of dequeable events.
 * @param queue The queue.
 * @returns The number of consumable events.
 */
int GE_EventQueue_GetNumDequeueable(const GE_EventQueue *queue);

/**
 * @brief Get the number of enqueable events.
 * @param queue The queue to get the length for.
 * @returns The number of enqueueable events.
 */
int GE_EventQueue_GetNumEnqueueable(const GE_EventQueue *queue);

/**
 * @brief Add events to the queue and increase head.
 * @param queue The queue.
 * @param numEvents The number of events to add.
 * @param events The events.
 * @returns The number of added events.
 */
int GE_EventQueue_Enqueue(GE_EventQueue *queue, int numEvents, const GE_Event *events);

/**
 * @brief Get events to the queue and increase tail.
 * @param queue The queue.
 * @param maxEvents The maximum number of events to get.
 * @param[out] events Write events here, pass NULL to discard events.
 * @returns The number of returned events.
 */
int GE_EventQueue_Dequeue(GE_EventQueue *queue, int maxEvents, GE_Event *events);

/**
 * @brief Return the number of pending events.
 * @param queue The queue.
 * @param[out] event If non-NULL write peeked event here.
 * @returns The number of pending events.
 */
int GE_EventQueue_Peek(GE_EventQueue *queue, GE_Event *event);

/**
 * Dump debug info (text) to stream.
 * @param queue The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 */
void GE_EventQueue_DebugDump(const GE_EventQueue *queue, FILE *f, unsigned int indent);

/**
 * Dump debug info (text) to stream.
 * @param event The object to dump.
 * @param f Dump to this stream.
 * @param indent Indent with this many spaces.
 */
void GE_Event_DebugDump(const GE_Event *event, FILE *f, unsigned int indent);

#ifdef __cplusplus
}
#endif

#endif /* GE_EVENT_H */
